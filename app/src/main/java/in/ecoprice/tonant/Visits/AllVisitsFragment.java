package in.ecoprice.tonant.Visits;


import android.app.SearchManager;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.BottomSheetDialog;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.volley.Cache;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.ParseError;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import in.ecoprice.tonant.Adapters.AllVisitsAdapter;
import in.ecoprice.tonant.Adapters.ShopSelectAdapter;
import in.ecoprice.tonant.Adapters.TodayVisitsAdapter;
import in.ecoprice.tonant.Models.TodayVisits;
import in.ecoprice.tonant.R;
import in.ecoprice.tonant.Retrofit.SharedPref;
import in.ecoprice.tonant.TargetTypesActivity;

public class AllVisitsFragment extends Fragment {


    public AllVisitsFragment() {
        // Required empty public constructor
    }

    private RecyclerView Gal_Rec;
    private AllVisitsAdapter adapter;
    private List<TodayVisits> listItems;
    public static List<TodayVisits> listItems1;
    private ProgressBar progressBar;
  //  SearchView sv;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_today_visits, container, false);

        Gal_Rec = (RecyclerView) view.findViewById(R.id.Rec_Gallery);
        Gal_Rec.setHasFixedSize(true);
        Gal_Rec.setNestedScrollingEnabled(false);
        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(getActivity().getApplicationContext(), 1);
        Gal_Rec.setLayoutManager(mLayoutManager);
        listItems = new ArrayList<>();
        listItems1 = new ArrayList<>();
        progressBar = (ProgressBar) view.findViewById(R.id.Prog_Assigned);
        /*sv= (SearchView) view.findViewById(R.id.mSearch);
        sv.setVisibility(View.VISIBLE);
        sv.setIconifiedByDefault(true);
      //  sv.setFocusable(true);
      //  sv.setIconified(false);
        //sv.requestFocusFromTouch();
        adapter =new AllVisitsAdapter(listItems , getActivity());
        sv.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String query) {
                adapter.getFilter().filter(query);
                return false;
            }
        });*/


        loadRecyclerViewData();
        return view;
    }

    private void loadRecyclerViewData() {
        progressBar.setVisibility(View.VISIBLE);
        Gal_Rec.setVisibility(View.GONE);
        String username = SharedPref.getStr("userName");
        String URL_DATA = "http://apiservices.tonantfarmers.com/api/MyOrdersbysales?Sales=" + username;
        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(URL_DATA, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                // mSwipeRefreshLayout.setRefreshing(false);
                /*  progressDialog.dismiss();*/
                Gal_Rec.setVisibility(View.VISIBLE);
                progressBar.setVisibility(View.GONE);
                try {
                    if (response.length() > 0) {
                        for (int i = 0; i < 30; i++) {
                            JSONObject obj = response.getJSONObject(i);
                            TodayVisits item = new TodayVisits(
                                    obj.getString("Id"),
                                    obj.getString("UserName"),
                                    obj.getString("ShopkeeparName"),
                                    obj.getString("Quanity"),
                                    obj.getString("DeliveryAddress"),
                                    obj.getString("Total"),
                                    obj.getString("Tax"),
                                    obj.getString("DeliveryCharges"),
                                    obj.getString("GST"),
                                    obj.getString("NetPay"),
                                    obj.getString("PaymentStatus"),
                                    obj.getString("Status"),
                                    obj.getString("PaidThrough"),
                                    obj.getString("PendingAmount"),
                                    obj.getString("Createddate"),
                                    obj.getString("OrdersList")

                            );
                            listItems = listItems1;
                            listItems.add(item);
                           // showBottomSheetDialog(item.getOrdersList());
                        }
                        adapter = new AllVisitsAdapter(listItems, getContext());
                        Gal_Rec.setAdapter(adapter);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                /*  progressDialog.dismiss();*/
                Gal_Rec.setVisibility(View.GONE);
                progressBar.setVisibility(View.GONE);
                VolleyLog.e("Error: ", error.getMessage());
                Toast.makeText(getActivity(), "Try Again ", Toast.LENGTH_LONG).show();

            }
        }){

            @Override
            protected Response<JSONArray> parseNetworkResponse(NetworkResponse response) {
                try {
                    Cache.Entry cacheEntry = HttpHeaderParser.parseCacheHeaders(response);
                    if (cacheEntry == null) {
                        cacheEntry = new Cache.Entry();
                    }
                    final long cacheHitButRefreshed = 3 * 60 * 1000; // in 3 minutes cache will be hit, but also refreshed on background
                    final long cacheExpired = 24 * 60 * 60 * 1000; // in 24 hours this cache entry expires completely
                    long now = System.currentTimeMillis();
                    final long softExpire = now + cacheHitButRefreshed;
                    final long ttl = now + cacheExpired;
                    cacheEntry.data = response.data;
                    cacheEntry.softTtl = softExpire;
                    cacheEntry.ttl = ttl;
                    String headerValue;
                    headerValue = response.headers.get("Date");
                    if (headerValue != null) {
                        cacheEntry.serverDate = HttpHeaderParser.parseDateAsEpoch(headerValue);
                    }
                    headerValue = response.headers.get("Last-Modified");
                    if (headerValue != null) {
                        cacheEntry.lastModified = HttpHeaderParser.parseDateAsEpoch(headerValue);
                    }
                    cacheEntry.responseHeaders = response.headers;
                    final String jsonString = new String(response.data,
                            HttpHeaderParser.parseCharset(response.headers));
                    return Response.success(new JSONArray(jsonString), cacheEntry);
                } catch (UnsupportedEncodingException e) {
                    return Response.error(new ParseError(e));
                } catch (JSONException e) {
                    return Response.error(new ParseError(e));
                }
            }

            @Override
            protected void deliverResponse(JSONArray response) {
                super.deliverResponse(response);
            }

            @Override
            public void deliverError(VolleyError error) {
                super.deliverError(error);
            }

            @Override
            protected VolleyError parseNetworkError(VolleyError volleyError) {
                return super.parseNetworkError(volleyError);
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(getContext().getApplicationContext());
        jsonArrayRequest.setRetryPolicy(new DefaultRetryPolicy(
                2000*5,
                /*DefaultRetryPolicy.DEFAULT_MAX_RETRIES*/ 3,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(jsonArrayRequest);
    }

/*
    private void showBottomSheetDialog(final OrdersList people) {
        if (mBehavior.getState() == BottomSheetBehavior.STATE_EXPANDED) {
            mBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
        }

        final View view = getLayoutInflater().inflate(R.layout.sheet_list, null);

        ((View) view.findViewById(R.id.lyt_preview)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(getContext(), "Preview '" + people.getProductName() + "' clicked", Toast.LENGTH_SHORT).show();
            }
        });

        ((View) view.findViewById(R.id.lyt_share)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(getContext(), "Share '" + people.getQty() + "' clicked", Toast.LENGTH_SHORT).show();
            }
        });

        ((View) view.findViewById(R.id.lyt_get_link)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(getContext(), "Get link '" + people.getProductName() + "' clicked", Toast.LENGTH_SHORT).show();
            }
        });

        ((View) view.findViewById(R.id.lyt_make_copy)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(getContext(), "Make a copy '" + people.getProductName() + "' clicked", Toast.LENGTH_SHORT).show();
            }
        });

        mBottomSheetDialog = new BottomSheetDialog(getContext());
        mBottomSheetDialog.setContentView(view);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            mBottomSheetDialog.getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        }

        mBottomSheetDialog.show();
        mBottomSheetDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                mBottomSheetDialog = null;
            }
        });
    }
*/
/*@Override
public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
       // super.onCreateOptionsMenu(menu);
        inflater.inflate(R.menu.search, menu);
       super.onCreateOptionsMenu(menu,inflater);


        MenuItem searchItem = menu.findItem(R.id.menu_search);
        SearchView searchView = (SearchView) searchItem.getActionView();
        if (searchView != null) {
            searchView.setIconifiedByDefault(true);
            searchView.setFocusable(true);
            searchView.setIconified(false);
            searchView.requestFocusFromTouch();
            searchView.setQueryHint(getString(R.string.search_hint));
            searchView.setMaxWidth(Integer.MAX_VALUE);
            searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
                @Override
                public boolean onQueryTextSubmit(String s) {
                    return false;
                }

                @Override
                public boolean onQueryTextChange(String query) {
                    adapter.getFilter().filter(query);
                    return false;
                }
            });
        }

        SearchManager searchManager = (SearchManager) getContext().getSystemService(Context.SEARCH_SERVICE);
        if(null!=searchManager ) {
            searchView.setSearchableInfo(searchManager.getSearchableInfo(getActivity().getComponentName()));
        }
        searchView.setIconifiedByDefault(false);

    }*/
}