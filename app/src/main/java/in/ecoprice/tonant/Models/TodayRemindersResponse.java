package in.ecoprice.tonant.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class TodayRemindersResponse implements Serializable
{

    @SerializedName("Id")
    @Expose
    private Integer id;
    @SerializedName("SalesPerson")
    @Expose
    private String salesPerson;
    @SerializedName("ShopKeeperName")
    @Expose
    private String shopKeeperName;
    @SerializedName("dateofcreation")
    @Expose
    private Object dateofcreation;
    @SerializedName("dateofvisittobe")
    @Expose
    private String dateofvisittobe;
    @SerializedName("reason")
    @Expose
    private String reason;
    @SerializedName("Status")
    @Expose
    private Object status;
    @SerializedName("VisiteStatus")
    @Expose
    private Object visiteStatus;
    @SerializedName("VisiteType")
    @Expose
    private String visiteType;
    @SerializedName("RetunStatus")
    @Expose
    private Boolean retunStatus;
    @SerializedName("ErrorMessage")
    @Expose
    private Object errorMessage;
    private final static long serialVersionUID = 3168923018775618714L;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getSalesPerson() {
        return salesPerson;
    }

    public void setSalesPerson(String salesPerson) {
        this.salesPerson = salesPerson;
    }

    public String getShopKeeperName() {
        return shopKeeperName;
    }

    public void setShopKeeperName(String shopKeeperName) {
        this.shopKeeperName = shopKeeperName;
    }

    public Object getDateofcreation() {
        return dateofcreation;
    }

    public void setDateofcreation(Object dateofcreation) {
        this.dateofcreation = dateofcreation;
    }

    public String getDateofvisittobe() {
        return dateofvisittobe;
    }

    public void setDateofvisittobe(String dateofvisittobe) {
        this.dateofvisittobe = dateofvisittobe;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public Object getStatus() {
        return status;
    }

    public void setStatus(Object status) {
        this.status = status;
    }

    public Object getVisiteStatus() {
        return visiteStatus;
    }

    public void setVisiteStatus(Object visiteStatus) {
        this.visiteStatus = visiteStatus;
    }

    public String getVisiteType() {
        return visiteType;
    }

    public void setVisiteType(String visiteType) {
        this.visiteType = visiteType;
    }

    public Boolean getRetunStatus() {
        return retunStatus;
    }

    public void setRetunStatus(Boolean retunStatus) {
        this.retunStatus = retunStatus;
    }

    public Object getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(Object errorMessage) {
        this.errorMessage = errorMessage;
    }

}
