package in.ecoprice.tonant.Models;

public class TodayReports {
    private String ProductName,SoldQunatity;

    public TodayReports(String productName, String soldQunatity) {
        ProductName = productName;
        SoldQunatity = soldQunatity;
    }

    public String getProductName() {
        return ProductName;
    }

    public void setProductName(String productName) {
        ProductName = productName;
    }

    public String getSoldQunatity() {
        return SoldQunatity;
    }

    public void setSoldQunatity(String soldQunatity) {
        SoldQunatity = soldQunatity;
    }
}
