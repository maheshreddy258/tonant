package in.ecoprice.tonant.Adapters;


import android.annotation.SuppressLint;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import java.util.ArrayList;
import java.util.List;

import in.ecoprice.tonant.Models.ProductsList;
import in.ecoprice.tonant.R;

/**
 * Created by nani on 11/17/2017.
 */

public class OldProductsAdapter extends RecyclerView.Adapter<OldProductsAdapter.ViewHolder> {

    private List<ProductsList> listitems;
    private Context ctx;
    private int quantities [];

    public OldProductsAdapter() {
        this.quantities = new int[getItemCount()];
    }
    public OldProductsAdapter(List<ProductsList> listitems, Context ctx) {
        this.listitems = listitems;
        this.ctx = ctx;

    }


   /* public GralleryAdapter(List<Gallery> listitems, Context ctx) {
        this.listitems = listitems;
        this.ctx = ctx;
    }*/

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.product_list,parent,false);

        return new ViewHolder(v,ctx,listitems);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position )  {
        final ProductsList listItem =listitems.get(position);
        Picasso.with(ctx)
                .load(listItem.getBannerImage())
                .into(holder.ser_img);
        holder.title.setText(listItem.getProductName());
        holder.coast.setText("₹"+listItem.getSalePrice());
        holder.CountText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                quantities[holder.getAdapterPosition()] =
                        Integer.parseInt(holder.CountText.getText().toString());
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        holder.Less.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int hol = Integer.parseInt(holder.CountText.getText().toString());
                hol--;
                if(hol<0){
                    hol=0;
                }
                String abc = String.valueOf(hol);
                holder.CountText.setText(abc);

                /*CartPref cartPref = new CartPref(ctx);
                String name = cartPref.getStr("ItemName");
                if (listItem.getProductName().equals(name)){
                   int item = cartPref.getInt("qnty");
                    cartPref.setInt("qnty", item--);
                    notifyDataSetChanged();
                    }else {
                    cartPref.setInt("position",position);
                    cartPref.setStr("ItemName",listItem.getProductName());
                    cartPref.setStr("ItemCoast",listItem.getSalePrice());
                    cartPref.setStr("ItemImage",listItem.getBannerImage());
                    cartPref.setInt("qnty", hol++);
                    notifyDataSetChanged();

                }*/
                }
        });
        holder.Plus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int hol = Integer.parseInt(holder.CountText.getText().toString());
                hol++;
                String abc = String.valueOf(hol);
                holder.CountText.setText(abc);

                /*if (listItem.getProductName().equals(name)){
                    int item = cartPref.getInt("qnty");
                    cartPref.setInt("qnty", item++);



                    notifyDataSetChanged();
                }else {
                    cartPref.setInt("position",position);
                    cartPref.setStr("ItemName",listItem.getProductName());
                    cartPref.setStr("ItemCoast",listItem.getSalePrice());
                    cartPref.setStr("ItemImage",listItem.getBannerImage());
                    cartPref.setInt("qnty", hol++);
                    notifyDataSetChanged();
                }*/
/*

*/

/*

                String ItemName = listItem.getProductName();
                Intent intent = new Intent("custom-message");
                //            intent.putExtra("quantity",Integer.parseInt(quantity.getText().toString()));
                String total = listItem.getSalePrice();
                intent.putExtra("quantity",abc);
                intent.putExtra("item",ItemName);
                intent.putExtra("total",total);
                LocalBroadcastManager.getInstance(context).sendBroadcast(intent);*/

                }

        });

        }


    @Override
    public int getItemCount() {
        return listitems.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder /*implements View.OnClickListener*/ {
        private ImageView ser_img;
        private TextView title,coast;
        private ImageButton Plus,Less;
        private EditText CountText;
       // private Button AddTOCart;
        private LinearLayout MainPro;
        Context ctx;
        ArrayList<ProductsList> completeds = new ArrayList<ProductsList>();

        @SuppressLint("CutPasteId")
        public ViewHolder(View View, final Context ctx, List<ProductsList> completeds) {
            super(View);

           /* View.setOnClickListener(this);*/
            this.completeds = (ArrayList<ProductsList>) completeds;
            this.ctx = ctx;
            ser_img = (ImageView) View.findViewById(R.id.ImagePro);
            title = (TextView) View.findViewById(R.id.TitlePro);
            coast = (TextView) View.findViewById(R.id.RatePro);
            //AddTOCart = (Button) View.findViewById(R.id.AddToCart);
            MainPro = (LinearLayout) View.findViewById(R.id.MainPro);
            Plus = (ImageButton) View.findViewById(R.id.Plus);
            Less = (ImageButton) View.findViewById(R.id.Less);
            CountText = (EditText) View.findViewById(R.id.CountText);

CountText.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(android.view.View view) {
        InputMethodManager im = (InputMethodManager) ctx.getSystemService(Context.INPUT_METHOD_SERVICE);
        im.showSoftInput(CountText, 0);
    }
});

        }

        /*@Override
        public void onClick(View v) {
            int position = getAdapterPosition();
            Products posts = this.completeds.get(position);
           // Toast.makeText(ctx, "Selected Item : " + position, Toast.LENGTH_SHORT).show();
            MainPro.setBackgroundResource(R.drawable.selector_row);



            *//* Intent intent = new Intent(this.ctx, VideoViewActivity.class);
            intent.putExtra("link", posts.getLink().replaceAll(" ", "%20"));
            intent.putExtra("name", posts.getName());*//**//*
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            this.ctx.startActivity(intent);*//*


        }*/

    }

    public int [] getQuantities(){
        return this.quantities;
    }

}


