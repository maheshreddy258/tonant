package in.ecoprice.tonant;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Handler;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatEditText;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ListView;
import android.widget.Toast;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import in.ecoprice.tonant.Remainders.CreateRemainderActivity;
import in.ecoprice.tonant.Remainders.SelectShopActivity;
import in.ecoprice.tonant.Retrofit.Responce.RemainderResp;
import in.ecoprice.tonant.Retrofit.SharedPref;
import in.ecoprice.tonant.Retrofit.network.ApiService;
import in.ecoprice.tonant.Retrofit.network.RetrofitBuilder;
import in.ecoprice.tonant.newChanges.NewCheckOutActivity;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UpdateRemainderActivity extends AppCompatActivity {


    private static final String TAG = "UpdateRemaider";
    TextInputEditText SelectShop,DateofVisitTobe,VisiteType;
    AppCompatEditText Remarks;
    Button button;
    ApiService service;
    Call<RemainderResp> call;
    private int mYear, mMonth, mDay, mHour, mMinute;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_remainder);
        service = RetrofitBuilder.createService(ApiService.class);
        SelectShop = (TextInputEditText) findViewById(R.id.SelectShop);
        DateofVisitTobe = (TextInputEditText) findViewById(R.id.DateofVisitTobe);
        VisiteType = (TextInputEditText) findViewById(R.id.VisiteType);
        Remarks = (AppCompatEditText) findViewById(R.id.Remarks);
        button = (Button) findViewById(R.id.BtnTarget);


      String dateofVisit =  getIntent().getStringExtra("dateofvisittobe");
      String name =  getIntent().getStringExtra("ShopKeeperNamename");
      String reason =  getIntent().getStringExtra("reason");
      String visittype =  getIntent().getStringExtra("VisiteType");
      final String id =  getIntent().getStringExtra("Id");
      SelectShop.setText(name);
      SelectShop.setEnabled(false);
      DateofVisitTobe.setText(dateofVisit);
      VisiteType.setText(visittype);
      Remarks.setText(reason);

        DateofVisitTobe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Get Current Date
                final Calendar c = Calendar.getInstance();
                mYear = c.get(Calendar.YEAR);
                mMonth = c.get(Calendar.MONTH);
                mDay = c.get(Calendar.DAY_OF_MONTH);


                DatePickerDialog datePickerDialog = new DatePickerDialog(UpdateRemainderActivity.this,
                        new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {

                                @SuppressLint("SimpleDateFormat")
                                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
                                c.set(year, monthOfYear , dayOfMonth);
                                String dateString = sdf.format(c.getTime());


                                /*Date yourDate= new Date(year, , dayOfMonth);
                                String strDate = null;
                                SimpleDateFormat dateFormatter = new SimpleDateFormat(
                                        "yyyy-MM-dd'T'HH:mm:ss");
                                strDate = dateFormatter.format(yourDate);*/

                                DateofVisitTobe.setText(dateString);

                            }
                        }, mYear, mMonth, mDay);
                datePickerDialog.show();
                datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
            }
        });

        //pendingammountcollection,feedback,new Order

        //




        VisiteType.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final String[] text = {"New Order","Pending Amount Collection","feedback"};
                android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(view.getRootView().getContext(), android.app.AlertDialog.THEME_DEVICE_DEFAULT_LIGHT);
                builder.setItems(text, new DialogInterface.OnClickListener() {
                    @SuppressLint("SetTextI18n")
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        ListView lw = ((android.app.AlertDialog)dialog).getListView();
                        Object checkedItem = lw.getAdapter().getItem(which);
                        VisiteType.setText(checkedItem.toString());

                    }
                });
                builder.show();
            }
        });
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SendRemainder(
                        id,
                        SelectShop.getText().toString(),
                        DateofVisitTobe.getText().toString(),
                        VisiteType.getText().toString(),
                        Remarks.getText().toString(),
                        SharedPref.getStr("userName")
                );
            }
        });
    }

    private void SendRemainder(String id ,String shopname, String dateOfvisit, String visiteType, String Remarks, String userName) {
        Log.w(TAG, "dateOfvisit: " + dateOfvisit);
        final ProgressDialog progressDialog= new ProgressDialog(UpdateRemainderActivity.this);
        progressDialog.setMessage("Loading...");
        progressDialog.show();
        progressDialog.setCancelable(false);
        call = service.UpdateReminder(id,shopname,dateOfvisit,visiteType,Remarks,userName);
        call.enqueue(new Callback<RemainderResp>() {
            @Override
            public void onResponse(Call<RemainderResp> call, Response<RemainderResp> response) {
                Log.w(TAG, "onResponse: " + response);
                response.body();
                boolean Loginstatus = response.body().isStatus();
                Log.w(TAG, "Loginstatus: " + Loginstatus);
                if (Loginstatus) {
                    Toast.makeText(UpdateRemainderActivity.this,response.body().getErrorMessage(),Toast.LENGTH_LONG).show();
                    Log.w(TAG, "status: " + Loginstatus);
                    Log.w(TAG, "ErrorMSG: " + response.body().getErrorMessage());

                    int TIME = 2000;
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            progressDialog.dismiss();
                            Intent intent = new Intent(UpdateRemainderActivity.this, ReminderActivity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP |
                                    Intent.FLAG_ACTIVITY_NEW_TASK);
                            intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                            startActivity(intent);
                            finish();

                        }
                    }, TIME);
                } else {
                    progressDialog.dismiss();
                    Toast.makeText(UpdateRemainderActivity.this,response.body().getErrorMessage(),Toast.LENGTH_SHORT).show();
                    Log.w(TAG, "status: in false" + Loginstatus);
                    Log.w(TAG, "ErrorMSG in false: " + response.body().getErrorMessage());

                }

            }

            @Override
            public void onFailure(Call<RemainderResp> call, Throwable t) {
                progressDialog.dismiss();
                Toast.makeText(UpdateRemainderActivity.this,""+t.getMessage(),Toast.LENGTH_LONG).show();
                Log.w(TAG, "onFailure: " + t.getMessage());
                // showForm();
            }
        });


    }
    public static String getFullDateTime(String timeStamp) {
        String convetedFormat = "";

        DateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ");
        DateFormat outputFormat = new SimpleDateFormat("h:mm a,  d MMM");
        try {
            Date date = inputFormat.parse(timeStamp);
            convetedFormat = outputFormat.format(date);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return convetedFormat;
    }

}
