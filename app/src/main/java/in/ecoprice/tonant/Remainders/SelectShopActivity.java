package in.ecoprice.tonant.Remainders;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import in.ecoprice.tonant.Adapters.SelectShopListAdapters;
import in.ecoprice.tonant.Models.Shoplist;
import in.ecoprice.tonant.R;
import in.ecoprice.tonant.Retrofit.SharedPref;

public class SelectShopActivity extends AppCompatActivity {

    private static final String TAG = "BankList";
    private RecyclerView Rec_Trends;
    private SelectShopListAdapters adapter;
    // private OnBoardListAdapter adapter123;
    private List<Shoplist> listItems;
    public static List<Shoplist> listItems1;
    private ProgressBar progressBar;
    SearchView sv;
    String cusNumber;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_shop);

        Rec_Trends = (RecyclerView) findViewById(R.id.Rec_Wishlist);
        progressBar = (ProgressBar) findViewById(R.id.ProgressBarWishlist);
        Rec_Trends.setHasFixedSize(true);
        LinearLayoutManager layoutManager= new LinearLayoutManager(SelectShopActivity.this, LinearLayoutManager.VERTICAL, false);
        Rec_Trends.setLayoutManager(layoutManager);
        Rec_Trends.setNestedScrollingEnabled(false);

        cusNumber = getIntent().getStringExtra("number");
        Log.w(TAG,"cusNumber"+cusNumber);

        listItems = new ArrayList<>();
        listItems1 = new ArrayList<>();
        loadRecyclerViewData();
        sv= (SearchView) findViewById(R.id.mSearch);
        sv.setIconifiedByDefault(true);
        sv.setFocusable(true);
        sv.setIconified(false);
        sv.requestFocusFromTouch();
        adapter =new SelectShopListAdapters(listItems , getApplicationContext());
        sv.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String query) {


                adapter.getFilter().filter(query);

                return false;
            }
        });
    }

    private void loadRecyclerViewData() {
        progressBar.setVisibility(View.VISIBLE);
        Rec_Trends.setVisibility(View.GONE);
        String username = SharedPref.getStr("userName");
        String URL_DATA = "http://apiservices.tonantfarmers.com/api/MyLeads?UserName="+username;
        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(URL_DATA, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                progressBar.setVisibility(View.GONE);
                Rec_Trends.setVisibility(View.VISIBLE);
                try {
                    if (response.length() > 0) {
                        for (int i = 0; i < response.length(); i++) {
                            JSONObject obj =response.getJSONObject(i);
                            Shoplist item = new Shoplist(
                                    obj.getString("Id"),
                                    obj.getString("First_Name"),
                                    obj.getString("Last_Name"),
                                    obj.getString("CROEMailID"),
                                    obj.getString("MobileNumber"),
                                    obj.getString("AadharNumber"),
                                    obj.getString("PANNumber"),
                                    obj.getString("ShopName"),
                                    obj.getString("ShopAddress"),
                                    obj.getString("CreateBy"),
                                    obj.getString("LeadType"),
                                    obj.getString("PIN_Code"),
                                    obj.getString("Latitude"),
                                    obj.getString("Longitude"),
                                    obj.getString("PendingBill")
                            );
                            listItems = listItems1;
                            listItems.add(item);
                        }
                        adapter = new SelectShopListAdapters(listItems , getApplicationContext());
                        Rec_Trends.setAdapter(adapter);
                    }
                }catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });
        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        requestQueue.add(jsonArrayRequest);

    }

}