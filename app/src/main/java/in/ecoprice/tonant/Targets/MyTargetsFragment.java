package in.ecoprice.tonant.Targets;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import in.ecoprice.tonant.Adapters.MyTargetsAdapter;
import in.ecoprice.tonant.Adapters.OrderAdapter;
import in.ecoprice.tonant.Models.MyTargets;
import in.ecoprice.tonant.Models.Orders;
import in.ecoprice.tonant.R;
import in.ecoprice.tonant.Retrofit.SharedPref;

/**
 * A simple {@link Fragment} subclass.
 */
public class MyTargetsFragment extends Fragment {


    public MyTargetsFragment() {
        // Required empty public constructor
    }
    private RecyclerView Gal_Rec;
    private RecyclerView.Adapter adapter;
    private List<MyTargets> listItems;
    public static List<MyTargets> listItems1;
    private ProgressBar progressBar;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_my_targets, container, false);

        Gal_Rec = (RecyclerView) view.findViewById(R.id.Rec_Gallery);
        Gal_Rec.setHasFixedSize(true);
        Gal_Rec.setNestedScrollingEnabled(false);
        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(getActivity().getApplicationContext(), 1);
        Gal_Rec.setLayoutManager(mLayoutManager);
        listItems = new ArrayList<>();
        listItems1 = new ArrayList<>();
        progressBar = (ProgressBar) view.findViewById(R.id.Prog_Assigned);
        loadRecyclerViewData();
        return  view;
    }

    private void loadRecyclerViewData() {
        progressBar.setVisibility(View.VISIBLE);
        Gal_Rec.setVisibility(View.GONE);
        String username = SharedPref.getStr("userName");
        String URL_DATA = "http://apiservices.tonantfarmers.com/api/MyTargets?UserName="+username;
        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(URL_DATA, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {

                        Gal_Rec.setVisibility(View.VISIBLE);
                        progressBar.setVisibility(View.GONE);
                        try {
                            if (response.length() > 0) {
                                for (int i = 0; i < response.length(); i++) {
                                    JSONObject obj =response.getJSONObject(i);
                                    MyTargets item = new MyTargets(
                                            obj.getString("Id"),
                                            obj.getString("TargetTitle"),
                                            obj.getString("OutletType"),
                                            obj.getString("OutletCount"),
                                            obj.getString("TargetDays"),
                                            obj.getString("SaleKgs")

                                    );
                                    listItems = listItems1;
                                    listItems.add(item);
                                }
                                adapter = new MyTargetsAdapter(listItems , getContext().getApplicationContext());
                                Gal_Rec.setAdapter(adapter);
                            }
                        }catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                /*  progressDialog.dismiss();*/
                Gal_Rec.setVisibility(View.GONE);
                progressBar.setVisibility(View.GONE);
                VolleyLog.e("Error: ", error.getMessage());
                Toast.makeText(getActivity(),"Try Again ",Toast.LENGTH_LONG).show();

            }
        });
        RequestQueue requestQueue = Volley.newRequestQueue(getContext().getApplicationContext());
        jsonArrayRequest.setRetryPolicy(new DefaultRetryPolicy(
                2000*5,
                /*DefaultRetryPolicy.DEFAULT_MAX_RETRIES*/ 3,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(jsonArrayRequest);
    }

}
