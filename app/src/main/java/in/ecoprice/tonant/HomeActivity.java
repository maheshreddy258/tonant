package in.ecoprice.tonant;

import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.location.LocationManager;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.animation.DecelerateInterpolator;
import android.widget.GridView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;

import java.util.ArrayList;
import java.util.List;

import in.ecoprice.tonant.Adapters.HomeAdapter;
import in.ecoprice.tonant.Retrofit.Responce.Homemodel;
import in.ecoprice.tonant.Retrofit.SharedPref;

public class HomeActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {


    private DrawerLayout mDrawerLayout;
    private static final String TAG = "MainActivity";
    private ActionBarDrawerToggle mToggle;
    private CardView AddShop, AddProducts, ShopList, ChangePassword, Profile, MyTargets, MyVisits, Remainders, pendingAmount, mCvmyorders, mCvpendingorders;
    private TextView Name, Date, CCOunt;
    private ProgressBar progressBar;
    //location
    protected static final String TAG123 = "LocationOnOff";
    private GoogleApiClient googleApiClient;
    final static int REQUEST_LOCATION = 199;
    InternetChecker internetChecker;
    TextView textView;
    int Count = 89;
    int Sceonds = 2000;

    GridView gridView;

    List<Homemodel> homemodelList;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        mDrawerLayout = (DrawerLayout) findViewById(R.id.Drawerlayout);
        mToggle = new ActionBarDrawerToggle(this, mDrawerLayout, R.string.open, R.string.close);
        mDrawerLayout.addDrawerListener(mToggle);
        mToggle.syncState();


        // gridView = findViewById(R.id.grid);


        homemodelList = new ArrayList<>();
        homemodelList.add(new Homemodel("Add Shop", R.drawable.ic_outline_home_24px));
        homemodelList.add(new Homemodel("Shop List", R.drawable.checklist));
        homemodelList.add(new Homemodel("Reminders", R.drawable.reminder));
        homemodelList.add(new Homemodel("Visits", R.drawable.visitor));
        homemodelList.add(new Homemodel("Reports", R.drawable.groceries));
        homemodelList.add(new Homemodel("My Orders", R.drawable.order));
        homemodelList.add(new Homemodel("Pending Orders", R.drawable.box));

        RecyclerView recyclerView = findViewById(R.id.recyclerview);
        HomeAdapter homeAdapter = new HomeAdapter(this, homemodelList);
        recyclerView.setLayoutManager(new GridLayoutManager(this, 4));
        recyclerView.setAdapter(homeAdapter);


        NavigationView navigation = findViewById(R.id.nv);

        navigation.setNavigationItemSelectedListener(this);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        /*setupDrawerContent(navigation);
        navigation.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                selecItemDrawer(menuItem);
                return true;
            }
        });*/


        //startCountAnimation();
        runOnUiThread(new Runnable() {
            public void run() {
                @SuppressLint("ObjectAnimatorBinding") ObjectAnimator anim = ObjectAnimator.ofFloat(progressBar, "progress", 0, 89);
                // @SuppressLint("ObjectAnimatorBinding") ObjectAnimator anim = ObjectAnimator.ofInt(mDonut_progress,"donut_progress",0,78);
                anim.setInterpolator(new DecelerateInterpolator());
                anim.setDuration(Sceonds);
                anim.start();
                anim.addListener(new Animator.AnimatorListener() {
                    @Override
                    public void onAnimationStart(Animator animation) {
                    }

                    @Override
                    public void onAnimationCancel(Animator animation) {
                    }

                    @Override
                    public void onAnimationRepeat(Animator animation) {
                    }

                    @Override
                    public void onAnimationEnd(Animator animation) {
                       /* if (78 >= 100) {
                            Toast.makeText(getContext(),"Hello i'm in 78%",Toast.LENGTH_SHORT).show();
                        }*/
                    }
                });
            }
        });

        this.setFinishOnTouchOutside(true);

        // Todo Location Already on  ... start
        final LocationManager manager = (LocationManager) HomeActivity.this.getSystemService(Context.LOCATION_SERVICE);
        if (manager.isProviderEnabled(LocationManager.GPS_PROVIDER) && hasGPSDevice(HomeActivity.this)) {
            // Toast.makeText(MainActivity.this,"Gps already enabled",Toast.LENGTH_SHORT).show();
            //finish();
        }
        // Todo Location Already on  ... end

        if (!hasGPSDevice(HomeActivity.this)) {
            // Toast.makeText(MainActivity.this,"Gps not Supported",Toast.LENGTH_SHORT).show();
        }

        if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER) && hasGPSDevice(HomeActivity.this)) {
            // Log.e("keshav","Gps already enabled");
            // Toast.makeText(MainActivity.this,"Gps not enabled",Toast.LENGTH_SHORT).show();
            enableLoc();
        } else {
            Log.e("loca", "Gps already enabled");
            // Toast.makeText(MainActivity.this,"Gps already enabled",Toast.LENGTH_SHORT).show();
        }

    }

/*
    public void selecItemDrawer(MenuItem menuItem) {

        switch (menuItem.getItemId()) {


            case R.id.navpendingamounts:

                startActivity(new Intent(HomeActivity.this, PendingAmountActivity.class));
                break;

            case R.id.navtargets:

                startActivity(new Intent(HomeActivity.this, MyTargetsActivity.class));
                break;

            case R.id.share:
                Intent sharingIntent = new Intent(Intent.ACTION_SEND);
                sharingIntent.setType("text/html");
                sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, Html.fromHtml("<p>This is the text shared.</p>"));
                startActivity(Intent.createChooser(sharingIntent, "Share using"));

                break;


        }

        menuItem.setChecked(true);
        mDrawerLayout.closeDrawers();

    }*/


    private void gotonextScreen(Class screenName) {
        Intent intent = new Intent(getApplicationContext(), screenName);
        startActivity(intent);
    }

   /* public void setupDrawerContent(NavigationView navigationView) {

        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                //selecItemDrawer(menuItem);
                return true;
            }
        });
    }
*/

    private void startCountAnimation() {
        ValueAnimator animator = ValueAnimator.ofInt(0, Count);
        animator.setDuration(Sceonds);
        animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            public void onAnimationUpdate(ValueAnimator animation) {
                CCOunt.setText(animation.getAnimatedValue().toString() + "/ 100"/*+" \n \n Actual Sales"*/);

            }
        });
        animator.start();
    }

    private void getVisits(final TextView textView123) {
        String username = SharedPref.getStr("userName");
        String URL_DATA = "http://apiservices.tonantfarmers.com/api/TodayVisitsCount?salesName=" + username;

        StringRequest stringRequest = new StringRequest(Request.Method.GET,
                URL_DATA,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String s) {
                        Log.w(TAG, "String Value " + s);
                        textView123.setText("Today visits " + s);
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.e("Error: ", error.getMessage());
                Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_LONG).show();

            }
        });
        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        requestQueue.add(stringRequest);
    }


    private boolean hasGPSDevice(Context context) {
        final LocationManager mgr = (LocationManager) context
                .getSystemService(Context.LOCATION_SERVICE);
        if (mgr == null)
            return false;
        final List<String> providers = mgr.getAllProviders();
        if (providers == null)
            return false;
        return providers.contains(LocationManager.GPS_PROVIDER);
    }

    private void enableLoc() {

        if (googleApiClient == null) {
            googleApiClient = new GoogleApiClient.Builder(HomeActivity.this)
                    .addApi(LocationServices.API)
                    .addConnectionCallbacks(new GoogleApiClient.ConnectionCallbacks() {
                        @Override
                        public void onConnected(Bundle bundle) {

                        }

                        @Override
                        public void onConnectionSuspended(int i) {
                            googleApiClient.connect();
                        }
                    })
                    .addOnConnectionFailedListener(new GoogleApiClient.OnConnectionFailedListener() {
                        @Override
                        public void onConnectionFailed(ConnectionResult connectionResult) {

                            Log.d("Location error", "Location error " + connectionResult.getErrorCode());
                        }
                    }).build();
            googleApiClient.connect();

            LocationRequest locationRequest = LocationRequest.create();
            locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
            locationRequest.setInterval(30 * 1000);
            locationRequest.setFastestInterval(5 * 1000);
            LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                    .addLocationRequest(locationRequest);

            builder.setAlwaysShow(true);

            PendingResult<LocationSettingsResult> result =
                    LocationServices.SettingsApi.checkLocationSettings(googleApiClient, builder.build());
            result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
                @Override
                public void onResult(LocationSettingsResult result) {
                    final Status status = result.getStatus();
                    switch (status.getStatusCode()) {
                        case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                            try {
                                // Show the dialog by calling startResolutionForResult(),
                                // and check the result in onActivityResult().
                                status.startResolutionForResult(HomeActivity.this, REQUEST_LOCATION);

                                // finish();
                            } catch (IntentSender.SendIntentException e) {
                                // Ignore the error.
                            }
                            break;
                    }
                }
            });
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (mToggle.onOptionsItemSelected(item)) {
            return true;
        }
        if (id == R.id.Logout) {
            SharedPref.deleteShared();
            startActivity(new Intent(HomeActivity.this, LoginActivity.class));
            finish();
        }

        if (id == R.id.Profile) {
            startActivity(new Intent(HomeActivity.this, ProfileActivity.class));
        }
        if (id == R.id.ChangePassword) {
            startActivity(new Intent(HomeActivity.this, ChangePasswordActivity.class));
        }


        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onStart() {
        SharedPref dataProccessor = new SharedPref(getApplicationContext());
        boolean status = dataProccessor.getBool("Loginstatus");

        if (!status) {
            startActivity(new Intent(HomeActivity.this, LoginActivity.class));
            finish();
        } else {
            Log.w(TAG, "Loginstatus: " + status);
        }

        super.onStart();
    }


    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_home) {
            // Handle the camera action
        } else if (id == R.id.navpendingamounts) {

        } else if (id == R.id.navtargets) {
            startActivity(new Intent(HomeActivity.this, MyTargetsActivity.class));

        } else if (id == R.id.share) {

            Intent sharingIntent = new Intent(Intent.ACTION_SEND);
            sharingIntent.setType("text/html");
            sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, Html.fromHtml("<p>This is the text shared.</p>"));
            startActivity(Intent.createChooser(sharingIntent, "Share using"));



        } else if (id == R.id.contact) {

        }

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
    }
