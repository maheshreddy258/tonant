package in.ecoprice.tonant;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatEditText;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.DatePicker;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.basgeekball.awesomevalidation.AwesomeValidation;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import in.ecoprice.tonant.Retrofit.Responce.AccessToken;
import in.ecoprice.tonant.Retrofit.Responce.CreateRemarksRes;
import in.ecoprice.tonant.Retrofit.SharedPref;
import in.ecoprice.tonant.Retrofit.network.ApiService;
import in.ecoprice.tonant.Retrofit.network.RetrofitBuilder;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class VisitActivity extends AppCompatActivity {

    private static final String TAG = "VisitActivity";
    private AppCompatEditText mRemarks;
    private AppCompatButton mAppCompatButton;
    private ProgressBar progressBar;
    ApiService service;
    Call<CreateRemarksRes> call;
    TextInputEditText RevisitDate;
    private int mYear, mMonth, mDay, mHour, mMinute;
    InternetChecker internetChecker;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_visit);
        getSupportActionBar().setTitle("Remarks");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        internetChecker = new InternetChecker(this);
        mRemarks = (AppCompatEditText) findViewById(R.id.Remarks);
        mAppCompatButton = (AppCompatButton) findViewById(R.id.AppCompatButton);
        progressBar= (ProgressBar) findViewById(R.id.progressBar);
        RevisitDate = (TextInputEditText) findViewById(R.id.RevisitDate);
        service = RetrofitBuilder.createService(ApiService.class);
        SharedPref sharedPref = new SharedPref(getApplicationContext());
        final String username =sharedPref.getStr("userName");
        final String shopuserNmae = getIntent().getStringExtra("ShopUserName");
        final String shopName = getIntent().getStringExtra("ShopName");
        Log.w(TAG,"Visit  ==> "+"shopuserNmae :"+shopuserNmae+" ShopName :"+shopName);
        progressBar.setVisibility(View.GONE);

        RevisitDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                final Calendar c = Calendar.getInstance();
                mYear = c.get(Calendar.YEAR);
                mMonth = c.get(Calendar.MONTH);
                mDay = c.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog dpd = new DatePickerDialog(VisitActivity.this,
                        new DatePickerDialog.OnDateSetListener() {

                            @SuppressLint("SetTextI18n")
                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {
                                SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
                                String dateString = format.format(c.getTime());
                                RevisitDate.setText(dateString);

                            }
                        }, mYear, mMonth, mDay);
                dpd.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
                dpd.show();
            }
        });


        mAppCompatButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Date
                Date c = Calendar.getInstance().getTime();
                SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy");
                //Time
                Date currentTime = Calendar.getInstance().getTime();
                SimpleDateFormat dateFormat = new SimpleDateFormat("hh.mm.ss aa");
                //Var
                String formattedDate = df.format(c);
                String output = dateFormat.format(currentTime);
                String remark = mRemarks.getText().toString();
                String rd = RevisitDate.getText().toString();
                String dateTime = formattedDate+" "+ output;
                mAppCompatButton.setVisibility(View.GONE);
                progressBar.setVisibility(View.VISIBLE);
                CreateRemarks(username,shopName,shopuserNmae,dateTime,remark,rd);
                Log.w(TAG,"Visit :=> "+username+shopName+shopuserNmae+dateTime+remark+"Rd"+rd);
                }
        });





    }


    private void CreateRemarks(String username, String shopName, String shopuserNmae, String dateTime, String remark,String rd) {
        call = service.CreateRemarks(username, shopName,shopuserNmae,dateTime,remark,rd);
        call.enqueue(new Callback<CreateRemarksRes>() {
            @Override
            public void onResponse(@NonNull Call<CreateRemarksRes> call, @NonNull Response<CreateRemarksRes> response) {
                Log.w(TAG, "onResponse: " + response);
                response.body();
                boolean Loginstatus = response.body().isRecordStatus();
                Log.w(TAG, "Loginstatus: " + Loginstatus);
                if (Loginstatus) {
                    Toast.makeText(VisitActivity.this,response.body().getErrorMessage(),Toast.LENGTH_LONG).show();
                    Log.w(TAG, "onResponse: " + response.body().getErrorMessage());
                    int SPLASH_TIME_OUT = 2000;
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                mAppCompatButton.setVisibility(View.VISIBLE);
                                progressBar.setVisibility(View.GONE);
                                startActivity(new Intent(VisitActivity.this, ShopListActivity.class));
                                finish();
                            }
                        }, SPLASH_TIME_OUT);
                } else{
                    Toast.makeText(VisitActivity.this,response.body().getErrorMessage(),Toast.LENGTH_LONG).show();
                    mAppCompatButton.setVisibility(View.VISIBLE);
                    progressBar.setVisibility(View.GONE);
                    }
                }
            @Override
            public void onFailure(Call<CreateRemarksRes> call, Throwable t) {
                Log.w(TAG, "onFailure: " + t.getMessage());
                mAppCompatButton.setVisibility(View.VISIBLE);
                progressBar.setVisibility(View.GONE);
            }
        });



    }



    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }



}
