package in.ecoprice.tonant.Adapters;


import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import com.balysv.materialripple.MaterialRippleLayout;

import java.util.ArrayList;
import java.util.List;

import in.ecoprice.tonant.Models.ShopFilter;
import in.ecoprice.tonant.Models.Shoplist;
import in.ecoprice.tonant.R;
import in.ecoprice.tonant.Remainders.CreateRemainderActivity;


/* Created by nani on 11/17/2017. */

public class SelectShopListAdapters extends RecyclerView.Adapter<SelectShopListAdapters.ViewHolder> implements Filterable {
    public List<Shoplist> completeds;
    private String name = "BankList";
    private Context ctx;
    private Context context;
    ShopFilter filter;
    public SelectShopListAdapters(List<Shoplist> listitems, Context ctx) {
        this.completeds = listitems;
        this.ctx = ctx;
    }

    @Override
    public SelectShopListAdapters.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.banklist_item,parent,false);
        ViewHolder viewHolder = new ViewHolder(v,ctx,completeds);




        return  viewHolder;
    }

    @SuppressLint({"SetTextI18n", "ClickableViewAccessibility"})
    @Override
    public void onBindViewHolder(final SelectShopListAdapters.ViewHolder holder, int position )  {
        final Shoplist listItem =completeds.get(position);
       holder.mBankName.setText(listItem.getShopName());
       }

    @Override
    public int getItemCount() {
        return completeds.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private TextView mBankName;
        Context ctx;
        ArrayList<Shoplist> completeds = new ArrayList<Shoplist>();

        @SuppressLint("CutPasteId")
        private ViewHolder(View View, Context ctx, List<Shoplist> completeds) {
            super(View);

            View.setOnClickListener(this);
            this.completeds = (ArrayList<Shoplist>) completeds;
            this.ctx = ctx;
            mBankName = (TextView) View.findViewById(R.id.BankName);


            }



        @Override
        public void onClick(View v) {
            int position = getAdapterPosition();
            Shoplist posts = this.completeds.get(position);
            Intent intent = new Intent(this.ctx, CreateRemainderActivity.class);
            intent.putExtra("shopName",  posts.getShopName());
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            this.ctx.startActivity(intent);

            MaterialRippleLayout.on(v)
                    .rippleColor(Color.BLACK)
                    .create();
        }

    }

    @Override
    public Filter getFilter() {
        if(filter==null)
        {
            filter=new ShopFilter(completeds,this);
        }

        return filter;
    }

}


