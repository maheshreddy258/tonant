package in.ecoprice.tonant.Remainders;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import in.ecoprice.tonant.Adapters.ProductAdapter;
import in.ecoprice.tonant.Adapters.TodayRemainderListAdapter;
import in.ecoprice.tonant.Models.Constants;
import in.ecoprice.tonant.Models.RemainderList;
import in.ecoprice.tonant.Models.TodayRemindersResponse;
import in.ecoprice.tonant.R;
import in.ecoprice.tonant.Retrofit.Responce.ProductListResponse;
import in.ecoprice.tonant.Retrofit.SharedPref;
import in.ecoprice.tonant.Retrofit.network.ApiClient;
import in.ecoprice.tonant.Retrofit.network.ApiService;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.android.volley.VolleyLog.TAG;

/**
 * A simple {@link Fragment} subclass.
 */
public class TodayRemaindersFragment extends Fragment {


    public TodayRemaindersFragment() {
        // Required empty public constructor
    }


    private RecyclerView Gal_Rec;
   // private RecyclerView.Adapter adapter;
    private List<RemainderList> listItems;
    public static List<RemainderList> listItems1;
    List<TodayRemindersResponse> RemindersList = new ArrayList<>();
    TodayRemainderListAdapter adapter;
    private ProgressBar progressBar;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_today_remainders, container, false);


        Gal_Rec = (RecyclerView) view.findViewById(R.id.Rec_Gallery);
        Gal_Rec.setHasFixedSize(true);
        Gal_Rec.setNestedScrollingEnabled(false);
        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(getActivity().getApplicationContext(), 1);
        Gal_Rec.setLayoutManager(mLayoutManager);
        listItems = new ArrayList<>();
        listItems1 = new ArrayList<>();
        progressBar = (ProgressBar) view.findViewById(R.id.Prog_Assigned);



        loadRecyclerViewData();
        return  view;
    }

    private void loadRecyclerViewData() {
        progressBar.setVisibility(View.VISIBLE);
        Gal_Rec.setVisibility(View.GONE);
        String username = SharedPref.getStr("userName");

        ApiService apiService = ApiClient.getClient().create(ApiService.class);
        Call<List<TodayRemindersResponse>> todayremindersresponsecall = apiService.getTodayRemindersList(username);
        todayremindersresponsecall.enqueue(new Callback<List<TodayRemindersResponse>>() {
            @Override
            public void onResponse(Call<List<TodayRemindersResponse>> call, Response<List<TodayRemindersResponse>> response) {

                progressBar.setVisibility(View.GONE);
                Gal_Rec.setVisibility(View.VISIBLE);

                if (response.isSuccessful() && response != null) {

                    /*List<TodayRemindersResponse> RemindersList = new ArrayList<>();*/
                    RemindersList = response.body();


                    Date calendar = Calendar.getInstance().getTime();
                    SimpleDateFormat dateFormatC = new SimpleDateFormat(Constants.HISTORYDATEFORMAT);
                    String strDateC = dateFormatC.format(calendar);
                    List<TodayRemindersResponse> RemindersList1 = new ArrayList<>();
                    for (TodayRemindersResponse todayRemindersResponse : RemindersList) {
                        //Log.e("reminderid", "" + todayRemindersResponse.id);
                        //convert the reminder date string to date format
                        if (todayRemindersResponse.getDateofvisittobe() != null && !todayRemindersResponse.getDateofvisittobe().isEmpty()) {
                            Date reminderDate = null;
                            try {
                                reminderDate = new SimpleDateFormat(Constants.HISTORYDATEFORMAT).parse(todayRemindersResponse.getDateofvisittobe());
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }
                                  /* Calendar reminderCalendar = Calendar.getInstance();
                                   reminderCalendar.setTime(reminderDate);*/
                            Date dateConversion = Calendar.getInstance().getTime();
                            SimpleDateFormat dateFormat = new SimpleDateFormat(Constants.HISTORYDATEFORMAT);
                            String strDate = dateFormat.format(reminderDate);


                            Log.e("Calendar date ", "" + strDateC);
                            Log.e("Visit date", "" + strDate);

                            if (strDate != null && strDateC != null && strDateC.equals(strDate)) {
                                RemindersList1.add(todayRemindersResponse);
                            }
                        }
                    }

                    if (RemindersList1!= null && RemindersList1.size() > 0) {



                        adapter = new TodayRemainderListAdapter(RemindersList1,getActivity());
                        Log.e("Reminder list....","sucesss....");
                        Gal_Rec.setAdapter(adapter);

                    } else {
                        Toast.makeText(getActivity(), "No Products found", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(getActivity(), "Failed To get the Reminders List", Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<List<TodayRemindersResponse>> call, Throwable t) {

                Toast.makeText(getActivity(), "" + t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });






/*        String URL_DATA = "http://apiservices.tonantfarmers.com/api/ReminderListbySalesName?UserName="+username;
        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(URL_DATA, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                Gal_Rec.setVisibility(View.VISIBLE);
                progressBar.setVisibility(View.GONE);
                try {
                    if (response.length() > 0) {

                        Date calendar = Calendar.getInstance().getTime();
                        SimpleDateFormat dateFormatC = new SimpleDateFormat(Constants.HISTORYDATEFORMAT);
                        String strDateC = dateFormatC.format(calendar);


                        for (int i = 0; i < response.length(); i++) {
                            JSONObject obj =response.getJSONObject(i);
                            RemainderList item = new RemainderList(
                                    obj.getString("Id"),
                                    obj.getString("ShopKeeperName"),
                                    obj.getString("dateofcreation"),
                                    obj.getString("dateofvisittobe"),
                                    obj.getString("reason"),
                                    obj.getString("VisiteType")

                            );

                            listItems = listItems1;
                            listItems.add(item);
                            }
                        adapter = new TodayRemainderListAdapter(listItems , getContext().getApplicationContext());
                        Gal_Rec.setAdapter(adapter);
                        CountDuplicates(listItems);
                        Log.w(TAG,"ToDay Remianders Date"+" "+CountDuplicates(listItems));

                    }
                }catch (JSONException e) {
                    e.printStackTrace();
                }


            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Gal_Rec.setVisibility(View.GONE);
                progressBar.setVisibility(View.GONE);
                VolleyLog.e("Error: ", error.getMessage());
                Toast.makeText(getActivity(),"Try Again ",Toast.LENGTH_LONG).show();

            }
        });
        RequestQueue requestQueue = Volley.newRequestQueue(getContext().getApplicationContext());
        jsonArrayRequest.setRetryPolicy(new DefaultRetryPolicy(
                2000*5,
                *//*DefaultRetryPolicy.DEFAULT_MAX_RETRIES*//* 3,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(jsonArrayRequest);*/
    }

    public int CountDuplicates(List<RemainderList> listitems) {
        int duplicates = 0;
        for (int i = 0; i < listitems.size()-1;i++)
        {
            boolean found = false;

            for (int j = i+1; !found && j < listitems.size(); j++)
            {
                if (listitems.get(i).getDateofvisittobe().equals(listitems.get(j).getDateofvisittobe()));
                found = true;
                Log.w(TAG,"ToDay Remianders Date"+" "+listitems.get(i).getDateofvisittobe()+"  "+(listitems.get(j).getDateofvisittobe()));
                duplicates++;
            }
        }
        return duplicates;
    }

}
