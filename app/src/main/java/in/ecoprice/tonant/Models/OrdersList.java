package in.ecoprice.tonant.Models;

public class OrdersList {
    private String Id,OrderId,ProductId,ProductName,Qty,UnitPrice,Price,ProductWait;

    public OrdersList(String id, String orderId, String productId, String productName, String qty, String unitPrice, String price, String productWait) {
        Id = id;
        OrderId = orderId;
        ProductId = productId;
        ProductName = productName;
        Qty = qty;
        UnitPrice = unitPrice;
        Price = price;
        ProductWait = productWait;
    }

    public String getId() {
        return Id;
    }

    public void setId(String id) {
        Id = id;
    }

    public String getOrderId() {
        return OrderId;
    }

    public void setOrderId(String orderId) {
        OrderId = orderId;
    }

    public String getProductId() {
        return ProductId;
    }

    public void setProductId(String productId) {
        ProductId = productId;
    }

    public String getProductName() {
        return ProductName;
    }

    public void setProductName(String productName) {
        ProductName = productName;
    }

    public String getQty() {
        return Qty;
    }

    public void setQty(String qty) {
        Qty = qty;
    }

    public String getUnitPrice() {
        return UnitPrice;
    }

    public void setUnitPrice(String unitPrice) {
        UnitPrice = unitPrice;
    }

    public String getPrice() {
        return Price;
    }

    public void setPrice(String price) {
        Price = price;
    }

    public String getProductWait() {
        return ProductWait;
    }

    public void setProductWait(String productWait) {
        ProductWait = productWait;
    }
}
