package in.ecoprice.tonant.dummy;

import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import in.ecoprice.tonant.Models.Cart;
import in.ecoprice.tonant.Models.Products;
import in.ecoprice.tonant.Models.ProductsList;

public class ModelCart{

    private static List<Cart> data = new ArrayList<Cart>();

    public static List<Cart> getData() {
        return data;
    }

    public static void setData(List<Cart> data) {
        ModelCart.data = data;
    }


    public int getCartSize() {

        return data.size();

    }


}
