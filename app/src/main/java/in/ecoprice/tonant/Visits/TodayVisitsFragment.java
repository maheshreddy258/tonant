package in.ecoprice.tonant.Visits;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Cache;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.ParseError;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import in.ecoprice.tonant.Adapters.MyTargetsAdapter;
import in.ecoprice.tonant.Adapters.TodayVisitsAdapter;
import in.ecoprice.tonant.Models.MyTargets;
import in.ecoprice.tonant.Models.TodayVisits;
import in.ecoprice.tonant.R;
import in.ecoprice.tonant.Retrofit.SharedPref;

/**
 * A simple {@link Fragment} subclass.
 */
public class TodayVisitsFragment extends Fragment {


    public TodayVisitsFragment() {
        // Required empty public constructor
    }

    private RecyclerView Gal_Rec;
    private RecyclerView.Adapter adapter;
    private List<TodayVisits> listItems;
    public static List<TodayVisits> listItems1;
    private ProgressBar progressBar;
   // private TextView TextError;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_today_visits, container, false);

        Gal_Rec = (RecyclerView) view.findViewById(R.id.Rec_Gallery);
        //TextError = (TextView) view.findViewById(R.id.TextError);
        Gal_Rec.setHasFixedSize(true);
        Gal_Rec.setNestedScrollingEnabled(false);
        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(getActivity().getApplicationContext(), 1);
        Gal_Rec.setLayoutManager(mLayoutManager);
        listItems = new ArrayList<>();
        listItems1 = new ArrayList<>();
        progressBar = (ProgressBar) view.findViewById(R.id.Prog_Assigned);





        loadRecyclerViewData();
        return view;
    }

    private void loadRecyclerViewData() {
        progressBar.setVisibility(View.VISIBLE);
        Gal_Rec.setVisibility(View.GONE);
        String username = SharedPref.getStr("userName");
        String URL_DATA = "http://apiservices.tonantfarmers.com/api/MyOrdersbysales?Sales=" + username;
        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(URL_DATA, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                // mSwipeRefreshLayout.setRefreshing(false);
                /*  progressDialog.dismiss();*/
                Gal_Rec.setVisibility(View.VISIBLE);
                progressBar.setVisibility(View.GONE);
               // TextError.setVisibility(View.VISIBLE);
                try {
                    if (response.length() > 0) {
                        for (int i = 0; i < 30; i++) {
                            JSONObject obj = response.getJSONObject(i);
                            TodayVisits item = new TodayVisits(
                                    obj.getString("Id"),
                                    obj.getString("UserName"),
                                    obj.getString("ShopkeeparName"),
                                    obj.getString("Quanity"),
                                    obj.getString("DeliveryAddress"),
                                    obj.getString("Total"),
                                    obj.getString("Tax"),
                                    obj.getString("DeliveryCharges"),
                                    obj.getString("GST"),
                                    obj.getString("NetPay"),
                                    obj.getString("PaymentStatus"),
                                    obj.getString("Status"),
                                    obj.getString("PaidThrough"),
                                    obj.getString("PendingAmount"),
                                    obj.getString("Createddate"),
                                    obj.getString("OrdersList")

                            );
                            listItems = listItems1;
                            listItems.add(item);
                        }
                        adapter = new TodayVisitsAdapter(listItems, getActivity());
                        Gal_Rec.setAdapter(adapter);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                /*  progressDialog.dismiss();*/
                Gal_Rec.setVisibility(View.GONE);
                progressBar.setVisibility(View.GONE);
                VolleyLog.e("Error: ", error.getMessage());
                Toast.makeText(getActivity(), "Try Again ", Toast.LENGTH_LONG).show();

            }
        }){

            @Override
            protected Response<JSONArray> parseNetworkResponse(NetworkResponse response) {
                try {
                    Cache.Entry cacheEntry = HttpHeaderParser.parseCacheHeaders(response);
                    if (cacheEntry == null) {
                        cacheEntry = new Cache.Entry();
                    }
                    final long cacheHitButRefreshed = 3 * 60 * 1000; // in 3 minutes cache will be hit, but also refreshed on background
                    final long cacheExpired = 24 * 60 * 60 * 1000; // in 24 hours this cache entry expires completely
                    long now = System.currentTimeMillis();
                    final long softExpire = now + cacheHitButRefreshed;
                    final long ttl = now + cacheExpired;
                    cacheEntry.data = response.data;
                    cacheEntry.softTtl = softExpire;
                    cacheEntry.ttl = ttl;
                    String headerValue;
                    headerValue = response.headers.get("Date");
                    if (headerValue != null) {
                        cacheEntry.serverDate = HttpHeaderParser.parseDateAsEpoch(headerValue);
                    }
                    headerValue = response.headers.get("Last-Modified");
                    if (headerValue != null) {
                        cacheEntry.lastModified = HttpHeaderParser.parseDateAsEpoch(headerValue);
                    }
                    cacheEntry.responseHeaders = response.headers;
                    final String jsonString = new String(response.data,
                            HttpHeaderParser.parseCharset(response.headers));
                    return Response.success(new JSONArray(jsonString), cacheEntry);
                } catch (UnsupportedEncodingException e) {
                    return Response.error(new ParseError(e));
                } catch (JSONException e) {
                    return Response.error(new ParseError(e));
                }
            }

            @Override
            protected void deliverResponse(JSONArray response) {
                super.deliverResponse(response);
            }

            @Override
            public void deliverError(VolleyError error) {
                super.deliverError(error);
            }

            @Override
            protected VolleyError parseNetworkError(VolleyError volleyError) {
                return super.parseNetworkError(volleyError);
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(getContext().getApplicationContext());
        jsonArrayRequest.setRetryPolicy(new DefaultRetryPolicy(
                2000*5,
                /*DefaultRetryPolicy.DEFAULT_MAX_RETRIES*/ 3,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(jsonArrayRequest);
    }
}