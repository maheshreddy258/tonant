package in.ecoprice.tonant.Retrofit.network;


import java.util.Date;
import java.util.List;

import in.ecoprice.tonant.Models.TodayRemindersResponse;
import in.ecoprice.tonant.Retrofit.Responce.AccessToken;
import in.ecoprice.tonant.Retrofit.Responce.AddShopResp;
import in.ecoprice.tonant.Retrofit.Responce.AddtoCartRes;
import in.ecoprice.tonant.Retrofit.Responce.BannerResponse;
import in.ecoprice.tonant.Retrofit.Responce.CreateRemarksRes;
import in.ecoprice.tonant.Retrofit.Responce.Example;
import in.ecoprice.tonant.Retrofit.Responce.MyOrdersResponse;
import in.ecoprice.tonant.Retrofit.Responce.OrderDetailsResponse;
import in.ecoprice.tonant.Retrofit.Responce.Ordersdetails;
import in.ecoprice.tonant.Retrofit.Responce.PendingOrdersResponse;
import in.ecoprice.tonant.Retrofit.Responce.PendingRes;
import in.ecoprice.tonant.Retrofit.Responce.ProductListResponse;
import in.ecoprice.tonant.Retrofit.Responce.RemainderResp;
import in.ecoprice.tonant.Retrofit.Responce.Result;
import in.ecoprice.tonant.Retrofit.Responce.Targets;
import in.ecoprice.tonant.Retrofit.Responce.UserRegRes;
import in.ecoprice.tonant.newChanges.NewExample;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Query;


public interface ApiService {

    @POST("SalesLogin")
    @FormUrlEncoded
    Call<AccessToken> SalesLogin(@Field("username") String username, @Field("pwd") String password);



    @POST("Login")
    @FormUrlEncoded
    Call<UserRegRes> Login(@Field("username") String username, @Field("pwd") String password);

    @POST("UserRegistration")
    @FormUrlEncoded
    Call<UserRegRes> UserRegistration(@Field("Name") String username, @Field("Phone") String Phone, @Field("Email") String Email, @Field("Password") String Password);


    @POST("CreateShopKeepar")
    @FormUrlEncoded
    Call<AddShopResp> AddShop(
            @Field("First_Name") String firstname,
            @Field("CROEMailID") String emailid,
            @Field("MobileNumber") String mobilenumber,
            @Field("ShopName") String shopname,
            @Field("ShopAddress") String shopAddress,
            @Field("LeadType") String leadby,
            @Field("PIN_Code") String pincode,
            @Field("Latitude") String lat,
            @Field("Longitude") String longi,
            @Field("CreateBy") String createdby,
            @Field("LoginUserName") String loginUsername,
            @Field("UserName") String username
    );



    @Headers({
            "Accept: application/json",
            "Content-Type: application/json"
    })
    @GET("BannerImges")
    Call<List<BannerResponse>> getBanners();

    @POST("CreateShopKeepar")
    @FormUrlEncoded
    Call<AddShopResp> Update(
            @Field("Id") String id,
            @Field("First_Name") String firstname,
            @Field("CROEMailID") String emailid,
            @Field("MobileNumber") String mobilenumber,
            @Field("ShopName") String shopname,
            @Field("ShopAddress") String shopAddress,
            @Field("LeadType") String leadby,
            @Field("PIN_Code") String pincode,
            @Field("CreateBy") String createdby,
            @Field("LoginUserName") String loginUsername,
            @Field("UserName") String username
    );
    @POST("Settings")
    @FormUrlEncoded
    Call<Result> ChangePassword(@Field("OldPassword") String oldpass,
                                @Field("SPassword") String newpass,
                                @Field("Cpassword") String repass,
                                @Field("UserName") String username );

    @POST("CreateRemarks")
    @FormUrlEncoded
    Call<CreateRemarksRes> CreateRemarks(@Field("SalesName") String SalesName,
                                         @Field("ShopName") String ShopName,
                                         @Field("ShopUserName") String ShopUserName,
                                         @Field("Date") String Date,
                                         @Field("Remark") String Remark,
                                         @Field("RevisitDate") String RevisitDate);


    @POST("CreateTarget")
    @FormUrlEncoded
    Call<Targets> CreateTarget(@Field("OutletType") String TargetType,
                               @Field("TargetTitle") String TargetTitle,
                               @Field("OutletCount") String Retailers,
                               @Field("TargetDays") String TargetDays,
                               @Field("SaleKgs") String SaleKgs,
                               @Field("AddedBy") String userName,
                               @Field("Applicablefrom") String Applicablefrom,
                               @Field("Applicaleto") String Applicaleto,
                               @Field("TargetType") String type,
                               @Field("UserRole") String UserRole,
                               @Field("Month") String Month,
                               @Field("BusinessAmountlimit") String BusinessAmountlimit,
                               @Field("LoginUserName") String LoginUserName


    );



    @POST("CreateTarget")
    @FormUrlEncoded
    Call<Targets> CreateTargetVisits (
                               @Field("TargetType") String TargetType,
                               @Field("TargetDays") String TargetDays,
                               @Field("incRetailsfortarget") String incRetailsfortarget,
                               @Field("IncDelarsfortarget") String IncDelarsfortarget,
                               @Field("AddedBy") String userName,
                               @Field("UserRole") String UserRole,
                               @Field("Month") String Month,
                               @Field("LoginUserName") String LoginUserName );



    @POST("Addreminder")
    @FormUrlEncoded
    Call<RemainderResp> Addreminder(@Field("ShopKeeperName") String ShopKeeperName,
                                    @Field("dateofvisittobe") String dateofvisittobe,
                                    @Field("VisiteType") String VisiteType,
                                    @Field("reason") String reason,
                                    @Field("SalesPerson") String SalesPerson);

    @POST("UpdateReminder")
    @FormUrlEncoded
    Call<RemainderResp> UpdateReminder(
                                    @Field("Id") String Id,
                                    @Field("ShopKeeperName") String ShopKeeperName,
                                    @Field("dateofvisittobe") String dateofvisittobe,
                                    @Field("VisiteType") String VisiteType,
                                    @Field("reason") String reason,
                                    @Field("SalesPerson") String SalesPerson);


    @POST("PendingAmount")
    @FormUrlEncoded
    Call<PendingRes> PendingAmount(@Field("ShopID") double ShopID,
                                   @Field("ShopName") String ShopName,
                                   @Field("ShopUsername") String ShopUsername,
                                   @Field("PendingAmount") String PendingAmount,
                                   @Field("PaidAmount") String PaidAmount,
                                   @Field("LoginUsername") String LoginUsername
                                      );

    //@FormUrlEncoded
    @POST("Addtocart")
    Call<AddtoCartRes> AddtoCart(@Body Example example);

    @POST("Addtocart")
    Call<AddtoCartRes> NewAddtoCart(@Body NewExample example);

    @GET("Addtocart")
    Call<AddtoCartRes> GetData(@Body NewExample example);

    /*@GET("MyOrdersbysales?Sales="+Ayup)
    Call<AddtoCartRes> GetOders(@Body NewExample example);*/

    @Headers({
            "Accept: application/json",
            "Content-Type: application/json"
    })
    @GET("AllProducts")
    Call<List<ProductListResponse>> getProductList();





    @Headers({
            "Accept: application/json",
            "Content-Type: application/json"
    })
    @GET("ReminderListbySalesName?")
    Call<List<TodayRemindersResponse>> getTodayRemindersList(@Query("UserName") String UserName);




    @Headers({
            "Accept: application/json",
            "Content-Type: application/json"
    })
    @GET("MyOrdersbysales?")
    Call<List<MyOrdersResponse>> getMyordersList(@Query("Sales") String Sales);



    @Headers({
            "Accept: application/json",
            "Content-Type: application/json"
    })
    @GET("PendingDelivery?")
    Call<List<PendingOrdersResponse>> getPendingordersList(@Query("salesName") String salesName);


    @GET("orderdetailslist?")
    Call<List<OrderDetailsResponse>> getOrderDetails(@Query("Id") int Id);

}


