package in.ecoprice.tonant;

import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.location.LocationManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.DecelerateInterpolator;
import android.widget.Button;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import in.ecoprice.tonant.Adapters.TodayReportsAdapter;
import in.ecoprice.tonant.Models.TodayReports;
import in.ecoprice.tonant.Retrofit.SharedPref;
import in.ecoprice.tonant.Targets.CreateTargetActivity;
import in.ecoprice.tonant.User.UserLoginActivity;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = "MainActivity";
    private Button Logout;
    private CardView AddShop,AddProducts,ShopList,ChangePassword,Profile,MyTargets,MyVisits,Remainders,pendingAmount,mCvmyorders,mCvpendingorders;
    private TextView Name,Date,CCOunt;
    private ProgressBar progressBar;
    //location
    protected static final String TAG123 = "LocationOnOff";
    private GoogleApiClient googleApiClient;
    final static int REQUEST_LOCATION = 199;
    InternetChecker internetChecker;
    TextView textView;
    int Count = 89;
    int Sceonds = 2000;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        internetChecker = new InternetChecker(this);
        Logout = (Button) findViewById(R.id.Logout);

        AddProducts = (CardView) findViewById(R.id.AddProducts);
        AddShop = (CardView) findViewById(R.id.AddShop);
        ShopList = (CardView) findViewById(R.id.ShopList);
        ChangePassword = (CardView) findViewById(R.id.ChangePassword);
        Profile = (CardView) findViewById(R.id.Profile);
        MyTargets = (CardView) findViewById(R.id.MyTargets);
        Remainders = (CardView) findViewById(R.id.Remainders);
        MyVisits = (CardView) findViewById(R.id.MyVisits);
        pendingAmount = (CardView) findViewById(R.id.pendingAmount);
        mCvmyorders = findViewById(R.id.myorders);
        mCvpendingorders = findViewById(R.id.pendingorders);


        Name = (TextView) findViewById(R.id.Name);
        CCOunt = (TextView) findViewById(R.id.CCOunt);
        Date = (TextView) findViewById(R.id.Date);
        textView = (TextView) findViewById(R.id.visits);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        SharedPref dataProccessor = new SharedPref(getApplicationContext());
        String usern = dataProccessor.getStr("userName");
        String upperString = usern.substring(0,1).toUpperCase() + usern.substring(1);
        Name.setText("Hi "+upperString+"!");
           //Date
        Date c = Calendar.getInstance().getTime();
        SimpleDateFormat df = new SimpleDateFormat("dd MMM yyyy");
        String formattedDate = df.format(c);
        Date.setText(formattedDate);
        
        getVisits(textView);
        
        Logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //SharedPref.deleteShared();
                startActivity(new Intent(MainActivity.this, UserLoginActivity.class));
                finish();
            }
        });
        AddProducts.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this, MyOrdersActivity.class));
            }
        });
        AddShop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this, AddShopActivity.class));
            }
        });
        ShopList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this, ShopListActivity.class));
            }
        });
        ChangePassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this, ChangePasswordActivity.class));
            }
        });
        Profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                startActivity(new Intent(MainActivity.this, VisitsReportsActivity.class));
              /*  final String[] text = {"New visits","Old visits","All visits"};
                android.app.AlertDialog.Builder   builder = new android.app.AlertDialog.Builder(MainActivity.this,
                                                                android.app.AlertDialog.THEME_DEVICE_DEFAULT_LIGHT);
                builder.setItems(text, new DialogInterface.OnClickListener() {
                    @SuppressLint("SetTextI18n")
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        ListView lw = ((android.app.AlertDialog)dialog).getListView();
                        Object checkedItem = lw.getAdapter().getItem(which);

                        if(checkedItem.toString().equals("New visits")){
                           Intent intent = new Intent(MainActivity.this, VisitsReportsActivity.class);
                           intent.putExtra("name",checkedItem.toString());
                           intent.putExtra("type","new");
                           startActivity(intent);
                        }else if (checkedItem.toString().equals("Old visits")){
                            Intent intent = new Intent(MainActivity.this, VisitsReportsActivity.class);
                            intent.putExtra("name",checkedItem.toString());
                            intent.putExtra("type","old");
                            startActivity(intent);
                        }else if (checkedItem.toString().equals("All visits")){
                            Intent intent = new Intent(MainActivity.this, VisitsReportsActivity.class);
                            intent.putExtra("name",checkedItem.toString());
                            intent.putExtra("type","all");
                            startActivity(intent);
                        }

                    }
                });
                builder.show();
*/
            }
        });
        MyTargets.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this, MyTargetsActivity.class));
            }
        });
        MyVisits.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this, MyVisitsActivity.class));
            }
        });

        Remainders.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this, ReminderActivity.class));
            }
        });

        pendingAmount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this, PendingAmountActivity.class));
            }
        });
        mCvmyorders.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this, MyOrderNewActivity.class));
            }
        });

        mCvpendingorders.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this, PendingOrdersActivity.class));
            }
        });

        startCountAnimation();
        runOnUiThread(new Runnable() {
            public void run() {
                @SuppressLint("ObjectAnimatorBinding") ObjectAnimator anim = ObjectAnimator.ofFloat(progressBar, "progress", 0, 89);
                // @SuppressLint("ObjectAnimatorBinding") ObjectAnimator anim = ObjectAnimator.ofInt(mDonut_progress,"donut_progress",0,78);
                anim.setInterpolator(new DecelerateInterpolator());
                anim.setDuration(Sceonds);
                anim.start();
                anim.addListener(new Animator.AnimatorListener() {
                    @Override
                    public void onAnimationStart(Animator animation) {
                    }
                    @Override
                    public void onAnimationCancel(Animator animation) {
                    }
                    @Override
                    public void onAnimationRepeat(Animator animation) {
                    }
                    @Override
                    public void onAnimationEnd(Animator animation) {
                       /* if (78 >= 100) {
                            Toast.makeText(getContext(),"Hello i'm in 78%",Toast.LENGTH_SHORT).show();
                        }*/
                    }
                });
            }
        });

        this.setFinishOnTouchOutside(true);

        // Todo Location Already on  ... start
        final LocationManager manager = (LocationManager) MainActivity.this.getSystemService(Context.LOCATION_SERVICE);
        if (manager.isProviderEnabled(LocationManager.GPS_PROVIDER) && hasGPSDevice(MainActivity.this)) {
           // Toast.makeText(MainActivity.this,"Gps already enabled",Toast.LENGTH_SHORT).show();
            //finish();
        }
        // Todo Location Already on  ... end

        if(!hasGPSDevice(MainActivity.this)){
           // Toast.makeText(MainActivity.this,"Gps not Supported",Toast.LENGTH_SHORT).show();
        }

        if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER) && hasGPSDevice(MainActivity.this)) {
           // Log.e("keshav","Gps already enabled");
           // Toast.makeText(MainActivity.this,"Gps not enabled",Toast.LENGTH_SHORT).show();
            enableLoc();
        }else{
            Log.e("loca","Gps already enabled");
           // Toast.makeText(MainActivity.this,"Gps already enabled",Toast.LENGTH_SHORT).show();
        }
    }

    private void startCountAnimation() {
        ValueAnimator animator = ValueAnimator.ofInt(0, Count);
        animator.setDuration(Sceonds);
        animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            public void onAnimationUpdate(ValueAnimator animation) {
                CCOunt.setText(animation.getAnimatedValue().toString()+"/ 100"/*+" \n \n Actual Sales"*/);

            }
        });
        animator.start();
    }

    private void getVisits(final TextView textView123) {
        String username = SharedPref.getStr("userName");
        String URL_DATA = "http://apiservices.tonantfarmers.com/api/TodayVisitsCount?salesName="+username;

        StringRequest stringRequest = new StringRequest(Request.Method.GET,
                URL_DATA,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String s) {
                        Log.w(TAG,"String Value "+s);
                        textView123.setText("Today visits "+s);
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.e("Error: ", error.getMessage());
                Toast.makeText(getApplicationContext(),error.getMessage(),Toast.LENGTH_LONG).show();

            }
        });
        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        requestQueue.add(stringRequest);
    }


    private boolean hasGPSDevice(Context context) {
        final LocationManager mgr = (LocationManager) context
                .getSystemService(Context.LOCATION_SERVICE);
        if (mgr == null)
            return false;
        final List<String> providers = mgr.getAllProviders();
        if (providers == null)
            return false;
        return providers.contains(LocationManager.GPS_PROVIDER);
    }

    private void enableLoc() {

        if (googleApiClient == null) {
            googleApiClient = new GoogleApiClient.Builder(MainActivity.this)
                    .addApi(LocationServices.API)
                    .addConnectionCallbacks(new GoogleApiClient.ConnectionCallbacks() {
                        @Override
                        public void onConnected(Bundle bundle) {

                        }

                        @Override
                        public void onConnectionSuspended(int i) {
                            googleApiClient.connect();
                        }
                    })
                    .addOnConnectionFailedListener(new GoogleApiClient.OnConnectionFailedListener() {
                        @Override
                        public void onConnectionFailed(ConnectionResult connectionResult) {

                            Log.d("Location error","Location error " + connectionResult.getErrorCode());
                        }
                    }).build();
            googleApiClient.connect();

            LocationRequest locationRequest = LocationRequest.create();
            locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
            locationRequest.setInterval(30 * 1000);
            locationRequest.setFastestInterval(5 * 1000);
            LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                    .addLocationRequest(locationRequest);

            builder.setAlwaysShow(true);

            PendingResult<LocationSettingsResult> result =
                    LocationServices.SettingsApi.checkLocationSettings(googleApiClient, builder.build());
            result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
                @Override
                public void onResult(LocationSettingsResult result) {
                    final Status status = result.getStatus();
                    switch (status.getStatusCode()) {
                        case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                            try {
                                // Show the dialog by calling startResolutionForResult(),
                                // and check the result in onActivityResult().
                                status.startResolutionForResult(MainActivity.this, REQUEST_LOCATION);

                               // finish();
                            } catch (IntentSender.SendIntentException e) {
                                // Ignore the error.
                            }
                            break;
                    }
                }
            });
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if(id == R.id.Logout){
            SharedPref.deleteShared();
            startActivity(new Intent(MainActivity.this, LoginActivity.class));
            finish();
        }

        if(id == R.id.Profile){
            startActivity(new Intent(MainActivity.this, ProfileActivity.class));
            }
        if(id == R.id.ChangePassword){
            startActivity(new Intent(MainActivity.this, ChangePasswordActivity.class));
        }


        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onStart() {
        SharedPref dataProccessor = new SharedPref(getApplicationContext());
        boolean status = dataProccessor.getBool("Loginstatus");

        if (!status){
            startActivity(new Intent(MainActivity.this, LoginActivity.class));
            finish();
        }else {
            Log.w(TAG, "Loginstatus: " + status);
        }

        super.onStart();
    }


    /*@Override
    protected void onResume() {
        internetChecker.isEveryThingEnabled();
        super.onResume();
    }

    @Override
    protected void onPause() {
        internetChecker.isEveryThingEnabled();
        super.onPause();
    }

    @Override
    protected void onRestart() {
        internetChecker.isEveryThingEnabled();
        super.onRestart();
    }*/
}
