package in.ecoprice.tonant.Models;

public class Shoplist {
    private String Id,First_Name,Last_Name,CROEMailID,MobileNumber,AadharNumber,PANNumber,ShopName,ShopAddress,CreateBy,LeadType;
    private String PIN_Code,Latitude,Longitude,PendingBill;

    public Shoplist(String id, String first_Name, String last_Name, String CROEMailID, String mobileNumber, String aadharNumber, String PANNumber, String shopName, String shopAddress, String createBy, String leadType, String PIN_Code, String latitude, String longitude, String pendingBill) {
        Id = id;
        First_Name = first_Name;
        Last_Name = last_Name;
        this.CROEMailID = CROEMailID;
        MobileNumber = mobileNumber;
        AadharNumber = aadharNumber;
        this.PANNumber = PANNumber;
        ShopName = shopName;
        ShopAddress = shopAddress;
        CreateBy = createBy;
        LeadType = leadType;
        this.PIN_Code = PIN_Code;
        Latitude = latitude;
        Longitude = longitude;
        PendingBill = pendingBill;
    }

    public String getId() {
        return Id;
    }

    public void setId(String id) {
        Id = id;
    }

    public String getFirst_Name() {
        return First_Name;
    }

    public void setFirst_Name(String first_Name) {
        First_Name = first_Name;
    }

    public String getLast_Name() {
        return Last_Name;
    }

    public void setLast_Name(String last_Name) {
        Last_Name = last_Name;
    }

    public String getCROEMailID() {
        return CROEMailID;
    }

    public void setCROEMailID(String CROEMailID) {
        this.CROEMailID = CROEMailID;
    }

    public String getMobileNumber() {
        return MobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        MobileNumber = mobileNumber;
    }

    public String getAadharNumber() {
        return AadharNumber;
    }

    public void setAadharNumber(String aadharNumber) {
        AadharNumber = aadharNumber;
    }

    public String getPANNumber() {
        return PANNumber;
    }

    public void setPANNumber(String PANNumber) {
        this.PANNumber = PANNumber;
    }

    public String getShopName() {
        return ShopName;
    }

    public void setShopName(String shopName) {
        ShopName = shopName;
    }

    public String getShopAddress() {
        return ShopAddress;
    }

    public void setShopAddress(String shopAddress) {
        ShopAddress = shopAddress;
    }

    public String getCreateBy() {
        return CreateBy;
    }

    public void setCreateBy(String createBy) {
        CreateBy = createBy;
    }

    public String getLeadType() {
        return LeadType;
    }

    public void setLeadType(String leadType) {
        LeadType = leadType;
    }

    public String getPIN_Code() {
        return PIN_Code;
    }

    public void setPIN_Code(String PIN_Code) {
        this.PIN_Code = PIN_Code;
    }

    public String getLatitude() {
        return Latitude;
    }

    public void setLatitude(String latitude) {
        Latitude = latitude;
    }

    public String getLongitude() {
        return Longitude;
    }

    public void setLongitude(String longitude) {
        Longitude = longitude;
    }

    public String getPendingBill() {
        return PendingBill;
    }

    public void setPendingBill(String pendingBill) {
        PendingBill = pendingBill;
    }
}
