package in.ecoprice.tonant;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.os.Handler;
import android.provider.Settings;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatEditText;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

import in.ecoprice.tonant.Retrofit.Responce.AddShopResp;
import in.ecoprice.tonant.Retrofit.SharedPref;
import in.ecoprice.tonant.Retrofit.network.ApiService;
import in.ecoprice.tonant.Retrofit.network.RetrofitBuilder;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ShopEditActivity extends AppCompatActivity {

    private static final String TAG = "ShopEditActivity";
    private TextInputLayout
            mFirst_Name,
            mCROEMailID,
            mMobileNumber,
            mShopName, mLeadType,
            mPIN_Code;
    private static final int REQUEST_LOCATION = 1;
    TextView textView;
    LocationManager locationManager;
    String lattitude,longitude;
    Button AddShopBtn;
    ApiService service;
    Call<AddShopResp> call;

    AppCompatEditText mShopAddress;

    String check;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shop_edit);

        getSupportActionBar().setTitle("Update Shop");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        service = RetrofitBuilder.createService(ApiService.class);
        mFirst_Name = (TextInputLayout) findViewById(R.id.Firstname);
        mCROEMailID = (TextInputLayout) findViewById(R.id.CROEMailID);
        mMobileNumber = (TextInputLayout) findViewById(R.id.MobileNumber);
        mShopName = (TextInputLayout) findViewById(R.id.ShopName);
        mShopAddress = (AppCompatEditText) findViewById(R.id.ShopAddress);
        mLeadType = (TextInputLayout) findViewById(R.id.LeadType);
        mPIN_Code = (TextInputLayout) findViewById(R.id.PIN_Code);
        textView = (TextView) findViewById(R.id.textview);
        AddShopBtn = (Button) findViewById(R.id.AddShopBtn);
        //Latitude,Longitude,LoginUserName,CreateBy
        mMobileNumber.getEditText().addTextChangedListener(numberWatcher);
        mCROEMailID.getEditText().addTextChangedListener(emailWatcher);
        mFirst_Name.getEditText().addTextChangedListener(nameWatcher);
        mShopAddress.setEnabled(false);

        //TextWatcher for Mobile -----------------------------------------------------




        mLeadType.getEditText().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final String[] text = {"Hostel","Hotel","Shop","Other"};
                android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(view.getRootView().getContext(), android.app.AlertDialog.THEME_DEVICE_DEFAULT_LIGHT);
                builder.setItems(text, new DialogInterface.OnClickListener() {
                    @SuppressLint("SetTextI18n")
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        ListView lw = ((android.app.AlertDialog)dialog).getListView();
                        Object checkedItem = lw.getAdapter().getItem(which);
                        mLeadType.getEditText().setText(checkedItem.toString());

                    }
                });
                builder.show();
            }
        });


        final String shopid =  getIntent().getStringExtra("ShopId");
        String shopUsername =  getIntent().getStringExtra("ShopUserName");
        String shopName =  getIntent().getStringExtra("ShopName");
        String mobile =  getIntent().getStringExtra("MobileNumber");
        String leadType =  getIntent().getStringExtra("LeadType");
        String pinCode = getIntent().getStringExtra("PIN_Code");
        String ShopAddress =  getIntent().getStringExtra("ShopAddress");
        String Name =  getIntent().getStringExtra("Name");
        String Email =  getIntent().getStringExtra("Email");

        mFirst_Name.getEditText().setText(Name);
        mCROEMailID.getEditText().setText(Email);
        mMobileNumber.getEditText().setText(mobile);
        mShopName.getEditText().setText(shopName);
        mShopAddress.setText(ShopAddress);
        mPIN_Code.getEditText().setText(pinCode);
        mLeadType.getEditText().setText(leadType);

        AddShopBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!validate(mLeadType)
                        || !validate(mFirst_Name)
                        || !validate(mMobileNumber)
                        || !validate(mShopName)
                        || !validateEd(mShopAddress)
                        ||  !validate(mLeadType)){
                    Log.w(TAG,"Response :"+ " Username Empty");

                }else {
                    AddShop(shopid,
                            mFirst_Name.getEditText().getText().toString(),
                            mCROEMailID.getEditText().getText().toString(),
                            mMobileNumber.getEditText().getText().toString(),
                            mShopName.getEditText().getText().toString(),
                            mShopAddress.getText().toString(),
                            mLeadType.getEditText().getText().toString(),
                            mPIN_Code.getEditText().getText().toString(),
                            lattitude,
                            longitude,
                            SharedPref.getStr("userName"),
                            SharedPref.getStr("userName")

                    );
                }
            }
        });






    }

    private void AddShop(String id, String firstname, String email, String mobilenumber, String shopname, String shopaddress, String leadType, String pinCode,String lati,String longi,String username,String loginusername) {
        final ProgressDialog progressDialog= new ProgressDialog(ShopEditActivity.this);
        progressDialog.setMessage("Loading...");
        progressDialog.show();
        progressDialog.setCancelable(false);
        progressDialog.setCanceledOnTouchOutside(false);
        call = service.Update(id,firstname,email,mobilenumber,shopname,shopaddress,leadType,pinCode,username,loginusername,mobilenumber);
        call.enqueue(new Callback<AddShopResp>() {
            @Override
            public void onResponse(Call<AddShopResp> call, Response<AddShopResp> response) {
                Log.w(TAG, "onResponse: " + response);
                response.body();
                boolean Loginstatus = response.body().isStatus();
                Log.w(TAG, "Loginstatus: " + Loginstatus);
                if (Loginstatus) {
                    Toast.makeText(ShopEditActivity.this,response.body().getErrorMSG(),Toast.LENGTH_LONG).show();
                    Log.w(TAG, "status: " + Loginstatus);
                    Log.w(TAG, "ErrorMSG: " + response.body().getErrorMSG());
                    //Log.w(TAG, "ErrorMSG: " + response.body().());

                    int TIME = 2000;
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            progressDialog.dismiss();
                            startActivity(new Intent(ShopEditActivity.this, MainActivity.class));
                            finish();
                        }
                    }, TIME);
                } else {
                    progressDialog.dismiss();
                    Toast.makeText(ShopEditActivity.this,response.body().getErrorMSG(),Toast.LENGTH_SHORT).show();
                    Log.w(TAG, "status: in false" + Loginstatus);
                    Log.w(TAG, "ErrorMSG in false: " + response.body().getErrorMSG());
                        /*ErrorMsg.setVisibility(View.VISIBLE);
                        int SPLASH_TIME_OUT = 50000;
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                                ErrorMsg.setVisibility(View.GONE);
                            }
                        }, SPLASH_TIME_OUT);*/
                }

            }

            @Override
            public void onFailure(Call<AddShopResp> call, Throwable t) {
                progressDialog.dismiss();
                Log.w(TAG, "onFailure: " + t.getMessage());
                // showForm();
            }
        });

    }


    public boolean validate(TextInputLayout textInputEditText) {
        boolean valid = true;
        textInputEditText.setEnabled(true);
        textInputEditText.setError(null);
        String  textInputEditText123 = textInputEditText.getEditText().getText().toString();
        if (textInputEditText123.length() == 0) {
            textInputEditText.setError("Required");
            textInputEditText.getEditText().requestFocus();
            valid = false;
        } else {
            textInputEditText.setError(null);
        }

        return valid;
    }


    public boolean validateEd(AppCompatEditText textInputEditText) {
        boolean valid = true;
        textInputEditText.setEnabled(true);
        textInputEditText.setError(null);
        String  textInputEditText123 = textInputEditText.getText().toString();
        if (textInputEditText123.length() == 0) {
            textInputEditText.setError("Required");
            textInputEditText.requestFocus();
            valid = false;
        } else {
            textInputEditText.setError(null);
        }

        return valid;
    }




    /*private void turnGPSOn(){
        Intent intent=new Intent("android.location.GPS_ENABLED_CHANGE");
        intent.putExtra("enabled", true);
        sendBroadcast(intent);
        startActivity(new Intent(AddShopActivity.this, MainActivity.class));
        finish();
    }*/



    @Override
    protected void onStart() {
        /*try {
            getLocation();
        } catch (IOException e) {
            e.printStackTrace();
        }*/
        super.onStart();
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }
    private boolean validateNumber(TextInputLayout textInputEditText) {
        textInputEditText.setEnabled(true);
        textInputEditText.setError(null);
        check = textInputEditText.getEditText().getText().toString();
        Log.e("inside number",check.length()+" ");
        if (check.length()>10) {
            return false;
        }else if(check.length()<10){
            return false;
        }
        return true;
    }

    private boolean validateEmail(TextInputLayout textInputEditText) {

        check = textInputEditText.getEditText().getText().toString();

        if (check.length() < 4 || check.length() > 40) {
            return false;
        } else if (!check.matches("^[A-za-z0-9.@]+")) {
            return false;
        } else if (!check.contains("@") || !check.contains(".")) {
            return false;
        }

        return true;
    }

    TextWatcher numberWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            //none
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            //none
        }

        @Override
        public void afterTextChanged(Editable s) {
            mMobileNumber.setEnabled(true);
            mMobileNumber.setError(null);
            check = s.toString();
            if (check.length()>10) {
                mMobileNumber.setEnabled(true);
                mMobileNumber.setError("Number cannot be grated than 10 digits");
            }else if(check.length()<10){
                mMobileNumber.setEnabled(true);
                mMobileNumber.setError("Number should be 10 digits");
            }
        }

    };

    TextWatcher emailWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            //none
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            //none
        }

        @Override
        public void afterTextChanged(Editable s) {
            mCROEMailID.setEnabled(true);
            mCROEMailID.setError(null);
            check = s.toString();

            if (check.length() < 4 || check.length() > 40) {
                mCROEMailID.setError("Email Must consist of 4 to 20 characters");
            } else if (!check.matches("^[A-za-z0-9.@]+")) {
                mCROEMailID.setError("Only . and @ characters allowed");
            } else if (!check.contains("@") || !check.contains(".")) {
                mCROEMailID.setError("Enter Valid Email");
            }

        }

    };

    TextWatcher nameWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            //none
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            //none
        }

        @Override
        public void afterTextChanged(Editable s) {
            mFirst_Name.setEnabled(true);
            mFirst_Name.setError(null);
            check = s.toString();

            if (check.length() < 4 || check.length() > 20) {
                mFirst_Name.setError("Name Must consist of 4 to 20 characters");
            }
        }

    };
}
