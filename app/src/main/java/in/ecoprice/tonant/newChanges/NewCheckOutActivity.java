package in.ecoprice.tonant.newChanges;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatEditText;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.GsonBuilder;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import in.ecoprice.tonant.MainActivity;
import in.ecoprice.tonant.Models.Cart;
import in.ecoprice.tonant.R;
import in.ecoprice.tonant.Remainders.CreateRemainderActivity;
import in.ecoprice.tonant.ReminderActivity;
import in.ecoprice.tonant.Retrofit.Responce.AddtoCartRes;
import in.ecoprice.tonant.Retrofit.Responce.Example;
import in.ecoprice.tonant.Retrofit.Responce.RemainderResp;
import in.ecoprice.tonant.Retrofit.SharedPref;
import in.ecoprice.tonant.Retrofit.network.ApiService;
import in.ecoprice.tonant.Retrofit.network.RetrofitBuilder;
import in.ecoprice.tonant.ShopListActivity;
import in.ecoprice.tonant.dummy.ModelCart;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class NewCheckOutActivity extends AppCompatActivity {

    private static final String TAG = "Checkout";
    ApiService service;
    Call call;
    Button send;
    TextView mNetAmount,mQnty,Shopname;
    TextInputLayout mPIN_Code,paid_amount,SelectDateTime;
    private LinearLayout SendDetails,RemarksLayout;
    TextInputEditText SelectShop,DateofVisitTobe,VisiteType;
    AppCompatEditText Remarks;
    Button button;
    private int mYear, mMonth, mDay, mHour, mMinute;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_check_out);
        service = RetrofitBuilder.createService(ApiService.class);


        SelectShop = (TextInputEditText) findViewById(R.id.SelectShop);
        DateofVisitTobe = (TextInputEditText) findViewById(R.id.DateofVisitTobe);
        VisiteType = (TextInputEditText) findViewById(R.id.VisiteType);
        Remarks = (AppCompatEditText) findViewById(R.id.Remarks);
        button = (Button) findViewById(R.id.BtnTarget);
        SendDetails = (LinearLayout) findViewById(R.id.SendDetails);
        RemarksLayout = (LinearLayout) findViewById(R.id.RemarksLayout);
        SendDetails.setVisibility(View.VISIBLE);


        SelectShop.setEnabled(false);
        VisiteType.setEnabled(false);
        //Remarks.setEnabled(false);

        send = (Button) findViewById(R.id.send);
        mNetAmount = (TextView) findViewById(R.id.NetAmount);
        mQnty = (TextView) findViewById(R.id.mQnty);
        Shopname = (TextView) findViewById(R.id.Shopname);
        mPIN_Code = (TextInputLayout) findViewById(R.id.PIN_Code);
        SelectDateTime = (TextInputLayout) findViewById(R.id.SelectDateTime);
        paid_amount = (TextInputLayout) findViewById(R.id.paid_amount);
        SelectDateTime.setVisibility(View.GONE);

        final List<ModelProducts> data = NewModelCart.getData();
        final String ShopName = getIntent().getStringExtra("ShopName");
        Shopname.setText("Shop Name :" + ShopName);






        int total = 0;
        Log.d("TAG", "start total = " + total);
        for (int i = 0; i < data.size(); i++) {
            Log.d("TAG", "at " + i + " quantity = " + data.get(i).getUnitPrice());
            total += data.get(i).getQuantity() * data.get(i).getUnitPrice();
            Log.d("TAG", "at " + i + " total = " + total);
            mNetAmount.setText("Total = " + total);
            paid_amount.getEditText().setText(""+total);
        }

        int total12 = 0;
        Log.d("TAG", "start total = " + total12);
        for (int i = 0; i < data.size(); i++) {
            Log.d("TAG", "at " + i + " quantity = " + data.get(i).getQuantity());
            total12 += data.get(i).getQuantity();
            Log.d("TAG", "at " + i + " total = " + total12);
            mQnty.setText("Qnty = " + total12);
        }

        mPIN_Code.getEditText().setText("Cash");
        mPIN_Code.getEditText().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final String[] text = {"Cash", "Account Transfer", "Paytm"};
                AlertDialog.Builder builder = new AlertDialog.Builder(view.getRootView().getContext(), AlertDialog.THEME_DEVICE_DEFAULT_LIGHT);
                builder.setItems(text, new DialogInterface.OnClickListener() {
                    @SuppressLint("SetTextI18n")
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        ListView lw = ((AlertDialog) dialog).getListView();
                        Object checkedItem = lw.getAdapter().getItem(which);
                        mPIN_Code.getEditText().setText(checkedItem.toString());

                    }
                });
                builder.show();
            }
        });

        SimpleDateFormat sdfDate = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        Date now = new Date();
        String strDate = sdfDate.format(now);
        SelectDateTime.getEditText().setText(strDate);
        Log.w(TAG,"Today Date :" +SelectDateTime.getEditText().getText().toString());
        SelectDateTime.getEditText().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Calendar c = Calendar.getInstance();
                mYear = c.get(Calendar.YEAR);
                mMonth = c.get(Calendar.MONTH);
                mDay = c.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog dpd = new DatePickerDialog(NewCheckOutActivity.this,
                        new DatePickerDialog.OnDateSetListener() {

                            @SuppressLint("SetTextI18n")
                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {

                                SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
                                String dateString = format.format(c.getTime());
                                SelectDateTime.getEditText().setText(dateString);

                            }
                        }, mYear, mMonth, mDay);
                dpd.show();
            }
        });

        send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!validate(paid_amount)) {
                    Log.w(TAG, "Response :" + " Username Empty");

                } else {
                    int total = 0;
                    Log.d("TAG", "start total = " + total);
                    for (int i = 0; i < data.size(); i++) {
                        Log.d("TAG", "at " + i + " quantity = " + data.get(i).getQuantity());
                        total += data.get(i).getQuantity() * data.get(i).getUnitPrice();
                        Log.d("TAG", "at " + i + " total = " + total);
                        mNetAmount.setText("Total = " + total);

                    }

                    int total12 = 0;
                    Log.d("TAG", "start total = " + total12);
                    for (int i = 0; i < data.size(); i++) {
                        Log.d("TAG", "at " + i + " quantity = " + data.get(i).getQuantity());
                        total12 += data.get(i).getQuantity();
                        Log.d("TAG", "at " + i + " total = " + total12);
                        mQnty.setText("Qnty = " + total12);
                    }

                    SharedPref sharedPref = new SharedPref(getApplicationContext());
                    final String NetAmount = String.valueOf(total);
                    final String paidAmount = paid_amount.getEditText().getText().toString();
                    final String Quantity = String.valueOf(total12);
                    final String paidthrough = mPIN_Code.getEditText().getText().toString();
                    final String PaymentStatus = "true";
                    final String shopuserNmae = getIntent().getStringExtra("ShopUserName");
                    final String ShopName = getIntent().getStringExtra("ShopName");
                    final String username = sharedPref.getStr("userName");
                    final String datetime = SelectDateTime.getEditText().getText().toString();
                    Log.w(TAG, String.valueOf(ModelCart.getData()));
                    InputMethodManager inputManager = (InputMethodManager) getApplicationContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                    inputManager.hideSoftInputFromWindow(NewCheckOutActivity.this.getCurrentFocus().getWindowToken(), InputMethodManager.RESULT_UNCHANGED_SHOWN);
                    Log.w(TAG, shopuserNmae + " " + ShopName + " " + username + " " + NetAmount + " " + paidthrough + " " + paidAmount + " " + Quantity + " " + PaymentStatus);
                    SendAddToCart(shopuserNmae, ShopName, username, NetAmount, paidthrough, paidAmount, Quantity, PaymentStatus,datetime);

                }


            }

            ;
        });


        DateofVisitTobe.setOnClickListener(new View.OnClickListener() {
                                               @Override
                                               public void onClick(View view) {
                                                   final Calendar c = Calendar.getInstance();
                                                   mYear = c.get(Calendar.YEAR);
                                                   mMonth = c.get(Calendar.MONTH);
                                                   mDay = c.get(Calendar.DAY_OF_MONTH);

                                                   DatePickerDialog dpd = new DatePickerDialog(NewCheckOutActivity.this,
                                                           new DatePickerDialog.OnDateSetListener() {

                                                               @SuppressLint("SetTextI18n")
                                                               @Override
                                                               public void onDateSet(DatePicker view, int year,
                                                                                     int monthOfYear, int dayOfMonth) {

                                                                   /*SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
                                                                   String dateString = format.format(c.getTime());
                                                                   ;*/

                                                                   @SuppressLint("SimpleDateFormat")
                                                                   SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
                                                                   c.set(year, monthOfYear , dayOfMonth);
                                                                   String dateString = sdf.format(c.getTime());
                                                                   DateofVisitTobe.setText(dateString);
                                                               }
                                                           }, mYear, mMonth, mDay);
                                                   dpd.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
                                                   dpd.show();
                                               }
        });
            //Log.w(TAG, data.toArray().toString());
        System.out.println(data.size());
        for(int i = 0; i<data.size();i++) {
                String text = data.get(i).getId() + "       " + data.get(i).getProductName() + "       " + data.get(i).getUnitPrice() + "       " + data.get(i).getQuantity();
                Log.w(TAG, text);
            }

    }

    private void SendAddToCart(String shopuserNmae, String shopName, String username, final String netAmount,
                               String paidthrough, String paidAmount, String quantity, String paymentStatus, String datetime) {

        List<ModelProducts> orderList =  NewModelCart.getData();
        final NewExample example = new NewExample();
        example.setShopUserName(shopuserNmae);
        example.setShopName(shopName);
        example.setSalesLogin(username);
        example.setNetAmount(Double.parseDouble(netAmount));
        example.setPaidthrough(paidthrough);
        example.setPaidAmount(Double.parseDouble(paidAmount));
        example.setQuantity(Integer.parseInt(quantity));
        example.setPaymentStatus(Boolean.parseBoolean(paymentStatus));
        example.setAgendaWeekDays(orderList);
        example.setCreatedDate(datetime);
        Log.w("printed gson => ",new GsonBuilder().setPrettyPrinting().create().toJson(example));

        final ProgressDialog progressDialog= new ProgressDialog(NewCheckOutActivity.this);
        progressDialog.setMessage("Loading...");
        progressDialog.show();
        progressDialog.setCancelable(false);
        progressDialog.setCanceledOnTouchOutside(false);

        call = service.NewAddtoCart(example);
        call.enqueue(new Callback<AddtoCartRes>() {
            @Override
            public void onResponse(@NonNull Call<AddtoCartRes> call, @NonNull Response<AddtoCartRes> response) {
                Log.w(TAG, "onResponse: " + response);
                response.body().toString();
                Log.d("Status :=>", String.valueOf(response.body().isStatus()));
                Log.d("Error MSG :=>", String.valueOf(response.body().getErrorMessage()));
                if (response.isSuccessful()) {
                    // AddtoCartRes addtoCartRes = response;
                    if (response.body().isStatus()) {
                      /*  final List<ModelProducts> data = NewModelCart.getData();
                                data.clear();*/
                        int netpay = response.body().getNetAmount();
                       int paid = response.body().getPaidAmount();


                            if(netpay == paid){
                                final List<ModelProducts> data = NewModelCart.getData();
                                data.clear();
                                Toast.makeText(NewCheckOutActivity.this, response.body().getErrorMessage(), Toast.LENGTH_LONG).show();
                                int TIME = 2000;
                                new Handler().postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        progressDialog.dismiss();
                                        Intent intent = new Intent(NewCheckOutActivity.this, ShopListActivity.class);
                                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP |
                                                Intent.FLAG_ACTIVITY_NEW_TASK);
                                        intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                                        startActivity(intent);
                                        finish();
                                    }
                                }, TIME);
                            }else if(netpay > paid){
                                final List<ModelProducts> data = NewModelCart.getData();
                                data.clear();
                                progressDialog.dismiss();
                                SendDetails.setVisibility(View.GONE);
                                RemarksLayout.setVisibility(View.VISIBLE);
                                final String ShopName = getIntent().getStringExtra("ShopName");
                                SelectShop.setText(ShopName);
                                VisiteType.setText("Pending Amount");
                                int sum = netpay-paid;
                                Remarks.setText(sum+" Pending Amount");
                                DateofVisitTobe.requestFocus();
                                button.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        if (!validate(DateofVisitTobe)){
                                            Log.d("Status :=>", "Date of visit is null");

                                        }else{
                                            SendRemainder(
                                                    SelectShop.getText().toString(),
                                                    DateofVisitTobe.getText().toString(),
                                                    VisiteType.getText().toString(),
                                                    Remarks.getText().toString(),
                                                    SharedPref.getStr("userName")
                                            );
                                        }



                                    }
                                });

                            }else if(netpay < paid){
                                final List<ModelProducts> data = NewModelCart.getData();
                                data.clear();
                                progressDialog.dismiss();
                                RemarksLayout.setVisibility(View.VISIBLE);
                                SendDetails.setVisibility(View.GONE);
                                final String ShopName = getIntent().getStringExtra("ShopName");
                                SelectShop.setText(ShopName);
                                DateofVisitTobe.requestFocus();
                                VisiteType.setText("Advanced Amount");
                                int sum = paid-netpay;
                                Remarks.setText(sum+" Advanced Amount for Next Order");
                                button.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        if (!validate(DateofVisitTobe)){
                                            Log.d("Status :=>", "Date of visit is null");

                                        }else {
                                            SendRemainder(
                                                    SelectShop.getText().toString(),
                                                    DateofVisitTobe.getText().toString(),
                                                    VisiteType.getText().toString(),
                                                    Remarks.getText().toString(),
                                                    SharedPref.getStr("userName")
                                            );
                                        }

                                    }
                                });
                             }





                    } else {
                        Toast.makeText(NewCheckOutActivity.this, "Try Again", Toast.LENGTH_LONG).show();
                        progressDialog.dismiss();
                        Log.d("error message", response.body().getErrorMessage());
                    }
                } else {
                    Toast.makeText(NewCheckOutActivity.this, "Error BODY :" + response.errorBody().toString(), Toast.LENGTH_LONG).show();
                    Log.w(TAG, "Error BODY :" + response.errorBody().toString());
                }

            }



            @Override
            public void onFailure(Call<AddtoCartRes> call, Throwable t) {
                Log.w(TAG, "onFailure: " + t.getMessage());
                progressDialog.dismiss();
            }
        });
    }

    private void SendRemainder(String shopname, String dateOfvisit, String visiteType, String Remarks, String userName) {
        Log.w(TAG, "dateOfvisit: " + dateOfvisit);
        final ProgressDialog progressDialog= new ProgressDialog(NewCheckOutActivity.this);
        progressDialog.setMessage("Loading...");
        progressDialog.show();
        progressDialog.setCancelable(false);
        call = service.Addreminder(shopname,dateOfvisit,visiteType,Remarks,userName);
        call.enqueue(new Callback<RemainderResp>() {
            @Override
            public void onResponse(Call<RemainderResp> call, Response<RemainderResp> response) {
                Log.w(TAG, "onResponse: " + response);
                response.body();
                boolean Loginstatus = response.body().isStatus();
                Log.w(TAG, "Loginstatus: " + Loginstatus);
                if (Loginstatus) {
                    Toast.makeText(NewCheckOutActivity.this,response.body().getErrorMessage(),Toast.LENGTH_LONG).show();
                    Log.w(TAG, "status: " + Loginstatus);
                    Log.w(TAG, "ErrorMSG: " + response.body().getErrorMessage());

                    int TIME = 2000;
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            progressDialog.dismiss();
                            Intent intent = new Intent(NewCheckOutActivity.this, ShopListActivity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP |
                                    Intent.FLAG_ACTIVITY_NEW_TASK);
                            intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                            startActivity(intent);
                            finish();
                        }
                    }, TIME);
                } else {
                    progressDialog.dismiss();
                    Toast.makeText(NewCheckOutActivity.this,response.body().getErrorMessage(),Toast.LENGTH_SHORT).show();
                    Log.w(TAG, "status: in false" + Loginstatus);
                    Log.w(TAG, "ErrorMSG in false: " + response.body().getErrorMessage());

                }

            }

            @Override
            public void onFailure(Call<RemainderResp> call, Throwable t) {
                progressDialog.dismiss();
                Toast.makeText(NewCheckOutActivity.this,""+t.getMessage(),Toast.LENGTH_LONG).show();
                Log.w(TAG, "onFailure: " + t.getMessage());
                // showForm();
            }
        });
    }


    public boolean validate(TextInputLayout textInputEditText) {
        boolean valid = true;
        textInputEditText.setEnabled(true);
        textInputEditText.setError(null);
        String  textInputEditText123 = textInputEditText.getEditText().getText().toString();
        if (textInputEditText123.length() == 0) {
            textInputEditText.setError("Required");
            textInputEditText.getEditText().requestFocus();
            valid = false;
        } else {
            textInputEditText.setError(null);
        }

        return valid;
    }


    public boolean validate(TextInputEditText textInputEditText) {
        boolean valid = true;
        textInputEditText.setEnabled(true);
        textInputEditText.setError(null);
        String  textInputEditText123 = textInputEditText.getText().toString();
        if (textInputEditText123.length() == 0) {
            textInputEditText.setError("Required");
            textInputEditText.requestFocus();
            valid = false;
        } else {
            textInputEditText.setError(null);
        }

        return valid;
    }
    @Override
    public void onBackPressed() {
        List<ModelProducts> data = NewModelCart.getData();
        data.clear();
        //  onBackPressed();
        super.onBackPressed();
    }

}




                /*} else {
                    Toast.makeText(CheckOutActivity.this,response.body().getErrorMessage(),Toast.LENGTH_LONG).show();
                    Toast.makeText(CheckOutActivity.this,"Try Again",Toast.LENGTH_LONG).show();
                    progressDialog.dismiss();
                    showForm();
                    tilEmail.setError("Invalid Username");
                    tilPassword.setError("Invalid Password");
                 ErrorMsg.setVisibility(View.VISIBLE);
                        int SPLASH_TIME_OUT = 50000;
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                                ErrorMsg.setVisibility(View.GONE);
                            }
                        }, SPLASH_TIME_OUT);
                }
                AddtoCartRes addtoCartRes = response.body();
                //Toast.makeText(CheckOutActivity.this,response.body().getErrorMessage(),Toast.LENGTH_LONG).show();

                    int TIME = 2000;
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            progressDialog.dismiss();
                            Intent intent = new Intent(CheckOutActivity.this,MainActivity.class);
                            startActivity(intent);
                            finish();
                        }
                    }, TIME);*/