package in.ecoprice.tonant.Retrofit.Responce;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class MyOrdersResponse implements Serializable
{

    @SerializedName("Id")
    @Expose
    public Integer id;
    @SerializedName("UserName")
    @Expose
    public String userName;
    @SerializedName("Quanity")
    @Expose
    public Double quanity;
    @SerializedName("DeliveryAddress")
    @Expose
    public String deliveryAddress;
    @SerializedName("Total")
    @Expose
    public Double total;
    @SerializedName("Tax")
    @Expose
    public Double tax;
    @SerializedName("DeliveryCharges")
    @Expose
    public Double deliveryCharges;
    @SerializedName("GST")
    @Expose
    public Double gST;
    @SerializedName("NetPay")
    @Expose
    public Double netPay;
    @SerializedName("Discount")
    @Expose
    public Double discount;
    @SerializedName("CouponId")
    @Expose
    public Integer couponId;
    @SerializedName("PaymentStatus")
    @Expose
    public String paymentStatus;
    @SerializedName("Status")
    @Expose
    public Integer status;
    @SerializedName("TransactionId")
    @Expose
    public String transactionId;
    @SerializedName("PaidThrough")
    @Expose
    public String paidThrough;
    @SerializedName("CODStatus")
    @Expose
    public Boolean cODStatus;
    @SerializedName("PendingAmount")
    @Expose
    public Double pendingAmount;
    @SerializedName("TrackingMessage")
    @Expose
    public String trackingMessage;
    @SerializedName("Createddate")
    @Expose
    public String createddate;
    @SerializedName("salesperson")
    @Expose
    public String salesperson;
    @SerializedName("ShopkeeparName")
    @Expose
    public String shopkeeparName;
    @SerializedName("Ordersdetails")
    @Expose
    public Ordersdetails ordersdetails;
    @SerializedName("OrdersList")
    @Expose
    public List<OrdersList> ordersList = null;
    @SerializedName("orderdetaisIds")
    @Expose
    public String orderdetaisIds;
    @SerializedName("PaidAmount")
    @Expose
    public Double paidAmount;
    @SerializedName("Userdetails")
    @Expose
    public Userdetails userdetails;
    @SerializedName("Comment")
    @Expose
    public String comment;
    @SerializedName("ProductOrderinfo")
    @Expose
    public List<ProductOrderinfo> productOrderinfo = null;
    @SerializedName("OrderBy")
    @Expose
    public String orderBy;
    @SerializedName("PayStatus")
    @Expose
    public Boolean payStatus;
    @SerializedName("tblOrderdetailslist")
    @Expose
    public List<TblOrderdetailslist> tblOrderdetailslist = null;
    @SerializedName("ReminderStatus")
    @Expose
    public String reminderStatus;
    @SerializedName("OldBalance")
    @Expose
    public Double oldBalance;
    @SerializedName("BalanceAmount")
    @Expose
    public Double balanceAmount;
    @SerializedName("PresentPaid")
    @Expose
    public Double presentPaid;
    @SerializedName("Reminder")
    @Expose
    public Reminder reminder;
    private final static long serialVersionUID = 5265682084033734273L;


    public class OrdersList implements Serializable
    {

        @SerializedName("Id")
        @Expose
        public Integer id;
        @SerializedName("OrderId")
        @Expose
        public Integer orderId;
        @SerializedName("ProductId")
        @Expose
        public Integer productId;
        @SerializedName("ProductName")
        @Expose
        public String productName;
        @SerializedName("Qty")
        @Expose
        public Double qty;
        @SerializedName("UnitPrice")
        @Expose
        public Double unitPrice;
        @SerializedName("Discount")
        @Expose
        public Double discount;
        @SerializedName("Price")
        @Expose
        public Double price;
        @SerializedName("Status")
        @Expose
        public Integer status;
        @SerializedName("DeliveryStatus")
        @Expose
        public Integer deliveryStatus;
        @SerializedName("TrackMessage")
        @Expose
        public String trackMessage;
        @SerializedName("Createdate")
        @Expose
        public String createdate;
        @SerializedName("DeliveryDate")
        @Expose
        public String deliveryDate;
        @SerializedName("DeliveryAssignStatus")
        @Expose
        public Integer deliveryAssignStatus;
        @SerializedName("ProductWait")
        @Expose
        public String productWait;
        @SerializedName("Weight")
        @Expose
        public Double weight;
        @SerializedName("QRCODEIMg")
        @Expose
        public String qRCODEIMg;
        @SerializedName("TotalAmount")
        @Expose
        public Double totalAmount;
        @SerializedName("ProductImage")
        @Expose
        public String productImage;
        private final static long serialVersionUID = 1699014493462043332L;

    }

    public class Ordersdetails implements Serializable
    {

        @SerializedName("Id")
        @Expose
        public Integer id;
        @SerializedName("OrderId")
        @Expose
        public Integer orderId;
        @SerializedName("ProductId")
        @Expose
        public Integer productId;
        @SerializedName("ProductName")
        @Expose
        public String productName;
        @SerializedName("Qty")
        @Expose
        public Double qty;
        @SerializedName("UnitPrice")
        @Expose
        public Double unitPrice;
        @SerializedName("Discount")
        @Expose
        public Double discount;
        @SerializedName("Price")
        @Expose
        public Double price;
        @SerializedName("Status")
        @Expose
        public Integer status;
        @SerializedName("DeliveryStatus")
        @Expose
        public Integer deliveryStatus;
        @SerializedName("TrackMessage")
        @Expose
        public String trackMessage;
        @SerializedName("Createdate")
        @Expose
        public String createdate;
        @SerializedName("DeliveryDate")
        @Expose
        public String deliveryDate;
        @SerializedName("DeliveryAssignStatus")
        @Expose
        public Integer deliveryAssignStatus;
        @SerializedName("ProductWait")
        @Expose
        public String productWait;
        @SerializedName("Weight")
        @Expose
        public Double weight;
        @SerializedName("QRCODEIMg")
        @Expose
        public String qRCODEIMg;
        @SerializedName("TotalAmount")
        @Expose
        public Double totalAmount;
        @SerializedName("ProductImage")
        @Expose
        public String productImage;
        private final static long serialVersionUID = -3169699284415950229L;

    }
    public class ProductOrderinfo implements Serializable
    {

        @SerializedName("productName")
        @Expose
        public String productName;
        @SerializedName("SaleAmount")
        @Expose
        public Double saleAmount;
        @SerializedName("Qty")
        @Expose
        public Double qty;
        @SerializedName("NetPay")
        @Expose
        public Double netPay;
        private final static long serialVersionUID = -5264964371235559337L;

    }


    public class Reminder implements Serializable
    {

        @SerializedName("Id")
        @Expose
        public Integer id;
        @SerializedName("SalesPerson")
        @Expose
        public String salesPerson;
        @SerializedName("ShopKeeperName")
        @Expose
        public String shopKeeperName;
        @SerializedName("dateofcreation")
        @Expose
        public String dateofcreation;
        @SerializedName("dateofvisittobe")
        @Expose
        public String dateofvisittobe;
        @SerializedName("reason")
        @Expose
        public String reason;
        @SerializedName("Status")
        @Expose
        public Boolean status;
        @SerializedName("VisiteStatus")
        @Expose
        public Boolean visiteStatus;
        @SerializedName("VisiteType")
        @Expose
        public String visiteType;
        @SerializedName("RetunStatus")
        @Expose
        public Boolean retunStatus;
        @SerializedName("ErrorMessage")
        @Expose
        public String errorMessage;
        private final static long serialVersionUID = -5912832648068265768L;

    }

    public class TblOrderdetailslist implements Serializable
    {

        @SerializedName("Id")
        @Expose
        public Integer id;
        @SerializedName("OrderId")
        @Expose
        public Integer orderId;
        @SerializedName("ProductId")
        @Expose
        public Integer productId;
        @SerializedName("ProductName")
        @Expose
        public String productName;
        @SerializedName("Qty")
        @Expose
        public Double qty;
        @SerializedName("UnitPrice")
        @Expose
        public Double unitPrice;
        @SerializedName("Discount")
        @Expose
        public Double discount;
        @SerializedName("Price")
        @Expose
        public Double price;
        @SerializedName("Status")
        @Expose
        public Integer status;
        @SerializedName("DeliveryStatus")
        @Expose
        public Integer deliveryStatus;
        @SerializedName("TrackMessage")
        @Expose
        public String trackMessage;
        @SerializedName("Createdate")
        @Expose
        public String createdate;
        @SerializedName("DeliveryDate")
        @Expose
        public String deliveryDate;
        @SerializedName("DeliveryAssignStatus")
        @Expose
        public Integer deliveryAssignStatus;
        @SerializedName("ProductWait")
        @Expose
        public String productWait;
        private final static long serialVersionUID = 1197719012966639343L;

    }


    public class Userdetails implements Serializable
    {

        @SerializedName("Id")
        @Expose
        public Integer id;
        @SerializedName("Name")
        @Expose
        public String name;
        @SerializedName("UserName")
        @Expose
        public String userName;
        @SerializedName("Phone")
        @Expose
        public String phone;
        @SerializedName("Email")
        @Expose
        public String email;
        @SerializedName("Status")
        @Expose
        public Boolean status;
        @SerializedName("RegisterDate")
        @Expose
        public String registerDate;
        @SerializedName("Password")
        @Expose
        public String password;
        @SerializedName("Gender")
        @Expose
        public String gender;
        @SerializedName("Address")
        @Expose
        public String address;
        @SerializedName("DOB")
        @Expose
        public String dOB;
        @SerializedName("Role")
        @Expose
        public String role;
        @SerializedName("ApiStatus")
        @Expose
        public Boolean apiStatus;
        @SerializedName("Displaymessage")
        @Expose
        public String displaymessage;
        private final static long serialVersionUID = -3402487955915136086L;

    }
}
