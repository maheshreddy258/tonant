package in.ecoprice.tonant.Models;

import android.widget.Filter;
import java.util.ArrayList;
import java.util.List;

import in.ecoprice.tonant.Adapters.SelectShopListAdapters;

public class ShopFilter extends Filter {

    SelectShopListAdapters adapter;
    List<Shoplist> filterList;

    public ShopFilter(List<Shoplist> filterList, SelectShopListAdapters adapter)
    {
        this.adapter=adapter;
        this.filterList=filterList;

    }
    @Override
    protected FilterResults performFiltering(CharSequence constraint) {
        FilterResults results=new FilterResults();


        if(constraint != null && constraint.length() > 0)
        {
            constraint=constraint.toString().toUpperCase();
            List<Shoplist> filteredPlayers=new ArrayList<>();


            for (int i=0;i<filterList.size();i++)
            {
                if (filterList.get(i).getShopName().toUpperCase().contains(constraint)) {
                filteredPlayers.add(filterList.get(i));
                }
            }

            results.count=filteredPlayers.size();
            results.values=filteredPlayers;
        }else
        {
            results.count=filterList.size();
            results.values=filterList;

        }

        return results;
    }

    @Override
    protected void publishResults(CharSequence constraint, FilterResults results) {

        adapter.completeds= (List<Shoplist>) results.values;

        adapter.notifyDataSetChanged();
    }



}

