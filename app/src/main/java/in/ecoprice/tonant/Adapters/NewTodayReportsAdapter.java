package in.ecoprice.tonant.Adapters;


import android.annotation.SuppressLint;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import in.ecoprice.tonant.Models.NewReportsModel;
import in.ecoprice.tonant.R;

/**
 * Created by nani on 11/17/2017.
 */

public class NewTodayReportsAdapter extends RecyclerView.Adapter<NewTodayReportsAdapter.ViewHolder> {
    private static List<NewReportsModel> listitems;
    private Context ctx;
    private Context context;

    public NewTodayReportsAdapter(List<NewReportsModel> listitems, Context ctx) {
        this.listitems = listitems;
        this.ctx = ctx;
    }

   /* public GralleryAdapter(List<Gallery> listitems, Context ctx) {
        this.listitems = listitems;
        this.ctx = ctx;
    }*/

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.new_order_item,parent,false);

        return new ViewHolder(v,ctx,listitems);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position )  {
        NewReportsModel listItem =listitems.get(position);
        holder.textView.setText(listItem.getProductName());
       double qnty = Double.parseDouble(listItem.getSaleinKg());
        holder.textView1.setText(new DecimalFormat("##.###").format(qnty)+" kg");
        holder.saleinKg.setText(""+listItem.getSoldInKg());

    }

    @Override
    public int getItemCount() {
        return listitems.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder /*implements View.OnClickListener*/ {
        private ImageView ser_img;
        private TextView textView,textView1,saleinKg;
        Context ctx;
        ArrayList<NewReportsModel> completeds = new ArrayList<NewReportsModel>();

        @SuppressLint("CutPasteId")
        public ViewHolder(View View, Context ctx, List<NewReportsModel> completeds) {
            super(View);

            /*View.setOnClickListener(this);*/
            this.completeds = (ArrayList<NewReportsModel>) completeds;
            this.ctx = ctx;
            textView = (TextView) View.findViewById(R.id.ProductName);
            textView1 = (TextView) View.findViewById(R.id.SoldQnty);
            saleinKg = (TextView) View.findViewById(R.id.saleinKg);


        }

       /* @Override
        public void onClick(View v) {
            int position = getAdapterPosition();
            Model posts = this.completeds.get(position);
            Intent intent = new Intent(this.ctx, VideoViewActivity.class);
            intent.putExtra("link", posts.getLink().replaceAll(" ", "%20"));
            intent.putExtra("name", posts.getName());
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            this.ctx.startActivity(intent);


        }*/

    }
}


