package in.ecoprice.tonant.newChanges;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.RectShape;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import in.ecoprice.tonant.Adapters.ShopSelectAdapter;
import in.ecoprice.tonant.MainActivity;
import in.ecoprice.tonant.Models.Shoplist;
import in.ecoprice.tonant.R;
import in.ecoprice.tonant.Retrofit.SharedPref;

public class NewPlaceOderActivity extends AppCompatActivity {

    private static final String TAG = "PlaceOder";
    TextView abcd;
   // private double total = 0.0;

    private LinearLayout LayoutNotNUll,LayoutNUll;
    private RecyclerView Rec_Assigned;
    private ShopSelectAdapter adapter;
    private List<Shoplist> listItems;
    public static List<Shoplist> listItems1;
    private ProgressBar progressBar;
    SearchView sv;
    private Button ButtonNext;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_place_oder);
        abcd = (TextView) findViewById(R.id.Abcd);
        LayoutNotNUll = (LinearLayout) findViewById(R.id.LayoutNotNUll);
        LayoutNUll = (LinearLayout) findViewById(R.id.LayoutNull);
        ButtonNext = (Button) findViewById(R.id.ButtonNext);
        sv= (SearchView) findViewById(R.id.mSearch);

        String itemname = getIntent().getStringExtra("itemname");
        float price = getIntent().getFloatExtra("Price", 0);
        //int qnty = getIntent().getIntExtra("pos", 0);


        TableLayout stk = (TableLayout) findViewById(R.id.table_main);
        TableRow tbrow0 = new TableRow(this);
        TextView tv1 = new TextView(this);
        tv1.setText(" Product name");
        tv1.setTextSize(20);
        tv1.setTextColor(Color.BLACK);
        tv1.setBackgroundResource(R.drawable.row_border);
        tbrow0.addView(tv1);
        TextView tv2 = new TextView(this);
        tv2.setText(" Price ");
        tv2.setTextColor(Color.BLACK);
        tv2.setTextSize(20);
        tv2.setBackgroundResource(R.drawable.row_border);
        tbrow0.addView(tv2);
        TextView tv3 = new TextView(this);
        tv3.setText(" Gm/Kg ");
        tv3.setTextColor(Color.BLACK);
        tv3.setTextSize(20);
        tv3.setBackgroundResource(R.drawable.row_border);
        tbrow0.addView(tv3);
        TextView tv4 = new TextView(this);
        tv4.setText(" Qnty ");
        tv4.setTextColor(Color.BLACK);
        tv4.setTextSize(20);
        tv4.setBackgroundResource(R.drawable.row_border);
        tbrow0.addView(tv4);
        stk.addView(tbrow0);

        final String ShopUserName = getIntent().getStringExtra("ShopUserName");
        final String ShopName = getIntent().getStringExtra("ShopName");
        Log.w(TAG,"PlaceOrder ==> "+"shopuserNmae :"+ShopUserName+" ShopName :"+ShopName);
        if (ShopUserName.equals("null")){
            LayoutNotNUll.setVisibility(View.VISIBLE);
            LayoutNUll.setVisibility(View.GONE);
        }else {
            LayoutNUll.setVisibility(View.VISIBLE);
            sv.setVisibility(View.GONE);
            LayoutNotNUll.setVisibility(View.GONE);
        }
        List<ModelProducts> data1 = NewModelCart.getData();
        if (data1.size()>0){
             ButtonNext.setVisibility(View.VISIBLE);
            ButtonNext.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(NewPlaceOderActivity.this, NewCheckOutActivity.class);
                    intent.putExtra("ShopUserName", ShopUserName);
                    intent.putExtra("ShopName", ShopName);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                    // finish();
                }
            });

        }else{

            /* setContentView(R.layout.emptycart);*/
            Toast.makeText(this, "Your Cart is empty....", Toast.LENGTH_SHORT).show();
            ButtonNext.setVisibility(View.INVISIBLE);
        }




        List<ModelProducts> data = NewModelCart.getData();
        System.out.println(data.size());
        Log.e("cart size",""+data.size());
        for (int i = 0; i < data.size(); i++) {
            String text = data.get(i).getProductName() + " " + data.get(i).getUnitPrice() + " " +
                          data.get(i).getQuantity()+ " " + data.get(i).getProductWait();
            Log.w(TAG, text);
            //System.out.println(data.get(i).getId());
           // removeDuplicates(data);
            init(data,i);
            }

        // Calculate total
         int total = 0;
        Log.d("TAG", "start total = " +total);
        for (int i = 0; i < data.size(); i++) {
            Log.d("TAG", "at "+i+ " quantity = " +data.get(i).getUnitPrice());
            total += data.get(i).getQuantity() * data.get(i).getUnitPrice();
            Log.d("TAG", "at "+i+ " total = " +total);
            abcd.setText("Total = " +total);
            //Constants.total_count= total;
        }


        Rec_Assigned = (RecyclerView) findViewById(R.id.Rec_Assigned);
        progressBar = (ProgressBar)  findViewById(R.id.Prog_Assigned);
        Rec_Assigned.setHasFixedSize(true);
        LinearLayoutManager layoutManager= new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        Rec_Assigned.setLayoutManager(layoutManager);
        Rec_Assigned.setNestedScrollingEnabled(false);
        Rec_Assigned.animate();
        listItems = new ArrayList<>();
        listItems1 = new ArrayList<>();



       /* final String shopuserNmae = getIntent().getStringExtra("ShopUserName");
        final String ShopName = getIntent().getStringExtra("ShopName");

      */





        /*sv.setIconifiedByDefault(true);
        sv.setFocusable(true);
        sv.setIconified(false);
        sv.requestFocusFromTouch();*/
        adapter =new ShopSelectAdapter(listItems , getApplicationContext());
        sv.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String query) {
                adapter.getFilter().filter(query);
                return false;
            }
        });
    }

    private void loadRecyclerViewData() {
        progressBar.setVisibility(View.VISIBLE);
        Rec_Assigned.setVisibility(View.GONE);
        SharedPref sharedPref = new SharedPref(getApplicationContext());
        String username = sharedPref.getStr("userName");
        String URL_DATA = "http://apiservices.tonantfarmers.com/api/MyLeads?UserName="+username;
        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(URL_DATA, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                progressBar.setVisibility(View.GONE);
                Rec_Assigned.setVisibility(View.VISIBLE);
                try {
                    if (response.length() > 0) {
                        for (int i = 0; i < response.length(); i++) {
                            JSONObject obj =response.getJSONObject(i);
                            Shoplist item = new Shoplist(
                                    obj.getString("Id"),
                                    obj.getString("First_Name"),
                                    obj.getString("Last_Name"),
                                    obj.getString("CROEMailID"),
                                    obj.getString("MobileNumber"),
                                    obj.getString("AadharNumber"),
                                    obj.getString("PANNumber"),
                                    obj.getString("ShopName"),
                                    obj.getString("ShopAddress"),
                                    obj.getString("CreateBy"),
                                    obj.getString("LeadType"),
                                    obj.getString("PIN_Code"),
                                    obj.getString("Latitude"),
                                    obj.getString("Longitude"),
                                    obj.getString("PendingBill")
                            );
                            listItems = listItems1;
                            listItems.add(item);
                        }
                        adapter = new ShopSelectAdapter(listItems , getApplicationContext());
                        Rec_Assigned.setAdapter(adapter);
                    }
                }catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressBar.setVisibility(View.GONE);
                Rec_Assigned.setVisibility(View.GONE);
                getErro();
                Toast.makeText(NewPlaceOderActivity.this,"Try Again :(",Toast.LENGTH_SHORT).show();

            }
        });
        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        requestQueue.add(jsonArrayRequest);

    }

    private void getErro() {
        Intent intent = new Intent(NewPlaceOderActivity.this,MainActivity.class);
        startActivity(intent);
        finish();
    }


    @SuppressLint("ResourceAsColor")
    private void UpdateTextView(String str) {
        //abcd.setText(text);
        LinearLayout MainLL = (LinearLayout) findViewById(R.id.main_layout);

        TextView text = new TextView(this);
            text.setText(str);
            text.setTextSize(18);

            text.setGravity(Gravity.LEFT);
            text.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
            MainLL.addView(text);

    }

    private void init(List<ModelProducts> data123,int i) {
            TableLayout stk = (TableLayout) findViewById(R.id.table_main);
            ShapeDrawable sd = new ShapeDrawable();
            sd.setShape(new RectShape());

        // Specify the border color of shape
        sd.getPaint().setColor(Color.BLACK);

        // Set the border width
        sd.getPaint().setStrokeWidth(10f);

        // Specify the style is a Stroke
        sd.getPaint().setStyle(Paint.Style.STROKE);

        // Finally, add the drawable background to TextView
        stk.setBackground(sd);





            TableRow tbrow = new TableRow(this);
            TextView t1v = new TextView(this);
            String name  = data123.get(i).getProductName();
            String s = name.substring(0,13);
            t1v.setText(s+"...");
            t1v.setTextColor(Color.GRAY);
            t1v.setGravity(Gravity.CENTER);
            t1v.setTextSize(18);

            t1v.setBackgroundResource(R.drawable.row_border);
            tbrow.addView(t1v);
            TextView t2v = new TextView(this);
            t2v.setText(""+new DecimalFormat("##.##").format(data123.get(i).getUnitPrice()));
            t2v.setTextColor(Color.GRAY);
            t2v.setGravity(Gravity.CENTER);
            t2v.setTextSize(18);
            t2v.setBackgroundResource(R.drawable.row_border);
            tbrow.addView(t2v);
            TextView t3v = new TextView(this);
            t3v.setText("" + data123.get(i).getProductWait()+" ");
            t3v.setTextColor(Color.GRAY);
            t3v.setGravity(Gravity.CENTER);
            t3v.setTextSize(18);
            t3v.setBackgroundResource(R.drawable.row_border);
            tbrow.addView(t3v);

        TextView t4v = new TextView(this);
        t4v.setText("" + data123.get(i).getQuantity()  );
        t4v.setTextColor(Color.GRAY);
        t4v.setGravity(Gravity.CENTER);
        t4v.setTextSize(18);
        t4v.setBackgroundResource(R.drawable.row_border);
        tbrow.addView(t4v);
        stk.addView(tbrow);
        }

        /*for (int i = 0; i < 25; i++) {
            TableRow tbrow = new TableRow(this);
            TextView t1v = new TextView(this);
            t1v.setText("" + i);
            t1v.setTextColor(Color.WHITE);
            t1v.setGravity(Gravity.CENTER);
            tbrow.addView(t1v);
            TextView t2v = new TextView(this);
            t2v.setText("Product " + i);
            t2v.setTextColor(Color.WHITE);
            t2v.setGravity(Gravity.CENTER);
            tbrow.addView(t2v);
            TextView t3v = new TextView(this);
            t3v.setText("Rs." + i);
            t3v.setTextColor(Color.WHITE);
            t3v.setGravity(Gravity.CENTER);
            tbrow.addView(t3v);
            TextView t4v = new TextView(this);
            t4v.setText("" + i * 15 / 32 * 10);
            t4v.setTextColor(Color.WHITE);
            t4v.setGravity(Gravity.CENTER);
            tbrow.addView(t4v);
            stk.addView(tbrow);

        }*/






   /* private void removeDuplicates(List<ModelProducts> list) {
        int count = list.size();

        for (int i = 0; i < count; i++)
        {
            for (int j = i + 1; j < count; j++)
            {
                Log.w(TAG,"Product name I :"+ list.get(i).getProductName());
                Log.w(TAG,"Product Wait I :"+ list.get(i).getProductWait());


                Log.w(TAG,"Product name J :"+ list.get(j).getProductName());
                Log.w(TAG,"Product Wait J :"+ list.get(j).getProductWait());
           *//*     if (list.get(i).getQuantity() < list.get(j).getQuantity())
            {
                list.get(i).setQuantity(list.get(j).getQuantity());
                Log.w(TAG,"Product HIght NUmber :"+ list.get(j).getQuantity());
            }*//*
                *//*list.get(i).setQuantity(list.get(j).getQuantity());
                list.get(i).setProductWait(list.get(j).getProductWait());*//*

                if (list.get(i).getProductWait().equals(list.get(j).getProductWait())) {
                    list.remove(j--);
                    count--;

                }
            }
        }
    }*/

    @Override
    public void onBackPressed() {
        List<ModelProducts> data = NewModelCart.getData();

       /* data.clear();*/
      //  onBackPressed();
        super.onBackPressed();
    }

    /*@Override
    protected void onStart() {
        InputMethodManager inputManager = (InputMethodManager) getApplicationContext().getSystemService(Context.INPUT_METHOD_SERVICE);
        inputManager.hideSoftInputFromWindow(NewPlaceOderActivity.this.getCurrentFocus().getWindowToken(), InputMethodManager.RESULT_UNCHANGED_SHOWN);

        super.onStart();
    }*/
}


