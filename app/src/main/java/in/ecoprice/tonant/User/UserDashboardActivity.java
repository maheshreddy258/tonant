package in.ecoprice.tonant.User;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import in.ecoprice.tonant.LoginActivity;
import in.ecoprice.tonant.MainActivity;
import in.ecoprice.tonant.R;
import in.ecoprice.tonant.Retrofit.SharedPref;

public class UserDashboardActivity extends AppCompatActivity {

    private static final String TAG = "UserDashBoard";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_dashboard);



    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if(id == R.id.Logout){
            SharedPref.deleteShared();
            startActivity(new Intent(UserDashboardActivity.this, LoginActivity.class));
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onStart() {
        SharedPref dataProccessor = new SharedPref(getApplicationContext());
        boolean status = dataProccessor.getBool("Loginstatus");

        if (!status){
            startActivity(new Intent(UserDashboardActivity.this, UserLoginActivity.class));
            finish();
        }else {
            Log.w(TAG, "Loginstatus: " + status);
        }

        super.onStart();
    }
}
