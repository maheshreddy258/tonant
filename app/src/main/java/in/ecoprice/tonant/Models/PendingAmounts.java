package in.ecoprice.tonant.Models;

public class PendingAmounts {
    private String id,First_Name,Last_Name,CROEMailID,MobileNumber,ShopName,ShopAddress,CreateDate,CreateBy,LeadType,UserName,PendingBill;

    public PendingAmounts(String id, String first_Name, String last_Name, String CROEMailID, String mobileNumber, String shopName, String shopAddress, String createDate, String createBy, String leadType, String userName, String pendingBill) {
        this.id = id;
        First_Name = first_Name;
        Last_Name = last_Name;
        this.CROEMailID = CROEMailID;
        MobileNumber = mobileNumber;
        ShopName = shopName;
        ShopAddress = shopAddress;
        CreateDate = createDate;
        CreateBy = createBy;
        LeadType = leadType;
        UserName = userName;
        PendingBill = pendingBill;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFirst_Name() {
        return First_Name;
    }

    public void setFirst_Name(String first_Name) {
        First_Name = first_Name;
    }

    public String getLast_Name() {
        return Last_Name;
    }

    public void setLast_Name(String last_Name) {
        Last_Name = last_Name;
    }

    public String getCROEMailID() {
        return CROEMailID;
    }

    public void setCROEMailID(String CROEMailID) {
        this.CROEMailID = CROEMailID;
    }

    public String getMobileNumber() {
        return MobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        MobileNumber = mobileNumber;
    }

    public String getShopName() {
        return ShopName;
    }

    public void setShopName(String shopName) {
        ShopName = shopName;
    }

    public String getShopAddress() {
        return ShopAddress;
    }

    public void setShopAddress(String shopAddress) {
        ShopAddress = shopAddress;
    }

    public String getCreateDate() {
        return CreateDate;
    }

    public void setCreateDate(String createDate) {
        CreateDate = createDate;
    }

    public String getCreateBy() {
        return CreateBy;
    }

    public void setCreateBy(String createBy) {
        CreateBy = createBy;
    }

    public String getLeadType() {
        return LeadType;
    }

    public void setLeadType(String leadType) {
        LeadType = leadType;
    }

    public String getUserName() {
        return UserName;
    }

    public void setUserName(String userName) {
        UserName = userName;
    }

    public String getPendingBill() {
        return PendingBill;
    }

    public void setPendingBill(String pendingBill) {
        PendingBill = pendingBill;
    }
}
