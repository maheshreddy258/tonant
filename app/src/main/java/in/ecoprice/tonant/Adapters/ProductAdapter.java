package in.ecoprice.tonant.Adapters;


import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.InputType;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.squareup.picasso.Picasso;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;

import in.ecoprice.tonant.Models.Cart;
import in.ecoprice.tonant.Models.ProductsList;
import in.ecoprice.tonant.Models.TodayRemindersResponse;
import in.ecoprice.tonant.PlaceOderActivity;
import in.ecoprice.tonant.R;
import in.ecoprice.tonant.Retrofit.CartPref;
import in.ecoprice.tonant.Retrofit.Responce.ProductListResponse;
import in.ecoprice.tonant.newChanges.ModelProducts;
import in.ecoprice.tonant.newChanges.NewModelCart;

import static com.android.volley.VolleyLog.TAG;

/**
 * Created by nani on 11/17/2017.
 */

public class ProductAdapter extends RecyclerView.Adapter<ProductAdapter.ViewHolder> {
    private List<ProductsList> listitems;
    private int abcdef;
    //   private int pos;
    private Context ctx;

    private List<ProductListResponse> productListResponseList;
    String check;
    private List<ModelProducts> AddToCart = new ArrayList<ModelProducts>();
    private String productW;
   /* public ProductAdapter(List<ProductsList> listitems, Context ctx) {
        this.listitems = listitems;
        this.ctx = ctx;

    }*/

    public ProductAdapter(Context ctx, List<ProductListResponse> productListResponseList) {
        this.ctx = ctx;
        this.productListResponseList = productListResponseList;
    }



  /* public ProductAdapter(List<ProductsList> listitems, Context ctx) {
        this.listitems = listitems;
        this.ctx = ctx;
    }*/
/*
    public ProductAdapter(Context applicationContext, List<ProductListResponse> productList) {
    }*/
/* public GralleryAdapter(List<Gallery> listitems, Context ctx) {
        this.listitems = listitems;
        this.ctx = ctx;
    }*/

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.new_product_list, parent, false);

        return new ViewHolder(v, ctx, productListResponseList);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, @SuppressLint("RecyclerView") final int position) {
        final ProductListResponse listItem = productListResponseList.get(position);
        //holder.ser_img.setImageBitmap(listItem.getBannerImage());

        //   pos = position;
        String URL = "http://tonantfarmers.com";
        Log.w(TAG, "Image URL :  " + URL + listItem.getBannerImage());
        //http://tonantfarmers.com/Product/kashmiri-chill-powder-500x500.jpg
    /*    Picasso.with(ctx)
                .load(URL+listItem.getBannerImage())
                .resize(70, 70)
                .into(holder.ser_img);
*/
        Glide.with(ctx)
                .load(URL + listItem.getBannerImage())
                .centerCrop()
                .into(holder.ser_img);


        holder.title.setText(listItem.getProductName());
        holder.coast.getEditText().setText("" + listItem.getSalePrice());
        // holder.coast.getEditText().requestFocus();

    }


    @Override
    public int getItemCount() {
        return productListResponseList.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    public class ViewHolder extends RecyclerView.ViewHolder /*implements View.OnClickListener*/ {

        private Button Add;
        private ImageView ser_img;
        private TextView title;
        private TextInputLayout coast;
        private ImageButton Plus, Less, Img100GmsLess, Img200GmsLess, Img500GmsLess, Img1kgsLess, Img2kgsLess, Img5kgsLess;
        private TextView CountText, update, delete;
        private RelativeLayout selectedIndicator100gm, selectedIndicator200gm, selectedIndicator500gm, selectedIndicator1kg, selectedIndicator2kg, selectedIndicator5kg;
        private FrameLayout Frame100gm, Frame200gm, Frame300gm, Frame1kg, Frame2kg, Frame5kg;
        private TextView Count100gm, Count200gm, Count500gm, Count1kg, Count2kg, Count5kg;
        private TextView g100, g200, g500, kg1, kg2, kg5;

        // private Button AddTOCart;
        private LinearLayout MainPro;
        Context ctx;
        // ArrayList<ProductsList> completeds = new ArrayList<ProductsList>();
        ArrayList<ProductListResponse> completeds2 = new ArrayList<ProductListResponse>();

        @SuppressLint("CutPasteId")
        public ViewHolder(final View View, final Context ctx, final List<ProductListResponse> completeds2) {
            super(View);

            /* View.setOnClickListener(this);*/
            this.completeds2 = (ArrayList<ProductListResponse>) completeds2;
            this.ctx = ctx;
            ser_img = (ImageView) View.findViewById(R.id.ImagePro);
            title = (TextView) View.findViewById(R.id.TitlePro);
            coast = (TextInputLayout) View.findViewById(R.id.RatePro);
            Add = (Button) View.findViewById(R.id.Add);
            //AddTOCart = (Button) View.findViewById(R.id.AddToCart);
            MainPro = (LinearLayout) View.findViewById(R.id.MainPro);
            Plus = (ImageButton) View.findViewById(R.id.Plus);
            Less = (ImageButton) View.findViewById(R.id.Less);
            update = (TextView) View.findViewById(R.id.update);
            delete = (TextView) View.findViewById(R.id.delete);

            Img100GmsLess = (ImageButton) View.findViewById(R.id.Img100GmsLess);
            Img200GmsLess = (ImageButton) View.findViewById(R.id.Img200GmsLess);
            Img500GmsLess = (ImageButton) View.findViewById(R.id.Img500GmsLess);
            Img1kgsLess = (ImageButton) View.findViewById(R.id.Img1kgsLess);
            Img2kgsLess = (ImageButton) View.findViewById(R.id.Img2kgsLess);
            Img5kgsLess = (ImageButton) View.findViewById(R.id.Img5kgsLess);
            CountText = (TextView) View.findViewById(R.id.CountText);

            Count100gm = (TextView) View.findViewById(R.id.Count100gm);
            Count200gm = (TextView) View.findViewById(R.id.Count200gm);
            Count500gm = (TextView) View.findViewById(R.id.Count500gm);
            Count1kg = (TextView) View.findViewById(R.id.Count1kg);
            Count2kg = (TextView) View.findViewById(R.id.Count2kg);
            Count5kg = (TextView) View.findViewById(R.id.Count5kg);

            selectedIndicator100gm = (RelativeLayout) View.findViewById(R.id.selectedIndicator100gm);
            selectedIndicator200gm = (RelativeLayout) View.findViewById(R.id.selectedIndicator200gm);
            selectedIndicator500gm = (RelativeLayout) View.findViewById(R.id.selectedIndicator500gm);
            selectedIndicator1kg = (RelativeLayout) View.findViewById(R.id.selectedIndicator1kg);
            selectedIndicator2kg = (RelativeLayout) View.findViewById(R.id.selectedIndicator2kg);
            selectedIndicator5kg = (RelativeLayout) View.findViewById(R.id.selectedIndicator5kg);

            Frame100gm = (FrameLayout) View.findViewById(R.id.Frame100gm);
            Frame200gm = (FrameLayout) View.findViewById(R.id.Frame200gm);
            Frame300gm = (FrameLayout) View.findViewById(R.id.Frame300gm);
            Frame1kg = (FrameLayout) View.findViewById(R.id.Frame1kg);
            Frame2kg = (FrameLayout) View.findViewById(R.id.Frame2kg);
            Frame5kg = (FrameLayout) View.findViewById(R.id.Frame5kg);

            g100 = (TextView) View.findViewById(R.id.g100);
            g200 = (TextView) View.findViewById(R.id.g200);
            g500 = (TextView) View.findViewById(R.id.g500);
            kg1 = (TextView) View.findViewById(R.id.kg1);
            kg2 = (TextView) View.findViewById(R.id.kg2);
            kg5 = (TextView) View.findViewById(R.id.kg5);


            final NewModelCart newModelCart = new NewModelCart();
            Frame100gm.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(android.view.View view) {

                    if (selectedIndicator100gm.getVisibility() == android.view.View.GONE) {

                        selectedIndicator100gm.setVisibility(android.view.View.VISIBLE);
                        selectedIndicator100gm.setEnabled(false);
                        selectedIndicator100gm.setClickable(false);
                        Count100gm.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(android.view.View view) {
                                int hol;
                                hol = Integer.parseInt(Count100gm.getText().toString());

                                hol++;

                                String abc = String.valueOf(hol);
                                Count100gm.setText(abc);
                            }
                        });
                    }
                }
            });
            Img100GmsLess.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(android.view.View view) {
                    int hol;
                    hol = Integer.parseInt(Count100gm.getText().toString());
                    hol--;
                    if (hol < 0) {
                        hol = 0;
                    }

                    if (hol == 0) {
                        selectedIndicator100gm.setVisibility(android.view.View.GONE);
                    }
                    String abc = String.valueOf(hol);
                    if(hol!=0){
                        Count100gm.setText(abc);
                    }

                }
            });
            Frame200gm.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(android.view.View view) {
                    if (selectedIndicator200gm.getVisibility() == android.view.View.GONE) {
                        selectedIndicator200gm.setVisibility(android.view.View.VISIBLE);
                        selectedIndicator200gm.setEnabled(false);
                        selectedIndicator200gm.setClickable(false);
                        Count200gm.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(android.view.View view) {
                                int hol;
                                hol = Integer.parseInt(Count200gm.getText().toString());
                                hol++;
                                String abc = String.valueOf(hol);
                                Count200gm.setText(abc);
                            }
                        });
                    }
                }
            });
            Img200GmsLess.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(android.view.View view) {
                    int hol;
                    hol = Integer.parseInt(Count200gm.getText().toString());
                    hol--;
                    if (hol < 0) {
                        hol = 0;
                    }
                    if (hol == 0) {
                        selectedIndicator200gm.setVisibility(android.view.View.GONE);
                    }
                    String abc = String.valueOf(hol);
                    if(hol!=0){
                        Count200gm.setText(abc);
                    }

                }
            });

            Frame300gm.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(android.view.View view) {
                    if (selectedIndicator500gm.getVisibility() == android.view.View.GONE) {
                        selectedIndicator500gm.setVisibility(android.view.View.VISIBLE);
                        selectedIndicator500gm.setEnabled(false);
                        selectedIndicator500gm.setClickable(false);
                        Count500gm.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(android.view.View view) {
                                int hol;
                                hol = Integer.parseInt(Count500gm.getText().toString());
                                hol++;
                                String abc = String.valueOf(hol);
                                Count500gm.setText(abc);
                            }
                        });
                    }
                }
            });
            Img500GmsLess.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(android.view.View view) {
                    int hol;
                    hol = Integer.parseInt(Count500gm.getText().toString());
                    hol--;
                    if (hol < 0) {
                        hol = 0;
                    }
                    if (hol == 0) {
                        selectedIndicator500gm.setVisibility(android.view.View.GONE);
                    }
                    String abc = String.valueOf(hol);
                    if(hol!=0){
                        Count500gm.setText(abc);
                    }

                }
            });
            Frame1kg.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(android.view.View view) {
                    if (selectedIndicator1kg.getVisibility() == android.view.View.GONE) {
                        selectedIndicator1kg.setVisibility(android.view.View.VISIBLE);
                        selectedIndicator1kg.setEnabled(false);
                        selectedIndicator1kg.setClickable(false);
                        Count1kg.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(android.view.View view) {
                                int hol;
                                hol = Integer.parseInt(Count1kg.getText().toString());
                                hol++;
                                String abc = String.valueOf(hol);
                                Count1kg.setText(abc);
                            }
                        });
                    }
                }
            });
            Img1kgsLess.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(android.view.View view) {
                    int hol;
                    hol = Integer.parseInt(Count1kg.getText().toString());
                    hol--;
                    if (hol < 0) {
                        hol = 0;
                    }
                    if (hol == 0) {
                        selectedIndicator1kg.setVisibility(android.view.View.GONE);
                    }
                    String abc = String.valueOf(hol);
                    if(hol!=0){
                        Count1kg.setText(abc);
                    }


                }
            });
            Frame2kg.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(android.view.View view) {
                    if (selectedIndicator2kg.getVisibility() == android.view.View.GONE) {
                        selectedIndicator2kg.setVisibility(android.view.View.VISIBLE);
                        selectedIndicator2kg.setEnabled(false);
                        selectedIndicator2kg.setClickable(false);
                        Count2kg.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(android.view.View view) {
                                int hol;
                                hol = Integer.parseInt(Count2kg.getText().toString());
                                hol++;
                                String abc = String.valueOf(hol);
                                Count2kg.setText(abc);
                            }
                        });
                    }
                }
            });
            Img2kgsLess.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(android.view.View view) {
                    int hol;
                    hol = Integer.parseInt(Count2kg.getText().toString());
                    hol--;
                    if (hol < 0) {
                        hol = 0;
                    }

                    if (hol == 0) {
                        selectedIndicator2kg.setVisibility(android.view.View.GONE);
                    }
                    String abc = String.valueOf(hol);
                    if(hol!=0){
                        Count2kg.setText(abc);
                    }

                }
            });
            Frame5kg.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(android.view.View view) {
                    if (selectedIndicator5kg.getVisibility() == android.view.View.GONE) {
                        selectedIndicator5kg.setVisibility(android.view.View.VISIBLE);
                        selectedIndicator5kg.setEnabled(false);
                        selectedIndicator5kg.setClickable(false);
                        Count5kg.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(android.view.View view) {
                                int hol;
                                hol = Integer.parseInt(Count5kg.getText().toString());
                                hol++;
                                String abc = String.valueOf(hol);
                                Count5kg.setText(abc);
                            }
                        });
                    }
                }
            });
            Img5kgsLess.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(android.view.View view) {
                    int hol;
                    hol = Integer.parseInt(Count5kg.getText().toString());
                    hol--;
                    if (hol < 0) {
                        hol = 0;
                    }
                    if (hol == 0) {
                        selectedIndicator5kg.setVisibility(android.view.View.GONE);
                    }
                    String abc = String.valueOf(hol);
                    if(hol!=0){
                        Count5kg.setText(abc);
                    }

                }
            });

            coast.getEditText().addTextChangedListener(new TextWatcher() {

                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void afterTextChanged(Editable editable) {


                    coast.setEnabled(true);
                    coast.setError(null);
                    check = editable.toString();
                    if (!TextUtils.isEmpty(check)) {
                        String text = check.toString();
                        if ("-".equals(text)) {
                            Log.w(TAG, "text :" + " null");
                        } else {
                            int pos = getAdapterPosition();
                            final ProductListResponse listItem = completeds2.get(pos);
                            double text12 = Double.parseDouble(check);
                            double min = listItem.getMinPrice();
                            if (text12 <= min) {
                                coast.setError("Price should be more than " + min + " per KG");
                                Add.setVisibility(View.GONE);
                            } else {
                                coast.setError(null);
                                Add.setVisibility(View.VISIBLE);
                            }
                        }

                    }
                }
            });


            Add.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int pos = getAdapterPosition();
                    final ProductListResponse listItem = completeds2.get(pos);
                    final double count100gm = Double.parseDouble(coast.getEditText().getText().toString()) / 1000;

                    if (Add.getText().toString().equals("Add")) {
                        if (selectedIndicator100gm.getVisibility() == View.VISIBLE) {
                            final ModelProducts modelProducts = new ModelProducts();
                            modelProducts.setProductName(listItem.getProductName());
                            modelProducts.setQuantity(Integer.parseInt(Count100gm.getText().toString()));
                            modelProducts.setProductWait(g100.getText().toString());
                            modelProducts.setUnitPrice(Double.parseDouble(new DecimalFormat("##.##").format(count100gm * 100)));
                            modelProducts.setId(listItem.getId());
                            modelProducts.setPrice(Double.parseDouble(new DecimalFormat("##.##").format(count100gm * 100)) * Integer.parseInt(Count100gm.getText().toString()));
                            AddToCart.add(modelProducts);
                            Log.w(TAG, "100gm :" + Count100gm.getText().toString() + " ProductWait " + g100.getText().toString());
                            newModelCart.setData(AddToCart);
                            //  notifyDataSetChanged();
                        } else {
                            Log.w(TAG, "100gm :" + "Not Added");

                        }
                        if (selectedIndicator200gm.getVisibility() == View.VISIBLE) {
                            final ModelProducts modelProducts = new ModelProducts();
                            modelProducts.setProductName(listItem.getProductName());
                            modelProducts.setQuantity(Integer.parseInt(Count200gm.getText().toString()));
                            modelProducts.setProductWait(g200.getText().toString());
                            modelProducts.setUnitPrice(Double.parseDouble(new DecimalFormat("##.##").format(count100gm * 200)));
                            modelProducts.setId(listItem.getId());
                            modelProducts.setPrice(Double.parseDouble(new DecimalFormat("##.##").format(count100gm * 200)) * Integer.parseInt(Count200gm.getText().toString()));
                            AddToCart.add(modelProducts);
                            Log.w(TAG, "200gm :" + Count200gm.getText().toString() + " ProductWait " + g200.getText().toString());
                            newModelCart.setData(AddToCart);
                            // notifyDataSetChanged();
                        } else {
                            Log.w(TAG, "200gm :" + "Not Added");

                        }
                        if (selectedIndicator500gm.getVisibility() == View.VISIBLE) {
                            final ModelProducts modelProducts = new ModelProducts();
                            modelProducts.setProductName(listItem.getProductName());
                            modelProducts.setQuantity(Integer.parseInt(Count500gm.getText().toString()));
                            modelProducts.setProductWait(g500.getText().toString());
                            modelProducts.setUnitPrice(Double.parseDouble(new DecimalFormat("##.##").format(count100gm * 500)));
                            modelProducts.setId(listItem.getId());
                            modelProducts.setPrice(Double.parseDouble(new DecimalFormat("##.##").format(count100gm * 500)) * Integer.parseInt(Count500gm.getText().toString()));
                            AddToCart.add(modelProducts);
                            Log.w(TAG, "500gm :" + Count500gm.getText().toString() + " ProductWait :" + g500.getText().toString());
                            newModelCart.setData(AddToCart);
                            ///notifyDataSetChanged();
                        } else {
                            Log.w(TAG, "500gm :" + "Not Added");

                        }
                        if (selectedIndicator1kg.getVisibility() == View.VISIBLE) {
                            final ModelProducts modelProducts = new ModelProducts();
                            modelProducts.setProductName(listItem.getProductName());
                            modelProducts.setQuantity(Integer.parseInt(Count1kg.getText().toString()));
                            modelProducts.setProductWait(kg1.getText().toString());
                            modelProducts.setUnitPrice(Double.parseDouble(new DecimalFormat("##.##").format(count100gm * 1000)));
                            modelProducts.setId(listItem.getId());
                            modelProducts.setPrice(Double.parseDouble(new DecimalFormat("##.##").format(count100gm * 1000)) * Integer.parseInt(Count1kg.getText().toString()));
                            AddToCart.add(modelProducts);
                            Log.w(TAG, "1kg :" + Count1kg.getText().toString() + " ProductWait " + kg1.getText().toString());
                            newModelCart.setData(AddToCart);
                            // notifyDataSetChanged();
                        } else {
                            Log.w(TAG, "1kg :" + "Not Added");

                        }
                        if (selectedIndicator2kg.getVisibility() == View.VISIBLE) {
                            final ModelProducts modelProducts = new ModelProducts();
                            modelProducts.setProductName(listItem.getProductName());
                            modelProducts.setQuantity(Integer.parseInt(Count2kg.getText().toString()));
                            modelProducts.setProductWait(kg2.getText().toString());
                            modelProducts.setUnitPrice(Double.parseDouble(new DecimalFormat("##.##").format(count100gm * 2000)));
                            modelProducts.setId(listItem.getId());
                            modelProducts.setPrice(Double.parseDouble(new DecimalFormat("##.##").format(count100gm * 2000)) * Integer.parseInt(Count2kg.getText().toString()));
                            AddToCart.add(modelProducts);
                            Log.w(TAG, "2kg :" + Count2kg.getText().toString() + " ProductWait " + kg2.getText().toString());
                            newModelCart.setData(AddToCart);
                            // notifyDataSetChanged();
                        } else {
                            Log.w(TAG, "2kg :" + "Not Added");

                        }
                        if (selectedIndicator5kg.getVisibility() == View.VISIBLE) {
                            final ModelProducts modelProducts = new ModelProducts();
                            modelProducts.setProductName(listItem.getProductName());
                            modelProducts.setQuantity(Integer.parseInt(Count5kg.getText().toString()));
                            modelProducts.setProductWait(kg5.getText().toString());
                            modelProducts.setUnitPrice(Double.parseDouble(new DecimalFormat("##.##").format(count100gm * 5000)));
                            modelProducts.setId(listItem.getId());
                            modelProducts.setPrice(Double.parseDouble(new DecimalFormat("##.##").format(count100gm * 5000)) * Integer.parseInt(Count5kg.getText().toString()));
                            AddToCart.add(modelProducts);
                            Log.w(TAG, "5kg :" + Count2kg.getText().toString() + " ProductWait " + kg5.getText().toString());
                            newModelCart.setData(AddToCart);
                            //notifyDataSetChanged();
                        } else {
                            Log.w(TAG, "5kg :" + "Not Added");

                        }

                        // notifyDataSetChanged();
                        Add.setVisibility(android.view.View.GONE);
                        update.setVisibility(android.view.View.VISIBLE);
                        delete.setVisibility(android.view.View.VISIBLE);
                        List<ModelProducts> data = NewModelCart.getData();
                        int total = 0;
                        for (int i = 0; i < data.size(); i++) {
                            total += data.get(i).getQuantity() * data.get(i).getUnitPrice();
                            Intent intent = new Intent("custom-message");
                            intent.putExtra("total", total + " Rs");
                            LocalBroadcastManager.getInstance(ctx).sendBroadcast(intent);

                        }
                    }
                }

            });


            update.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(android.view.View view) {
                    int pos = getAdapterPosition();
                    final ProductListResponse listItem = completeds2.get(pos);
                    Iterator<ModelProducts> iter = AddToCart.iterator();
                    while (iter.hasNext()) {
                        ModelProducts user = iter.next();
                        if (user.getProductName().equals(listItem.getProductName())) {
                            iter.remove();
                            Log.w(TAG, "Removed :" + user.getProductName());

                        }
                    }


                    final double count100gm = Double.parseDouble(coast.getEditText().getText().toString()) / 1000;
                    if (selectedIndicator100gm.getVisibility() == View.VISIBLE) {
                        final ModelProducts modelProducts = new ModelProducts();
                        modelProducts.setProductName(listItem.getProductName());
                        modelProducts.setQuantity(Integer.parseInt(Count100gm.getText().toString()));
                        modelProducts.setProductWait(g100.getText().toString());
                        modelProducts.setUnitPrice(Double.parseDouble(new DecimalFormat("##.##").format(count100gm * 100)));
                        modelProducts.setId(listItem.getId());
                        modelProducts.setPrice(Double.parseDouble(new DecimalFormat("##.##").format(count100gm * 100))
                                * Integer.parseInt(Count100gm.getText().toString()));
                        AddToCart.add(modelProducts);
                        Log.w(TAG, "100gm :" + Count100gm.getText().toString() + " ProductWait " + g100.getText().toString());
                        newModelCart.RemoveAllProducts(AddToCart);
                        newModelCart.setData(AddToCart);
                        //  notifyDataSetChanged();
                    } else {
                        Log.w(TAG, "100gm :" + "Not Added");

                    }
                    if (selectedIndicator200gm.getVisibility() == View.VISIBLE) {
                        final ModelProducts modelProducts = new ModelProducts();
                        modelProducts.setProductName(listItem.getProductName());
                        modelProducts.setQuantity(Integer.parseInt(Count200gm.getText().toString()));
                        modelProducts.setProductWait(g200.getText().toString());
                        modelProducts.setUnitPrice(Double.parseDouble(new DecimalFormat("##.##").format(count100gm * 200)));
                        modelProducts.setId(listItem.getId());
                        modelProducts.setPrice(Double.parseDouble(new DecimalFormat("##.##").format(count100gm * 200))
                                * Integer.parseInt(Count200gm.getText().toString()));
                        AddToCart.add(modelProducts);
                        Log.w(TAG, "200gm :" + Count200gm.getText().toString() + " ProductWait " + g200.getText().toString());
                        newModelCart.setData(AddToCart);

                        // notifyDataSetChanged();
                    } else {
                        Log.w(TAG, "200gm :" + "Not Added");

                    }
                    if (selectedIndicator500gm.getVisibility() == View.VISIBLE) {
                        final ModelProducts modelProducts = new ModelProducts();
                        modelProducts.setProductName(listItem.getProductName());
                        modelProducts.setQuantity(Integer.parseInt(Count500gm.getText().toString()));
                        modelProducts.setProductWait(g500.getText().toString());
                        modelProducts.setUnitPrice(Double.parseDouble(new DecimalFormat("##.##").format(count100gm * 500)));
                        modelProducts.setId(listItem.getId());
                        modelProducts.setPrice(Double.parseDouble(new DecimalFormat("##.##").format(count100gm * 500)) * Integer.parseInt(Count500gm.getText().toString()));
                        AddToCart.add(modelProducts);
                        Log.w(TAG, "500gm :" + Count500gm.getText().toString() + " ProductWait :" + g500.getText().toString());
                        newModelCart.setData(AddToCart);
                        ///notifyDataSetChanged();
                    } else {
                        Log.w(TAG, "500gm :" + "Not Added");

                    }
                    if (selectedIndicator1kg.getVisibility() == View.VISIBLE) {
                        final ModelProducts modelProducts = new ModelProducts();
                        modelProducts.setProductName(listItem.getProductName());
                        modelProducts.setQuantity(Integer.parseInt(Count1kg.getText().toString()));
                        modelProducts.setProductWait(kg1.getText().toString());
                        modelProducts.setUnitPrice(Double.parseDouble(new DecimalFormat("##.##").format(count100gm * 1000)));
                        modelProducts.setId(listItem.getId());
                        modelProducts.setPrice(Double.parseDouble(new DecimalFormat("##.##").format(count100gm * 1000))
                                                                                    * Integer.parseInt(Count1kg.getText().toString()));
                        AddToCart.add(modelProducts);
                        Log.w(TAG, "1kg :" + Count1kg.getText().toString() + " ProductWait " + kg1.getText().toString());
                        newModelCart.setData(AddToCart);
                        // notifyDataSetChanged();
                    } else {
                        Log.w(TAG, "1kg :" + "Not Added");

                    }
                    if (selectedIndicator2kg.getVisibility() == View.VISIBLE) {
                        final ModelProducts modelProducts = new ModelProducts();
                        modelProducts.setProductName(listItem.getProductName());
                        modelProducts.setQuantity(Integer.parseInt(Count2kg.getText().toString()));
                        modelProducts.setProductWait(kg2.getText().toString());
                        modelProducts.setUnitPrice(Double.parseDouble(new DecimalFormat("##.##").format(count100gm * 2000)));
                        modelProducts.setId(listItem.getId());
                        modelProducts.setPrice(Double.parseDouble(new DecimalFormat("##.##").format(count100gm * 2000))
                                                                                    * Integer.parseInt(Count2kg.getText().toString()));
                        AddToCart.add(modelProducts);
                        Log.w(TAG, "2kg :" + Count2kg.getText().toString() + " ProductWait " + kg2.getText().toString());
                        newModelCart.setData(AddToCart);
                        // notifyDataSetChanged();
                    } else {
                        Log.w(TAG, "2kg :" + "Not Added");

                    }
                    if (selectedIndicator5kg.getVisibility() == View.VISIBLE) {
                        final ModelProducts modelProducts = new ModelProducts();
                        modelProducts.setProductName(listItem.getProductName());
                        modelProducts.setQuantity(Integer.parseInt(Count5kg.getText().toString()));
                        modelProducts.setProductWait(kg5.getText().toString());
                        modelProducts.setUnitPrice(Double.parseDouble(new DecimalFormat("##.##").format(count100gm * 5000)));
                        modelProducts.setId(listItem.getId());
                        modelProducts.setPrice(Double.parseDouble(new DecimalFormat("##.##").format(count100gm * 5000))
                                                                                    * Integer.parseInt(Count5kg.getText().toString()));
                        AddToCart.add(modelProducts);
                        Log.w(TAG, "5kg :" + Count2kg.getText().toString() + " ProductWait " + kg5.getText().toString());
                        newModelCart.setData(AddToCart);
                        //notifyDataSetChanged();
                    } else {
                        Log.w(TAG, "5kg :" + "Not Added");

                    }
                    List<ModelProducts> data = NewModelCart.getData();
                    Log.e("data",""+data);
                    int total = 0;
                    if(data!=null && data.size()>0){
                        for (int i = 0; i < data.size(); i++) {
                            Log.d("TAG", "at " + i + " quantity = " + data.get(i).getUnitPrice());
                            total += data.get(i).getQuantity() * data.get(i).getUnitPrice();
                            Log.e("total",""+total);
                            Intent intent = new Intent("custom-message");
                            intent.putExtra("total", total + " Rs");
                            LocalBroadcastManager.getInstance(ctx).sendBroadcast(intent);

                        }
                    }else{
                        Intent intent = new Intent("custom-message");
                        intent.putExtra("total",  "0 Rs");
                        LocalBroadcastManager.getInstance(ctx).sendBroadcast(intent);
                    }

                    Toast.makeText(ctx, " Updated ", Toast.LENGTH_SHORT).show();
                    Add.setVisibility(android.view.View.GONE);
                    update.setVisibility(android.view.View.VISIBLE);
                    delete.setVisibility(android.view.View.VISIBLE);


                }
            });

            delete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(android.view.View view) {
                    Add.setVisibility(android.view.View.VISIBLE);
                    delete.setVisibility(android.view.View.GONE);
                    update.setVisibility(android.view.View.GONE);
                    int pos = getAdapterPosition();
                    final ProductListResponse listItem = completeds2.get(pos);
                    final double count100gm = Double.parseDouble(coast.getEditText().getText().toString()) / 1000;
                    Iterator<ModelProducts> iter = AddToCart.iterator();
                    while (iter.hasNext()) {
                        ModelProducts user = iter.next();
                        if (user.getProductName().equals(listItem.getProductName())) {
                            iter.remove();
                            Log.w(TAG, "Removed :" + user.getProductName());
                            selectedIndicator100gm.setVisibility(android.view.View.GONE);
                            selectedIndicator200gm.setVisibility(android.view.View.GONE);
                            selectedIndicator500gm.setVisibility(android.view.View.GONE);
                            selectedIndicator1kg.setVisibility(android.view.View.GONE);
                            selectedIndicator2kg.setVisibility(android.view.View.GONE);
                            selectedIndicator5kg.setVisibility(android.view.View.GONE);


                            List<ModelProducts> data = NewModelCart.getData();
                            int total = 0;
                            for (int i = 0; i < data.size(); i++) {
                                Log.d("TAG Quantity", "at " + i + " quantity = " + data.get(i).getUnitPrice());
                                total += data.get(i).getQuantity() * data.get(i).getUnitPrice();

                                Intent intent = new Intent("custom-message");
                                intent.putExtra("total", "0 Rs");
                                if (i == 0) {
                                    intent.putExtra("total", "0 Rs");
                                } else {
                                    intent.putExtra("total", total + " Rs");
                                }
                                LocalBroadcastManager.getInstance(ctx).sendBroadcast(intent);

                            }


                        }
                    }
                    Log.w(TAG, "Removed dummy:" + listItem.getProductName());
                    List<ModelProducts> data = NewModelCart.getData();
                    int total = 0;
                    for (int i = 0; i < data.size(); i++) {
                        total += data.get(i).getQuantity() * data.get(i).getUnitPrice();
                        Intent intent = new Intent("custom-message");
                        intent.putExtra("total", total + " Rs");
                        LocalBroadcastManager.getInstance(ctx).sendBroadcast(intent);

                    }
                }
            });


            /*CountText.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(android.view.View view) {
                    InputMethodManager im = (InputMethodManager) ctx.getSystemService(Context.INPUT_METHOD_SERVICE);
                    im.showSoftInput(CountText, 0);
                }*/
            /* });*/


        }

        /*@Override
        public void onClick(View v) {
            int position = getAdapterPosition();
            Products posts = this.completeds.get(position);
           // Toast.makeText(ctx, "Selected Item : " + position, Toast.LENGTH_SHORT).show();
            MainPro.setBackgroundResource(R.drawable.selector_row);



            *//* Intent intent = new Intent(this.ctx, VideoViewActivity.class);
            intent.putExtra("link", posts.getLink().replaceAll(" ", "%20"));
            intent.putExtra("name", posts.getName());*//**//*
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            this.ctx.startActivity(intent);*//*


        }*/

    }


}




 /*holder.Less.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int hol;
                hol = Integer.parseInt(holder.CountText.getText().toString());
                hol--;
                if(hol<0){
                    hol=0;
                }
                String abc = String.valueOf(hol);
                holder.CountText.setText(abc);
                abcdef = hol;
                Log.w(TAG, String.valueOf(newModelCart.getCartSize()));
               // if(newModelCart.checkProductInCart(modelProducts)){
                    Log.w(TAG,"Qntity Decressed  =>"+"Product Name :"+listItem.getProductName()+" Qntity :"+hol);
                    modelProducts.setProductName(listitems.get(position).getProductName());
                    modelProducts.setQuantity(hol);
                    modelProducts.setProductWait(productW);
                    modelProducts.setPrice(listitems.get(position).getSalePrice());
                    modelProducts.setId(listitems.get(position).getId());
                    AddToCart.add(modelProducts);
               // }
                notifyDataSetChanged();

            }
        });
        holder.Plus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int hol;
                 hol = Integer.parseInt(holder.CountText.getText().toString());
                hol++;
                String abc = String.valueOf(hol);
                holder.CountText.setText(abc);
                modelProducts.setProductName(listItem.getProductName());
                modelProducts.setQuantity(hol);
                modelProducts.setProductWait(productW);
                modelProducts.setPrice(listItem.getSalePrice());
                modelProducts.setId(listItem.getId());
                AddToCart.add(modelProducts);
                notifyDataSetChanged();

            }

        });*/


        /*holder.Add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (holder.selectedIndicator100gm.getVisibility() == View.VISIBLE){
                    modelProducts.setProductName(listItem.getProductName());
                    modelProducts.setQuantity(Integer.parseInt(holder.Count100gm.getText().toString()));
                    modelProducts.setProductWait("100gm");
                    modelProducts.setPrice(listItem.getSalePrice());
                    modelProducts.setId(listItem.getId());
                    AddToCart.add(modelProducts);
                    Log.w(TAG,"100gm :"+holder.Count100gm.getText().toString()+" ProductWait "+"100gm");
                    newModelCart.setData(AddToCart);
                    notifyDataSetChanged();

                    }else if (holder.selectedIndicator200gm.getVisibility() == View.VISIBLE){
                    modelProducts.setProductName(listItem.getProductName());
                    modelProducts.setQuantity(Integer.parseInt(holder.Count200gm.getText().toString()));
                    modelProducts.setProductWait("200gm");
                    modelProducts.setPrice(listItem.getSalePrice());
                    modelProducts.setId(listItem.getId());
                    AddToCart.add(modelProducts);
                    Log.w(TAG,"200gm :"+holder.Count200gm.getText().toString()+" ProductWait "+"200gm");
                    newModelCart.setData(AddToCart);
                    notifyDataSetChanged();

                }
                if (holder.selectedIndicator500gm.getVisibility() == View.VISIBLE){
                    modelProducts.setProductName(listItem.getProductName());
                    modelProducts.setQuantity(Integer.parseInt(holder.Count500gm.getText().toString()));
                    modelProducts.setProductWait("500gm");
                    modelProducts.setPrice(listItem.getSalePrice());
                    modelProducts.setId(listItem.getId());
                    AddToCart.add(modelProducts);
                    newModelCart.setData(AddToCart);
                    Log.w(TAG,"500gm :"+holder.Count500gm.getText().toString()+" ProductWait "+"500gm");
                    notifyDataSetChanged();

                }
                 if (holder.selectedIndicator1kg.getVisibility() == View.VISIBLE){
                    modelProducts.setProductName(listItem.getProductName());
                    modelProducts.setQuantity(Integer.parseInt(holder.Count1kg.getText().toString()));
                    modelProducts.setProductWait("1kg");
                    modelProducts.setPrice(listItem.getSalePrice());
                    modelProducts.setId(listItem.getId());
                    AddToCart.add(modelProducts);
                    newModelCart.setData(AddToCart);
                    Log.w(TAG,"1kg :"+holder.Count1kg.getText().toString()+" ProductWait "+"1kg");
                    notifyDataSetChanged();

                }

                 if (holder.selectedIndicator2kg.getVisibility() == View.VISIBLE){
                    modelProducts.setProductName(listItem.getProductName());
                    modelProducts.setQuantity(Integer.parseInt(holder.Count2kg.getText().toString()));
                    modelProducts.setProductWait("2kg");
                    modelProducts.setPrice(listItem.getSalePrice());
                    modelProducts.setId(listItem.getId());
                    AddToCart.add(modelProducts);
                    newModelCart.setData(AddToCart);
                    Log.w(TAG,"2kg :"+holder.Count2kg.getText().toString()+" ProductWait "+"2kg");
                    notifyDataSetChanged();

                }

                 if (holder.selectedIndicator5kg.getVisibility() == View.VISIBLE){
                    modelProducts.setProductName(listItem.getProductName());
                    modelProducts.setQuantity(Integer.parseInt(holder.Count5kg.getText().toString()));
                    modelProducts.setProductWait("5kg");
                    modelProducts.setPrice(listItem.getSalePrice());
                    modelProducts.setId(listItem.getId());
                    AddToCart.add(modelProducts);
                    newModelCart.setData(AddToCart);
                    Log.w(TAG,"5kg :"+holder.Count2kg.getText().toString()+" ProductWait "+"5kg");
                    notifyDataSetChanged();

                }else {
                    Log.w(TAG,"-->No Products are There<--");

                }





            }
        });*/
