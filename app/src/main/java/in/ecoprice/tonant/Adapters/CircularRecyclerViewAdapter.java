package in.ecoprice.tonant.Adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.squareup.picasso.Picasso;

import java.util.List;
import de.hdodenhof.circleimageview.CircleImageView;
import in.ecoprice.tonant.GlideApp;
import in.ecoprice.tonant.Models.ProductsList;
import in.ecoprice.tonant.R;
import in.ecoprice.tonant.Retrofit.Responce.ProductListResponse;

import static com.android.volley.VolleyLog.TAG;


public class CircularRecyclerViewAdapter extends RecyclerView.Adapter<CircularRecyclerViewAdapter.CircularViewHolder>{


    private List<ProductListResponse> listitems;
    private Context ctx;

    public CircularRecyclerViewAdapter(List<ProductListResponse> listitems, Context ctx) {
        this.listitems = listitems;
        this.ctx = ctx;
    }

    @NonNull
    @Override
    public CircularViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.cicrcularproducts, viewGroup, false);

        return new CircularViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull CircularViewHolder circularViewHolder, int i) {


        String URL = "http://tonantfarmers.com";
        Log.w(TAG, "Image URL :  " + URL + listitems.get(i).getBannerImage());



     /*   Picasso.with(ctx)
                .load(URL + listitems.get(i).getBannerImage())
                .into(circularViewHolder.image);*/

        GlideApp.with(ctx)
                .load(URL + listitems.get(i).getBannerImage())
                .centerCrop()
                .apply(new RequestOptions().override(900, 900))
                .into(circularViewHolder.image);

        circularViewHolder.name.setText(listitems.get(i).getProductName());
    }

    @Override
    public int getItemCount() {
        return listitems.size();
    }

    public class CircularViewHolder extends RecyclerView.ViewHolder{


        ImageView image;
        TextView name;

        public CircularViewHolder(@NonNull View itemView) {
            super(itemView);

             image =itemView.findViewById(R.id.imageviewhome2);
            name = itemView.findViewById(R.id.text);
        }
    }
}
