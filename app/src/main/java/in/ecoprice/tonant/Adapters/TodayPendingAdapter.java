package in.ecoprice.tonant.Adapters;


import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import in.ecoprice.tonant.Models.PendingAmounts;
import in.ecoprice.tonant.Models.PendingVisitsFilter;
import in.ecoprice.tonant.R;
import in.ecoprice.tonant.Visits.PendingAmountRecActivity;

public class TodayPendingAdapter extends RecyclerView.Adapter<TodayPendingAdapter.ViewHolder> /*implements Filterable*/ {
     public List<PendingAmounts> listitems;
     private String TAG = "TodayPendingAdapter";
    private Context ctx;
   // PendingVisitsFilter filter;
    public TodayPendingAdapter(List<PendingAmounts> listitems, Context ctx) {
        this.listitems = listitems;
        this.ctx = ctx;

    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.today_visits, parent, false);

        return new ViewHolder(v, ctx, listitems);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {
        final PendingAmounts listItem = listitems.get(position);
        holder.ShopName.setText(listItem.getShopName());
        holder.Username.setText(listItem.getUserName());
        holder.Total.setVisibility(View.GONE);
        holder.NetPay.setVisibility(View.GONE);
        holder.Pending.setText("Pending ₹"+listItem.getPendingBill());
        holder.TotalQntity.setVisibility(View.GONE);
        holder.TodayDate.setText("Lead type : "+listItem.getLeadType());
        Date c = Calendar.getInstance().getTime();
        SimpleDateFormat df = new SimpleDateFormat("dd.MMM.yyyy");
        String formattedDate = df.format(c);

        if ((listItem.getCreateDate()).equals("null")){
            Log.w(TAG,listItem.getCreateBy()+" is Null");
        }else {
            Log.w(TAG,listItem.getCreateBy()+" is Something");
            if (formattedDate.equals(parseTime(listItem.getCreateDate()))) {
                holder.Card.setVisibility(View.VISIBLE);
            } else {
                holder.Card.setVisibility(View.GONE);
            }
        }






    }

    @Override
    public int getItemCount() {
        return listitems.size();
    }
    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }
    /*@Override
    public Filter getFilter() {
        if(filter==null)
        {
         //   filter=new TodayPendingAdapter(listitems,this);
        }

        return filter;
    }*/


    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private TextView ShopName,Username,Total,NetPay,Pending,TotalQntity,TodayDate;
        private LinearLayout Card,layoutOne,layoutTwo;
        Context ctx;
        ArrayList<PendingAmounts> completeds = new ArrayList<PendingAmounts>();

        @SuppressLint("CutPasteId")
        public ViewHolder(View View, final Context ctx, List<PendingAmounts> completeds) {
            super(View);

            View.setOnClickListener(this);
            this.completeds = (ArrayList<PendingAmounts>) completeds;
            this.ctx = ctx;
            ShopName = (TextView) View.findViewById(R.id.ShopName);
            Username = (TextView) View.findViewById(R.id.Username);
            Total = (TextView) View.findViewById(R.id.Total);
            Pending = (TextView) View.findViewById(R.id.Pending);
            NetPay = (TextView) View.findViewById(R.id.NetPay);
            TotalQntity = (TextView) View.findViewById(R.id.TotalQntity);
            TodayDate = (TextView) View.findViewById(R.id.TodayDate);
            Card = (LinearLayout) View.findViewById(R.id.Card);

            layoutOne = View.findViewById(R.id.LayoutOne);
            layoutTwo = View.findViewById(R.id.LayoutTwo);

            layoutOne.setVisibility(android.view.View.GONE);
            layoutTwo.setVisibility(android.view.View.GONE);

            }

        @Override
        public void onClick(final View view) {
            int position = getAdapterPosition();
            PendingAmounts posts = this.completeds.get(position);
            Intent intent = new Intent(ctx, PendingAmountRecActivity.class);
            intent.putExtra("ShopId", posts.getMobileNumber());
            intent.putExtra("ShopName", posts.getShopName());
            intent.putExtra("ShopUserName", posts.getMobileNumber());
            intent.putExtra("PendingAmount", posts.getPendingBill());
            intent.putExtra("MobileNumber", listitems.get(position).getUserName());
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            this.ctx.startActivity(intent);
        }




        }

    private String parseTime(String date){
        SimpleDateFormat format = new SimpleDateFormat("yyyy-dd-MM HH:mm:ss");
        try {
            Date date1 = format.parse(date.replace("T"," "));
            String d= new SimpleDateFormat("dd.MM.yyyy").format(date1);
            return d;
        }catch (Exception e){
            e.printStackTrace();
        }
        return "";
    }
}


