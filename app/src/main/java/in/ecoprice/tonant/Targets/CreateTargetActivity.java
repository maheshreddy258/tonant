package in.ecoprice.tonant.Targets;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Handler;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.volley.Cache;
import com.android.volley.NetworkResponse;
import com.android.volley.ParseError;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.GsonBuilder;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import butterknife.internal.Utils;
import in.ecoprice.tonant.Adapters.ProductAdapter;
import in.ecoprice.tonant.Adapters.ProductBusinessAdapter;
import in.ecoprice.tonant.AddShopActivity;
import in.ecoprice.tonant.MainActivity;
import in.ecoprice.tonant.Models.ProductsList;
import in.ecoprice.tonant.MyTargetsActivity;
import in.ecoprice.tonant.PendingAmountActivity;
import in.ecoprice.tonant.R;
import in.ecoprice.tonant.ReportsFromToActivity;
import in.ecoprice.tonant.Retrofit.Responce.AddShopResp;
import in.ecoprice.tonant.Retrofit.Responce.Targets;
import in.ecoprice.tonant.Retrofit.SharedPref;
import in.ecoprice.tonant.Retrofit.network.ApiService;
import in.ecoprice.tonant.Retrofit.network.RetrofitBuilder;
import in.ecoprice.tonant.Visits.PendingAmountRecActivity;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CreateTargetActivity extends AppCompatActivity {

    private static final String TAG = "CreateTarget";
    TextInputLayout TargetType,BusinessType,TargetDuration,NoNewShopkeepers,NoOldShopkeepers;
    Button CreateTarget,CreateOverall,CreateParticular;
    ApiService service;
    Call<Targets> call;
    TextInputLayout fromDate,toDate;
    private int mYear, mMonth, mDay, mHour, mMinute;
    LinearLayout VisitsLayout,BussinessLayout,SelectDateLayout,Overall,Particular;
    private ProductBusinessAdapter adapter;
    private RecyclerView Gal_Rec;
    private ProgressBar progressBar;
    private List<ProductsList> listItems;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_target);
        service = RetrofitBuilder.createService(ApiService.class);

        TargetType = (TextInputLayout) findViewById(R.id.TargetType);
        BusinessType = (TextInputLayout) findViewById(R.id.BusinessType);
        TargetDuration = (TextInputLayout) findViewById(R.id.TargetDuration);
        CreateTarget = (Button) findViewById(R.id.BtnTargetVisit);
        CreateOverall = (Button) findViewById(R.id.CreateOverall);
        CreateParticular = (Button) findViewById(R.id.CreateParticular);
        VisitsLayout = (LinearLayout) findViewById(R.id.VisitsLayout);
        BussinessLayout = (LinearLayout) findViewById(R.id.BussinessLayout);
        SelectDateLayout = (LinearLayout) findViewById(R.id.SelectDateLayout);
        Particular = (LinearLayout) findViewById(R.id.Particular);
        Overall = (LinearLayout) findViewById(R.id.Overall);
        fromDate = (TextInputLayout) findViewById(R.id.fromDate);
        NoOldShopkeepers = (TextInputLayout) findViewById(R.id.NoOldShopkeepers);
        NoNewShopkeepers = (TextInputLayout) findViewById(R.id.NoNewShopkeepers);
        toDate = (TextInputLayout) findViewById(R.id.toDate);
        TargetType.requestFocus();
        TargetType.setFocusable(true);
        TargetType.requestFocusFromTouch();
        TargetType.setFocusableInTouchMode(true);


        progressBar = (ProgressBar)  findViewById(R.id.Prog_Assigned);
        Gal_Rec = (RecyclerView) findViewById(R.id.Rec_Gallery);
        Gal_Rec.setHasFixedSize(true);
        Gal_Rec.setNestedScrollingEnabled(false);
        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(getApplicationContext(), 1);
        Gal_Rec.setLayoutManager(mLayoutManager);
        listItems = new ArrayList<>();

        final String[] text = {"Visits","Bussiness"};
        android.app.AlertDialog.Builder   builder = new android.app.AlertDialog.Builder(CreateTargetActivity.this, android.app.AlertDialog.THEME_DEVICE_DEFAULT_LIGHT);
        builder.setItems(text, new DialogInterface.OnClickListener() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onClick(DialogInterface dialog, int which) {
                ListView lw = ((android.app.AlertDialog)dialog).getListView();
                Object checkedItem = lw.getAdapter().getItem(which);
                TargetType.getEditText().setText(checkedItem.toString());
                if(checkedItem.toString().equals("Visits")){
                    VisitsLayout.setVisibility(View.VISIBLE);
                    BussinessLayout.setVisibility(View.GONE);
                }else if (checkedItem.toString().equals("Bussiness")){
                    VisitsLayout.setVisibility(View.GONE);
                    BussinessLayout.setVisibility(View.VISIBLE);
                }else{
                    VisitsLayout.setVisibility(View.GONE);
                    BussinessLayout.setVisibility(View.GONE);
                }

            }
        });
        builder.show();


        TargetType.getEditText().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final String[] text = {"Visits","Bussiness"};
                android.app.AlertDialog.Builder   builder = new android.app.AlertDialog.Builder(view.getRootView().getContext(), android.app.AlertDialog.THEME_DEVICE_DEFAULT_LIGHT);
                builder.setItems(text, new DialogInterface.OnClickListener() {
                    @SuppressLint("SetTextI18n")
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        ListView lw = ((android.app.AlertDialog)dialog).getListView();
                        Object checkedItem = lw.getAdapter().getItem(which);
                        TargetType.getEditText().setText(checkedItem.toString());
                        if(checkedItem.toString().equals("Visits")){
                            VisitsLayout.setVisibility(View.VISIBLE);
                            BussinessLayout.setVisibility(View.GONE);
                        }else if (checkedItem.toString().equals("Bussiness")){
                            VisitsLayout.setVisibility(View.GONE);
                            BussinessLayout.setVisibility(View.VISIBLE);

                            final String[] text = {"Particular","Overall"};
                            android.app.AlertDialog.Builder   builder = new android.app.AlertDialog.Builder(CreateTargetActivity.this, android.app.AlertDialog.THEME_DEVICE_DEFAULT_LIGHT);
                            builder.setItems(text, new DialogInterface.OnClickListener() {
                                @SuppressLint("SetTextI18n")
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    ListView lw = ((android.app.AlertDialog)dialog).getListView();
                                    Object checkedItem = lw.getAdapter().getItem(which);

                                    BusinessType.getEditText().setText(checkedItem.toString());
                                    if(checkedItem.toString().equals("Particular")){
                                        Particular.setVisibility(View.VISIBLE);
                                        Overall.setVisibility(View.GONE);
                                    }else if (checkedItem.toString().equals("Overall")){
                                        Particular.setVisibility(View.GONE);
                                        Overall.setVisibility(View.VISIBLE);
                                    }else{
                                        Particular.setVisibility(View.GONE);
                                        Overall.setVisibility(View.GONE);
                                    }

                                }
                            });
                            builder.show();

                        }else{
                            VisitsLayout.setVisibility(View.GONE);
                            BussinessLayout.setVisibility(View.GONE);
                        }

                    }
                });
                builder.show();
                //ShowBuilder(builder);
            }
        });
        //builder.show();
        BusinessType.getEditText().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final String[] text = {"Particular","Overall"};
                android.app.AlertDialog.Builder   builder = new android.app.AlertDialog.Builder(CreateTargetActivity.this, android.app.AlertDialog.THEME_DEVICE_DEFAULT_LIGHT);
                builder.setItems(text, new DialogInterface.OnClickListener() {
                    @SuppressLint("SetTextI18n")
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        ListView lw = ((android.app.AlertDialog)dialog).getListView();
                        Object checkedItem = lw.getAdapter().getItem(which);
                        BusinessType.getEditText().setText(checkedItem.toString());
                        if(checkedItem.toString().equals("Particular")){
                            Particular.setVisibility(View.VISIBLE);
                            Overall.setVisibility(View.GONE);
                        }else if (checkedItem.toString().equals("Overall")){
                            Particular.setVisibility(View.GONE);
                            Overall.setVisibility(View.VISIBLE);
                        }else{
                            Particular.setVisibility(View.GONE);
                            Overall.setVisibility(View.GONE);
                        }

                    }
                });
                builder.show();
            }
        });
        fromDate.getEditText().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Calendar c = Calendar.getInstance();
                mYear = c.get(Calendar.YEAR);
                mMonth = c.get(Calendar.MONTH);
                mDay = c.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog datePickerDialog = new DatePickerDialog(CreateTargetActivity.this,
                        new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {
                                @SuppressLint("SimpleDateFormat")
                                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
                                c.set(year, monthOfYear , dayOfMonth);
                                String dateString = sdf.format(c.getTime());
                                fromDate.getEditText().setText(dateString);

                            }
                        }, mYear, mMonth, mDay);
                datePickerDialog.show();
                datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
            }
        });

        toDate.getEditText().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Calendar c = Calendar.getInstance();
                mYear = c.get(Calendar.YEAR);
                mMonth = c.get(Calendar.MONTH);
                mDay = c.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog datePickerDialog = new DatePickerDialog(CreateTargetActivity.this,
                        new DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {
                                @SuppressLint("SimpleDateFormat")
                                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
                                c.set(year, monthOfYear , dayOfMonth);
                                String dateString = sdf.format(c.getTime());
                                toDate.getEditText().setText(dateString);
                                }
                        }, mYear, mMonth, mDay);
                datePickerDialog.show();
                datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
            }
        });

        TargetDuration.getEditText().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final String[] text = {"Daily","Weekly","Select Date"};
                android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(view.getRootView().getContext(), android.app.AlertDialog.THEME_DEVICE_DEFAULT_LIGHT);
                builder.setItems(text, new DialogInterface.OnClickListener() {
                    @SuppressLint("SetTextI18n")
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        ListView lw = ((android.app.AlertDialog)dialog).getListView();
                        Object checkedItem = lw.getAdapter().getItem(which);
                        TargetDuration.getEditText().setText(checkedItem.toString());
                        if(checkedItem.toString().equals("Select Date")){
                            SelectDateLayout.setVisibility(View.VISIBLE);
                        }else{
                            SelectDateLayout.setVisibility(View.GONE);

                        }

                    }
                });
                builder.show();
            }
        });

        String addedby = SharedPref.getStr("userName");
        Log.w(TAG,"addedby :"+addedby);
        CreateTarget.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("NewApi")
            @Override
            public void onClick(View view) {
                if(validate(TargetDuration)){
                    Log.w(TAG,"From Date Null");
                }else if(validate(NoNewShopkeepers)){
                    Log.w(TAG,"Done with validation");
                }else if(validate(NoOldShopkeepers)){
                    Log.w(TAG,"Done with validation");
                }else {
                    Date c = Calendar.getInstance().getTime();
                    SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
                    String dateString = format.format(c.getTime());

                    Calendar cal12 = Calendar.getInstance();
                    cal12.add(Calendar.HOUR_OF_DAY, 10);
                    DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
                    System.out.println(dateFormat.format(cal12.getTime()));
                    String formattedDate102 = dateFormat.format(cal12.getTime());

                    Calendar cal=Calendar.getInstance();
                    SimpleDateFormat month_date = new SimpleDateFormat("MMM");
                    String month_name = month_date.format(cal.getTime());

                    String addedby = SharedPref.getStr("userName");
                    Log.w(TAG,"addedby :"+addedby);

                    SendCreateTarget(
                        TargetType.getEditText().getText().toString(),
                        TargetDuration.getEditText().getText().toString(),
                        NoNewShopkeepers.getEditText().getText().toString(),
                        NoOldShopkeepers.getEditText().getText().toString(),
                        addedby,
                        dateString,
                        formattedDate102,
                        month_name,
                        addedby

                        );
                }


                }
        });




        loadRecyclerViewData();




    }


    private void loadRecyclerViewData() {
        listItems.clear();
        progressBar.setVisibility(View.VISIBLE);
        Gal_Rec.setVisibility(View.GONE);
        String URL_DATA = "http://apiservices.tonantfarmers.com/api/AllProducts";
        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(URL_DATA, new com.android.volley.Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                progressBar.setVisibility(View.GONE);
                Gal_Rec.setVisibility(View.VISIBLE);
                try {
                    if (response.length() > 0) {
                        for (int i = 0; i < response.length(); i++) {
                            JSONObject obj = response.getJSONObject(i);
                            ProductsList item = new ProductsList(
                                    obj.getInt("Id"),
                                    obj.getInt("MRP"),
                                    obj.getInt("StockQuantity"),
                                    obj.getInt("ProductStatus"),
                                    obj.getDouble("SalePrice"),
                                    obj.getDouble("DiscountPercentage"),
                                    obj.getDouble("minPrice"),
                                    obj.getString("ProductName"),
                                    obj.getString("Description"),
                                    obj.getString("StockUnit"),
                                    obj.getString("BannerImage"),
                                    obj.getString("SortDesc"),
                                    obj.getString("BrandName"),
                                    obj.getString("DeliveryCharges"),
                                    obj.getString("ShortName")
                            );
                            listItems.add(item);
                            adapter = new ProductBusinessAdapter(listItems, getApplicationContext());
                            Gal_Rec.setAdapter(adapter);

                        }
                        adapter = new ProductBusinessAdapter(listItems, getApplicationContext());
                        Gal_Rec.setAdapter(adapter);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressBar.setVisibility(View.GONE);
                Gal_Rec.setVisibility(View.GONE);


            }
        });
        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        requestQueue.add(jsonArrayRequest);






    }


    private void SendCreateTarget(String TargetType, String TargetDuration, String NoNewShopkeepers,
                                  String NoOldShopkeepers, String addedby, String dateString, String formattedDate102,
                                  String month_name, String addedby1) {

        final ProgressDialog progressDialog= new ProgressDialog(CreateTargetActivity.this);
        progressDialog.setMessage("Loading...");
        progressDialog.show();
        progressDialog.setCancelable(false);

        String targetDays = "";
        if (TargetDuration.equals("Daily")){
           targetDays = "1";
        }else if (TargetDuration.equals("Weekly")){
           targetDays = "7";
        }
        call = service.CreateTargetVisits(TargetType,targetDays,NoNewShopkeepers,NoOldShopkeepers,addedby,"Sales",month_name,addedby);
        call.enqueue(new Callback<Targets>() {
            @Override
            public void onResponse(Call<Targets> call, Response<Targets> response) {
              //  Log.w("printed gson => ",new GsonBuilder().setPrettyPrinting().create().toJson(response));
                Log.w(TAG, "onResponse: " + response);
                response.body();
                boolean Loginstatus = response.body().isRecordStatus();
                Log.w(TAG, "Loginstatus: " + Loginstatus);
                if (Loginstatus) {
                    Toast.makeText(CreateTargetActivity.this,response.body().getErrorMessage(),Toast.LENGTH_LONG).show();
                    Log.w(TAG, "status: " + Loginstatus);
                    Log.w(TAG, "ErrorMSG: " + response.body().getErrorMessage());

                    int TIME = 2000;
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            progressDialog.dismiss();
                            Intent intent = new Intent(CreateTargetActivity.this, MyTargetsActivity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP |
                                    Intent.FLAG_ACTIVITY_NEW_TASK);
                            intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                            startActivity(intent);
                            finish();
                        }
                    }, TIME);
                } else {
                    progressDialog.dismiss();
                    Toast.makeText(CreateTargetActivity.this,response.body().getErrorMessage(),Toast.LENGTH_SHORT).show();
                    Log.w(TAG, "status: in false" + Loginstatus);
                    Log.w(TAG, "ErrorMSG in false: " + response.body().getErrorMessage());

                }

            }

            @Override
            public void onFailure(Call<Targets> call, Throwable t) {
                progressDialog.dismiss();
                Toast.makeText(CreateTargetActivity.this,""+t.getMessage(),Toast.LENGTH_LONG).show();
                Log.w(TAG, "onFailure: " + t.getMessage());
                // showForm();
            }
        });

    }
    public boolean validate(TextInputLayout textInputEditText) {
        boolean valid = false;
        textInputEditText.setEnabled(true);
        textInputEditText.setError(null);
        String  textInputEditText123 = textInputEditText.getEditText().getText().toString();
        if (textInputEditText123.length() == 0) {
            textInputEditText.setError("Required");
            textInputEditText.getEditText().requestFocus();
            valid = true;
        } else {
            textInputEditText.setError(null);
        }

        return valid;
    }

}
