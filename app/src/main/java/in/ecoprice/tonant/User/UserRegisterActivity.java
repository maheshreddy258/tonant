package in.ecoprice.tonant.User;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.transition.TransitionManager;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.basgeekball.awesomevalidation.AwesomeValidation;
import com.basgeekball.awesomevalidation.ValidationStyle;

import butterknife.ButterKnife;
import in.ecoprice.tonant.ForgotPasswordActivity;
import in.ecoprice.tonant.MainActivity;
import in.ecoprice.tonant.R;
import in.ecoprice.tonant.Retrofit.Responce.AccessToken;
import in.ecoprice.tonant.Retrofit.Responce.UserRegRes;
import in.ecoprice.tonant.Retrofit.SharedPref;
import in.ecoprice.tonant.Retrofit.network.ApiService;
import in.ecoprice.tonant.Retrofit.network.RetrofitBuilder;
import in.ecoprice.tonant.VisitActivity;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UserRegisterActivity extends AppCompatActivity {



    private static final String TAG = "Login";
    TextInputLayout tilEmail,tilPhone,tillName,tilPassword;
    LinearLayout container;
    // LinearLayout formContainer;
    ProgressBar loader;
    FrameLayout Button;
    ApiService service;
    AwesomeValidation validator;
    Call<UserRegRes> call;
    TextView forgotPass, ErrorMsg;
    TextView forgot_pass,login_now;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_register);


        ButterKnife.bind(this);
        service = RetrofitBuilder.createService(ApiService.class);
        validator = new AwesomeValidation(ValidationStyle.TEXT_INPUT_LAYOUT);

        tilEmail = (TextInputLayout) findViewById(R.id.til_Email);
        tillName = (TextInputLayout) findViewById(R.id.til_Name);
        tilPhone = (TextInputLayout) findViewById(R.id.til_Phone);
        tilPassword = (TextInputLayout) findViewById(R.id.till_password);
        container = (LinearLayout) findViewById(R.id.container);
        //formContainer = (LinearLayout) findViewById(R.id.form_container);
        loader = (ProgressBar) findViewById(R.id.loader);
        Button = (FrameLayout) findViewById(R.id.btn_login);
        forgotPass = (TextView) findViewById(R.id.forgotPass);
        ErrorMsg = (TextView) findViewById(R.id.ErrorMsg);
        forgot_pass = (TextView) findViewById(R.id.forgot_pass);
        login_now = (TextView) findViewById(R.id.login_now);


        Button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                InputMethodManager inputManager = (InputMethodManager) getApplicationContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                inputManager.hideSoftInputFromWindow(UserRegisterActivity.this.getCurrentFocus().getWindowToken(), InputMethodManager.RESULT_UNCHANGED_SHOWN);

                String name = tillName.getEditText().getText().toString();
                String phone = tilPhone.getEditText().getText().toString();
                String email = tilEmail.getEditText().getText().toString();
                String pasword = tilPassword.getEditText().getText().toString();

                if (!validate(tillName ) || !validate(tilPhone ) || !validate(tilEmail) || !validate(tilPassword )) {
                    Log.w(TAG, "Response :" + " Username Empty");
                } else {
                    login(name,phone,email, pasword);
                    ErrorMsg.setVisibility(View.GONE);
                }

            }
        });



        login_now.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(UserRegisterActivity.this, UserLoginActivity.class));
                finish();
            }
        });

        forgot_pass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(UserRegisterActivity.this, ForgotPasswordActivity.class));
                finish();
            }
        });
    }


    private void login(final String name, final String phone,final String email, final String pasword) {
        tilEmail.setError(null);
        tilPassword.setError(null);
        showLoading();
       // Log.w(TAG, "Login form" + "Username :" + username + "Password :" + pasword);
        call = service.UserRegistration(name, email,phone,pasword);
        call.enqueue(new Callback<UserRegRes>() {
            @Override
            public void onResponse(Call<UserRegRes> call, Response<UserRegRes> response) {
                Log.w(TAG, "onResponse: " + response);
                response.body();
                boolean Loginstatus = response.body().isStatus();
                Log.w(TAG, "Loginstatus: " + Loginstatus);
                if (Loginstatus) {
                    Toast.makeText(UserRegisterActivity.this,""+response.body().getDisplaymessage()+" Please Login",Toast.LENGTH_LONG).show();
                    int SPLASH_TIME_OUT = 2000;
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            startActivity(new Intent(UserRegisterActivity.this, UserLoginActivity.class));
                            finish();
                        }
                    }, SPLASH_TIME_OUT);

                    response.raw().toString();



                } else {
                    showForm();
                    tilEmail.setError("Invalid Username");
                    tilPassword.setError("Invalid Password");
                        /*ErrorMsg.setVisibility(View.VISIBLE);
                        int SPLASH_TIME_OUT = 50000;
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                                ErrorMsg.setVisibility(View.GONE);
                            }
                        }, SPLASH_TIME_OUT);*/
                }

            }

            @Override
            public void onFailure(Call<UserRegRes> call, Throwable t) {
                Log.w(TAG, "onFailure: " + t.getMessage());
                showForm();
            }
        });


    }

    public boolean validate(TextInputLayout textInputEditText) {
        boolean valid = true;
        textInputEditText.setEnabled(true);
        textInputEditText.setError(null);
        String textInputEditText123 = textInputEditText.getEditText().getText().toString();
        if (textInputEditText123.length() == 0) {
            textInputEditText.setError("Required");
            valid = false;
        } else {
            textInputEditText.setError(null);
        }

        return valid;
    }


    @SuppressLint("NewApi")
    private void showLoading() {
        tilEmail.setError(null);
        tilPassword.setError(null);
        TransitionManager.beginDelayedTransition(Button);
        loader.setVisibility(View.VISIBLE);
        Button.setVisibility(View.GONE);
    }

    @SuppressLint("NewApi")
    private void showForm() {
        TransitionManager.beginDelayedTransition(Button);
        loader.setVisibility(View.GONE);
        Button.setVisibility(View.VISIBLE);
    }
}
