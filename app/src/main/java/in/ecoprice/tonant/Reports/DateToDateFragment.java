package in.ecoprice.tonant.Reports;


import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import in.ecoprice.tonant.R;
import in.ecoprice.tonant.ReportsFromToActivity;
import in.ecoprice.tonant.ReportsTodayActivity;

/**
 * A simple {@link Fragment} subclass.
 */
public class DateToDateFragment extends Fragment {


    public DateToDateFragment() {
        // Required empty public constructor
    }
    private static final String TAG = "reportsFromDateToDate";
    TextInputLayout fromDate,toDate;
    private int mYear, mMonth, mDay, mHour, mMinute;
    private Button BtnGetReports;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_date_to_date, container, false);





        fromDate = (TextInputLayout) view.findViewById(R.id.fromDate);
        toDate = (TextInputLayout) view.findViewById(R.id.toDate);
        BtnGetReports = (Button) view.findViewById(R.id.BtnGetReports);

        fromDate.getEditText().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Get Current Date
                final Calendar c = Calendar.getInstance();
                mYear = c.get(Calendar.YEAR);
                mMonth = c.get(Calendar.MONTH);
                mDay = c.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog datePickerDialog = new DatePickerDialog(getContext(),
                        new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {

                                @SuppressLint("SimpleDateFormat")
                                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                                c.set(year, monthOfYear , dayOfMonth);
                                String dateString = sdf.format(c.getTime());
                                fromDate.getEditText().setText(dateString);

                            }
                        }, mYear, mMonth, mDay);
                datePickerDialog.show();
            }
        });

        toDate.getEditText().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Get Current Date
                final Calendar c = Calendar.getInstance();
                mYear = c.get(Calendar.YEAR);
                mMonth = c.get(Calendar.MONTH);
                mDay = c.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog datePickerDialog = new DatePickerDialog(getContext(),
                        new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {

                                @SuppressLint("SimpleDateFormat")
                                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                                c.set(year, monthOfYear , dayOfMonth);
                                String dateString = sdf.format(c.getTime());
                                toDate.getEditText().setText(dateString);

                            }
                        }, mYear, mMonth, mDay);
                datePickerDialog.show();
            }
        });


        BtnGetReports.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(validate(fromDate)){
                    Log.w(TAG,"From Date Null");
                }else if(validate(toDate)){
                    Log.w(TAG,"to Date Null");
                }else {
                    String fromDatestr = fromDate.getEditText().getText().toString();
                    String toDatestr = toDate.getEditText().getText().toString();
                    Log.w(TAG,"fromDatestr =>: "+fromDatestr);
                    Log.w(TAG,"toDatestr =>: "+toDatestr);
                    Intent intent = new Intent(getContext(),ReportsTodayActivity.class);
                    intent.putExtra("fromDatestr",fromDatestr);
                    intent.putExtra("toDatestr",toDatestr);
                    startActivity(intent);
                    getActivity().finish();
                }
            }
        });


        return view;
    }

    public boolean validate(TextInputLayout textInputEditText) {
        boolean valid = false;
        textInputEditText.setEnabled(true);
        textInputEditText.setError(null);
        String  textInputEditText123 = textInputEditText.getEditText().getText().toString();
        if (textInputEditText123.length() == 0) {
            textInputEditText.setError("Required");
            textInputEditText.getEditText().requestFocus();
            valid = true;
        } else {
            textInputEditText.setError(null);
        }

        return valid;
    }

}
