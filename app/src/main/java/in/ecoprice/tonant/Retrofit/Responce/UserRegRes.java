package in.ecoprice.tonant.Retrofit.Responce;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UserRegRes {

    // Designation

    @SerializedName("Displaymessage")
    @Expose
    private String Displaymessage;

    @SerializedName("ApiStatus")
    @Expose
    private boolean ApiStatus;

    @SerializedName("Status")
    @Expose
    private boolean Status;

    public String getDisplaymessage() {
        return Displaymessage;
    }

    public void setDisplaymessage(String displaymessage) {
        Displaymessage = displaymessage;
    }

    public boolean isStatus() {
        return Status;
    }

    public void setStatus(boolean status) {
        Status = status;
    }

    public boolean isApiStatus() {
        return ApiStatus;
    }

    public void setApiStatus(boolean apiStatus) {
        ApiStatus = apiStatus;
    }
}

