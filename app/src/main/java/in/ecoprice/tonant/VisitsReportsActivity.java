package in.ecoprice.tonant;

import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import in.ecoprice.tonant.Reports.OrdersPagerAdapter;
import in.ecoprice.tonant.VisitsReports.VisitReportsPagerAdapter;

public class VisitsReportsActivity extends AppCompatActivity {


    private Toolbar mToolbar;
    private ViewPager mViewPager;
    private VisitReportsPagerAdapter mSectionsPagerAdapter;
    private TabLayout mTabLayout;
    private InternetChecker internetChecker;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_visits_reports);
        mToolbar = (Toolbar) findViewById(R.id.main_page_toolbar);
        setSupportActionBar(mToolbar);
        setTitle("Reports");
        //getSupportActionBar().setTitle(getIntent().getStringExtra("name"));
        mToolbar.setTitleTextColor(0xFFFFFFFF);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.arrowleft);
        internetChecker = new InternetChecker(this);

        mViewPager = (ViewPager) findViewById(R.id.container);
        mSectionsPagerAdapter = new VisitReportsPagerAdapter(getSupportFragmentManager());

        mViewPager.setAdapter(mSectionsPagerAdapter);

        mTabLayout = (TabLayout) findViewById(R.id.main_tabs);
        mTabLayout.setupWithViewPager(mViewPager);


    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }

    }
}
