package in.ecoprice.tonant.Models;

public class MyTargets {
    private String Id,TargetTitle,TargetType,Retailers,TargetDays,SalesKgs;

    public MyTargets(String id, String targetTitle, String targetType, String retailers, String targetDays, String salesKgs) {
        Id = id;
        TargetTitle = targetTitle;
        TargetType = targetType;
        Retailers = retailers;
        TargetDays = targetDays;
        SalesKgs = salesKgs;
    }

    public String getId() {
        return Id;
    }

    public void setId(String id) {
        Id = id;
    }

    public String getTargetTitle() {
        return TargetTitle;
    }

    public void setTargetTitle(String targetTitle) {
        TargetTitle = targetTitle;
    }

    public String getTargetType() {
        return TargetType;
    }

    public void setTargetType(String targetType) {
        TargetType = targetType;
    }

    public String getRetailers() {
        return Retailers;
    }

    public void setRetailers(String retailers) {
        Retailers = retailers;
    }

    public String getTargetDays() {
        return TargetDays;
    }

    public void setTargetDays(String targetDays) {
        TargetDays = targetDays;
    }

    public String getSalesKgs() {
        return SalesKgs;
    }

    public void setSalesKgs(String salesKgs) {
        SalesKgs = salesKgs;
    }
}
