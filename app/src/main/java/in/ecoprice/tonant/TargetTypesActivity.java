package in.ecoprice.tonant;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import in.ecoprice.tonant.Adapters.NewTodayReportsAdapter;
import in.ecoprice.tonant.Adapters.TodayReportsAdapter;
import in.ecoprice.tonant.Models.MyTargets;
import in.ecoprice.tonant.Models.NewReportsModel;
import in.ecoprice.tonant.Models.TodayReports;
import in.ecoprice.tonant.Retrofit.SharedPref;

public class TargetTypesActivity extends AppCompatActivity {


    private LinearLayout Layout;
    private static final String TAG = "TargetTypesActivity";
    private RecyclerView Gal_Rec;
    private RecyclerView.Adapter adapter;
    private List<NewReportsModel> listItems;
    public static List<NewReportsModel> listItems1;
    private ProgressBar progressBar;
    private TextView
            TargetTitle,
            OutletType,
            OutletCoun,
            TargetDays;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_target_types);


        Gal_Rec = (RecyclerView) findViewById(R.id.Rec_Gallery);
        Gal_Rec.setHasFixedSize(true);
        Gal_Rec.setNestedScrollingEnabled(false);
        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(getApplicationContext(), 1);
        Gal_Rec.setLayoutManager(mLayoutManager);
        listItems = new ArrayList<>();
        listItems1 = new ArrayList<>();
        progressBar = (ProgressBar) findViewById(R.id.Prog_Assigned);
        Layout = (LinearLayout) findViewById(R.id.Layout);
        TargetTitle = (TextView) findViewById(R.id.TargetTitle);
        OutletType = (TextView) findViewById(R.id.OutletType);
        OutletCoun = (TextView) findViewById(R.id.OutletCount);
        TargetDays = (TextView) findViewById(R.id.TargetDays);


      String type =  getIntent().getStringExtra("name");
      loadRecyclerViewData(type);


    }


    private void loadRecyclerViewData(final String type) {
        progressBar.setVisibility(View.VISIBLE);
        Gal_Rec.setVisibility(View.GONE);
        Layout.setVisibility(View.GONE);
        String username = SharedPref.getStr("userName");
        String URL_DATA = "http://apiservices.tonantfarmers.com/api/AllTargtesList?Status=true";
        StringRequest stringRequest = new StringRequest(Request.Method.GET,
                URL_DATA,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String s) {
                    Gal_Rec.setVisibility(View.VISIBLE);
                        Layout.setVisibility(View.VISIBLE);
                       progressBar.setVisibility(View.GONE);

                        try {
                            JSONObject jsonObject = new JSONObject(s);
                            JSONObject array = jsonObject.getJSONObject(type);
                             String mTargetTitle = array.getString("TargetTitle");
                             String mOutletType = array.getString("OutletType");
                             String mOutletCount = array.getString("OutletCount");
                             String mTargetDays = array.getString("TargetDays");

                                    TargetTitle.setText(mTargetTitle);
                                    OutletType.setText(mOutletType);
                                    OutletCoun.setText(mOutletCount);
                                    TargetDays.setText(mTargetDays);


                             JSONArray array123 = array.getJSONArray("ProductTarget");
                            for(int i=0; i<array123.length(); i++ ){
                                JSONObject obj =array123.getJSONObject(i);
                                NewReportsModel item = new NewReportsModel(
                                        obj.getString("productName"),
                                        obj.getString("SaleinKg"),
                                        obj.getString("SoldInKg")

                                );
                                listItems = listItems1;
                                listItems.add(item);
                                adapter = new NewTodayReportsAdapter(listItems , getApplicationContext());
                                Gal_Rec.setAdapter(adapter);
                            }
                            adapter = new NewTodayReportsAdapter(listItems , getApplicationContext());
                            Gal_Rec.setAdapter(adapter);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                /*  progressDialog.dismiss();*/
                Gal_Rec.setVisibility(View.GONE);
                Layout.setVisibility(View.GONE);
                progressBar.setVisibility(View.GONE);
                VolleyLog.e("Error: ", error.getMessage());
                Toast.makeText(getApplicationContext(),"Try Again ",Toast.LENGTH_LONG).show();

            }
        });
        RequestQueue requestQueue = Volley.newRequestQueue(TargetTypesActivity.this);
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                2000*5,
                /*DefaultRetryPolicy.DEFAULT_MAX_RETRIES*/ 3,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }

}
