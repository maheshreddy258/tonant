package in.ecoprice.tonant;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

public class SplashScreenActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);
        ImageView mImage = (ImageView) findViewById(R.id.imgsplash);
        Animation mAnim = AnimationUtils.loadAnimation(this, R.anim.sp);
        mImage.setAnimation(mAnim);

        int SPLASH_TIME_OUT = 2000;
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                startActivity(new Intent(SplashScreenActivity.this, Home2Activity.class));
                finish();
            }
        }, SPLASH_TIME_OUT);
    }
}
