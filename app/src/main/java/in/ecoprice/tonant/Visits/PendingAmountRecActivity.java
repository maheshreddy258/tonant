package in.ecoprice.tonant.Visits;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Handler;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.JsonObject;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import in.ecoprice.tonant.Adapters.MyTargetsAdapter;
import in.ecoprice.tonant.AddShopActivity;
import in.ecoprice.tonant.MainActivity;
import in.ecoprice.tonant.Models.MyTargets;
import in.ecoprice.tonant.MyTargetsActivity;
import in.ecoprice.tonant.PendingAmountActivity;
import in.ecoprice.tonant.R;
import in.ecoprice.tonant.Retrofit.Responce.PendingRes;
import in.ecoprice.tonant.Retrofit.Responce.Targets;
import in.ecoprice.tonant.Retrofit.SharedPref;
import in.ecoprice.tonant.Retrofit.network.ApiService;
import in.ecoprice.tonant.Retrofit.network.RetrofitBuilder;
import in.ecoprice.tonant.ShopListActivity;
import in.ecoprice.tonant.Targets.CreateTargetActivity;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PendingAmountRecActivity extends AppCompatActivity {


    private static final String TAG = "PendingAmount";
    TextView PendingAmount,MobileNumber,Shopname;
    TextInputLayout paid_amount;
    Button send;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pending_amount_rec);


        send = (Button) findViewById(R.id.send);
        PendingAmount = (TextView) findViewById(R.id.PendingAmount);
        MobileNumber = (TextView) findViewById(R.id.MobileNumber);
        Shopname = (TextView) findViewById(R.id.Shopname);
        paid_amount = (TextInputLayout) findViewById(R.id.paid_amount);

        final String shopId = getIntent().getStringExtra("ShopId");
       final String shopUsername = getIntent().getStringExtra("ShopUserName");
       final String pendint = getIntent().getStringExtra("PendingAmount");
       final String username = getIntent().getStringExtra("MobileNumber");
       final String ShopName = getIntent().getStringExtra("ShopName");

        PendingAmount.setText(""+pendint);
        MobileNumber.setText(""+username);
        Shopname.setText(""+ShopName);



       send.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View view) {
               if (!validate(paid_amount)) {
                   Log.w(TAG, "Response :" + " Username Empty");

               }else {
                   /*SendPending(shopId,
                           PendingAmount.getText().toString(),
                           MobileNumber.getText().toString(),
                           Shopname.getText().toString(),
                           paid_amount.getEditText().getText().toString(),
                           SharedPref.getStr("userName")
                   );*/



                   final double pending = Double.parseDouble(PendingAmount.getText().toString());
                   final double paid = Double.parseDouble(paid_amount.getEditText().getText().toString());

                   /*final double thenPending = pending - paid;
                   Log.w(TAG, "Order ID :" +shopId);
                   Log.w(TAG, "Pending :" +pending);
                   Log.w(TAG, "Paid :" +paid);
                   Log.w(TAG, "Then Pending :" +thenPending);*/

                  getPendingAmount(shopId,paid);

               }




           }
       });


    }

    private void getPendingAmount(String id,double paid) {
        final ProgressDialog progressDialog= new ProgressDialog(PendingAmountRecActivity.this);
        progressDialog.setMessage("Loading...");
        progressDialog.show();
        progressDialog.setCancelable(false);
        progressDialog.setCanceledOnTouchOutside(false);
        String URL = "http://apiservices.tonantfarmers.com/api/PendingAmountCollections?Id="+id+"&PendingAmount="+paid;
        Log.w(TAG, "Url  :" +URL);
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET,
                URL, null, new com.android.volley.Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.d(TAG, response.toString());

                try {

                    boolean name = response.getBoolean("ReturnStatus");
                    if (name){
                        String msg = response.getString("ErrorMessage");

                        Toast.makeText(getApplicationContext(),
                                msg, Toast.LENGTH_LONG).show();
                        int TIME = 2000;
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                progressDialog.dismiss();
                                Intent intent = new Intent(PendingAmountRecActivity.this, PendingAmountActivity.class);
                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP |
                                        Intent.FLAG_ACTIVITY_NEW_TASK);
                                intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                                startActivity(intent);
                                finish();
                            }
                        }, TIME);
                    }else{
                        Toast.makeText(getApplicationContext(),
                               "Try Again",

                                Toast.LENGTH_LONG).show();
                        progressDialog.dismiss();
                    }



                } catch (JSONException e) {
                    e.printStackTrace();
                    progressDialog.dismiss();
                    Toast.makeText(getApplicationContext(),
                            "Error: " + e.getMessage(),
                            Toast.LENGTH_LONG).show();
                }
            }
        }, new com.android.volley.Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                Toast.makeText(getApplicationContext(),
                        error.getMessage(), Toast.LENGTH_SHORT).show();

            }
        });
        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        requestQueue.add(jsonObjReq);
    }


    public boolean validate(TextInputLayout textInputEditText) {
        boolean valid = true;
        textInputEditText.setEnabled(true);
        textInputEditText.setError(null);
        String  textInputEditText123 = textInputEditText.getEditText().getText().toString();
        if (textInputEditText123.length() == 0) {
            textInputEditText.setError("Required");
            textInputEditText.getEditText().requestFocus();
            valid = false;
        } else {
            textInputEditText.setError(null);
        }

        return valid;
    }
}
