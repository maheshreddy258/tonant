package in.ecoprice.tonant.VisitsReports;


import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import in.ecoprice.tonant.Adapters.VisitsReportsAdapter;
import in.ecoprice.tonant.Models.VisitReports;
import in.ecoprice.tonant.R;
import in.ecoprice.tonant.Reports.OrdersPagerAdapter;
import in.ecoprice.tonant.Retrofit.SharedPref;

/**
 * A simple {@link Fragment} subclass.
 */
public class WeekFragment extends Fragment {


    public WeekFragment() {
        // Required empty public constructor
    }
    private static final String TAG = "ReportsTodayActivity";
    private RecyclerView Gal_Rec;
    private RecyclerView.Adapter adapter;
    private List<VisitReports> listItems;
    public static List<VisitReports> listItems1;
    private ProgressBar progressBar;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_new, container, false);

        Gal_Rec = (RecyclerView) view.findViewById(R.id.Rec_Gallery);
        Gal_Rec.setHasFixedSize(true);
        Gal_Rec.setNestedScrollingEnabled(false);
        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(getContext(), 1);
        Gal_Rec.setLayoutManager(mLayoutManager);
        listItems = new ArrayList<>();
        listItems1 = new ArrayList<>();
        progressBar = (ProgressBar) view.findViewById(R.id.Prog_Assigned);

        String type = getActivity().getIntent().getStringExtra("type");
        loadRecyclerViewData(type,getWeekStartDate()+" 00:00:00",getWeekEndDate()+" 23:00:00");
        return  view;
    }

    private void loadRecyclerViewData(String type,String fromDate,String formattedDate) {
        progressBar.setVisibility(View.VISIBLE);
        Gal_Rec.setVisibility(View.GONE);
        String username = SharedPref.getStr("userName");
        String URL_DATA = "http://apiservices.tonantfarmers.com/api/AdminVisitdReport?fromdate="+fromDate+"&todate="+formattedDate+"&OutletType="+type+"&userName="+username;
        String url = URL_DATA.replaceAll(" ", "%20");
        Log.w(TAG,"Url Reports replaceAll : "+url);
        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(url, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                Gal_Rec.setVisibility(View.VISIBLE);
                progressBar.setVisibility(View.GONE);
                try {
                    for(int i=0; i<response.length(); i++ ){
                        JSONObject obj =response.getJSONObject(i);
                        VisitReports item = new VisitReports(
                                obj.getString("SalesName"),
                                obj.getString("VisitNme"),
                                obj.getString("ShoPName"),
                                obj.getString("DateofVisit"),
                                obj.getString("Address"),
                                obj.getString("Phone"),
                                obj.getInt("Status")

                        );
                        listItems = listItems1;
                        listItems.add(item);
                    }
                    adapter = new VisitsReportsAdapter(listItems , getActivity());
                    Gal_Rec.setAdapter(adapter);

                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Gal_Rec.setVisibility(View.GONE);
                progressBar.setVisibility(View.GONE);
                VolleyLog.e("Error: ", error.getMessage());
                Toast.makeText(getActivity(),error.getMessage(),Toast.LENGTH_LONG).show();
            }
        });
        RequestQueue requestQueue = Volley.newRequestQueue(getContext().getApplicationContext());
        jsonArrayRequest.setRetryPolicy(new DefaultRetryPolicy(
                2000*5,
                /*DefaultRetryPolicy.DEFAULT_MAX_RETRIES*/ 3,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(jsonArrayRequest);
    }

    public static String getWeekStartDate() {
        String date = "";
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        Calendar calendar = Calendar.getInstance();
        while (calendar.get(Calendar.DAY_OF_WEEK) != Calendar.MONDAY) {
            calendar.add(Calendar.DATE, -1);
        }
        date = df.format(calendar.getTime());

        return date;
    }

    public static String getWeekEndDate() {
        String date = "";
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        Calendar calendar = Calendar.getInstance();
        while (calendar.get(Calendar.DAY_OF_WEEK) != Calendar.SUNDAY) {
            calendar.add(Calendar.DATE, 1);
        }
        //calendar.add(Calendar.DATE, -1);
        date = df.format(calendar.getTime());

        return date;
    }
}
