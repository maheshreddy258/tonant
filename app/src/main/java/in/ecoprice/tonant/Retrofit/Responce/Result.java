package in.ecoprice.tonant.Retrofit.Responce;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Result {

    @SerializedName("UpdateStatus")
    @Expose
    private boolean UpdateStatus;
    @SerializedName("ErrorMessage")
    @Expose
    private String ErrorMessage;

    public boolean isUpdateStatus() {
        return UpdateStatus;
    }

    public void setUpdateStatus(boolean updateStatus) {
        UpdateStatus = updateStatus;
    }

    public String getErrorMessage() {
        return ErrorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        ErrorMessage = errorMessage;
    }
}
