package in.ecoprice.tonant;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import in.ecoprice.tonant.Adapters.ShopSelectAdapter;
import in.ecoprice.tonant.Models.Cart;
import in.ecoprice.tonant.Models.SelectShopFilter;
import in.ecoprice.tonant.Models.Shoplist;
import in.ecoprice.tonant.Retrofit.SharedPref;
import in.ecoprice.tonant.dummy.ModelCart;

public class PlaceOderActivity extends AppCompatActivity {

    private static final String TAG = "PlaceOder";
    TextView abcd;
    List<Cart> data123 = new ArrayList<Cart>();
   // private double total = 0.0;

    private LinearLayout LayoutNotNUll,LayoutNUll;
    private RecyclerView Rec_Assigned;
    private ShopSelectAdapter adapter;
    private List<Shoplist> listItems;
    public static List<Shoplist> listItems1;
    private ProgressBar progressBar;
    SearchView sv;
    private Button ButtonNext;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_place_oder);
        abcd = (TextView) findViewById(R.id.Abcd);
        LayoutNotNUll = (LinearLayout) findViewById(R.id.LayoutNotNUll);
        LayoutNUll = (LinearLayout) findViewById(R.id.LayoutNull);
        ButtonNext = (Button) findViewById(R.id.ButtonNext);
        sv= (SearchView) findViewById(R.id.mSearch);

        String itemname = getIntent().getStringExtra("itemname");
        float price = getIntent().getFloatExtra("Price", 0);
        //int qnty = getIntent().getIntExtra("pos", 0);


        TableLayout stk = (TableLayout) findViewById(R.id.table_main);
        TableRow tbrow0 = new TableRow(this);
        TextView tv1 = new TextView(this);
        tv1.setText(" Product name");
        tv1.setTextSize(20);
        tv1.setTextColor(Color.BLACK);
        tv1.setBackgroundResource(R.drawable.row_border);
        tbrow0.addView(tv1);
        TextView tv2 = new TextView(this);
        tv2.setText(" Price ");
        tv2.setTextColor(Color.BLACK);
        tv2.setTextSize(20);
        tv2.setBackgroundResource(R.drawable.row_border);
        tbrow0.addView(tv2);
        TextView tv3 = new TextView(this);
        tv3.setText(" Qnty ");
        tv3.setTextColor(Color.BLACK);
        tv3.setTextSize(20);
        tv3.setBackgroundResource(R.drawable.row_border);
        tbrow0.addView(tv3);
       /* TextView tv4 = new TextView(this);
        tv4.setText(" Gm/Kg ");
        tv4.setTextColor(Color.BLACK);
        tv4.setTextSize(20);
        tv4.setBackgroundResource(R.drawable.row_border);
        tbrow0.addView(tv4);*/
        stk.addView(tbrow0);



        final String shopuserNmae = getIntent().getStringExtra("ShopUserName");
        final String ShopName = getIntent().getStringExtra("ShopName");
        Log.w(TAG,"PlaceOrder ==> "+"shopuserNmae :"+shopuserNmae+" ShopName :"+ShopName);
        if (shopuserNmae.equals("null")){
            LayoutNotNUll.setVisibility(View.VISIBLE);
            LayoutNUll.setVisibility(View.GONE);
        }else {
            LayoutNUll.setVisibility(View.VISIBLE);
            sv.setVisibility(View.GONE);
            LayoutNotNUll.setVisibility(View.GONE);
        }

        ButtonNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(PlaceOderActivity.this, CheckOutActivity.class);
                intent.putExtra("ShopUserName", shopuserNmae);
                intent.putExtra("ShopName", ShopName);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                 startActivity(intent);
            }
        });


        List<Cart> data = ModelCart.getData();
        System.out.println(data.size());
        for (int i = 0; i < data.size(); i++) {
            String text = data.get(i).getProductName() + "       " + data.get(i).getSalePrice() + "       " + data.get(i).getQnty();
            Log.w(TAG, text);
            //System.out.println(data.get(i).getId());
            removeDuplicates(data);
            init(data,i);
            }

        // Calculate total
        int total = 0;
        Log.d("TAG", "start total = " +total);
        for (int i = 0; i < data.size(); i++) {
            Log.d("TAG", "at "+i+ " quantity = " +data.get(i).getQnty());
            total += data.get(i).getQnty() * data.get(i).getSalePrice();
            Log.d("TAG", "at "+i+ " total = " +total);
            abcd.setText("Total = " +total);
        }


        Rec_Assigned = (RecyclerView) findViewById(R.id.Rec_Assigned);
        progressBar = (ProgressBar)  findViewById(R.id.Prog_Assigned);
        Rec_Assigned.setHasFixedSize(true);
        LinearLayoutManager layoutManager= new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        Rec_Assigned.setLayoutManager(layoutManager);
        Rec_Assigned.setNestedScrollingEnabled(false);
        Rec_Assigned.animate();
        listItems = new ArrayList<>();
        listItems1 = new ArrayList<>();



       /* final String shopuserNmae = getIntent().getStringExtra("ShopUserName");
        final String ShopName = getIntent().getStringExtra("ShopName");

      */





        //sv.setIconifiedByDefault(true);
        //sv.setFocusable(true);
        //sv.setIconified(false);
        //sv.requestFocusFromTouch();
        adapter =new ShopSelectAdapter(listItems , getApplicationContext());
        sv.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String query) {
                adapter.getFilter().filter(query);
                return false;
            }
        });
    }

    private void loadRecyclerViewData() {
        progressBar.setVisibility(View.VISIBLE);
        Rec_Assigned.setVisibility(View.GONE);
        SharedPref sharedPref = new SharedPref(getApplicationContext());
        String username = sharedPref.getStr("userName");
        String URL_DATA = "http://apiservices.tonantfarmers.com/api/MyLeads?UserName="+username;
        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(URL_DATA, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                progressBar.setVisibility(View.GONE);
                Rec_Assigned.setVisibility(View.VISIBLE);
                try {
                    if (response.length() > 0) {
                        for (int i = 0; i < response.length(); i++) {
                            JSONObject obj =response.getJSONObject(i);
                            Shoplist item = new Shoplist(
                                    obj.getString("Id"),
                                    obj.getString("First_Name"),
                                    obj.getString("Last_Name"),
                                    obj.getString("CROEMailID"),
                                    obj.getString("MobileNumber"),
                                    obj.getString("AadharNumber"),
                                    obj.getString("PANNumber"),
                                    obj.getString("ShopName"),
                                    obj.getString("ShopAddress"),
                                    obj.getString("CreateBy"),
                                    obj.getString("LeadType"),
                                    obj.getString("PIN_Code"),
                                    obj.getString("Latitude"),
                                    obj.getString("Longitude"),
                                    obj.getString("PendingBill")
                            );
                            listItems = listItems1;
                            listItems.add(item);
                        }
                        adapter = new ShopSelectAdapter(listItems , getApplicationContext());
                        Rec_Assigned.setAdapter(adapter);
                    }
                }catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressBar.setVisibility(View.GONE);
                Rec_Assigned.setVisibility(View.GONE);
                getErro();
                Toast.makeText(PlaceOderActivity.this,"Try Again :(",Toast.LENGTH_SHORT).show();

            }
        });
        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        requestQueue.add(jsonArrayRequest);

    }

    private void getErro() {
        Intent intent = new Intent(PlaceOderActivity.this,MainActivity.class);
        startActivity(intent);
        finish();
    }


    @SuppressLint("ResourceAsColor")
    private void UpdateTextView(String str) {
        //abcd.setText(text);
        LinearLayout MainLL = (LinearLayout) findViewById(R.id.main_layout);

        TextView text = new TextView(this);
            text.setText(str);
            text.setTextSize(18);

            text.setGravity(Gravity.LEFT);
            text.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
            MainLL.addView(text);

    }




    private void init(List<Cart> data123,int i) {


        TableLayout stk = (TableLayout) findViewById(R.id.table_main);

            TableRow tbrow = new TableRow(this);
           // tbrow.setBackgroundResource(R.drawable.row_border);
            TextView t1v = new TextView(this);
            t1v.setText(data123.get(i).getProductName());
            t1v.setTextColor(Color.GRAY);
            t1v.setGravity(Gravity.CENTER);
            t1v.setTextSize(18);
            t1v.setBackgroundResource(R.drawable.row_border);
            tbrow.addView(t1v);
            TextView t2v = new TextView(this);
            t2v.setText(""+data123.get(i).getSalePrice());
            t2v.setTextColor(Color.GRAY);
            t2v.setGravity(Gravity.CENTER);
            t2v.setTextSize(18);
            t2v.setBackgroundResource(R.drawable.row_border);
            tbrow.addView(t2v);
            TextView t3v = new TextView(this);
            t3v.setText("" + data123.get(i).getQnty());
            t3v.setTextColor(Color.GRAY);
            t3v.setGravity(Gravity.CENTER);
            t3v.setTextSize(18);
            t3v.setBackgroundResource(R.drawable.row_border);
            tbrow.addView(t3v);

       /* TextView t4v = new TextView(this);
        t4v.setText("" + data123.get(i).getQnty()+" gm");
        t4v.setTextColor(Color.GRAY);
        t4v.setGravity(Gravity.CENTER);
        t4v.setTextSize(18);
        t4v.setBackgroundResource(R.drawable.row_border);
        tbrow.addView(t4v);*/
        stk.addView(tbrow);
        }

        /*for (int i = 0; i < 25; i++) {
            TableRow tbrow = new TableRow(this);
            TextView t1v = new TextView(this);
            t1v.setText("" + i);
            t1v.setTextColor(Color.WHITE);
            t1v.setGravity(Gravity.CENTER);
            tbrow.addView(t1v);
            TextView t2v = new TextView(this);
            t2v.setText("Product " + i);
            t2v.setTextColor(Color.WHITE);
            t2v.setGravity(Gravity.CENTER);
            tbrow.addView(t2v);
            TextView t3v = new TextView(this);
            t3v.setText("Rs." + i);
            t3v.setTextColor(Color.WHITE);
            t3v.setGravity(Gravity.CENTER);
            tbrow.addView(t3v);
            TextView t4v = new TextView(this);
            t4v.setText("" + i * 15 / 32 * 10);
            t4v.setTextColor(Color.WHITE);
            t4v.setGravity(Gravity.CENTER);
            tbrow.addView(t4v);
            stk.addView(tbrow);

        }*/






    private void removeDuplicates(List<Cart> list) {
        int count = list.size();

        for (int i = 0; i < count; i++)
        {
            for (int j = i + 1; j < count; j++)
            {
                /*Log.w(TAG,"Product Hight NUmber I :"+ list.get(i).getQnty());
                Log.w(TAG,"Product Hight NUmber J :"+ list.get(j).getQnty());
               */ if (list.get(i).getQnty() < list.get(j).getQnty())
                {
                    list.get(i).setQnty(list.get(j).getQnty());
                    Log.w(TAG,"Product HIght NUmber :"+ list.get(j).getQnty());
                }

                if (list.get(i).getProductName().equals(list.get(j).getProductName()))
                {
                    list.remove(j--);
                    count--;

                }
            }
            }
    }

    @Override
    public void onBackPressed() {
        List<Cart> data = ModelCart.getData();
        data.clear();
      //  onBackPressed();
        super.onBackPressed();
    }

    @Override
    protected void onStart() {
        InputMethodManager inputManager = (InputMethodManager) getApplicationContext().getSystemService(Context.INPUT_METHOD_SERVICE);
        inputManager.hideSoftInputFromWindow(PlaceOderActivity.this.getCurrentFocus().getWindowToken(), InputMethodManager.RESULT_UNCHANGED_SHOWN);

        super.onStart();
    }
}


