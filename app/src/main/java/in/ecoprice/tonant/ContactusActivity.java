package in.ecoprice.tonant;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.widget.ImageView;
import android.widget.TextView;

public class ContactusActivity extends AppCompatActivity {


    CardView mCvcontact,mCvaddress;
    TextView mTvHead;
    ImageView mimgcon;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contactus);

        setTitle("Contact Us");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        mCvcontact = findViewById(R.id.cvcontact);
        mCvaddress = findViewById(R.id.cvaddress);
        mTvHead = findViewById(R.id.tvhead);
    }


    @Override
    public boolean onSupportNavigateUp() {

        onBackPressed();
        return true;
    }
}
