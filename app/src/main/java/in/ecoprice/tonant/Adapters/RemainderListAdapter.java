package in.ecoprice.tonant.Adapters;


import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import in.ecoprice.tonant.Models.RemainderList;
import in.ecoprice.tonant.R;
import in.ecoprice.tonant.UpdateRemainderActivity;

public class RemainderListAdapter extends RecyclerView.Adapter<RemainderListAdapter.ViewHolder>{
     public List<RemainderList> listitems;
    private Context ctx;
    public RemainderListAdapter(List<RemainderList> listitems, Context ctx) {
        this.listitems = listitems;
        this.ctx = ctx;

    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.remainder_item, parent, false);

        return new ViewHolder(v, ctx, listitems);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {
        final RemainderList listItem = listitems.get(position);
        holder.ShopName.setText(listItem.getShopKeeperName());
        holder.VisiteType.setText(listItem.getVisiteType());
        holder.Reason.setText(listItem.getReason());
        holder.IDRemainder.setText("ID :"+listItem.getId());
        holder.NextVisit.setText(parseTime(listItem.getDateofvisittobe()));
        Log.e("next visit........",""+listItem.getDateofvisittobe());
        Log.e("REASON........",""+listItem.getReason());

        }

    @Override
    public int getItemCount() {
        return listitems.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private TextView ShopName,VisiteType,Reason,NextVisit,IDRemainder;
        Context ctx;
        ArrayList<RemainderList> completeds = new ArrayList<RemainderList>();

        @SuppressLint("CutPasteId")
        public ViewHolder(View View, final Context ctx, List<RemainderList> completeds) {
            super(View);

            View.setOnClickListener(this);
            this.completeds = (ArrayList<RemainderList>) completeds;
            this.ctx = ctx;
            ShopName = (TextView) View.findViewById(R.id.ShopName);
            VisiteType = (TextView) View.findViewById(R.id.VisiteType);
            Reason = (TextView) View.findViewById(R.id.Reason);
            NextVisit = (TextView) View.findViewById(R.id.NextVisit);
            IDRemainder = (TextView) View.findViewById(R.id.IDRemainder);
           // CreatedDate = (TextView) View.findViewById(R.id.CreatedDate);


            }

        @Override
        public void onClick(View view) {
            int position = getAdapterPosition();
            RemainderList posts = this.completeds.get(position);
             Intent intent = new Intent(this.ctx, UpdateRemainderActivity.class);
            intent.putExtra("dateofvisittobe", posts.getDateofvisittobe());
            intent.putExtra("ShopKeeperNamename", posts.getShopKeeperName());
            intent.putExtra("reason", posts.getReason());
            intent.putExtra("VisiteType", posts.getVisiteType());
            intent.putExtra("Id", posts.getId());
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            this.ctx.startActivity(intent);
        }
    }

    private String parseTime(String date){
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        try {
            Date date1 = format.parse(date.replace("T"," "));
            String d= new SimpleDateFormat("dd.MM.yyyy").format(date1);
            return d;
        }catch (Exception e){
            e.printStackTrace();
        }
        return "";
    }
}


