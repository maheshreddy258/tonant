package in.ecoprice.tonant.Adapters;


import android.annotation.SuppressLint;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import in.ecoprice.tonant.Models.OrdersList;
import in.ecoprice.tonant.R;

/**
 * Created by nani on 11/17/2017.
 */

public class OrdersViewAdapter extends RecyclerView.Adapter<OrdersViewAdapter.ViewHolder> {
    private static List<OrdersList> listitems;
    private Context ctx;
    private Context context;

    public OrdersViewAdapter(List<OrdersList> listitems, Context ctx) {
        this.listitems = listitems;
        this.ctx = ctx;
    }

   /* public GralleryAdapter(List<Gallery> listitems, Context ctx) {
        this.listitems = listitems;
        this.ctx = ctx;
    }*/

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.oderview_item,parent,false);

        return new ViewHolder(v,ctx,listitems);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position )  {
        OrdersList listItem =listitems.get(position);
        holder.ProductName.setText(listItem.getProductName());
        holder.Price.setText(listItem.getPrice());
        holder.UnitPrice.setText(listItem.getUnitPrice());
        holder.Qnty.setText(listItem.getQty());
        holder.ProductWait.setText(listItem.getProductWait());


    }

    @Override
    public int getItemCount() {
        return listitems.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder /*implements View.OnClickListener*/ {
        private TextView Price,UnitPrice,Qnty,ProductName,ProductWait;
        Context ctx;
        ArrayList<OrdersList> completeds = new ArrayList<OrdersList>();

        @SuppressLint("CutPasteId")
        public ViewHolder(View View, Context ctx, List<OrdersList> completeds) {
            super(View);

            /*View.setOnClickListener(this);*/
            this.completeds = (ArrayList<OrdersList>) completeds;
            this.ctx = ctx;
            Price = (TextView) View.findViewById(R.id.Price);
            UnitPrice = (TextView) View.findViewById(R.id.UnitPrice);
            Qnty = (TextView) View.findViewById(R.id.Qnty);
            ProductName = (TextView) View.findViewById(R.id.ProductName);
            ProductWait = (TextView) View.findViewById(R.id.ProductWait);


        }

       /* @Override
        public void onClick(View v) {
            int position = getAdapterPosition();
            Model posts = this.completeds.get(position);
            Intent intent = new Intent(this.ctx, VideoViewActivity.class);
            intent.putExtra("link", posts.getLink().replaceAll(" ", "%20"));
            intent.putExtra("name", posts.getName());
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            this.ctx.startActivity(intent);


        }*/

    }
}


