package in.ecoprice.tonant;

import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import in.ecoprice.tonant.Adapters.MyOrderAdapter;
import in.ecoprice.tonant.Retrofit.Responce.MyOrdersResponse;
import in.ecoprice.tonant.Retrofit.SharedPref;
import in.ecoprice.tonant.Retrofit.network.ApiClient;
import in.ecoprice.tonant.Retrofit.network.ApiService;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MyOrderNewActivity extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener {


    Toolbar toolbar;
    RecyclerView mRecycler;
    MyOrderAdapter myOrderAdapter;
    List<MyOrdersResponse>  myOrdersResponseList;
    SharedPref sharedPref;
    String userName,city1,student1;
    ProgressBar progressbar;

    InternetChecker internetChecker;
    SwipeRefreshLayout mSwipeRefreshLayout;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_order_new);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        internetChecker = new InternetChecker(this);


        mRecycler = (RecyclerView) findViewById(R.id.Rec_myorders);
        progressbar = (ProgressBar)  findViewById(R.id.Prog_Assigned);
        mRecycler.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        mRecycler.setItemAnimator(new DefaultItemAnimator());
        myOrdersResponseList = new ArrayList<>();
       // listItems1 = new ArrayList<>();

        // SwipeRefreshLayout
        mSwipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_container);
        mSwipeRefreshLayout.setOnRefreshListener(MyOrderNewActivity.this);
        mSwipeRefreshLayout.setColorSchemeResources(R.color.colorPrimary,
                android.R.color.holo_green_dark,
                android.R.color.holo_orange_dark,
                android.R.color.holo_blue_dark);
        if(myOrdersResponseList!=null) {
            myOrdersResponseList.clear();
        }
        loadRecyclerViewData();


    }

    private void loadRecyclerViewData() {



        SharedPref sharedPref = new SharedPref(getApplicationContext());
        String username = sharedPref.getStr("userName");

        progressbar.setVisibility(View.VISIBLE);


            ApiService apiInterface = ApiClient.getClient().create(ApiService.class);
            Call<List<MyOrdersResponse>> call = apiInterface.getMyordersList(username);
            call.enqueue(new Callback<List<MyOrdersResponse>>() {
                @Override
                public void onResponse(Call<List<MyOrdersResponse>> call, Response<List<MyOrdersResponse>> response) {

                    progressbar.setVisibility(View.GONE);
                    if (response.isSuccessful() && response != null) {

                        List<MyOrdersResponse> myOrdersResponseList = new ArrayList<>();
                        myOrdersResponseList= response.body();

                        if (myOrdersResponseList != null && myOrdersResponseList.size() > 0) {


                            myOrderAdapter = new MyOrderAdapter(getApplicationContext(), myOrdersResponseList);
                            mRecycler.setAdapter(myOrderAdapter);

                        }
                        else {
                            Toast.makeText(getApplicationContext(), "No Orders Found", Toast.LENGTH_SHORT).show();
                        }
                    }

                }

                @Override
                public void onFailure(Call<List<MyOrdersResponse>> call, Throwable t) {

                    progressbar.setVisibility(View.GONE);
                    Toast.makeText(getApplicationContext(), "" + t.getMessage(), Toast.LENGTH_SHORT).show();

                }
            });



    }

    @Override
    public void onRefresh() {
        loadRecyclerViewData();
    }
    @Override
    public boolean onSupportNavigateUp() {

        onBackPressed();
        return true;
    }
}
