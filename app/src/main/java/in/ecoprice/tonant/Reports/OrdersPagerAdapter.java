package in.ecoprice.tonant.Reports;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

/**
 * Created by Nani on 11/06/17.
 */

public class OrdersPagerAdapter extends FragmentStatePagerAdapter {

   Context context;
    public OrdersPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {

        switch(position) {
            case 0:
                TodayOrdersFragment dailyFragment = new TodayOrdersFragment();
                return dailyFragment;

            case 1:
                WeekOrdersFragment weeklyFragment = new WeekOrdersFragment();
                return  weeklyFragment;

            case 2:
                DateToDateFragment monthlyFragment = new DateToDateFragment();
                return monthlyFragment;

            default:
                return  null;
        }

    }





    @Override
    public int getCount() {
        return 3;
    }

    public CharSequence getPageTitle(int position){

        switch (position) {
            case 0:
                return "Today";

            case 1:
                return "Weekly";

            case 2:
                return "Date to Date";

            default:
                return null;
        }

    }



}
