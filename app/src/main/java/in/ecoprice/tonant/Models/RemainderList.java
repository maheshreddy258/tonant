package in.ecoprice.tonant.Models;

public class RemainderList {
    private String Id,ShopKeeperName,dateofcreation,dateofvisittobe,reason,VisiteType;

    public RemainderList(String id, String shopKeeperName, String dateofcreation, String dateofvisittobe, String reason, String visiteType) {
        Id = id;
        ShopKeeperName = shopKeeperName;
        this.dateofcreation = dateofcreation;
        this.dateofvisittobe = dateofvisittobe;
        this.reason = reason;
        VisiteType = visiteType;
    }

    public String getId() {
        return Id;
    }

    public void setId(String id) {
        Id = id;
    }

    public String getShopKeeperName() {
        return ShopKeeperName;
    }

    public void setShopKeeperName(String shopKeeperName) {
        ShopKeeperName = shopKeeperName;
    }

    public String getDateofcreation() {
        return dateofcreation;
    }

    public void setDateofcreation(String dateofcreation) {
        this.dateofcreation = dateofcreation;
    }

    public String getDateofvisittobe() {
        return dateofvisittobe;
    }

    public void setDateofvisittobe(String dateofvisittobe) {
        this.dateofvisittobe = dateofvisittobe;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public String getVisiteType() {
        return VisiteType;
    }

    public void setVisiteType(String visiteType) {
        VisiteType = visiteType;
    }
}
