package in.ecoprice.tonant.Adapters;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.List;

import in.ecoprice.tonant.AddShopActivity;
import in.ecoprice.tonant.HomeActivity;
import in.ecoprice.tonant.Models.Orders;
import in.ecoprice.tonant.MyOrderNewActivity;
import in.ecoprice.tonant.MyVisitsActivity;
import in.ecoprice.tonant.PendingOrdersActivity;
import in.ecoprice.tonant.R;
import in.ecoprice.tonant.ReminderActivity;
import in.ecoprice.tonant.Retrofit.Responce.Homemodel;
import in.ecoprice.tonant.ShopListActivity;
import in.ecoprice.tonant.VisitsReportsActivity;

public class HomeAdapter extends RecyclerView.Adapter<HomeAdapter.HomeViewHolder>{

  private Context cntx;
  private List<Homemodel> homemodels;

    public HomeAdapter(Context cntx, List<Homemodel> homemodels) {
        this.cntx = cntx;
        this.homemodels = homemodels;
    }

    @NonNull
    @Override
    public HomeViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view;
        LayoutInflater inflater =LayoutInflater.from(cntx);
        view=inflater.inflate(R.layout.carditemhome,viewGroup,false);
        return new HomeViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull HomeViewHolder homeViewHolder, int i) {

        homeViewHolder.mImghome.setImageResource(homemodels.get(i).getImg());
        homeViewHolder.mText.setText(homemodels.get(i).getTitle());

    }

    @Override
    public int getItemCount() {
        return homemodels.size();
    }

    public class HomeViewHolder extends RecyclerView.ViewHolder {


        ImageView mImghome;
        TextView mText;
        RelativeLayout relativeLayout;

        public HomeViewHolder(@NonNull View itemView) {
            super(itemView);
            cntx = itemView.getContext();
            mImghome = itemView.findViewById(R.id.homeimg);
            mText = itemView.findViewById(R.id.hometext);
            relativeLayout = itemView.findViewById(R.id.homeitem);
            relativeLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    final Intent intent;


                    switch (getAdapterPosition()){
                        case 0:
                            intent =  new Intent(cntx, AddShopActivity.class);
                            break;

                        case 1:
                            intent =  new Intent(cntx, ShopListActivity.class);
                            break;

                        case 2:
                            intent =  new Intent(cntx, ReminderActivity.class);
                            break;

                        case 3:
                            intent =  new Intent(cntx, MyVisitsActivity.class);
                            break;
                        case 4:
                            intent =  new Intent(cntx, VisitsReportsActivity.class);
                            break;

                        case 5:
                            intent =  new Intent(cntx, MyOrderNewActivity.class);
                            break;
                        case 6:
                            intent =  new Intent(cntx, PendingOrdersActivity.class);
                            break;


                        default:
                            intent =  new Intent(cntx, HomeActivity.class);
                            break;
                    }
                    cntx.startActivity(intent);

                }
            });
        }
    }
}
