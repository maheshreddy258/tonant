package in.ecoprice.tonant.Adapters;


import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import in.ecoprice.tonant.Models.CustomFilter;
import in.ecoprice.tonant.Models.MyTargets;
import in.ecoprice.tonant.ProductListActivity;
import in.ecoprice.tonant.R;
import in.ecoprice.tonant.VisitActivity;

/**
 * Created by nani on 11/17/2017.
 */

public class MyTargetsAdapter extends RecyclerView.Adapter<MyTargetsAdapter.ViewHolder>{
     public List<MyTargets> listitems;
    private Context ctx;
    CustomFilter filter;
    public MyTargetsAdapter(List<MyTargets> listitems, Context ctx) {
        this.listitems = listitems;
        this.ctx = ctx;

    }


   /* public GralleryAdapter(List<Gallery> listitems, Context ctx) {
        this.listitems = listitems;
        this.ctx = ctx;
    }*/

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.my_targets, parent, false);

        return new ViewHolder(v, ctx, listitems);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {
        final MyTargets listItem = listitems.get(position);

        holder.targetTitle.setText(listItem.getTargetTitle());
        holder.TargetType.setText(listItem.getTargetType());
        holder.TargetDays.setText(listItem.getTargetDays());
        holder.Retailers.setText(" "+listItem.getRetailers());
        holder.SalesKGS.setText(" "+listItem.getSalesKgs());


        }

    @Override
    public int getItemCount() {
        return listitems.size();
    }



    public class ViewHolder extends RecyclerView.ViewHolder /*implements View.OnClickListener*/ {

        private TextView targetTitle,TargetType,TargetDays,Retailers,SalesKGS;
        Context ctx;
        ArrayList<MyTargets> completeds = new ArrayList<MyTargets>();

        @SuppressLint("CutPasteId")
        public ViewHolder(View View, final Context ctx, List<MyTargets> completeds) {
            super(View);

           /* View.setOnClickListener(this);*/
            this.completeds = (ArrayList<MyTargets>) completeds;
            this.ctx = ctx;
            targetTitle = (TextView) View.findViewById(R.id.targetTitle);
            TargetType = (TextView) View.findViewById(R.id.TargetType);
            TargetDays = (TextView) View.findViewById(R.id.TargetDays);
            Retailers = (TextView) View.findViewById(R.id.Retailers);
            SalesKGS = (TextView) View.findViewById(R.id.SalesKGS);





        }


    }
}


