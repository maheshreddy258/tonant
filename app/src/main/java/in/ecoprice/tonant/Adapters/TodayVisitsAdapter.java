package in.ecoprice.tonant.Adapters;


import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import in.ecoprice.tonant.Models.TodayVisits;
import in.ecoprice.tonant.OrderListViewActviity;
import in.ecoprice.tonant.R;

import static com.android.volley.VolleyLog.TAG;

public class TodayVisitsAdapter extends RecyclerView.Adapter<TodayVisitsAdapter.ViewHolder>{
     public List<TodayVisits> listitems;
    private Context ctx;
    public TodayVisitsAdapter(List<TodayVisits> listitems, Context ctx) {
        this.listitems = listitems;
        this.ctx = ctx;

    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.today_visits, parent, false);

        return new ViewHolder(v, ctx, listitems);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {
        final TodayVisits listItem = listitems.get(position);
        holder.ShopName.setText(listItem.getShopkeeparName());
        holder.Username.setText(listItem.getUserName());
        holder.Total.setText("Total "+listItem.getTotal());
        holder.NetPay.setText("NetPay "+listItem.getNetPay());
        holder.Pending.setText("Pending "+listItem.getPendingAmount());
        holder.TotalQntity.setText("TotalQntity : "+listItem.getQuanity());
        Date c = Calendar.getInstance().getTime();
        SimpleDateFormat df = new SimpleDateFormat("dd.MM.yyyy");
        String formattedDate = df.format(c);

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        SimpleDateFormat output = new SimpleDateFormat("dd.MM.yyyy");
        Date d = null;
       // Date CreatedDate = null;
        try {
            d = sdf.parse(listItem.getCreateddate());
        //    CreatedDate = sdf.parse(listItem.getCreateddate());
        } catch (ParseException e) {
            e.printStackTrace();
        }
        String formattedTime = output.format(d);
        holder.TodayDate.setText("Date : "+formattedTime);
     //   Log.w(TAG," Today Date := "+formattedDate+" Visit Date := "+formattedTime);

        if (formattedDate.equals(formattedTime)){
            holder.Card.setVisibility(View.VISIBLE);
        }else {
            holder.Card.setVisibility(View.GONE);

        }


    }

    @Override
    public int getItemCount() {
        return listitems.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private TextView ShopName,Username,Total,NetPay,Pending,TotalQntity,TodayDate;
        private LinearLayout Card;
        Context ctx;
        ArrayList<TodayVisits> completeds = new ArrayList<TodayVisits>();

        @SuppressLint("CutPasteId")
        public ViewHolder(View View, final Context ctx, List<TodayVisits> completeds) {
            super(View);

            View.setOnClickListener(this);
            this.completeds = (ArrayList<TodayVisits>) completeds;
            this.ctx = ctx;
            ShopName = (TextView) View.findViewById(R.id.ShopName);
            Username = (TextView) View.findViewById(R.id.Username);
            Total = (TextView) View.findViewById(R.id.Total);
            Pending = (TextView) View.findViewById(R.id.Pending);
            NetPay = (TextView) View.findViewById(R.id.NetPay);
            TotalQntity = (TextView) View.findViewById(R.id.TotalQntity);
            TodayDate = (TextView) View.findViewById(R.id.TodayDate);
            Card = (LinearLayout) View.findViewById(R.id.Card);



            }

        @Override
        public void onClick(View view) {
            int position = getAdapterPosition();
            TodayVisits posts = this.completeds.get(position);
            Intent intent = new Intent(this.ctx, OrderListViewActviity.class);
            intent.putExtra("name", posts.getShopkeeparName());
            intent.putExtra("pos", position);
            intent.putExtra("jsonArray", posts.getOrdersList());
            intent.putExtra("pending", posts.getPendingAmount());
            intent.putExtra("total", posts.getTotal());
            intent.putExtra("netpay", posts.getNetPay());
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            this.ctx.startActivity(intent);
        }
    }
   /* private String parseTime(String date){
        SimpleDateFormat format = new SimpleDateFormat("yyyy-dd-MM HH:mm:ss");
        try {
            Date date1 = format.parse(date.replace("T"," "));
            String d= new SimpleDateFormat("dd.MM.yyyy").format(date1);
            return d;
        }catch (Exception e){
            e.printStackTrace();
        }
        return "";
    }*/
}


