package in.ecoprice.tonant.Retrofit.Responce;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.squareup.moshi.Json;

public class AccessToken {

    // Designation

    @SerializedName("UserName")
    @Expose
   private String Username;
    @SerializedName("ShopName")
    @Expose
    private String shopName;
    @SerializedName("Loginstatus")
    @Expose
   private boolean Status;

    public String getUsername() {
        return Username;
    }

    public void setUsername(String username) {
        Username = username;
    }

    public boolean isStatus() {
        return Status;
    }

    public void setStatus(boolean status) {
        Status = status;
    }
}

