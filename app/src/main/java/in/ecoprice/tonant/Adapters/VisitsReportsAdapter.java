package in.ecoprice.tonant.Adapters;


import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import in.ecoprice.tonant.Models.VisitReports;
import in.ecoprice.tonant.R;

public class VisitsReportsAdapter extends RecyclerView.Adapter<VisitsReportsAdapter.ViewHolder>{
     public List<VisitReports> listitems;
    private Context ctx;
    public VisitsReportsAdapter(List<VisitReports> listitems, Context ctx) {
        this.listitems = listitems;
        this.ctx = ctx;

    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.visitreports, parent, false);

        return new ViewHolder(v, ctx, listitems);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {
        final VisitReports listItem = listitems.get(position);
        holder.ShopName.setText(listItem.getVisitNme());
        holder.Username.setText(listItem.getPhone());
        Date c = Calendar.getInstance().getTime();
        SimpleDateFormat df = new SimpleDateFormat("dd.MM.yyyy");
        String formattedDate = df.format(c);

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        SimpleDateFormat output = new SimpleDateFormat("dd.MM.yyyy");
        Date d = null;
        try {
            d = sdf.parse(listItem.getDateofVisit());
        } catch (ParseException e) {
            e.printStackTrace();
        }
        String formattedTime = output.format(d);
        holder.TodayDate.setText("Date : "+formattedTime);

        if (listItem.getStatus() == 0){
            holder.Status.setBackgroundColor(Color.parseColor("#396A19"));
            holder.StatusName.setText("visit");
        }else if(listItem.getStatus() == 1){
            holder.Status.setBackgroundColor(Color.parseColor("#396A19"));
            holder.StatusName.setText("Product Purchased");
        }else if(listItem.getStatus() == 2){
            holder.Status.setBackgroundColor(Color.parseColor("#396A19"));
            holder.StatusName.setText("");
        }

    }

    @Override
    public int getItemCount() {
        return listitems.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder/* implements View.OnClickListener */{

        private TextView ShopName,Username,TodayDate,StatusName;
        private LinearLayout Card;
        private View Status;
        Context ctx;
        ArrayList<VisitReports> completeds = new ArrayList<VisitReports>();

        @SuppressLint("CutPasteId")
        public ViewHolder(View View, final Context ctx, List<VisitReports> completeds) {
            super(View);

            /*View.setOnClickListener(this);*/
            this.completeds = (ArrayList<VisitReports>) completeds;
            this.ctx = ctx;
            ShopName = (TextView) View.findViewById(R.id.ShopName);
            Username = (TextView) View.findViewById(R.id.Username);
            TodayDate = (TextView) View.findViewById(R.id.TodayDate);
            StatusName = (TextView) View.findViewById(R.id.StatusName);
            Card = (LinearLayout) View.findViewById(R.id.Card);
            Status = (View) View.findViewById(R.id.Status);



            }

       /* @Override
        public void onClick(View view) {
            int position = getAdapterPosition();
            TodayVisits posts = this.completeds.get(position);
            Intent intent = new Intent(this.ctx, OrderListViewActviity.class);
            intent.putExtra("name", posts.getShopkeeparName());
            intent.putExtra("pos", position);
            intent.putExtra("jsonArray", posts.getOrdersList());
            intent.putExtra("pending", posts.getPendingAmount());
            intent.putExtra("total", posts.getTotal());
            intent.putExtra("netpay", posts.getNetPay());
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            this.ctx.startActivity(intent);
        }*/
    }
   /* private String parseTime(String date){
        SimpleDateFormat format = new SimpleDateFormat("yyyy-dd-MM HH:mm:ss");
        try {
            Date date1 = format.parse(date.replace("T"," "));
            String d= new SimpleDateFormat("dd.MM.yyyy").format(date1);
            return d;
        }catch (Exception e){
            e.printStackTrace();
        }
        return "";
    }*/
}


