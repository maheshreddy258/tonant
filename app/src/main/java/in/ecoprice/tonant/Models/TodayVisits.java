package in.ecoprice.tonant.Models;

import com.google.gson.JsonArray;

import org.json.JSONArray;

public class TodayVisits {
    private String Id,UserName,ShopkeeparName,Quanity,DeliveryAddress,Total,Tax,DeliveryCharges,GST,NetPay,PaymentStatus,Status,PaidThrough,PendingAmount,Createddate;
    private String ordersList;

    public TodayVisits(String id, String userName, String shopkeeparName, String quanity, String deliveryAddress, String total, String tax, String deliveryCharges, String GST, String netPay, String paymentStatus, String status, String paidThrough, String pendingAmount, String createddate, String ordersList) {
        Id = id;
        UserName = userName;
        ShopkeeparName = shopkeeparName;
        Quanity = quanity;
        DeliveryAddress = deliveryAddress;
        Total = total;
        Tax = tax;
        DeliveryCharges = deliveryCharges;
        this.GST = GST;
        NetPay = netPay;
        PaymentStatus = paymentStatus;
        Status = status;
        PaidThrough = paidThrough;
        PendingAmount = pendingAmount;
        Createddate = createddate;
        this.ordersList = ordersList;
    }

    public String getId() {
        return Id;
    }

    public void setId(String id) {
        Id = id;
    }

    public String getUserName() {
        return UserName;
    }

    public void setUserName(String userName) {
        UserName = userName;
    }

    public String getShopkeeparName() {
        return ShopkeeparName;
    }

    public void setShopkeeparName(String shopkeeparName) {
        ShopkeeparName = shopkeeparName;
    }

    public String getQuanity() {
        return Quanity;
    }

    public void setQuanity(String quanity) {
        Quanity = quanity;
    }

    public String getDeliveryAddress() {
        return DeliveryAddress;
    }

    public void setDeliveryAddress(String deliveryAddress) {
        DeliveryAddress = deliveryAddress;
    }

    public String getTotal() {
        return Total;
    }

    public void setTotal(String total) {
        Total = total;
    }

    public String getTax() {
        return Tax;
    }

    public void setTax(String tax) {
        Tax = tax;
    }

    public String getDeliveryCharges() {
        return DeliveryCharges;
    }

    public void setDeliveryCharges(String deliveryCharges) {
        DeliveryCharges = deliveryCharges;
    }

    public String getGST() {
        return GST;
    }

    public void setGST(String GST) {
        this.GST = GST;
    }

    public String getNetPay() {
        return NetPay;
    }

    public void setNetPay(String netPay) {
        NetPay = netPay;
    }

    public String getPaymentStatus() {
        return PaymentStatus;
    }

    public void setPaymentStatus(String paymentStatus) {
        PaymentStatus = paymentStatus;
    }

    public String getStatus() {
        return Status;
    }

    public void setStatus(String status) {
        Status = status;
    }

    public String getPaidThrough() {
        return PaidThrough;
    }

    public void setPaidThrough(String paidThrough) {
        PaidThrough = paidThrough;
    }

    public String getPendingAmount() {
        return PendingAmount;
    }

    public void setPendingAmount(String pendingAmount) {
        PendingAmount = pendingAmount;
    }

    public String getCreateddate() {
        return Createddate;
    }

    public void setCreateddate(String createddate) {
        Createddate = createddate;
    }

    public String getOrdersList() {
        return ordersList;
    }

    public void setOrdersList(String ordersList) {
        this.ordersList = ordersList;
    }
}
