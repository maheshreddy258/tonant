package in.ecoprice.tonant.Adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.LinearLayout;
import android.widget.TextView;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import in.ecoprice.tonant.Models.PendingAmounts;
import in.ecoprice.tonant.Models.PendingVisitsFilter;
import in.ecoprice.tonant.R;
import in.ecoprice.tonant.Visits.PendingAmountRecActivity;

import static com.android.volley.VolleyLog.TAG;


public class PendingVisitsAdapter extends RecyclerView.Adapter<PendingVisitsAdapter.ViewHolder> implements Filterable {
     public List<PendingAmounts> listitems;
    private Context ctx;
    PendingVisitsFilter filter;
    public PendingVisitsAdapter(List<PendingAmounts> listitems, Context ctx) {
        this.listitems = listitems;
        this.ctx = ctx;
        }
    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.pending_amounts, parent, false);
        return new ViewHolder(v, ctx, listitems);
    }
    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {
        final PendingAmounts listItem = listitems.get(position);

        //final PendingAmounts listItem = listitems.get(position);
        holder.ShopName.setText(listItem.getShopName());
        holder.Username.setText(listItem.getUserName());
        holder.Pending.setText("Pending ₹"+listItem.getPendingBill());
        holder.CardFrag.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ctx, PendingAmountRecActivity.class);
                intent.putExtra("ShopId", listItem.getId());
                intent.putExtra("ShopUserName", listItem.getMobileNumber());
                intent.putExtra("PendingAmount", listItem.getPendingBill());
                intent.putExtra("MobileNumber", listItem.getUserName());
                intent.putExtra("ShopName", listItem.getShopName());
                Log.w(TAG,"Created Date :"+listitems.get(position).getCreateDate());
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                ctx.startActivity(intent);
            }
        });

        }

    @Override
    public int getItemCount() {
        return listitems.size();
    }
    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    public class ViewHolder extends RecyclerView.ViewHolder/* implements View.OnClickListener */{

        private TextView ShopName,Username,Pending;
        private CardView CardFrag;
        Context ctx;
        ArrayList<PendingAmounts> completeds = new ArrayList<PendingAmounts>();

        @SuppressLint("CutPasteId")
        public ViewHolder(View View, final Context ctx, List<PendingAmounts> completeds) {
            super(View);
           /* View.setOnClickListener(this);*/
            this.completeds = (ArrayList<PendingAmounts>) completeds;
            this.ctx = ctx;
            ShopName = (TextView) View.findViewById(R.id.ShopName);
            Username = (TextView) View.findViewById(R.id.Username);
            Pending = (TextView) View.findViewById(R.id.Pending);
            CardFrag = (CardView) View.findViewById(R.id.CardFrag);

            }

       /* @Override
        public void onClick(final View view) {

        }*/
        }

    @Override
    public Filter getFilter() {
        if(filter==null)
        {
            filter=new PendingVisitsFilter(listitems,this);
        }
        return filter;
    }

}


