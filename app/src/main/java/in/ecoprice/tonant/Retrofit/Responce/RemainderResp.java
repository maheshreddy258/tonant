package in.ecoprice.tonant.Retrofit.Responce;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class RemainderResp {

    // Designation

    @SerializedName("ErrorMessage")
    @Expose
   private String ErrorMessage;

    @SerializedName("RetunStatus")
    @Expose
   private boolean Status;

    public String getErrorMessage() {
        return ErrorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        ErrorMessage = errorMessage;
    }

    public boolean isStatus() {
        return Status;
    }

    public void setStatus(boolean status) {
        Status = status;
    }
}

