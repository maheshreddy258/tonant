package in.ecoprice.tonant.Adapters;


import android.annotation.SuppressLint;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputEditText;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import in.ecoprice.tonant.Models.Cart;
import in.ecoprice.tonant.Models.ProductsList;
import in.ecoprice.tonant.R;
import in.ecoprice.tonant.Retrofit.CartPref;
import in.ecoprice.tonant.dummy.ModelCart;

/**
 * Created by nani on 11/17/2017.
 */

public class TakeOrderAdapter extends RecyclerView.Adapter<TakeOrderAdapter.ViewHolder>  {
    private List<ProductsList> listitems;
    private Context ctx;
    private List<Cart> productsList = new ArrayList();
    public TakeOrderAdapter(List<ProductsList> listitems, Context ctx) {
        this.listitems = listitems;
        this.ctx = ctx;
        setHasStableIds(false);

    }


   /* public GralleryAdapter(List<Gallery> listitems, Context ctx) {
        this.listitems = listitems;
        this.ctx = ctx;
    }*/

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.take_order, parent, false);

        return new ViewHolder(v, ctx, listitems);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {
        final ProductsList listItem = listitems.get(position);
        String URL = "";
        Picasso.with(ctx)
                .load(listItem.getBannerImage())
                .into(holder.ser_img);
        holder.title.setText(listItem.getProductName());
        holder.coast.setText(""+listItem.getSalePrice());
        holder.Qnty.setText("0");
        listitems.get(position).setQnty(Integer.parseInt(holder.Qnty.getText().toString()));
        listitems.get(position).setSalePrice(Double.parseDouble(holder.coast.getText().toString()));



        holder.Qnty.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                productAdapter(s,position);
                String text = s.toString();
               // updatePrice( holder.coast,text);

                /*  int value = 1;
                if (! TextUtils.isEmpty(s)) {
                    String text = s.toString();
                    if ("-".equals(text)) {
                        notifyDataSetChanged();
                    } else {
                        value = Integer.parseInt(text);
                        notifyDataSetChanged();
                    }
                    listItem.setQnty(value);
                    notifyDataSetChanged();
                }*/
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                productAdapter(s,position);
                String text = s.toString();
                //updatePrice(holder.coast,text);
               /* int value = 1;
                if (! TextUtils.isEmpty(s)) {
                    String text = s.toString();
                    if ("-".equals(text)) {
                        notifyDataSetChanged();
                    } else {
                        value = Integer.parseInt(text);
                        notifyDataSetChanged();
                    }
                    listItem.setQnty(value);
                    notifyDataSetChanged();
                }*/


            }

            @Override
            public void afterTextChanged(Editable s) {
                productAdapter(s,position);
                String text = s.toString();
               // updatePrice( holder.coast,text);

                /*int value = 0;
                if (! TextUtils.isEmpty(s)) {
                    String text = s.toString();
                    if ("-".equals(text)) {
                        notifyDataSetChanged();
                    } else {
                        value = Integer.parseInt(text);
                        notifyDataSetChanged();
                    }
                    listItem.setQnty(value);
                }*/
            }
        });
        holder.coast.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                productAdaptercoast(s,position);
              /*  int value = 1;
                if (! TextUtils.isEmpty(s)) {
                    String text = s.toString();
                    if ("-".equals(text)) {
                        notifyDataSetChanged();
                    } else {
                        value = Integer.parseInt(text);
                        notifyDataSetChanged();
                    }
                    listItem.setQnty(value);
                    notifyDataSetChanged();
                }*/
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                productAdaptercoast(s,position);

               /* int value = 1;
                if (! TextUtils.isEmpty(s)) {
                    String text = s.toString();
                    if ("-".equals(text)) {
                        notifyDataSetChanged();
                    } else {
                        value = Integer.parseInt(text);
                        notifyDataSetChanged();
                    }
                    listItem.setQnty(value);
                    notifyDataSetChanged();
                }*/


            }

            @Override
            public void afterTextChanged(Editable s) {
                productAdaptercoast(s,position);
                /*int value = 0;
                if (! TextUtils.isEmpty(s)) {
                    String text = s.toString();
                    if ("-".equals(text)) {
                        notifyDataSetChanged();
                    } else {
                        value = Integer.parseInt(text);
                        notifyDataSetChanged();
                    }
                    listItem.setQnty(value);
                }*/
            }
        });



  }

    /*private void updatePrice(TextInputEditText coast, String s) {
       // String text = s.toString();


        int value = 0;
        if (! TextUtils.isEmpty(s)) {
            if ("-".equals(s)) {
                //notifyDataSetChanged();
            } else {
                value = Integer.parseInt(s);
                if (value<=10){
                    coast.setText("120");
                }else if (value<=20){
                    coast.setText("100");
                }else if (value<=30){
                    coast.setText("80");
                }else if (value>=30){
                    coast.setText("80");
                }
            }

        }




    }*/

    private void productAdaptercoast(CharSequence s, int position) {
       // notifyDataSetChanged();
        String value = "";
        if (! TextUtils.isEmpty(s)) {
            String text = s.toString();
            if ("-".equals(text)) {
                //notifyDataSetChanged();
            } else {
                value = text;
                //notifyDataSetChanged();
            }
            listitems.get(position).setSalePrice(Double.parseDouble(value));
            //listitems.get(po).setSalePrice(value);
           // notifyDataSetChanged();
           // notify(listitems);

        }
    }

    private void productAdapter(CharSequence s,int po) {
        int value = 0;
                if (! TextUtils.isEmpty(s)) {
                    String text = s.toString();
                    if ("-".equals(text)) {
                        //notifyDataSetChanged();
                    } else {
                        value = Integer.parseInt(text);
                        //notifyDataSetChanged();
                    }
                    listitems.get(po).setQnty(value);
                   // notifyDataSetChanged();
                    //listitems.get(po).setSalePrice(value);
                    //notifyDataSetChanged();
                   // notify(listitems);
                }

    }

    @Override
    public int getItemCount() {
        return listitems.size();
    }



    public class ViewHolder extends RecyclerView.ViewHolder /*implements View.OnClickListener*/ {

        private ImageView ser_img;
        private TextView title;
        private ImageButton Plus,Less;
        private TextInputEditText Qnty,coast;
        // private Button AddTOCart;
        private LinearLayout MainPro;
        Context ctx;
        ArrayList<ProductsList> completeds = new ArrayList<ProductsList>();

        @SuppressLint("CutPasteId")
        public ViewHolder(View View, final Context ctx, List<ProductsList> completeds) {
            super(View);

            /* View.setOnClickListener(this);*/
            this.completeds = (ArrayList<ProductsList>) completeds;
            this.ctx = ctx;
            ser_img = (ImageView) View.findViewById(R.id.ImagePro);
            title = (TextView) View.findViewById(R.id.TitlePro);
            coast = (TextInputEditText) View.findViewById(R.id.RatePro);
            Qnty = (TextInputEditText) View.findViewById(R.id.Qnty);
            View.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(android.view.View view) {
                    notifyDataSetChanged();
                }
            });

           // coast.setEnabled(false);
            /*CountText.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(android.view.View view) {
                    InputMethodManager im = (InputMethodManager) ctx.getSystemService(Context.INPUT_METHOD_SERVICE);
                    im.showSoftInput(CountText, 0);
                }*/
           /* });*/





        }

        /*@Override
        public void onClick(View v) {
            int position = getAdapterPosition();
            Products posts = this.completeds.get(position);
           // Toast.makeText(ctx, "Selected Item : " + position, Toast.LENGTH_SHORT).show();
            MainPro.setBackgroundResource(R.drawable.selector_row);



            *//* Intent intent = new Intent(this.ctx, VideoViewActivity.class);
            intent.putExtra("link", posts.getLink().replaceAll(" ", "%20"));
            intent.putExtra("name", posts.getName());*//**//*
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            this.ctx.startActivity(intent);*//*


        }*/

    }

  /*  public void notify(List<ProductsList> list) {
        if (listitems != null) {
            listitems.clear();
            listitems.addAll(list);

        } else {
            listitems = list;
        }
        notifyDataSetChanged();
    }*/

}


