package in.ecoprice.tonant.Remainders;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import in.ecoprice.tonant.Targets.ManagementTargetsFragment;
import in.ecoprice.tonant.Targets.MyTargetsFragment;

/**
 * Created by Nani on 11/06/17.
 */

public class RemaindersPagerAdapter extends FragmentStatePagerAdapter {

   Context context;
    public RemaindersPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {

        switch(position) {
            case 0:
                TodayRemaindersFragment weeklyFragment = new TodayRemaindersFragment();
                return  weeklyFragment;
            case 1:
                AllRemaindersFragment dailyFragment = new AllRemaindersFragment();
                return dailyFragment;
            default:
            return  null;
        }

    }

    @Override
    public int getCount() {
        return 2;
    }

    public CharSequence getPageTitle(int position){

        switch (position) {
            case 0:
                return "Today Remainders";

            case 1:
                return "ALL Remianders";

            default:
                return null;
        }

    }



}
