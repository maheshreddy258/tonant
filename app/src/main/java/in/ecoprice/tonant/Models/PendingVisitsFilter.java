package in.ecoprice.tonant.Models;

import android.widget.Filter;

import java.util.ArrayList;
import java.util.List;

import in.ecoprice.tonant.Adapters.PendingVisitsAdapter;

public class PendingVisitsFilter extends Filter {

    PendingVisitsAdapter adapter;
    List<PendingAmounts> filterList;

    public PendingVisitsFilter(List<PendingAmounts> filterList, PendingVisitsAdapter adapter)
    {
        this.adapter=adapter;
        this.filterList=filterList;

    }
    @Override
    protected FilterResults performFiltering(CharSequence constraint) {
        FilterResults results=new FilterResults();


        if(constraint != null && constraint.length() > 0)
        {
            constraint=constraint.toString().toUpperCase();
            List<PendingAmounts> filteredPlayers=new ArrayList<>();


            for (int i=0;i<filterList.size();i++)
            {
                String firLast = filterList.get(i).getMobileNumber();

                if(firLast.toUpperCase().contains(constraint))
                {
                    filteredPlayers.add(filterList.get(i));
                } else if (filterList.get(i).getMobileNumber().toUpperCase().contains(constraint)) {
                    filteredPlayers.add(filterList.get(i));
                }


                String area = filterList.get(i).getShopName();

                if(area.toUpperCase().contains(constraint))
                {
                    filteredPlayers.add(filterList.get(i));
                } else if (filterList.get(i).getShopName().toUpperCase().contains(constraint)) {
                    filteredPlayers.add(filterList.get(i));
                }


                String mobile = filterList.get(i).getUserName();

                if(mobile.toUpperCase().contains(constraint))
                {
                    filteredPlayers.add(filterList.get(i));
                } else if (filterList.get(i).getUserName().toUpperCase().contains(constraint)) {
                    filteredPlayers.add(filterList.get(i));
                }

            }

            results.count=filteredPlayers.size();
            results.values=filteredPlayers;
        }else
        {
            results.count=filterList.size();
            results.values=filterList;

        }

        return results;
    }

    @Override
    protected void publishResults(CharSequence constraint, FilterResults results) {

        adapter.listitems= (List<PendingAmounts>) results.values;

        adapter.notifyDataSetChanged();
    }



}

