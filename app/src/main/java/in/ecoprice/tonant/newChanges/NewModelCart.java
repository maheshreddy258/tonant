package in.ecoprice.tonant.newChanges;

import java.util.ArrayList;
import java.util.List;

public class NewModelCart {

    private static List<ModelProducts> cartProducts = new ArrayList<ModelProducts>();

    public static List<ModelProducts> getData() {
        return cartProducts;
    }

    public static void setData(List<ModelProducts> data) {
        NewModelCart.cartProducts = data;
    }

    public boolean remove(List<ModelProducts> data) {

       return cartProducts.contains(data);
    }

    public int getCartSize() {

        return cartProducts.size();

    }

    public boolean RemoveProduct(List<ModelProducts> data) {

        return cartProducts.remove(data);
        }

    public boolean UpdateCart(List<ModelProducts> data) {
        return cartProducts.contains(data);

    }

    public boolean RemoveAllProducts(List<ModelProducts> aProduct) {

        return cartProducts.remove(aProduct);



    }


}
