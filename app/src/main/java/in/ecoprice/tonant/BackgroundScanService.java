package in.ecoprice.tonant;

import android.annotation.SuppressLint;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Handler;
import android.os.IBinder;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.concurrent.TimeUnit;

import in.ecoprice.tonant.Adapters.TodayRemainderListAdapter;
import in.ecoprice.tonant.Models.RemainderList;
import in.ecoprice.tonant.Retrofit.SharedPref;

import static com.android.volley.VolleyLog.TAG;

public class BackgroundScanService extends Service {
    private String TAG = "BgService fromTonant";

    private final Handler handler = new Handler();
    private boolean isRunning; // Flag indicating if service is already running.
    private int devicesCount; // Total discovered devices count

    @Override
    public void onCreate() {
        super.onCreate();
        getDataFromServer();
        isRunning = false;
    }



    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        //Check if service is already active
        if (isRunning) {
            Toast.makeText(this, "Service is already running.", Toast.LENGTH_SHORT).show();
            return START_STICKY;
        }

        getDataFromServer();

        isRunning = true;
        return START_STICKY;
    }

    private void getDataFromServer() {
        Toast.makeText(this, "Getting Data.", Toast.LENGTH_SHORT).show();
        String username = SharedPref.getStr("userName");
        String URL_DATA = "http://apiservices.tonantfarmers.com/api/ReminderListbySalesName?UserName="+username;
        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(URL_DATA, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                try {
                    if (response.length() > 0) {
                        for (int i = 0; i < response.length(); i++) {
                            JSONObject obj =response.getJSONObject(i);
                            RemainderList item = new RemainderList(
                                    obj.getString("Id"),
                                    obj.getString("ShopKeeperName"),
                                    obj.getString("dateofcreation"),
                                    obj.getString("dateofvisittobe"),
                                    obj.getString("reason"),
                                    obj.getString("VisiteType")

                            );
                            createEddystoneListener(item.getShopKeeperName(),item.getDateofvisittobe(),item.getVisiteType());

                        }

                    }
                }catch (JSONException e) {
                    e.printStackTrace();
                }


            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                /*  progressDialog.dismiss();*/

                VolleyLog.e("Error: ", error.getMessage());
                Toast.makeText(getApplicationContext(),"Try Again ",Toast.LENGTH_LONG).show();

            }
        });
        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        requestQueue.add(jsonArrayRequest);





    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }



    private void createEddystoneListener(String name,String date,String content) {
                Log.w(TAG,"Shop name :"+name+"Date :"+date+"Content "+content);
                NotificationCompat.Builder mBuilder =   new NotificationCompat.Builder(getApplicationContext())
                        .setSmallIcon(R.mipmap.app_icon) // notification icon
                        .setContentTitle(name) // title for notification
                        .setContentText(date+" "+content) // message for notification
                        .setAutoCancel(true); // clear notification after click
                playNotificationSound();
                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                @SuppressLint("WrongConstant") PendingIntent pi = PendingIntent.getActivity(getApplicationContext(),0,intent, Intent.FLAG_ACTIVITY_NEW_TASK);
                mBuilder.setContentIntent(pi);
                NotificationManager mNotificationManager =
                        (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                mNotificationManager.notify(0, mBuilder.build());

            }


    public void playNotificationSound() {
        try {
            Uri alarmSound = Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE
                    + "://" + getApplicationContext().getPackageName() + "/raw/notification_sound");
            Ringtone r = RingtoneManager.getRingtone(getApplicationContext(), alarmSound);
            r.play();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }



    @Override
    public void onDestroy() {
        handler.removeCallbacksAndMessages(null);
        Toast.makeText(BackgroundScanService.this, "Scanning service stopped.", Toast.LENGTH_SHORT).show();
        super.onDestroy();
    }


}
