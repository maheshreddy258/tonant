package in.ecoprice.tonant;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import in.ecoprice.tonant.Adapters.VisitsReportsAdapter;
import in.ecoprice.tonant.Models.VisitReports;
import in.ecoprice.tonant.Retrofit.SharedPref;

public class VisitsReportsViewActivity extends AppCompatActivity {


    private static final String TAG = "ReportsTodayActivity";
    private RecyclerView Gal_Rec;
    private RecyclerView.Adapter adapter;
    private List<VisitReports> listItems;
    public static List<VisitReports> listItems1;
    private ProgressBar progressBar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_visits_reports_view);


      String fromDate =  getIntent().getStringExtra("fromDatestr");
      String toDate =  getIntent().getStringExtra("toDatestr");
      String type =  getIntent().getStringExtra("type");

        Gal_Rec = (RecyclerView) findViewById(R.id.Rec_Gallery);
        Gal_Rec.setHasFixedSize(true);
        Gal_Rec.setNestedScrollingEnabled(false);
        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(VisitsReportsViewActivity.this, 1);
        Gal_Rec.setLayoutManager(mLayoutManager);
        listItems = new ArrayList<>();
        listItems1 = new ArrayList<>();
        progressBar = (ProgressBar) findViewById(R.id.Prog_Assigned);
        loadRecyclerViewData(type,fromDate+" 00:00:00",toDate+" 23:00:00");


    }

    private void loadRecyclerViewData(String type,String fromDate,String formattedDate) {
        progressBar.setVisibility(View.VISIBLE);
        Gal_Rec.setVisibility(View.GONE);
        String username = SharedPref.getStr("userName");
        String URL_DATA = "http://apiservices.tonantfarmers.com/api/AdminVisitdReport?fromdate="+fromDate+"&todate="+formattedDate+"&OutletType="+type+"&userName="+username;
        String url = URL_DATA.replaceAll(" ", "%20");
        Log.w(TAG,"Url Reports replaceAll : "+url);
        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(url, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                Gal_Rec.setVisibility(View.VISIBLE);
                progressBar.setVisibility(View.GONE);
                try {
                    for(int i=0; i<response.length(); i++ ){
                        JSONObject obj =response.getJSONObject(i);
                        VisitReports item = new VisitReports(
                                obj.getString("SalesName"),
                                obj.getString("VisitNme"),
                                obj.getString("ShoPName"),
                                obj.getString("DateofVisit"),
                                obj.getString("Address"),
                                obj.getString("Phone"),
                                obj.getInt("Status")

                        );
                        listItems = listItems1;
                        listItems.add(item);
                    }
                    adapter = new VisitsReportsAdapter(listItems , getApplicationContext());
                    Gal_Rec.setAdapter(adapter);

                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Gal_Rec.setVisibility(View.GONE);
                progressBar.setVisibility(View.GONE);
                VolleyLog.e("Error: ", error.getMessage());
                Toast.makeText(getApplicationContext(),error.getMessage(),Toast.LENGTH_LONG).show();
            }
        });
        RequestQueue requestQueue = Volley.newRequestQueue(VisitsReportsViewActivity.this);
        jsonArrayRequest.setRetryPolicy(new DefaultRetryPolicy(
                2000*5,
                /*DefaultRetryPolicy.DEFAULT_MAX_RETRIES*/ 3,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(jsonArrayRequest);
    }

}
