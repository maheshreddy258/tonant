package in.ecoprice.tonant.Remainders;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Handler;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatEditText;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

import in.ecoprice.tonant.MyTargetsActivity;
import in.ecoprice.tonant.R;
import in.ecoprice.tonant.ReminderActivity;
import in.ecoprice.tonant.Retrofit.Responce.RemainderResp;
import in.ecoprice.tonant.Retrofit.Responce.Targets;
import in.ecoprice.tonant.Retrofit.SharedPref;
import in.ecoprice.tonant.Retrofit.network.ApiService;
import in.ecoprice.tonant.Retrofit.network.RetrofitBuilder;
import in.ecoprice.tonant.ShopListActivity;
import in.ecoprice.tonant.Targets.CreateTargetActivity;
import in.ecoprice.tonant.UpdateRemainderActivity;
import in.ecoprice.tonant.VisitActivity;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CreateRemainderActivity extends AppCompatActivity {

    private static final String TAG = "CreateRemaider";
    TextInputEditText SelectShop,DateofVisitTobe,VisiteType;
    AppCompatEditText Remarks;
    Button button;
    ApiService service;
    Call<RemainderResp> call;
    private int mYear, mMonth, mDay, mHour, mMinute;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_remainder);
        service = RetrofitBuilder.createService(ApiService.class);

        SelectShop = (TextInputEditText) findViewById(R.id.SelectShop);
        DateofVisitTobe = (TextInputEditText) findViewById(R.id.DateofVisitTobe);
        VisiteType = (TextInputEditText) findViewById(R.id.VisiteType);
        Remarks = (AppCompatEditText) findViewById(R.id.Remarks);
        button = (Button) findViewById(R.id.BtnTarget);

        DateofVisitTobe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Get Current Date
                final Calendar c = Calendar.getInstance();
                mYear = c.get(Calendar.YEAR);
                mMonth = c.get(Calendar.MONTH);
                mDay = c.get(Calendar.DAY_OF_MONTH);


                DatePickerDialog datePickerDialog = new DatePickerDialog(CreateRemainderActivity.this,
                        new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {

                                @SuppressLint("SimpleDateFormat")
                                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
                                c.set(year, monthOfYear , dayOfMonth);
                                String dateString = sdf.format(c.getTime());


                                /*Date yourDate= new Date(year, , dayOfMonth);
                                String strDate = null;
                                SimpleDateFormat dateFormatter = new SimpleDateFormat(
                                        "yyyy-MM-dd'T'HH:mm:ss");
                                strDate = dateFormatter.format(yourDate);*/

                                DateofVisitTobe.setText(dateString);

                            }
                        }, mYear, mMonth, mDay);
                datePickerDialog.show();
                datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
            }
        });

        //pendingammountcollection,feedback,new Order

        //
       String shopname = getIntent().getStringExtra("shopName");
       SelectShop.setText(shopname);
        SelectShop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(CreateRemainderActivity.this,SelectShopActivity.class);
                startActivity(intent);
                finish();
            }
        });


        VisiteType.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final String[] text = {"New Order","Pending Amount Collection","feedback"};
                android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(view.getRootView().getContext(), android.app.AlertDialog.THEME_DEVICE_DEFAULT_LIGHT);
                builder.setItems(text, new DialogInterface.OnClickListener() {
                    @SuppressLint("SetTextI18n")
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        ListView lw = ((android.app.AlertDialog)dialog).getListView();
                        Object checkedItem = lw.getAdapter().getItem(which);
                        VisiteType.setText(checkedItem.toString());

                    }
                });
                builder.show();
            }
        });
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SendRemainder(
                        SelectShop.getText().toString(),
                        DateofVisitTobe.getText().toString(),
                        VisiteType.getText().toString(),
                        Remarks.getText().toString(),
                        SharedPref.getStr("userName")
                );
            }
        });



    }

    private void SendRemainder(String shopname, String dateOfvisit, String visiteType, String Remarks, String userName) {
        Log.w(TAG, "dateOfvisit: " + dateOfvisit);
        final ProgressDialog progressDialog= new ProgressDialog(CreateRemainderActivity.this);
        progressDialog.setMessage("Loading...");
        progressDialog.show();
        progressDialog.setCancelable(false);
        call = service.Addreminder(shopname,dateOfvisit,visiteType,Remarks,userName);
        call.enqueue(new Callback<RemainderResp>() {
            @Override
            public void onResponse(Call<RemainderResp> call, Response<RemainderResp> response) {
                Log.w(TAG, "onResponse: " + response);
                response.body();
                boolean Loginstatus = response.body().isStatus();
                Log.w(TAG, "Loginstatus: " + Loginstatus);
                if (Loginstatus) {
                    Toast.makeText(CreateRemainderActivity.this,response.body().getErrorMessage(),Toast.LENGTH_LONG).show();
                    Log.w(TAG, "status: " + Loginstatus);
                    Log.w(TAG, "ErrorMSG: " + response.body().getErrorMessage());

                    int TIME = 2000;
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            progressDialog.dismiss();
                            startActivity(new Intent(CreateRemainderActivity.this, ReminderActivity.class));
                            finish();
                        }
                    }, TIME);
                } else {
                    progressDialog.dismiss();
                    Toast.makeText(CreateRemainderActivity.this,response.body().getErrorMessage(),Toast.LENGTH_SHORT).show();
                    Log.w(TAG, "status: in false" + Loginstatus);
                    Log.w(TAG, "ErrorMSG in false: " + response.body().getErrorMessage());

                }

            }

            @Override
            public void onFailure(Call<RemainderResp> call, Throwable t) {
                progressDialog.dismiss();
                Toast.makeText(CreateRemainderActivity.this,""+t.getMessage(),Toast.LENGTH_LONG).show();
                Log.w(TAG, "onFailure: " + t.getMessage());
                // showForm();
            }
        });


    }
}
