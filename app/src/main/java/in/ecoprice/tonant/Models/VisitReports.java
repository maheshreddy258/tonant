package in.ecoprice.tonant.Models;

public class VisitReports {
    private  String SalesName,VisitNme,ShoPName,DateofVisit,Address,Phone;
    private int Status;

    public VisitReports(String salesName, String visitNme, String shoPName, String dateofVisit, String address, String phone, int status) {
        SalesName = salesName;
        VisitNme = visitNme;
        ShoPName = shoPName;
        DateofVisit = dateofVisit;
        Address = address;
        Phone = phone;
        Status = status;
    }

    public String getSalesName() {
        return SalesName;
    }

    public void setSalesName(String salesName) {
        SalesName = salesName;
    }

    public String getVisitNme() {
        return VisitNme;
    }

    public void setVisitNme(String visitNme) {
        VisitNme = visitNme;
    }

    public String getShoPName() {
        return ShoPName;
    }

    public void setShoPName(String shoPName) {
        ShoPName = shoPName;
    }

    public String getDateofVisit() {
        return DateofVisit;
    }

    public void setDateofVisit(String dateofVisit) {
        DateofVisit = dateofVisit;
    }

    public String getAddress() {
        return Address;
    }

    public void setAddress(String address) {
        Address = address;
    }

    public String getPhone() {
        return Phone;
    }

    public void setPhone(String phone) {
        Phone = phone;
    }

    public int getStatus() {
        return Status;
    }

    public void setStatus(int status) {
        Status = status;
    }
}
