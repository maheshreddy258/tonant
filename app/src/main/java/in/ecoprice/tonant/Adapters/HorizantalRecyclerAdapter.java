package in.ecoprice.tonant.Adapters;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import in.ecoprice.tonant.R;

import static android.provider.ContactsContract.CommonDataKinds.Website.URL;

public class HorizantalRecyclerAdapter extends RecyclerView.Adapter<HorizantalRecyclerAdapter.HorizantalViewHolder>{


    Context ctx;
    private String[] items;

    public HorizantalRecyclerAdapter(String[] items) {
        this.items = items;
    }

    @NonNull
    @Override
    public HorizantalViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        LayoutInflater layoutInflater = LayoutInflater.from(viewGroup.getContext());
        View view =layoutInflater.inflate(R.layout.horizantalrecycleritem,viewGroup,false);


        return new HorizantalViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull HorizantalViewHolder horizantalViewHolder, int i) {

        /*Glide.with(ctx)
                .load(URL + items.getBannerImage())
                .centerCrop()
                .into(horizantalViewHolder.mImage);

        horizantalViewHolder.mText.setText(items.getId(i).);*/
    }

    @Override
    public int getItemCount() {
        return 0;
    }

    public class HorizantalViewHolder extends RecyclerView.ViewHolder{

         ImageView mImage;
         TextView mText;
         CardView mcv;
        public HorizantalViewHolder(@NonNull View itemView) {
            super(itemView);

            mImage =itemView.findViewById(R.id.productsimage);
            mText=itemView.findViewById(R.id.producttext);
            mcv=itemView.findViewById(R.id.productcv);
        }
    }


}
