package in.ecoprice.tonant;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Handler;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatEditText;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.basgeekball.awesomevalidation.AwesomeValidation;
import com.google.android.gms.location.LocationListener;

import java.io.IOException;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

import in.ecoprice.tonant.Retrofit.Responce.AccessToken;
import in.ecoprice.tonant.Retrofit.Responce.AddShopResp;
import in.ecoprice.tonant.Retrofit.SharedPref;
import in.ecoprice.tonant.Retrofit.network.ApiService;
import in.ecoprice.tonant.Retrofit.network.RetrofitBuilder;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddShopActivity extends AppCompatActivity {

    private static final String TAG = "AddShop";
    private TextInputLayout
            mFirst_Name,
            mCROEMailID,
            mMobileNumber,
            mShopName, mLeadType,
            mPIN_Code;
    private static final int REQUEST_LOCATION = 1;
    TextView textView;
    LocationManager locationManager;
    String lattitude,longitude;
    Button AddShopBtn;
    ApiService service;
    Call<AddShopResp> call;

    AppCompatEditText  mShopAddress;
   private LocationResolver mLocationResolver;
    String check;
    @SuppressLint("NewApi")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_shop);
        getSupportActionBar().setTitle("Add Shop");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        service = RetrofitBuilder.createService(ApiService.class);
        mLocationResolver=new LocationResolver(this);
        mFirst_Name = (TextInputLayout) findViewById(R.id.Firstname);
        mCROEMailID = (TextInputLayout) findViewById(R.id.CROEMailID);
        mMobileNumber = (TextInputLayout) findViewById(R.id.MobileNumber);
        mShopName = (TextInputLayout) findViewById(R.id.ShopName);
        mShopAddress = (AppCompatEditText) findViewById(R.id.ShopAddress);
        mLeadType = (TextInputLayout) findViewById(R.id.LeadType);
        mPIN_Code = (TextInputLayout) findViewById(R.id.PIN_Code);
        textView = (TextView) findViewById(R.id.textview);
        AddShopBtn = (Button) findViewById(R.id.AddShopBtn);
        //Latitude,Longitude,LoginUserName,CreateBy
        mMobileNumber.getEditText().addTextChangedListener(numberWatcher);
        mCROEMailID.getEditText().addTextChangedListener(emailWatcher);
        mFirst_Name.getEditText().addTextChangedListener(nameWatcher);
        //mShopAddress.setEnabled(false);

        mLeadType.getEditText().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final String[] text = {"Fast food","Tiffin centre","Hostel","Hotel","Shop","Other"};
                android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(view.getRootView().getContext(), android.app.AlertDialog.THEME_DEVICE_DEFAULT_LIGHT);
                builder.setItems(text, new DialogInterface.OnClickListener() {
                    @SuppressLint("SetTextI18n")
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        ListView lw = ((android.app.AlertDialog)dialog).getListView();
                        Object checkedItem = lw.getAdapter().getItem(which);
                        mLeadType.getEditText().setText(checkedItem.toString());

                    }
                });
                builder.show();
            }
        });


        AddShopBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!validate(mLeadType)
                        || !validate(mFirst_Name)
                        || !validate(mMobileNumber)
                        || !validate(mShopName)
                        || !validateEd(mShopAddress)
                        ||  !validate(mLeadType)){
                    Log.w(TAG,"Response :"+ " Username Empty");

                }else {
                    AddShop(mFirst_Name.getEditText().getText().toString(),
                            mCROEMailID.getEditText().getText().toString(),
                            mMobileNumber.getEditText().getText().toString(),
                            mShopName.getEditText().getText().toString(),
                            mShopAddress.getText().toString(),
                            mLeadType.getEditText().getText().toString(),
                            mPIN_Code.getEditText().getText().toString(),
                            lattitude,
                            longitude,
                            SharedPref.getStr("userName"),
                            SharedPref.getStr("userName")

                    );
                }
            }
        });


            mLocationResolver.resolveLocation(this, new LocationResolver.OnLocationResolved() {
                @Override
                public void onLocationResolved(Location location) throws IOException {

                    if (location != null) {


                    }else {

                    }


                    double latti = location.getLatitude();
                    double longi = location.getLongitude();
                    lattitude = String.valueOf(latti);
                    longitude = String.valueOf(longi);
                    Geocoder geocoder;
                    List<Address> addresses;
                    geocoder = new Geocoder(AddShopActivity.this, Locale.getDefault());
                    addresses = geocoder.getFromLocation(latti, longi, 1);
                    final String address = addresses.get(0).getAddressLine(0);
                    final String cies = addresses.get(0).getPostalCode();
                    final String city = addresses.get(0).getLocality();
                    final String state = addresses.get(0).getAdminArea();
                    final String country = addresses.get(0).getCountryName();
                    final String pin_code = addresses.get(0).getPostalCode();

                    final ProgressDialog progressDialog= new ProgressDialog(AddShopActivity.this);
            progressDialog.setMessage("Please wait while we Getting your Location");
            progressDialog.show();
            progressDialog.setCancelable(false);
            progressDialog.setCanceledOnTouchOutside(false);
            int TIME = 2000;
            new Handler().postDelayed(new Runnable() {
           @Override
           public void run() {
                progressDialog.dismiss();
               textView.setText("Your current location is from GPS_PROVIDER"+ "\n" + "Lattitude = " + lattitude
                       + "\n" + "Longitude = " + longitude+" \n Address ="+address+ "\n" + "City = " + city
                       + "\n" +"State = " + state+" \n Country ="+country+" \n Pincode ="+pin_code);

               Log.w(TAG,"http://maps.google.com/?q="+lattitude+","+longitude);
               Log.w(TAG,"Address :"+address+"Line :"+cies);
               mPIN_Code.getEditText().setText(pin_code);
               mShopAddress.setText(address);
            }
            }, TIME);

            }
            });



//        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
//        if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
//            buildAlertMessageNoGps();
//        } else if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
//
//            try {
//                getLocation();
//
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
//
//            final ProgressDialog progressDialog= new ProgressDialog(AddShopActivity.this);
//            progressDialog.setMessage("Please Wait we are Getting Your Location");
//            progressDialog.show();
//            progressDialog.setCancelable(false);
//            progressDialog.setCanceledOnTouchOutside(false);
//            int TIME = 2000;
//            new Handler().postDelayed(new Runnable() {
//            @Override
//            public void run() {
//                progressDialog.dismiss();
//            }
//            }, TIME);
//            }



    }
    private void AddShop(String firstname, String email, final String mobilenumber, String shopname,
                         String shopaddress, String leadType, String pinCode,
                         String lati, String longi, String username, String loginusername) {
        final ProgressDialog progressDialog= new ProgressDialog(AddShopActivity.this);
        progressDialog.setMessage("Loading...");
        progressDialog.show();
        progressDialog.setCancelable(false);
        progressDialog.setCanceledOnTouchOutside(false);
        call = service.AddShop(firstname,email,mobilenumber,shopname,shopaddress,
                leadType,pinCode,lati,longi,username,loginusername,mobilenumber);
        call.enqueue(new Callback<AddShopResp>() {
            @Override
            public void onResponse(Call<AddShopResp> call, Response<AddShopResp> response) {
                Log.w(TAG, "onResponse: " + response);
                response.body();
                boolean Loginstatus = response.body().isStatus();
                Log.w(TAG, "Loginstatus: " + Loginstatus);
                if (Loginstatus) {
                    Toast.makeText(AddShopActivity.this,response.body().getErrorMSG(),Toast.LENGTH_LONG).show();
                    Log.w(TAG, "status: " + Loginstatus);
                    Log.w(TAG, "ErrorMSG: " + response.body().getErrorMSG());
                    //Log.w(TAG, "ErrorMSG: " + response.body().());

                    int TIME = 2000;
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            progressDialog.dismiss();
                            startActivity(new Intent(AddShopActivity.this, Home2Activity.class));
                            finish();
                        }
                    }, TIME);
                } else if(response.body().getErrorMSG().equals("UserName already exists...")){
                    Toast.makeText(AddShopActivity.this,response.body().getErrorMSG(),Toast.LENGTH_SHORT).show();
                    progressDialog.dismiss();
                    //startActivity(new Intent(AddShopActivity.this, MainActivity.class));
                    Intent intent = new Intent(AddShopActivity.this,ShopListActivity.class);
                    intent.putExtra("username",mobilenumber);
                    startActivity(intent);
                    finish();
                }else {
                    progressDialog.dismiss();
                  Toast.makeText(AddShopActivity.this,response.body().getErrorMSG(),Toast.LENGTH_SHORT).show();
                    Log.w(TAG, "status: in false" + Loginstatus);
                    Log.w(TAG, "ErrorMSG in false: " + response.body().getErrorMSG());
                        /*ErrorMsg.setVisibility(View.VISIBLE);
                        int SPLASH_TIME_OUT = 50000;
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                                ErrorMsg.setVisibility(View.GONE);
                            }
                        }, SPLASH_TIME_OUT);*/
                }

            }

            @Override
            public void onFailure(Call<AddShopResp> call, Throwable t) {
                progressDialog.dismiss();
                Log.w(TAG, "onFailure: " + t.getMessage());
               // showForm();
            }
        });

    }


    public boolean validate(TextInputLayout textInputEditText) {
        boolean valid = true;
        textInputEditText.setEnabled(true);
        textInputEditText.setError(null);
        String  textInputEditText123 = textInputEditText.getEditText().getText().toString();
        if (textInputEditText123.length() == 0) {
            textInputEditText.setError("Required");
            textInputEditText.getEditText().requestFocus();
            valid = false;
        } else {
            textInputEditText.setError(null);
        }

        return valid;
    }


    public boolean validateEd(AppCompatEditText textInputEditText) {
        boolean valid = true;
        textInputEditText.setEnabled(true);
        textInputEditText.setError(null);
        String  textInputEditText123 = textInputEditText.getText().toString();
        if (textInputEditText123.length() == 0) {
            textInputEditText.setError("Required");
            textInputEditText.requestFocus();
            valid = false;
        } else {
            textInputEditText.setError(null);
        }

        return valid;
    }


   /* @SuppressLint("SetTextI18n")
    private void getLocation() throws IOException {


        if (ActivityCompat.checkSelfPermission(AddShopActivity.this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission
                (AddShopActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(AddShopActivity.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_LOCATION);
        } else {
           // Location location2 = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
            Location location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
           // Location location1 = locationManager.getLastKnownLocation(LocationManager. PASSIVE_PROVIDER);
            if (location != null) {

                double latti = location.getLatitude();
                double longi = location.getLongitude();
                lattitude = String.valueOf(latti);
                longitude = String.valueOf(longi);
                Geocoder geocoder;
                List<Address> addresses;
                geocoder = new Geocoder(this, Locale.getDefault());
                addresses = geocoder.getFromLocation(latti, longi, 1);
                String address = addresses.get(0).getAddressLine(0);
                String cies = addresses.get(0).getPostalCode();
                String city = addresses.get(0).getLocality();
                String state = addresses.get(0).getAdminArea();
                String country = addresses.get(0).getCountryName();
                String pin_code = addresses.get(0).getPostalCode();
                textView.setText("Your current location is from GPS_PROVIDER"+ "\n" + "Lattitude = " + lattitude
                        + "\n" + "Longitude = " + longitude+" \n Address ="+address+ "\n" + "City = " + city
                        + "\n" +"State = " + state+" \n Country ="+country+" \n Pincode ="+pin_code);

                Log.w(TAG,"http://maps.google.com/?q="+lattitude+","+longitude);
                Log.w(TAG,"Address :"+address+"Line :"+cies);
                mPIN_Code.getEditText().setText(pin_code);
                mShopAddress.setText(address);


            } *//*else  if (location1 != null) {
                double latti = location1.getLatitude();
                double longi = location1.getLongitude();
                lattitude = String.valueOf(latti);
                longitude = String.valueOf(longi);
                Geocoder geocoder;
                List<Address> addresses;
                geocoder = new Geocoder(this, Locale.getDefault());

                addresses = geocoder.getFromLocation(latti, longi, 1);
                String address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
                String city = addresses.get(0).getLocality();
                String state = addresses.get(0).getAdminArea();
                String country = addresses.get(0).getCountryName();
                String pin_code = addresses.get(0).getPostalCode();
                textView.setText("Your current location is from GPS_PROVIDER"+ "\n" + "Lattitude = " + lattitude
                        + "\n" + "Longitude = " + longitude+" \n Address ="+address+ "\n" + "City = " + city
                        + "\n" +" State = " + state+" \n Country ="+country+" \n Pincode ="+pin_code);


            }}*//* *//*else  if (location2 != null) {
                progressDialog.dismiss();
                double latti = location2.getLatitude();
                double longi = location2.getLongitude();
                lattitude = String.valueOf(latti);
                longitude = String.valueOf(longi);
                Geocoder geocoder;
                List<Address> addresses;
                geocoder = new Geocoder(this, Locale.getDefault());
                addresses = geocoder.getFromLocation(latti, longi, 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5
                String address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
                String city = addresses.get(0).getLocality();
                String state = addresses.get(0).getAdminArea();
                String country = addresses.get(0).getCountryName();
                String pin_code = addresses.get(0).getPostalCode();
                textView.setText("Your current location is from NETWORK_PROVIDER" + "\n" + "Lattitude = " + lattitude
                        + "\n" + "Longitude = " + longitude + " \n Address =" + address + "\n" + "City = " + city
                        + "\n" + " State = " + state + " \n Country =" + country + " \n Pincode =" + pin_code);
                Log.w(TAG,"http://maps.google.com/?q="+lattitude+","+longitude);
               // Log.w(TAG,"Address :"+address+"Line :"+cies);
                mPIN_Code.getEditText().setText(pin_code);
                mShopAddress.getEditText().setText(city+","+state);
            }*//*
            else{

               // progressDialog.dismiss();
              getReTryToGetLocation();


            }
        }
    }*/

    private void getReTryToGetLocation()  {
        Toast.makeText(this,"Unble to Trace your location.Try Again",Toast.LENGTH_SHORT).show();
        Intent intent = new Intent(AddShopActivity.this,Home2Activity.class);
        startActivity(intent);
        finish();

       // getLocation();
    }

    /*private void turnGPSOn(){
        Intent intent=new Intent("android.location.GPS_ENABLED_CHANGE");
        intent.putExtra("enabled", true);
        sendBroadcast(intent);
        startActivity(new Intent(AddShopActivity.this, MainActivity.class));
        finish();
    }*/

/*
    protected void buildAlertMessageNoGps() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Please Turn ON your GPS Connection")
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, final int id) {
                        startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                        try {
                            getLocation();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, final int id) {
                        dialog.cancel();
                    }
                });
        final AlertDialog alert = builder.create();
        alert.show();
    }
*/



    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }
    private boolean validateNumber(TextInputLayout textInputEditText) {
        textInputEditText.setEnabled(true);
        textInputEditText.setError(null);
        check = textInputEditText.getEditText().getText().toString();
        Log.e("inside number",check.length()+" ");
        if (check.length()>10) {
            return false;
        }else if(check.length()<10){
            return false;
        }
        return true;
    }

    private boolean validateEmail(TextInputLayout textInputEditText) {

        check = textInputEditText.getEditText().getText().toString();

        if (check.length() < 4 || check.length() > 40) {
            return false;
        } else if (!check.matches("^[A-za-z0-9.@]+")) {
            return false;
        } else if (!check.contains("@") || !check.contains(".")) {
            return false;
        }

        return true;
    }

      TextWatcher numberWatcher = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                //none
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                //none
            }

            @Override
            public void afterTextChanged(Editable s) {
                mMobileNumber.setEnabled(true);
                mMobileNumber.setError(null);
                check = s.toString();
                if (check.length()>10) {
                    mMobileNumber.setEnabled(true);
                    mMobileNumber.setError("Number cannot be grated than 10 digits");
                }else if(check.length()<10){
                    mMobileNumber.setEnabled(true);
                    mMobileNumber.setError("Number should be 10 digits");
                }
            }

        };

    TextWatcher emailWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            //none
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            //none
        }

        @Override
        public void afterTextChanged(Editable s) {
            mCROEMailID.setEnabled(true);
            mCROEMailID.setError(null);
            check = s.toString();

            if (check.length() < 4 || check.length() > 40) {
                mCROEMailID.setError("Email Must consist of 4 to 20 characters");
            } else if (!check.matches("^[A-za-z0-9.@]+")) {
                mCROEMailID.setError("Only . and @ characters allowed");
            } else if (!check.contains("@") || !check.contains(".")) {
                mCROEMailID.setError("Enter Valid Email");
            }

        }

    };
    @Override
    protected void onStart() {
        super.onStart();
        mLocationResolver.onStart();
    }

    @Override
    protected void onStop() {
        super.onStop();
        mLocationResolver.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mLocationResolver.onDestroy();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        mLocationResolver.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }
    TextWatcher nameWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            //none
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            //none
        }

        @Override
        public void afterTextChanged(Editable s) {
            mFirst_Name.setEnabled(true);
            mFirst_Name.setError(null);
            check = s.toString();

            if (check.length() < 4 || check.length() > 20) {
                mFirst_Name.setError("Name Must consist of 4 to 20 characters");
            }
        }

    };
}
