package in.ecoprice.tonant;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import in.ecoprice.tonant.Adapters.TakeOrderAdapter;
import in.ecoprice.tonant.Models.Cart;
import in.ecoprice.tonant.Models.ProductsList;
import in.ecoprice.tonant.Models.RecyclerItemClickListener;
import in.ecoprice.tonant.Retrofit.CartPref;
import in.ecoprice.tonant.dummy.ModelCart;

public class TakeOrderActivity extends AppCompatActivity {



    private static final String TAG = "Product";
    private RecyclerView Gal_Rec;
    private TextView TextTotal;
    private TakeOrderAdapter adapter;
    private List<ProductsList> listItems;

    String URL_DATA = "http://apiservices.tonantfarmers.com/api/AllProducts";
    private AppCompatButton ButtonBtn;
    private List<Cart> productsList = new ArrayList();
    private ProgressBar progressBar;
    int clickon;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_take_order);
        getSupportActionBar().setTitle("Take Order");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        ButtonBtn = (AppCompatButton) findViewById(R.id.ButtonBtn);
        progressBar = (ProgressBar)  findViewById(R.id.Prog_Assigned);
        Gal_Rec = (RecyclerView) findViewById(R.id.Rec_Gallery);
        Gal_Rec.setHasFixedSize(true);
        Gal_Rec.setNestedScrollingEnabled(false);
        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(getApplicationContext(), 1);
        Gal_Rec.setLayoutManager(mLayoutManager);
        listItems = new ArrayList<>();
        TextTotal = (TextView) findViewById(R.id.TextTotal);



        Gal_Rec.addOnItemTouchListener(new RecyclerItemClickListener(getApplicationContext(), new RecyclerItemClickListener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, final int position) {
                       //  productsList.add(listItems.get(position));
                        Cart cart = new Cart();
                        if (listItems.get(position).getQnty() == 0){
                            Log.d("TAG", "getQnty :"+listItems.get(position).getProductName()+" is Added");
                        }else {
                            cart.setProductName(listItems.get(position).getProductName());
                            cart.setPosition(position);
                            double abcd = listItems.get(position).getSalePrice()*listItems.get(position).getQnty();
                            cart.setPrice(abcd);
                            cart.setSalePrice(listItems.get(position).getSalePrice());
                            cart.setId(listItems.get(position).getId());
                            cart.setQnty(listItems.get(position).getQnty());
                            Log.d("TAG", "getQnty :"+listItems.get(position).getProductName()+" is Updated");
                            productsList.add(cart);
                            ModelCart.setData(productsList);
                            }



                        List<Cart> data = ModelCart.getData();
                        int total = 0;
                        Log.d("TAG", "start total = " +total);
                        for (int i = 0; i < data.size(); i++) {
                            Log.d("TAG", "at "+i+ " quantity = " +data.get(i).getQnty());
                            total += data.get(i).getQnty() * data.get(i).getSalePrice();
                            Log.d("TAG", "at "+i+ " total = " +total);

                        }
                        TextTotal.setText(total+" Rs");

                    }
                })
        );

        //ModelCart.setData(productsList);
        final String shopuserNmae = getIntent().getStringExtra("ShopUserName");
        final String ShopName = getIntent().getStringExtra("ShopName");
        Log.w(TAG,"TakeOrder ==> "+"shopuserNmae :"+shopuserNmae+" ShopName :"+ShopName);


        ButtonBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               // removeDuplicates(productsList);
                ModelCart.setData(productsList);
                Intent intent = new Intent(TakeOrderActivity.this,PlaceOderActivity.class);
                intent.putExtra("strings", String.valueOf(productsList));
                intent.putExtra("ShopUserName", shopuserNmae);
                intent.putExtra("ShopName", ShopName);
                InputMethodManager inputManager = (InputMethodManager) getApplicationContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                inputManager.hideSoftInputFromWindow(TakeOrderActivity.this.getCurrentFocus().getWindowToken(), InputMethodManager.RESULT_UNCHANGED_SHOWN);


                //intent.putExtra("pos", position);

                startActivity(intent);
            }
        });


        loadRecyclerViewData();

    }

    private void loadRecyclerViewData() {
        progressBar.setVisibility(View.VISIBLE);
        Gal_Rec.setVisibility(View.GONE);
        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(URL_DATA, new Response.Listener<JSONArray>() {

            @Override
            public void onResponse(JSONArray response) {
                progressBar.setVisibility(View.GONE);
                Gal_Rec.setVisibility(View.VISIBLE);
                try {
                    if (response.length() > 0) {
                        for (int i = 0; i < response.length(); i++) {
                            JSONObject obj = response.getJSONObject(i);
                            ProductsList item = new ProductsList(
                                    obj.getInt("Id"),
                                    obj.getInt("MRP"),
                                    obj.getInt("StockQuantity"),
                                    obj.getInt("ProductStatus"),
                                    obj.getDouble("SalePrice"),
                                    obj.getDouble("DiscountPercentage"),
                                    obj.getDouble("minPrice"),
                                    obj.getString("ProductName"),
                                    obj.getString("Description"),
                                    obj.getString("StockUnit"),
                                    obj.getString("BannerImage"),
                                    obj.getString("SortDesc"),
                                    obj.getString("BrandName"),
                                    obj.getString("DeliveryCharges"),
                                    obj.getString("ShortName")
                            );
                            listItems.add(item);
                        }
                        adapter = new TakeOrderAdapter(listItems, getApplicationContext());
                        Gal_Rec.setAdapter(adapter);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressBar.setVisibility(View.GONE);
                Gal_Rec.setVisibility(View.GONE);
                TringAgain();


            }
        });
        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        requestQueue.add(jsonArrayRequest);


    }

    private void TringAgain() {
        Toast.makeText(TakeOrderActivity.this, "Try Again :(", Toast.LENGTH_SHORT).show();

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }


      /* ProductsList selItem =  listItems.get(position);
                      final String itemname =  listItems.get(position).getProductName();*/
                      /*final float price = Float.parseFloat(listItems.get(position).getSalePrice());
                      final int qnty =  listItems.get(position).getQnty();
                      float abcd = price*qnty;
                       String xyx= String.valueOf(abcd);
                       TextTotal.setText(xyx);*/
    //productsList.add(listItems.get(position));
                        /*Cart new_item= new Cart(listItems.get(position).getSalePrice(),listItems.get(position).getProductName(),listItems.get(position).getSalePrice(),listItems.get(position).getQnty(),listItems.get(position).getId());
                        productsList.add(new_item);*/


    //int i = 0;
                        /*Cart cart = new Cart();
                        productsList.clear();
                        if (cart.getProductName().equals(listItems.get(position).getProductName())){
                            double abcd = listItems.get(position).getSalePrice()*listItems.get(position).getQnty();
                            cart.setProductName(listItems.get(position).getProductName());
                            cart.setPrice(abcd);
                            cart.setSalePrice(listItems.get(position).getSalePrice());
                            cart.setId(listItems.get(position).getId());
                            cart.setQnty(listItems.get(position).getQnty());
                            productsList.add(cart);
                            //i++;
                        }else {
                            cart.setProductName(listItems.get(position).getProductName());
                            productsList.add(cart);
                        }*/


}
