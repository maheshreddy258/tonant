package in.ecoprice.tonant.Adapters;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import in.ecoprice.tonant.MainActivity;
import in.ecoprice.tonant.MyOrdersProductlistViewActivity;
import in.ecoprice.tonant.R;
import in.ecoprice.tonant.Retrofit.Responce.MyOrdersResponse;
import in.ecoprice.tonant.ShopListActivity;

public class MyOrderAdapter extends RecyclerView.Adapter<MyOrderAdapter.MyOderViewHolder> {


    private Context mContext;

    List<MyOrdersResponse> myOrdersResponses;
    List<MyOrdersResponse.OrdersList> myorders;

    public MyOrderAdapter(Context mContext, List<MyOrdersResponse> myOrdersResponses) {
        this.mContext = mContext;
        this.myOrdersResponses = myOrdersResponses;
    }

    @NonNull
    @Override
    public MyOderViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {



        View itemView = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.orderitem, viewGroup, false);

        return new MyOderViewHolder(itemView);

    }

    @Override
    public void onBindViewHolder(@NonNull MyOderViewHolder myOderViewHolder, final int i) {


        myOderViewHolder.mTvtitleshop.setText(myOrdersResponses.get(i).shopkeeparName);

        myOderViewHolder.mTvorderid.setText("Order Id:"+""+myOrdersResponses.get(i).id);
        myOderViewHolder.mTvdproduct.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                gotoInfo(i);
                //mContext.startActivity(new Intent(mContext, MyOrdersProductlistViewActivity.class));
            }
        });
        myOderViewHolder.mTvdamount.setText("₹" + myOrdersResponses.get(i).netPay);
        myOderViewHolder.mTvdweight.setText(""+myOrdersResponses.get(i).quanity);


    }

    private void gotoInfo(int i) {

        Intent intent = new Intent(mContext, MyOrdersProductlistViewActivity.class);
        intent.putExtra("Id",myOrdersResponses.get(i).id);

        Log.e("idddd", String.valueOf(myOrdersResponses.get(i).id));
        mContext.startActivity(intent);


       /* Intent intent = new Intent(mContext, MyOrdersProductlistViewActivity.class);
        intent.putExtra("ProductName",myorders.get(i).productName);
*/

    }

    @Override
    public int getItemCount() {
        return myOrdersResponses.size();
    }

    public class MyOderViewHolder extends RecyclerView.ViewHolder{


        TextView mTvtitleshop,mTvshopaddress,mTvProduct,mTvdproduct,mTvQuantity,mTVdQuatity,mTvAmountdes,mTvdamount,mTvweight,mTvdweight,mTvorderid;
        ImageView mImage,mimgorder;

        public MyOderViewHolder(@NonNull View itemView) {
            super(itemView);

            mImage = itemView.findViewById(R.id.image);
            mimgorder=itemView.findViewById(R.id.imageorder);
            mTvorderid = itemView.findViewById(R.id.orderid);
            mTvtitleshop = itemView.findViewById(R.id.TitleShop);
           // mTvshopaddress = itemView.findViewById(R.id.Address);
           // mTvProduct = itemView.findViewById(R.id.tvProductname);
            mTvdproduct=itemView.findViewById(R.id.productname);
           //mTvQuantity = itemView.findViewById(R.id.tvquantity);
            //mTVdQuatity =itemView.findViewById(R.id.dquantity);
            mTvAmountdes = itemView.findViewById(R.id.tvamount);
            mTvdamount = itemView.findViewById(R.id.damount);
            //mTvweight = itemView.findViewById(R.id.tvweight);
            mTvdweight= itemView.findViewById(R.id.dweight);


        }
    }



}
