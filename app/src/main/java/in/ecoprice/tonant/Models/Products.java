package in.ecoprice.tonant.Models;

public class Products {
    private String title,image;
    private int coast;

    public Products(String title, String image, int coast) {
        this.title = title;
        this.image = image;
        this.coast = coast;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public int getCoast() {
        return coast;
    }

    public void setCoast(int coast) {
        this.coast = coast;
    }
}
