package in.ecoprice.tonant.Adapters;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import in.ecoprice.tonant.MyOrdersProductlistViewActivity;
import in.ecoprice.tonant.R;
import in.ecoprice.tonant.Retrofit.Responce.PendingOrdersResponse;

public class PendingOrdersAdapter extends RecyclerView.Adapter<PendingOrdersAdapter.PendingOrdersViewHolder>{



    private Context mContext;

    List<PendingOrdersResponse> PendingOrdersResponses;

    public PendingOrdersAdapter(Context mContext, List<PendingOrdersResponse> pendingOrdersResponses) {
        this.mContext = mContext;
        PendingOrdersResponses = pendingOrdersResponses;
    }

    @NonNull
    @Override
    public PendingOrdersViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {


        View itemView = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.orderitem, viewGroup, false);

        return new PendingOrdersViewHolder(itemView);

    }

    @Override
    public void onBindViewHolder(@NonNull PendingOrdersViewHolder pendingOrdersViewHolder, final int i) {

        pendingOrdersViewHolder.mTvpendingtitleshop.setText(PendingOrdersResponses.get(i).shopkeeparName);

        pendingOrdersViewHolder.mTvorderid.setText("Order Id:"+""+PendingOrdersResponses.get(i).id);
        pendingOrdersViewHolder.mTvdproduct.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                gotoInfo(i);
                //mContext.startActivity(new Intent(mContext, MyOrdersProductlistViewActivity.class));
            }
        });
        pendingOrdersViewHolder.mTvdamount.setText("₹" + PendingOrdersResponses.get(i).netPay);
        pendingOrdersViewHolder.mTvdweight.setText(""+PendingOrdersResponses.get(i).quanity);



    }


    private void gotoInfo(int i) {

        Intent intent = new Intent(mContext, MyOrdersProductlistViewActivity.class);
        intent.putExtra("Id",PendingOrdersResponses.get(i).id);

        Log.e("idddd", String.valueOf(PendingOrdersResponses.get(i).id));
        mContext.startActivity(intent);


       /* Intent intent = new Intent(mContext, MyOrdersProductlistViewActivity.class);
        intent.putExtra("ProductName",myorders.get(i).productName);
*/

    }

    @Override
    public int getItemCount() {
        return PendingOrdersResponses.size();
    }

    public class PendingOrdersViewHolder extends RecyclerView.ViewHolder{

        TextView mTvpendingtitleshop,mTvshopaddress,mTvProduct,mTvdproduct,mTvQuantity,mTVdQuatity,mTvAmountdes,mTvdamount,mTvweight,mTvdweight,mTvorderid;
        ImageView mImage,mimgorder;


        public PendingOrdersViewHolder(@NonNull View itemView) {
            super(itemView);

            mImage = itemView.findViewById(R.id.image);
            mimgorder=itemView.findViewById(R.id.imageorder);
            mTvorderid = itemView.findViewById(R.id.orderid);
            mTvpendingtitleshop = itemView.findViewById(R.id.TitleShop);
            // mTvshopaddress = itemView.findViewById(R.id.Address);
            // mTvProduct = itemView.findViewById(R.id.tvProductname);
            mTvdproduct=itemView.findViewById(R.id.productname);
            //mTvQuantity = itemView.findViewById(R.id.tvquantity);
            //mTVdQuatity =itemView.findViewById(R.id.dquantity);
            mTvAmountdes = itemView.findViewById(R.id.tvamount);
            mTvdamount = itemView.findViewById(R.id.damount);
            //mTvweight = itemView.findViewById(R.id.tvweight);
            mTvdweight= itemView.findViewById(R.id.dweight);



        }
    }
}
