package in.ecoprice.tonant;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.transition.TransitionManager;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.basgeekball.awesomevalidation.AwesomeValidation;
import com.basgeekball.awesomevalidation.ValidationStyle;
import com.basgeekball.awesomevalidation.utility.RegexTemplate;

import in.ecoprice.tonant.Retrofit.Responce.Result;
import in.ecoprice.tonant.Retrofit.SharedPref;
import in.ecoprice.tonant.Retrofit.network.ApiService;
import in.ecoprice.tonant.Retrofit.network.RetrofitBuilder;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import thebat.lib.validutil.ValidUtils;

public class ChangePasswordActivity extends AppCompatActivity {

    private static final String TAG = "ChangePassword";
    private Toolbar mToolbar;
    private TextInputEditText mOldpassword,mNewPass,mRePassword;
    private Button mBtn_Change;
    ApiService service;
    AwesomeValidation validator;
    Call<Result> call;
    LinearLayout container;
    LinearLayout formContainer;
    ProgressBar loader;
    ValidUtils validUtils;
  //  private Toolbar supportActionBar;
    private InternetChecker internetChecker;
    @SuppressLint("NewApi")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);

        mToolbar = (Toolbar) findViewById(R.id.main_page_toolbar);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setTitle("Change Password");
        mToolbar.setTitleTextColor(0xFFFFFFFF);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.arrowleft);
        internetChecker = new InternetChecker(this);

        mOldpassword = (TextInputEditText) findViewById(R.id.oldpassword);
        mNewPass = (TextInputEditText) findViewById(R.id.newPass);
        mRePassword = (TextInputEditText) findViewById(R.id.rePassword);
        mBtn_Change = (Button) findViewById(R.id.Btn_Change);
        container = (LinearLayout) findViewById(R.id.container);
        formContainer = (LinearLayout) findViewById(R.id.form_container);
        loader = (ProgressBar) findViewById(R.id.loader);
        service = RetrofitBuilder.createService(ApiService.class);
        //tokenManager = TokenManager.getInstance(getSharedPreferences("prefs", MODE_PRIVATE));
        validator = new AwesomeValidation(ValidationStyle.BASIC);
        validator.addValidation(ChangePasswordActivity.this, R.id.oldpassword, RegexTemplate.NOT_EMPTY, R.string.ErrorMsg);
        validator.addValidation(ChangePasswordActivity.this, R.id.newPass, RegexTemplate.NOT_EMPTY, R.string.ErrorMsg);
        validator.addValidation(ChangePasswordActivity.this, R.id.rePassword, RegexTemplate.NOT_EMPTY, R.string.ErrorMsg);
        validUtils = new ValidUtils();

        mBtn_Change.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                InputMethodManager inputManager = (InputMethodManager) getApplicationContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                inputManager.hideSoftInputFromWindow(ChangePasswordActivity.this.getCurrentFocus().getWindowToken(), InputMethodManager.RESULT_UNCHANGED_SHOWN);
                if (validUtils.validateEditTexts(mOldpassword,mNewPass,mRePassword)){
                    ChangePassword();
                } else {
                    Log.w(TAG, "Empty: ");

                }
            }
        });
    }



    private void ChangePassword() {
        final String oldpass = mOldpassword.getText().toString();
        final String newpass = mNewPass.getText().toString();
        final String repass = mRePassword.getText().toString();
        SharedPref dataProccessor = new SharedPref(getApplicationContext());
        String username = dataProccessor.getStr("userName");
        validator.clear();
        if (validator.validate()) {
            showLoading();
            call = service.ChangePassword(oldpass, newpass,repass,username);
            call.enqueue(new Callback<Result>() {
                @Override
                public void onResponse(Call<Result> call, Response<Result> response) {

                    Log.w(TAG, "onResponse: " + response);
                    response.body();
                    if (response.isSuccessful()) {
                        String errorMsg =  response.body().getErrorMessage();
                        boolean sta = response.body().isUpdateStatus();
                        Log.w(TAG, "sta: " + sta);
                        Log.w(TAG, "errorMsg: " + errorMsg);
                        if (!sta){
                            Toast.makeText(ChangePasswordActivity.this,errorMsg, Toast.LENGTH_LONG).show();
                            showForm();
                        }else {
                            SharedPref dataProccessor = new SharedPref(getApplicationContext());
                            dataProccessor.setStr("Password",newpass);
                            Log.w("Password Changed Status",response.raw().toString());
                            Toast.makeText(ChangePasswordActivity.this,errorMsg, Toast.LENGTH_LONG).show();
                            startActivity(new Intent(ChangePasswordActivity.this, MainActivity.class));
                            finish();
                        }
                    } else {
                        showForm();
                    }

                }


                @Override
                public void onFailure(Call<Result> call, Throwable t) {
                    Log.w(TAG, "onFailure: " + t.getMessage());
                    showForm();
                }
            });

        }

    }

    @SuppressLint("NewApi")
    private void showLoading(){
        TransitionManager.beginDelayedTransition(container);
        formContainer.setVisibility(View.GONE);
        loader.setVisibility(View.VISIBLE);
    }
    @SuppressLint("NewApi")
    private void showForm(){
        TransitionManager.beginDelayedTransition(container);
        formContainer.setVisibility(View.VISIBLE);
        loader.setVisibility(View.GONE);
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

}