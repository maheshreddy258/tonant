package in.ecoprice.tonant;

import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.CardView;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.support.v4.view.GravityCompat;
import android.support.v7.app.ActionBarDrawerToggle;
import android.view.MenuItem;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;

import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.animation.DecelerateInterpolator;
import android.widget.GridView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;

import java.util.ArrayList;
import java.util.List;

import in.ecoprice.tonant.Adapters.CircularRecyclerViewAdapter;
import in.ecoprice.tonant.Adapters.HomeAdapter;
import in.ecoprice.tonant.Adapters.ProductAdapter;
import in.ecoprice.tonant.Adapters.SlideAdapter;
import in.ecoprice.tonant.Retrofit.Responce.BannerResponse;
import in.ecoprice.tonant.Retrofit.Responce.Homemodel;
import in.ecoprice.tonant.Retrofit.Responce.ProductListResponse;
import in.ecoprice.tonant.Retrofit.SharedPref;
import in.ecoprice.tonant.Retrofit.network.ApiClient;
import in.ecoprice.tonant.Retrofit.network.ApiService;
import retrofit2.Call;
import retrofit2.Callback;

public class Home2Activity extends AppCompatActivity  implements NavigationView.OnNavigationItemSelectedListener {

    private DrawerLayout mDrawerLayout;
    private static final String TAG = "MainActivity";
    private ActionBarDrawerToggle mToggle;
    private CardView AddShop, AddProducts, ShopList, ChangePassword, Profile, MyTargets, MyVisits, Remainders, pendingAmount, mCvmyorders, mCvpendingorders;
    private TextView Name, Date, CCOunt,mobile;
    private ProgressBar progressBar;
    //location
    protected static final String TAG123 = "LocationOnOff";
    private GoogleApiClient googleApiClient;
    final static int REQUEST_LOCATION = 199;
    InternetChecker internetChecker;
    TextView textView;
    int Count = 89;
    int Sceonds = 2000;
    TextView name;
    GridView gridView;
    private ViewPager mViewPager;
    private SlideAdapter slideAdapter;
    List<Homemodel> homemodelList;
    CircularRecyclerViewAdapter circularRecyclerViewAdapter;
    RecyclerView circularrecyclerView;

    boolean isSelect = false;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home2);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        navigationView.setNavigationItemSelectedListener(this);
        View headerView = navigationView.getHeaderView(0);


        LinearLayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
         circularrecyclerView = findViewById(R.id.recycler);
        circularrecyclerView.setLayoutManager(layoutManager);
        initRecyclerView();


        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);


        Name = (TextView)headerView.findViewById(R.id.Namehome);
        SharedPref dataProccessor = new SharedPref(getApplicationContext());
        String usern = dataProccessor.getStr("userName");
        String upperString = usern.substring(0,1).toUpperCase() + usern.substring(1);
        Name.setText("Hi "+upperString);


        homemodelList = new ArrayList<>();
        homemodelList.add(new Homemodel("Add Shop", R.drawable.ic_outline_home_24px));
        homemodelList.add(new Homemodel("Take Order", R.drawable.box));
        homemodelList.add(new Homemodel("Reminders", R.drawable.reminder));
        homemodelList.add(new Homemodel("Visits", R.drawable.visitor));
        homemodelList.add(new Homemodel("Reports", R.drawable.groceries));
        homemodelList.add(new Homemodel("My Orders", R.drawable.order));
        homemodelList.add(new Homemodel("Pending" +"\n"+"Orders", R.drawable.checklist));
      //  homemodelList.add(new Homemodel("Payment"+"\n"+" Status", R.drawable.cash));

        RecyclerView recyclerView = findViewById(R.id.recyclerview);
        HomeAdapter homeAdapter = new HomeAdapter(this, homemodelList);
        recyclerView.setLayoutManager(new GridLayoutManager(this, 4));
        recyclerView.setAdapter(homeAdapter);


        runOnUiThread(new Runnable() {
            public void run() {
                @SuppressLint("ObjectAnimatorBinding") ObjectAnimator anim = ObjectAnimator.ofFloat(progressBar, "progress", 0, 89);
                // @SuppressLint("ObjectAnimatorBinding") ObjectAnimator anim = ObjectAnimator.ofInt(mDonut_progress,"donut_progress",0,78);
                anim.setInterpolator(new DecelerateInterpolator());
                anim.setDuration(Sceonds);
                anim.start();
                anim.addListener(new Animator.AnimatorListener() {
                    @Override
                    public void onAnimationStart(Animator animation) {
                    }
                    @Override
                    public void onAnimationCancel(Animator animation) {
                    }
                    @Override
                    public void onAnimationRepeat(Animator animation) {
                    }
                    @Override
                    public void onAnimationEnd(Animator animation) {
                       /* if (78 >= 100) {
                            Toast.makeText(getContext(),"Hello i'm in 78%",Toast.LENGTH_SHORT).show();
                        }*/
                    }
                });
            }
        });

        this.setFinishOnTouchOutside(true);

        // Todo Location Already on  ... start
        final LocationManager manager = (LocationManager) Home2Activity.this.getSystemService(Context.LOCATION_SERVICE);
        if (manager.isProviderEnabled(LocationManager.GPS_PROVIDER) && hasGPSDevice(Home2Activity.this)) {
            // Toast.makeText(MainActivity.this,"Gps already enabled",Toast.LENGTH_SHORT).show();
            //finish();
        }
        // Todo Location Already on  ... end

        if (!hasGPSDevice(Home2Activity.this)) {
            // Toast.makeText(MainActivity.this,"Gps not Supported",Toast.LENGTH_SHORT).show();
        }

        if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER) && hasGPSDevice(Home2Activity.this)) {
            // Log.e("keshav","Gps already enabled");
            // Toast.makeText(MainActivity.this,"Gps not enabled",Toast.LENGTH_SHORT).show();
            enableLoc();
        } else {
            Log.e("loca", "Gps already enabled");
            // Toast.makeText(MainActivity.this,"Gps already enabled",Toast.LENGTH_SHORT).show();
        }


        viewPager();
    }

    private void viewPager() {

        mViewPager = (ViewPager) findViewById(R.id.viewPager);

        ApiService apiService = ApiClient.getClient().create(ApiService.class);
        Call<List<BannerResponse>> call = apiService.getBanners();
        call.enqueue(new Callback<List<BannerResponse>>() {
            @Override
            public void onResponse(Call<List<BannerResponse>> call, retrofit2.Response<List<BannerResponse>> response) {


                if (response.isSuccessful() && response != null) {

                    List<BannerResponse> Bannerlist = new ArrayList<>();
                    Bannerlist = response.body();
                    if (Bannerlist != null && Bannerlist.size() > 0) {

                        slideAdapter = new SlideAdapter(getApplicationContext(), Bannerlist);
                        mViewPager.setAdapter(slideAdapter);

                    } else {
                        Toast.makeText(getApplicationContext(), "No Banners found", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(getApplicationContext(), "Failed To get the Banners", Toast.LENGTH_SHORT).show();
                }


            }

            @Override
            public void onFailure(Call<List<BannerResponse>> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "" + t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });




    }


    private void startCountAnimation() {
        ValueAnimator animator = ValueAnimator.ofInt(0, Count);
        animator.setDuration(Sceonds);
        animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            public void onAnimationUpdate(ValueAnimator animation) {
                CCOunt.setText(animation.getAnimatedValue().toString() + "/ 100"/*+" \n \n Actual Sales"*/);

            }
        });
        animator.start();
    }


    private boolean hasGPSDevice(Context context) {
        final LocationManager mgr = (LocationManager) context
                .getSystemService(Context.LOCATION_SERVICE);
        if (mgr == null)
            return false;
        final List<String> providers = mgr.getAllProviders();
        if (providers == null)
            return false;
        return providers.contains(LocationManager.GPS_PROVIDER);
    }

    private void enableLoc() {

        if (googleApiClient == null) {
            googleApiClient = new GoogleApiClient.Builder(Home2Activity.this)
                    .addApi(LocationServices.API)
                    .addConnectionCallbacks(new GoogleApiClient.ConnectionCallbacks() {
                        @Override
                        public void onConnected(Bundle bundle) {

                        }

                        @Override
                        public void onConnectionSuspended(int i) {
                            googleApiClient.connect();
                        }
                    })
                    .addOnConnectionFailedListener(new GoogleApiClient.OnConnectionFailedListener() {
                        @Override
                        public void onConnectionFailed(ConnectionResult connectionResult) {

                            Log.d("Location error", "Location error " + connectionResult.getErrorCode());
                        }
                    }).build();
            googleApiClient.connect();

            LocationRequest locationRequest = LocationRequest.create();
            locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
            locationRequest.setInterval(30 * 1000);
            locationRequest.setFastestInterval(5 * 1000);
            LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                    .addLocationRequest(locationRequest);

            builder.setAlwaysShow(true);

            PendingResult<LocationSettingsResult> result =
                    LocationServices.SettingsApi.checkLocationSettings(googleApiClient, builder.build());
            result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
                @Override
                public void onResult(LocationSettingsResult result) {
                    final Status status = result.getStatus();
                    switch (status.getStatusCode()) {
                        case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                            try {
                                // Show the dialog by calling startResolutionForResult(),
                                // and check the result in onActivityResult().
                                status.startResolutionForResult(Home2Activity.this, REQUEST_LOCATION);

                                // finish();
                            } catch (IntentSender.SendIntentException e) {
                                // Ignore the error.
                            }
                            break;
                    }
                }
            });
        }
    }


    private void getVisits(final TextView textView123) {
        String username = SharedPref.getStr("userName");
        String URL_DATA = "http://apiservices.tonantfarmers.com/api/TodayVisitsCount?salesName=" + username;

        StringRequest stringRequest = new StringRequest(Request.Method.GET,
                URL_DATA,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String s) {
                        Log.w(TAG, "String Value " + s);
                        textView123.setText("Today visits " + s);
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.e("Error: ", error.getMessage());
                Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_LONG).show();

            }
        });
        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        requestQueue.add(stringRequest);
    }


    @Override
    protected void onStart() {
        SharedPref dataProccessor = new SharedPref(getApplicationContext());
        boolean status = dataProccessor.getBool("Loginstatus");

        if (!status) {
            startActivity(new Intent(Home2Activity.this, LoginActivity.class));
            finish();
        } else {
            Log.w(TAG, "Loginstatus: " + status);
        }

        super.onStart();
    }
    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.home2, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();
        int id2 = item.getItemId();
       /* if (mToggle.onOptionsItemSelected(item)) {
            return true;
        }*/
      /*  if (id == R.id.Logout) {
            SharedPref.deleteShared();
            startActivity(new Intent(Home2Activity.this, LoginActivity.class));
            finish();
        }*/

        if (id2 == R.id.Profilehome) {
            startActivity(new Intent(Home2Activity.this, ProfileActivity.class));
        }
        if (id2 == R.id.ChangePasswordhome) {
            startActivity(new Intent(Home2Activity.this, ChangePasswordActivity.class));
        }


        return super.onOptionsItemSelected(item);


    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_home) {

            //startActivity(new Intent(Home2Activity.this, Home2Activity.class));
            // Handle the camera action
        } else if (id == R.id.nav_pendingamounts) {

            startActivity(new Intent(Home2Activity.this, PendingAmountActivity.class));


        } else if (id == R.id.nav_targets) {
            startActivity(new Intent(Home2Activity.this, MyTargetsActivity.class));

        } else if (id == R.id.nav_share) {

            Intent sharingIntent = new Intent(Intent.ACTION_SEND);
            sharingIntent.setType("text/html");
            sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, Html.fromHtml("<p>Follow Tonant Farmers@Tonant!.." +
                    "https://play.google.com/store/apps/details?id=in.ecoprice.tonant&hl=en</p>"));
            startActivity(Intent.createChooser(sharingIntent, "Share using"));



        } else if (id == R.id.nav_contactus) {
            startActivity(new Intent(Home2Activity.this, ContactusActivity.class));
        }

        else if (id == R.id.navlogout) {
            SharedPref.deleteShared();
            startActivity(new Intent(Home2Activity.this, LoginActivity.class));
            finish();
        }

     else if (id == R.id.nav_aboutus) {
        startActivity(new Intent(Home2Activity.this, AboutusActivity.class));
    }


        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void initRecyclerView(){

        ApiService apiService = ApiClient.getClient().create(ApiService.class);
        Call<List<ProductListResponse>> productListResponseCall = apiService.getProductList();
        productListResponseCall.enqueue(new Callback<List<ProductListResponse>>() {
            @Override
            public void onResponse(Call<List<ProductListResponse>> call, retrofit2.Response<List<ProductListResponse>> response) {

                if (response.isSuccessful() && response != null) {

                    List<ProductListResponse> productList = new ArrayList<>();
                    productList = response.body();

                    if (productList != null && productList.size() > 0) {


                        circularRecyclerViewAdapter = new CircularRecyclerViewAdapter(productList,getApplicationContext());
                        Log.e("productlist", "sucesss....");
                        circularrecyclerView.setAdapter(circularRecyclerViewAdapter);

                    } else {
                        Toast.makeText(getApplicationContext(), "No Products found", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(getApplicationContext(), "Failed To get the Products List", Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<List<ProductListResponse>> call, Throwable t) {

                Toast.makeText(getApplicationContext(), "" + t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });


    }



    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            Fragment fragment;

            switch (item.getItemId()) {
                case R.id.navigation_shop:


                       // item.setIcon(R.drawable.ic_outline_home_24px);
                   // toolbar.setTitle("Shop");
                    return true;
                case R.id.navigation_gifts:


                    //item.setIcon(R.drawable.ic_outline_home_24px);
                   // toolbar.setTitle("My Gifts");
                    return true;

                case R.id.navigation_profile:
                    startActivity(new Intent(Home2Activity.this, ProfileActivity.class));
                    //item.toolbar.setTitle("Profile");
                    return true;
            }
            return false;
        }
    };
}
