package in.ecoprice.tonant;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import in.ecoprice.tonant.Adapters.OrdersViewAdapter;
import in.ecoprice.tonant.Models.OrdersList;
import in.ecoprice.tonant.Models.TodayVisits;

public class OrderListViewActviity extends AppCompatActivity {


    private RecyclerView Gal_Rec;
    private RecyclerView.Adapter adapter;
    private List<OrdersList> listItems;
    public static List<OrdersList> listItems1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_list_view_actviity);
        // TextView textView = (TextView) findViewById(R.id.tagsname);
        String name = getIntent().getStringExtra("name");
        getSupportActionBar().setTitle(name);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Gal_Rec = (RecyclerView) findViewById(R.id.Rec_Gallery);
        Gal_Rec.setHasFixedSize(true);
        Gal_Rec.setNestedScrollingEnabled(false);
        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(getApplicationContext(), 1);
        Gal_Rec.setLayoutManager(mLayoutManager);
        listItems = new ArrayList<>();
        listItems1 = new ArrayList<>();
        Intent intent = getIntent();

        TextView  mTotal = (TextView) findViewById(R.id.Total);
        TextView mPending = (TextView) findViewById(R.id.Pending);
        TextView  mNetPay = (TextView) findViewById(R.id.NetPay);
        mTotal.setText("Total "+intent.getStringExtra("total"));
        mPending.setText("Pending "+intent.getStringExtra("pending") );
        mNetPay.setText("Netpay "+ intent.getStringExtra("netpay"));
        String jsonArray = intent.getStringExtra("jsonArray");
        try {
            //  JSONObject jsonObject = new JSONObject(jsonArray);
            JSONArray array = new JSONArray(jsonArray);
            for(int i=0; i<array.length(); i++ ){
                JSONObject obj =array.getJSONObject(i);
                OrdersList item = new OrdersList(
                        obj.getString("Id"),
                        obj.getString("OrderId"),
                        obj.getString("ProductId"),
                        obj.getString("ProductName"),
                        obj.getString("Qty"),
                        obj.getString("UnitPrice"),
                        obj.getString("Price"),
                        obj.getString("ProductWait")

                );
                listItems = listItems1;
                listItems.add(item);
            }
            adapter = new OrdersViewAdapter(listItems , getApplicationContext());
            Gal_Rec.setAdapter(adapter);

        } catch (JSONException e) {
            e.printStackTrace();
        }





    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

}