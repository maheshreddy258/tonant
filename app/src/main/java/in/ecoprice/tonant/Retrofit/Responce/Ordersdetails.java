package in.ecoprice.tonant.Retrofit.Responce;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Ordersdetails  implements Serializable
{

    @SerializedName("Id")
    @Expose
    public Integer id;
    @SerializedName("OrderId")
    @Expose
    public Integer orderId;
    @SerializedName("ProductId")
    @Expose
    public Integer productId;
    @SerializedName("ProductName")
    @Expose
    public String productName;
    @SerializedName("Qty")
    @Expose
    public Double qty;
    @SerializedName("UnitPrice")
    @Expose
    public Double unitPrice;
    @SerializedName("Discount")
    @Expose
    public Double discount;
    @SerializedName("Price")
    @Expose
    public Double price;
    @SerializedName("Status")
    @Expose
    public Integer status;
    @SerializedName("DeliveryStatus")
    @Expose
    public Integer deliveryStatus;
    @SerializedName("TrackMessage")
    @Expose
    public String trackMessage;
    @SerializedName("Createdate")
    @Expose
    public String createdate;
    @SerializedName("DeliveryDate")
    @Expose
    public String deliveryDate;
    @SerializedName("DeliveryAssignStatus")
    @Expose
    public Integer deliveryAssignStatus;
    @SerializedName("ProductWait")
    @Expose
    public String productWait;
    @SerializedName("Weight")
    @Expose
    public Double weight;
    @SerializedName("QRCODEIMg")
    @Expose
    public String qRCODEIMg;
    @SerializedName("TotalAmount")
    @Expose
    public Double totalAmount;
    @SerializedName("ProductImage")
    @Expose
    public String productImage;
    private final static long serialVersionUID = -3169699284415950229L;
}
