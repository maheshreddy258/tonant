package in.ecoprice.tonant;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import in.ecoprice.tonant.Retrofit.SharedPref;

public class ProfileActivity extends AppCompatActivity {

    private Toolbar mToolbar;
    private String string = "";
    private TextView mProName, mProUsername, mProDesignation, mProDOB, mProEmail, mProPhonenumber, mProAlternative;
    InternetChecker internetChecker;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        mToolbar = (Toolbar) findViewById(R.id.main_page_toolbar);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setTitle("Profile");
        mToolbar.setTitleTextColor(0xFFFFFFFF);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.arrowleft);
        internetChecker = new InternetChecker(this);

        mProName = (TextView) findViewById(R.id.ProName);
        mProUsername = (TextView) findViewById(R.id.ProUsername);
        mProDesignation = (TextView) findViewById(R.id.ProDesignation);
        mProDOB = (TextView) findViewById(R.id.ProDOB);
        mProEmail = (TextView) findViewById(R.id.ProEmail);
        mProPhonenumber = (TextView) findViewById(R.id.ProPhoneNumber);
        mProAlternative = (TextView) findViewById(R.id.Alternative);
        /*banar1 = (CircularTextView) findViewById(R.id.banar1);
        banar1.setStrokeWidth(1);
        banar1.setStrokeColor("#e7e7e7");
        banar1.setSolidColor("#f1f1f1");*/

        getJsonData();

    }

    private void getJsonData() {
        RequestQueue queue = Volley.newRequestQueue(this);
        SharedPref dataProccessor = new SharedPref(getApplicationContext());
        final String username = dataProccessor.getStr("userName");
        final String url = "http://apiservices.tonantfarmers.com/api/MyProfileInfo?UserName=" + username;
        JsonObjectRequest getRequest = new JsonObjectRequest(Request.Method.GET, url, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d("Response", response.toString());
                        try {

                            String UserName = response.getString("UserName");
                            String Name = response.getString("Name");
                            String Designation = response.getString("Designation");
                            String Email = response.getString("Email");
                            String PhoneNumber = response.getString("PhoneNumber");
                            String Alternativenumber = response.getString("Alternativenumber");
                            String DOB = response.getString("DOB");

                            mProName.setText(Name);
                            mProUsername.setText(UserName);
                            mProDesignation.setText(Designation);
                            mProEmail.setText(Email);
                            mProPhonenumber.setText(PhoneNumber);
                            mProAlternative.setText(Alternativenumber);
                            mProDOB.setText(DOB);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d("Error.Response", String.valueOf(error));
                        getJsonData();
                    }
                }
        );
        queue.add(getRequest);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

}