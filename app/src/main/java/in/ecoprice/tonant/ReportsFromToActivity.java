package in.ecoprice.tonant;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;

import java.text.SimpleDateFormat;
import java.util.Calendar;

public class ReportsFromToActivity extends AppCompatActivity {

    private static final String TAG = "reportsFromDateToDate";
    TextInputLayout fromDate,toDate;
    private int mYear, mMonth, mDay, mHour, mMinute;
    private Button BtnGetReports;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reports_from_to);
        getSupportActionBar().setTitle("Reports");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        fromDate = (TextInputLayout) findViewById(R.id.fromDate);
        toDate = (TextInputLayout) findViewById(R.id.toDate);
        BtnGetReports = (Button) findViewById(R.id.BtnGetReports);

        fromDate.getEditText().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Get Current Date
                final Calendar c = Calendar.getInstance();
                mYear = c.get(Calendar.YEAR);
                mMonth = c.get(Calendar.MONTH);
                mDay = c.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog datePickerDialog = new DatePickerDialog(ReportsFromToActivity.this,
                        new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {

                                @SuppressLint("SimpleDateFormat")
                                SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy");
                                c.set(year, monthOfYear , dayOfMonth);
                                String dateString = sdf.format(c.getTime());
                                fromDate.getEditText().setText(dateString);

                            }
                        }, mYear, mMonth, mDay);
                datePickerDialog.show();
            }
        });

        toDate.getEditText().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Get Current Date
                final Calendar c = Calendar.getInstance();
                mYear = c.get(Calendar.YEAR);
                mMonth = c.get(Calendar.MONTH);
                mDay = c.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog datePickerDialog = new DatePickerDialog(ReportsFromToActivity.this,
                        new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {

                                @SuppressLint("SimpleDateFormat")
                                SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy");
                                c.set(year, monthOfYear , dayOfMonth);
                                String dateString = sdf.format(c.getTime());
                                toDate.getEditText().setText(dateString);

                            }
                        }, mYear, mMonth, mDay);
                datePickerDialog.show();
            }
        });


        BtnGetReports.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(validate(fromDate)){
                    Log.w(TAG,"From Date Null");
                }else if(validate(toDate)){
                    Log.w(TAG,"to Date Null");
                }else {
                 String fromDatestr = fromDate.getEditText().getText().toString();
                 String toDatestr = toDate.getEditText().getText().toString();
                 Log.w(TAG,"fromDatestr =>: "+fromDatestr);
                 Log.w(TAG,"toDatestr =>: "+toDatestr);
                 Intent intent = new Intent(ReportsFromToActivity.this,ReportsTodayActivity.class);
                 intent.putExtra("fromDatestr",fromDatestr);
                 intent.putExtra("toDatestr",toDatestr);
                 startActivity(intent);
                 finish();
                 }
            }
        });
    }

    public boolean validate(TextInputLayout textInputEditText) {
        boolean valid = false;
        textInputEditText.setEnabled(true);
        textInputEditText.setError(null);
        String  textInputEditText123 = textInputEditText.getEditText().getText().toString();
        if (textInputEditText123.length() == 0) {
            textInputEditText.setError("Required");
            textInputEditText.getEditText().requestFocus();
            valid = true;
        } else {
            textInputEditText.setError(null);
        }

        return valid;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }

    }
}
