package in.ecoprice.tonant.Adapters;


import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputLayout;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import in.ecoprice.tonant.Models.ProductsList;
import in.ecoprice.tonant.R;
import in.ecoprice.tonant.newChanges.ModelProducts;
import in.ecoprice.tonant.newChanges.NewModelCart;

import static com.android.volley.VolleyLog.TAG;

/**
 * Created by nani on 11/17/2017.
 */

public class ProductBusinessAdapter extends RecyclerView.Adapter<ProductBusinessAdapter.ViewHolder>  {
    private List<ProductsList> listitems;
    private Context ctx;
    public ProductBusinessAdapter(List<ProductsList> listitems, Context ctx) {
        this.listitems = listitems;
        this.ctx = ctx;

    }
    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.product_item_business, parent, false);

        return new ViewHolder(v, ctx, listitems);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, @SuppressLint("RecyclerView") final int position) {
        final ProductsList listItem = listitems.get(position);

        holder.ProductName.setText(listItem.getProductName());
       // holder.TextPro.getEditText().setText("0");


        }



    @Override
    public int getItemCount() {
        return listitems.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    public class ViewHolder extends RecyclerView.ViewHolder /*implements View.OnClickListener*/ {

        private TextView ProductName;
        private TextInputLayout TextPro;
        private LinearLayout MainPro;
        Context ctx;
        ArrayList<ProductsList> completeds = new ArrayList<ProductsList>();

        @SuppressLint("CutPasteId")
        public ViewHolder(final View View, final Context ctx, final List<ProductsList> completeds) {
            super(View);

            /* View.setOnClickListener(this);*/
            this.completeds = (ArrayList<ProductsList>) completeds;
            this.ctx = ctx;
            ProductName = (TextView) View.findViewById(R.id.ProductName);
            TextPro = (TextInputLayout) View.findViewById(R.id.TextPro);
            MainPro = (LinearLayout) View.findViewById(R.id.MainPro);


            }


    }




    }

