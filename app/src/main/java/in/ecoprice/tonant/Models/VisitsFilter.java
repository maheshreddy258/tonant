package in.ecoprice.tonant.Models;

import android.widget.Filter;

import java.util.ArrayList;
import java.util.List;

import in.ecoprice.tonant.Adapters.AllVisitsAdapter;

public class VisitsFilter extends Filter {

    AllVisitsAdapter adapter;
    List<TodayVisits> filterList;

    public VisitsFilter(List<TodayVisits> filterList, AllVisitsAdapter adapter)
    {
        this.adapter=adapter;
        this.filterList=filterList;

    }
    @Override
    protected FilterResults performFiltering(CharSequence constraint) {
        FilterResults results=new FilterResults();


        if(constraint != null && constraint.length() > 0)
        {
            constraint=constraint.toString().toUpperCase();
            List<TodayVisits> filteredPlayers=new ArrayList<>();


            for (int i=0;i<filterList.size();i++)
            {
                String firLast = filterList.get(i).getId();

                if(firLast.toUpperCase().contains(constraint))
                {
                    filteredPlayers.add(filterList.get(i));
                } else if (filterList.get(i).getId().toUpperCase().contains(constraint)) {
                    filteredPlayers.add(filterList.get(i));
                }


                String area = filterList.get(i).getShopkeeparName();

                if(area.toUpperCase().contains(constraint))
                {
                    filteredPlayers.add(filterList.get(i));
                } else if (filterList.get(i).getShopkeeparName().toUpperCase().contains(constraint)) {
                    filteredPlayers.add(filterList.get(i));
                }


                String mobile = filterList.get(i).getUserName();

                if(mobile.toUpperCase().contains(constraint))
                {
                    filteredPlayers.add(filterList.get(i));
                } else if (filterList.get(i).getUserName().toUpperCase().contains(constraint)) {
                    filteredPlayers.add(filterList.get(i));
                }

            }

            results.count=filteredPlayers.size();
            results.values=filteredPlayers;
        }else
        {
            results.count=filterList.size();
            results.values=filterList;

        }

        return results;
    }

    @Override
    protected void publishResults(CharSequence constraint, FilterResults results) {

        adapter.listitems= (List<TodayVisits>) results.values;

        adapter.notifyDataSetChanged();
    }



}

