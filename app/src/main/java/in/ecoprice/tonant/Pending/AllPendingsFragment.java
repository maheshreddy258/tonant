package in.ecoprice.tonant.Pending;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.volley.Cache;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.ParseError;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import in.ecoprice.tonant.Adapters.PendingVisitsAdapter;
import in.ecoprice.tonant.InternetChecker;
import in.ecoprice.tonant.Models.PendingAmounts;
import in.ecoprice.tonant.Models.TodayVisits;
import in.ecoprice.tonant.PendingAmountActivity;
import in.ecoprice.tonant.R;
import in.ecoprice.tonant.Retrofit.SharedPref;

/**
 * A simple {@link Fragment} subclass.
 */
public class AllPendingsFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener {


    public AllPendingsFragment() {
        // Required empty public constructor
    }
    Context context;
    private RecyclerView Gal_Rec;
    private PendingVisitsAdapter adapter;
    private List<PendingAmounts> listItems;
    public static List<PendingAmounts> listItems1;
    private ProgressBar progressBar;
    private InternetChecker internetChecker;
   // SwipeRefreshLayout mSwipeRefreshLayout;
    private Context mContext;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_all_pendings, container, false);
        Gal_Rec = (RecyclerView) view.findViewById(R.id.Rec_Gallery);
        Gal_Rec.setHasFixedSize(true);
        Gal_Rec.setNestedScrollingEnabled(false);
        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(mContext, 1);
        Gal_Rec.setLayoutManager(mLayoutManager);
        listItems = new ArrayList<>();
        listItems1 = new ArrayList<>();
        progressBar = (ProgressBar) view.findViewById(R.id.Prog_Assigned);

        // SwipeRefreshLayout
        /*mSwipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.swipe_container);
        mSwipeRefreshLayout.setOnRefreshListener(this);
        mSwipeRefreshLayout.setColorSchemeResources(R.color.colorPrimary,
                android.R.color.holo_green_dark,
                android.R.color.holo_orange_dark,
                android.R.color.holo_blue_dark);*/

       //  context.getApplicationContext();
         /*mSwipeRefreshLayout.post(new Runnable() {

            @Override
            public void run() {

                mSwipeRefreshLayout.setRefreshing(true);

                // Fetching data from server

            }
        });*/
        loadRecyclerViewData();
      return  view;
    }

    private void loadRecyclerViewData() {
       // mSwipeRefreshLayout.setRefreshing(true);
        progressBar.setVisibility(View.VISIBLE);
        Gal_Rec.setVisibility(View.GONE);
        String username = SharedPref.getStr("userName");
        String URL_DATA = "http://apiservices.tonantfarmers.com/api/MyLeadspendingbalance?UserName=" + username;
        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(URL_DATA, new Response.Listener<JSONArray>() {

            @Override
            public void onResponse(JSONArray response) {
                // mSwipeRefreshLayout.setRefreshing(false);
                /*  progressDialog.dismiss();*/
                Gal_Rec.setVisibility(View.VISIBLE);
                progressBar.setVisibility(View.GONE);
                try {
                    if (response.length() > 0) {
                        for (int i = 0; i < response.length(); i++) {
                            JSONObject obj = response.getJSONObject(i);
                            PendingAmounts item = new PendingAmounts(
                                    obj.getString("Id"),
                                    obj.getString("First_Name"),
                                    obj.getString("Last_Name"),
                                    obj.getString("CROEMailID"),
                                    obj.getString("MobileNumber"),
                                    obj.getString("ShopName"),
                                    obj.getString("ShopAddress"),
                                    obj.getString("CreateDate"),
                                    obj.getString("CreateBy"),
                                    obj.getString("LeadType"),
                                    obj.getString("UserName"),
                                    obj.getString("PendingBill")

                            );
                            listItems = listItems1;
                            listItems.add(item);
                        }
                        adapter = new PendingVisitsAdapter(listItems, mContext);
                        Gal_Rec.setAdapter(adapter);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
              //  mSwipeRefreshLayout.setRefreshing(false);

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
               // mSwipeRefreshLayout.setRefreshing(false);
                Gal_Rec.setVisibility(View.GONE);
                progressBar.setVisibility(View.GONE);
                VolleyLog.e("Error: ", error.getMessage());
                // Toast.makeText(mContext, "Try Again ", Toast.LENGTH_LONG).show();

            }
        }){

            @Override
            protected Response<JSONArray> parseNetworkResponse(NetworkResponse response) {
                try {
                    Cache.Entry cacheEntry = HttpHeaderParser.parseCacheHeaders(response);
                    if (cacheEntry == null) {
                        cacheEntry = new Cache.Entry();
                    }
                    final long cacheHitButRefreshed = 1 * 60 * 1000; // in 3 minutes cache will be hit, but also refreshed on background
                    final long cacheExpired = 24 * 60 * 60 * 1000; // in 24 hours this cache entry expires completely
                    long now = System.currentTimeMillis();
                    final long softExpire = now + cacheHitButRefreshed;
                    final long ttl = now + cacheExpired;
                    cacheEntry.data = response.data;
                    cacheEntry.softTtl = softExpire;
                    cacheEntry.ttl = ttl;
                    String headerValue;
                    headerValue = response.headers.get("Date");
                    if (headerValue != null) {
                        cacheEntry.serverDate = HttpHeaderParser.parseDateAsEpoch(headerValue);
                    }
                    headerValue = response.headers.get("Last-Modified");
                    if (headerValue != null) {
                        cacheEntry.lastModified = HttpHeaderParser.parseDateAsEpoch(headerValue);
                    }
                    cacheEntry.responseHeaders = response.headers;
                    final String jsonString = new String(response.data,
                            HttpHeaderParser.parseCharset(response.headers));
                    return Response.success(new JSONArray(jsonString), cacheEntry);
                } catch (UnsupportedEncodingException e) {
                    return Response.error(new ParseError(e));
                } catch (JSONException e) {
                    return Response.error(new ParseError(e));
                }
            }

            @Override
            protected void deliverResponse(JSONArray response) {
                super.deliverResponse(response);
            }

            @Override
            public void deliverError(VolleyError error) {
                super.deliverError(error);
            }

            @Override
            protected VolleyError parseNetworkError(VolleyError volleyError) {
                return super.parseNetworkError(volleyError);
            }
        };


        jsonArrayRequest.setRetryPolicy(new DefaultRetryPolicy(
                0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        requestQueue.add(jsonArrayRequest);



    }


    @Override
    public void onRefresh() {
        loadRecyclerViewData();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
    }



}
