package in.ecoprice.tonant.Models;

public class Orders {
    private String Id,ProductName,SoldQty;

    public Orders(String id, String productName, String soldQty) {
        Id = id;
        ProductName = productName;
        SoldQty = soldQty;
    }

    public String getId() {
        return Id;
    }

    public void setId(String id) {
        Id = id;
    }

    public String getProductName() {
        return ProductName;
    }

    public void setProductName(String productName) {
        ProductName = productName;
    }

    public String getSoldQty() {
        return SoldQty;
    }

    public void setSoldQty(String soldQty) {
        SoldQty = soldQty;
    }
}
