package in.ecoprice.tonant.Models;

import android.widget.Filter;
import java.util.ArrayList;
import java.util.List;

import in.ecoprice.tonant.Adapters.ShoplistAdapter;

public class CustomFilter extends Filter {

    ShoplistAdapter adapter;
    List<Shoplist> filterList;

    public CustomFilter(List<Shoplist> filterList, ShoplistAdapter adapter)
    {
        this.adapter=adapter;
        this.filterList=filterList;

    }
    @Override
    protected FilterResults performFiltering(CharSequence constraint) {
        FilterResults results=new FilterResults();


        if(constraint != null && constraint.length() > 0)
        {
            constraint=constraint.toString().toUpperCase();
            List<Shoplist> filteredPlayers=new ArrayList<>();


            for (int i=0;i<filterList.size();i++)
            {
                String firLast = filterList.get(i).getFirst_Name()+" "+filterList.get(i).getLast_Name();

                if(firLast.toUpperCase().contains(constraint))
                {
                    filteredPlayers.add(filterList.get(i));
                } else if (filterList.get(i).getShopName().toUpperCase().contains(constraint)) {
                    filteredPlayers.add(filterList.get(i));
                }


                String area = filterList.get(i).getShopAddress();

                if(area.toUpperCase().contains(constraint))
                {
                    filteredPlayers.add(filterList.get(i));
                } else if (filterList.get(i).getShopAddress().toUpperCase().contains(constraint)) {
                    filteredPlayers.add(filterList.get(i));
                }


                String mobile = filterList.get(i).getMobileNumber();

                if(mobile.toUpperCase().contains(constraint))
                {
                    filteredPlayers.add(filterList.get(i));
                } else if (filterList.get(i).getMobileNumber().toUpperCase().contains(constraint)) {
                    filteredPlayers.add(filterList.get(i));
                }


                String id = filterList.get(i).getId();

                if(id.toUpperCase().contains(constraint))
                {
                    filteredPlayers.add(filterList.get(i));
                } else if (filterList.get(i).getId().toUpperCase().contains(constraint)) {
                    filteredPlayers.add(filterList.get(i));
                }

            }

            results.count=filteredPlayers.size();
            results.values=filteredPlayers;
        }else
        {
            results.count=filterList.size();
            results.values=filterList;

        }

        return results;
    }

    @Override
    protected void publishResults(CharSequence constraint, FilterResults results) {

        adapter.listitems= (List<Shoplist>) results.values;

        adapter.notifyDataSetChanged();
    }



}

