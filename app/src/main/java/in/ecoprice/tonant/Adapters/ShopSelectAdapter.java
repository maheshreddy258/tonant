package in.ecoprice.tonant.Adapters;


import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import in.ecoprice.tonant.CheckOutActivity;
import in.ecoprice.tonant.Models.CustomFilter;
import in.ecoprice.tonant.Models.Products;
import in.ecoprice.tonant.Models.SelectShopFilter;
import in.ecoprice.tonant.Models.Shoplist;
import in.ecoprice.tonant.R;

/**
 * Created by nani on 11/17/2017.
 */

public class ShopSelectAdapter extends RecyclerView.Adapter<ShopSelectAdapter.ViewHolder> implements Filterable {
     public List<Shoplist> listitems;
    private Context ctx;
    SelectShopFilter filter;
    public ShopSelectAdapter(List<Shoplist> listitems, Context ctx) {
        this.listitems = listitems;
        this.ctx = ctx;

    }


   /* public GralleryAdapter(List<Gallery> listitems, Context ctx) {
        this.listitems = listitems;
        this.ctx = ctx;
    }*/

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.select_item, parent, false);

        return new ViewHolder(v, ctx, listitems);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {
        final Shoplist listItem = listitems.get(position);

        holder.title.setText(listItem.getShopName());



        }

    @Override
    public int getItemCount() {
        return listitems.size();
    }

    @Override
    public Filter getFilter() {
        if(filter==null)
        {
            filter=new SelectShopFilter(listitems,this);
        }

        return filter;
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private TextView title;

        Context ctx;
        ArrayList<Shoplist> completeds = new ArrayList<Shoplist>();

        @SuppressLint("CutPasteId")
        public ViewHolder(View View, final Context ctx, List<Shoplist> completeds) {
            super(View);

            View.setOnClickListener(this);
            this.completeds = (ArrayList<Shoplist>) completeds;
            this.ctx = ctx;
            title = (TextView) View.findViewById(R.id.SelectName);





        }

        @Override
        public void onClick(View v) {
            int position = getAdapterPosition();
            Shoplist posts = this.completeds.get(position);
           // Toast.makeText(ctx, "Selected Item : " + position, Toast.LENGTH_SHORT).show();
            Intent intent = new Intent(this.ctx, CheckOutActivity.class);
            intent.putExtra("ShopUserName", posts.getMobileNumber());
            intent.putExtra("ShopName", posts.getShopName());
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            this.ctx.startActivity(intent);




        }

    }
}


