package in.ecoprice.tonant.Retrofit.Responce;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import in.ecoprice.tonant.Models.Cart;

public class Example {
    @SerializedName("ShopUserName")
    @Expose
    private String ShopUserName;
    @SerializedName("ShopName")
    @Expose
    private String ShopName;
    @SerializedName("SalesLogin")
    @Expose
    private String SalesLogin;
    @SerializedName("NetAmount")
    @Expose
    private double NetAmount;
    @SerializedName("paidthrough")
    @Expose
    private String paidthrough;


    @SerializedName("paidAmount")
    @Expose
    private double paidAmount;
    @SerializedName("Quantity")
    @Expose
    private int Quantity;
    @SerializedName("PaymentStatus")
    @Expose
    private boolean PaymentStatus;

   /* @SerializedName("CartItems")
    @Expose
    private List<Cart> CartItems = null;
*/

    public Example() {

    }

    @SerializedName("CartItems")
    @Expose
    private List<Cart> agendaWeekDays = null;

    public String getShopUserName() {
        return ShopUserName;
    }

    public void setShopUserName(String shopUserName) {
        ShopUserName = shopUserName;
    }

    public String getShopName() {
        return ShopName;
    }

    public void setShopName(String shopName) {
        ShopName = shopName;
    }

    public String getSalesLogin() {
        return SalesLogin;
    }

    public void setSalesLogin(String salesLogin) {
        SalesLogin = salesLogin;
    }

    public double getNetAmount() {
        return NetAmount;
    }

    public void setNetAmount(double netAmount) {
        NetAmount = netAmount;
    }

    public String getPaidthrough() {
        return paidthrough;
    }

    public void setPaidthrough(String paidthrough) {
        this.paidthrough = paidthrough;
    }

    public double getPaidAmount() {
        return paidAmount;
    }

    public void setPaidAmount(double paidAmount) {
        this.paidAmount = paidAmount;
    }

    public int getQuantity() {
        return Quantity;
    }

    public void setQuantity(int quantity) {
        Quantity = quantity;
    }

    public boolean isPaymentStatus() {
        return PaymentStatus;
    }

    public void setPaymentStatus(boolean paymentStatus) {
        PaymentStatus = paymentStatus;
    }

    public List<Cart> getAgendaWeekDays() {
        return agendaWeekDays;
    }

    public void setAgendaWeekDays(List<Cart> agendaWeekDays) {
        this.agendaWeekDays = agendaWeekDays;
    }
}

