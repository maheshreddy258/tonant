package in.ecoprice.tonant.Pending;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import in.ecoprice.tonant.Visits.AllVisitsFragment;
import in.ecoprice.tonant.Visits.TodayVisitsFragment;

/**
 * Created by Nani on 11/06/17.
 */

public class PendingPagerAdapter extends FragmentStatePagerAdapter {

   Context context;
    public PendingPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {

        switch(position) {
            case 0:
                TodayPendingsFragment dailyFragment = new TodayPendingsFragment();
                return dailyFragment;

            /*case 1:
                PendingAmoutFragment PendingAmoutFragment = new PendingAmoutFragment();
                return  PendingAmoutFragment;*/

            case 1:
                AllPendingsFragment weeklyFragment = new AllPendingsFragment();
                return  weeklyFragment;

            default:
                return  null;
        }

    }





    @Override
    public int getCount() {
        return 2;
    }

    public CharSequence getPageTitle(int position){

        switch (position) {
            case 0:
                return "Today Pendings";

           /* case 1:
                return "Pending Amounts";*/

            case 1:
                return "All Pendings";

            default:
                return null;
        }

    }



}
