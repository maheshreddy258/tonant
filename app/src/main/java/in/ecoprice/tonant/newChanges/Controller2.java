package in.ecoprice.tonant.newChanges;

import java.util.ArrayList;
import android.app.Application;

public class Controller2 extends Application{

    private  ArrayList<ModelProducts> myProducts = new ArrayList<ModelProducts>();
    private  NewModelCart myCart = new NewModelCart();


    public ModelProducts getProducts(int pPosition) {

        return myProducts.get(pPosition);
    }

    public void setProducts(ModelProducts Products) {

        myProducts.add(Products);

    }

    public NewModelCart getCart() {

        return myCart;

    }

    public int getProductsArraylistSize() {

        return myProducts.size();
    }

}
