package in.ecoprice.tonant.Adapters;


import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import cn.pedant.SweetAlert.SweetAlertDialog;
import in.ecoprice.tonant.Models.Orders;
import in.ecoprice.tonant.Models.TodayVisits;
import in.ecoprice.tonant.Models.VisitsFilter;
import in.ecoprice.tonant.OrderListViewActviity;
import in.ecoprice.tonant.R;

import static com.android.volley.VolleyLog.TAG;

public class AllVisitsAdapter extends RecyclerView.Adapter<AllVisitsAdapter.ViewHolder> implements Filterable {
     public List<TodayVisits> listitems;
    private Context ctx;
    VisitsFilter filter;


    public AllVisitsAdapter(List<TodayVisits> listitems, Context ctx) {
        this.listitems = listitems;
        this.ctx = ctx;

    }
    public AllVisitsAdapter() {


    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.today_visits, parent, false);

        return new ViewHolder(v, ctx, listitems);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {
        final TodayVisits listItem = listitems.get(position);
        holder.ShopName.setText(listItem.getShopkeeparName());
        holder.Username.setText(listItem.getUserName());
        holder.Total.setText("Total "+listItem.getTotal());
        holder.NetPay.setText("NetPay "+listItem.getNetPay());
        holder.Pending.setText("Pending "+listItem.getPendingAmount());
        holder.TotalQntity.setText("TotalQntity : "+listItem.getQuanity());
        holder.TodayDate.setText("Date : "+parseTime(listItem.getCreateddate()));



        Date c = Calendar.getInstance().getTime();
        SimpleDateFormat df = new SimpleDateFormat("yyyy");
        String formattedDate = df.format(c);

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        SimpleDateFormat output = new SimpleDateFormat("yyyy");
        Date d = null;
        // Date CreatedDate = null;
        try {
            d = sdf.parse(listItem.getCreateddate());
            //    CreatedDate = sdf.parse(listItem.getCreateddate());
        } catch (ParseException e) {
            e.printStackTrace();
        }
        String formattedTime = output.format(d);
        holder.TodayDate.setText("Date : "+formattedTime);
        Log.w(TAG," Today Date := "+formattedDate+" Visit Date := "+formattedTime);

        if (formattedDate.equals(formattedTime)){
            holder.Card.setVisibility(View.VISIBLE);
        }else {
            holder.Card.setVisibility(View.GONE);

        }



    }

    @Override
    public int getItemCount() {
        return listitems.size();
    }

    @Override
    public Filter getFilter() {
        if(filter==null)
        {
            filter=new VisitsFilter(listitems,this);
        }

        return filter;
    }


    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private TextView ShopName,Username,Total,NetPay,Pending,TotalQntity,TodayDate;
        Context ctx;
        private LinearLayout Card;
        ArrayList<TodayVisits> completeds = new ArrayList<TodayVisits>();

        @SuppressLint("CutPasteId")
        public ViewHolder(View View, final Context ctx, List<TodayVisits> completeds) {
            super(View);

            View.setOnClickListener(this);
            this.completeds = (ArrayList<TodayVisits>) completeds;
            this.ctx = ctx;
            ShopName = (TextView) View.findViewById(R.id.ShopName);
            Username = (TextView) View.findViewById(R.id.Username);
            Total = (TextView) View.findViewById(R.id.Total);
            Pending = (TextView) View.findViewById(R.id.Pending);
            NetPay = (TextView) View.findViewById(R.id.NetPay);
            TotalQntity = (TextView) View.findViewById(R.id.TotalQntity);
            TodayDate = (TextView) View.findViewById(R.id.TodayDate);
            Card = (LinearLayout) View.findViewById(R.id.Card);
            }
        @Override
        public void onClick(View view) {

            int position = getAdapterPosition();
            TodayVisits posts = this.completeds.get(position);
            Intent intent = new Intent(this.ctx, OrderListViewActviity.class);
            intent.putExtra("name", posts.getShopkeeparName());
            intent.putExtra("pending", posts.getPendingAmount());
            intent.putExtra("total", posts.getTotal());
            intent.putExtra("netpay", posts.getNetPay());
            intent.putExtra("pos", position);
            intent.putExtra("jsonArray", posts.getOrdersList());
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            this.ctx.startActivity(intent);


           /* int position = getAdapterPosition();
            TodayVisits posts = this.completeds.get(position);
            String product = "";
            final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(ctx, android.R.layout.select_dialog_item);
            for(int i=0; i<posts.getOrdersList().length(); i++ ){
                JSONObject obj = null;
                try {
                    obj = posts.getOrdersList().getJSONObject(i);
                    OrdersList item = new OrdersList(
                            obj.getString("Id"),
                            obj.getString("OrderId"),
                            obj.getString("ProductId"),
                            obj.getString("ProductName"),
                            obj.getString("Qty"),
                            obj.getString("UnitPrice"),
                            obj.getString("Price"),
                            obj.getString("ProductWait")
                    );
                   String name  = item.getProductName();
                   String qntity  = item.getQty();
                   String price  = item.getPrice();
                   String wait  = item.getProductWait();
                   product = " "+name+" "+qntity+"  "+price+"  "+wait;
                   arrayAdapter.add(product);

                } catch (JSONException e) {
                    e.printStackTrace();
                }
                }

            AlertDialog.Builder builderSingle = new AlertDialog.Builder(ctx);
            // builderSingle.setIcon(R.drawable.ic_launcher);
            builderSingle.setTitle("Product List");
            builderSingle.setNegativeButton("Ok", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });

            builderSingle.setAdapter(arrayAdapter, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    String strName = arrayAdapter.getItem(which);
                        *//*AlertDialog.Builder builderInner = new AlertDialog.Builder(ctx);
                        builderInner.setMessage(strName);
                        builderInner.setTitle("Your Selected Item is");
                        builderInner.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog,int which) {
                                dialog.dismiss();
                            }
                        });
                        builderInner.show();*//*
                }
            });
            builderSingle.show();*/

        }
    }
    private String parseTime(String date){
        SimpleDateFormat format = new SimpleDateFormat("yyyy-dd-MM HH:mm:ss");
        try {
            Date date1 = format.parse(date.replace("T"," "));
            String d= new SimpleDateFormat("dd.MM.yyyy").format(date1);
            return d;
        }catch (Exception e){
            e.printStackTrace();
        }
        return "";
    }
}


