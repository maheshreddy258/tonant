package in.ecoprice.tonant.Retrofit.Responce;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class BannerResponse implements Serializable
{

    @SerializedName("Id")
    @Expose
    public Integer id;
    @SerializedName("Title")
    @Expose
    public String title;
    @SerializedName("ShortDescription")
    @Expose
    public String shortDescription;
    @SerializedName("BannerImage")
    @Expose
    public String bannerImage;
    @SerializedName("Status")
    @Expose
    public Boolean status;
    @SerializedName("CreatedBy")
    @Expose
    public String createdBy;
    @SerializedName("CreatedOn")
    @Expose
    public String createdOn;
    @SerializedName("NavigationLink")
    @Expose
    public String navigationLink;
    private final static long serialVersionUID = -4132665875839710535L;

}
