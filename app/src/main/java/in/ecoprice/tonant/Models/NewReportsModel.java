package in.ecoprice.tonant.Models;

public class NewReportsModel {

    private String productName,SaleinKg,SoldInKg;

    public NewReportsModel(String productName, String saleinKg, String soldInKg) {
        this.productName = productName;
        SaleinKg = saleinKg;
        SoldInKg = soldInKg;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getSaleinKg() {
        return SaleinKg;
    }

    public void setSaleinKg(String saleinKg) {
        SaleinKg = saleinKg;
    }

    public String getSoldInKg() {
        return SoldInKg;
    }

    public void setSoldInKg(String soldInKg) {
        SoldInKg = soldInKg;
    }
}
