package in.ecoprice.tonant;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.List;

import in.ecoprice.tonant.Adapters.OrderAdapter;
import in.ecoprice.tonant.Adapters.TodayReportsAdapter;
import in.ecoprice.tonant.Models.Orders;
import in.ecoprice.tonant.Models.TodayReports;
import in.ecoprice.tonant.Retrofit.SharedPref;

public class ReportsTodayActivity extends AppCompatActivity {


    private static final String TAG = "ReportsTodayActivity";
    private RecyclerView Gal_Rec;
    private RecyclerView.Adapter adapter;
    private List<TodayReports> listItems;
    public static List<TodayReports> listItems1;
    private LinearLayout linearLayout;
    private ProgressBar progressBar;
    private TextView mTotalSaleAmount,mTotalPending;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reports_today);
        getSupportActionBar().setTitle("Reports");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        Gal_Rec = (RecyclerView) findViewById(R.id.Rec_Gallery);
        Gal_Rec.setHasFixedSize(true);
        Gal_Rec.setNestedScrollingEnabled(false);
        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(getApplicationContext(), 1);
        Gal_Rec.setLayoutManager(mLayoutManager);
        listItems = new ArrayList<>();
        listItems1 = new ArrayList<>();
        linearLayout = (LinearLayout) findViewById(R.id.TextTable);
        progressBar = (ProgressBar) findViewById(R.id.Prog_Assigned);
        mTotalSaleAmount = (TextView) findViewById(R.id.TotalSaleAmount);
        mTotalPending = (TextView) findViewById(R.id.TotalPending);
        String fromDatestr = getIntent().getStringExtra("fromDatestr")+" 00:00:00";
        String toDatestr = getIntent().getStringExtra("toDatestr")+" 23:00:00";
        loadRecyclerViewData(fromDatestr,toDatestr);
    }
    private void loadRecyclerViewData(String fromDate,String toDate) {
        /*final ProgressDialog progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage("Loading...");
        progressDialog.setCancelable(false);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();*/
        progressBar.setVisibility(View.VISIBLE);
        Gal_Rec.setVisibility(View.GONE);
        linearLayout.setVisibility(View.GONE);
        String username = SharedPref.getStr("userName");
        String URL_DATA = "http://apiservices.tonantfarmers.com/api/SaleReportsfromdate?Sales="+username+"&Fromdate="+fromDate+"&Todate="+toDate;
        String url = URL_DATA.replaceAll(" ", "%20");
        Log.w(TAG,"Url Reports replaceAll : "+url);
        StringRequest stringRequest = new StringRequest(Request.Method.GET,
                url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String s) {
                        // mSwipeRefreshLayout.setRefreshing(false);
                        /*  progressDialog.dismiss();*/
                        Gal_Rec.setVisibility(View.VISIBLE);
                        linearLayout.setVisibility(View.VISIBLE);
                        progressBar.setVisibility(View.GONE);
                        try {
                            JSONObject jsonObject = new JSONObject(s);
                         String TotalSaleAmount = jsonObject.getString("TotalSaleAmount");
                         String TotalPending = jsonObject.getString("TotalPending");
                         Log.w(TAG,"TotalSaleAmount : "+TotalSaleAmount);
                         Log.w(TAG,"TotalPending : "+TotalPending);
                         mTotalPending.setText(TotalPending);
                         mTotalSaleAmount.setText(TotalSaleAmount);

                            JSONArray array = jsonObject.getJSONArray("product");
                            for(int i=0; i<array.length(); i++ ){
                                JSONObject obj =array.getJSONObject(i);
                                TodayReports item = new TodayReports(
                                        obj.getString("ProductName"),
                                        obj.getString("SoldQunatity")

                                );
                                listItems = listItems1;
                                listItems.add(item);
                            }
                            adapter = new TodayReportsAdapter(listItems , getApplicationContext());
                            Gal_Rec.setAdapter(adapter);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                /*  progressDialog.dismiss();*/
                Gal_Rec.setVisibility(View.GONE);
                linearLayout.setVisibility(View.GONE);
                progressBar.setVisibility(View.GONE);
                VolleyLog.e("Error: ", error.getMessage());
                Toast.makeText(getApplicationContext(),error.getMessage(),Toast.LENGTH_LONG).show();

            }
        });
        RequestQueue requestQueue = Volley.newRequestQueue(ReportsTodayActivity.this);
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                2000*5,
                /*DefaultRetryPolicy.DEFAULT_MAX_RETRIES*/ 3,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

}
