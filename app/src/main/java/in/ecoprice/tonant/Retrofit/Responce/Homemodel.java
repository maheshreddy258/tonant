package in.ecoprice.tonant.Retrofit.Responce;

public class Homemodel {


    private String Title;
    private int img;

    public Homemodel() {
    }

    public Homemodel(String title, int img) {
        Title = title;
        this.img = img;
    }


    public String getTitle() {
        return Title;
    }

    public void setTitle(String title) {
        Title = title;
    }

    public int getImg() {
        return img;
    }

    public void setImg(int img) {
        this.img = img;
    }
}
