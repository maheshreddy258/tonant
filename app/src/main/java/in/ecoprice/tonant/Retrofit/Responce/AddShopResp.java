package in.ecoprice.tonant.Retrofit.Responce;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.squareup.moshi.Json;

public class AddShopResp {

    // Designation

    @SerializedName("Status")
    @Expose
   private boolean status;

    @SerializedName("ErrorMessage")
    @Expose
   private String ErrorMSG;

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getErrorMSG() {
        return ErrorMSG;
    }

    public void setErrorMSG(String errorMSG) {
        ErrorMSG = errorMSG;
    }
}

