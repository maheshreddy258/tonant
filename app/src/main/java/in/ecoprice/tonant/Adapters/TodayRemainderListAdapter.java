package in.ecoprice.tonant.Adapters;


import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import in.ecoprice.tonant.Models.Cart;
import in.ecoprice.tonant.Models.RemainderList;
import in.ecoprice.tonant.Models.TodayRemindersResponse;
import in.ecoprice.tonant.R;
import in.ecoprice.tonant.UpdateRemainderActivity;

import static com.android.volley.VolleyLog.TAG;

public class TodayRemainderListAdapter extends RecyclerView.Adapter<TodayRemainderListAdapter.ViewHolder>{
     public List<RemainderList> listitems;
     private String TAG = "TodayRemainders";
    private Context ctx;

     private List<TodayRemindersResponse> todayRemindersResponseList;

    public TodayRemainderListAdapter(List<TodayRemindersResponse> todayRemindersResponseList, Context ctx) {
        this.todayRemindersResponseList = todayRemindersResponseList;
        this.ctx = ctx;
    }


   /* public TodayRemainderListAdapter(List<RemainderList> listitems, Context ctx) {
        this.listitems = listitems;
        this.ctx = ctx;

    }*/


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.remainder_item, parent, false);

        return new ViewHolder(v, ctx, todayRemindersResponseList);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {
        final TodayRemindersResponse listItem = todayRemindersResponseList.get(position);
        holder.ShopName.setText(listItem.getShopKeeperName());
        holder.VisiteType.setText(listItem.getVisiteType());
        holder.Reason.setText(listItem.getReason());
        holder.IDRemainder.setText("ID :"+listItem.getId());
        holder.NextVisit.setText(parsDate(listItem.getDateofvisittobe()));
       /* Date c = Calendar.getInstance().getTime();
        SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
        String formattedDate = df.format(c);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        SimpleDateFormat output = new SimpleDateFormat("dd.MM.yyyy");


        Log.e("next visit date...",""+listItem.getDateofvisittobe());
        Log.e("date..",""+formattedDate);
        Date d = null;
        Date d1 = null;*/
      /*  try {
            d = sdf.parse(listItem.getDateofvisittobe());
            d1 = sdf.parse(listItem.getDateofvisittobe());
            }
            catch (ParseException e) {
            e.printStackTrace();
        }*/
//        String formattedTime = output.format(d);
  //      String formattedTime12 = output.format(d1);
       // holder.IDRemainder.setText("ID :"+listItem.getId());
       // holder.NextVisit.setText(""+formattedTime);
       // Log.w(TAG," Today Date := "+formattedDate+" Visit Date := "+formattedTime);
/*
        int count = 0;
        if (formattedDate.equals(formattedTime)){
            holder.Card.setVisibility(View.VISIBLE);
            count++;
            }else {
            holder.Card.setVisibility(View.GONE);
            }*/

        //Log.w(TAG,"Count "+count);
        }


/*
    public int CountDuplicates(List<RemainderList> listitems) {
        int duplicates = 0;
        for (int i = 0; i < listitems.size()-1;i++)
        {
            boolean found = false;

            for (int j = i+1; !found && j < listitems.size(); j++)
            {
                if (listitems.get(i).getDateofvisittobe().equals(listitems.get(j).getDateofvisittobe()));
                found = true;
                Log.w(TAG,"ToDay Remianders Date"+" "+listitems.get(i).getDateofvisittobe()+"  "+(listitems.get(j).getDateofvisittobe()));
                duplicates++;
            }
        }
        return duplicates;
    }
*/

    @Override
    public int getItemCount() {
        return todayRemindersResponseList.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private TextView ShopName,VisiteType,Reason,NextVisit,IDRemainder;
        private LinearLayout Card;
        Context ctx;
        ArrayList<TodayRemindersResponse> completeds = new ArrayList<TodayRemindersResponse>();

        @SuppressLint("CutPasteId")
        public ViewHolder(View View, final Context ctx, List<TodayRemindersResponse> completeds) {
            super(View);

            View.setOnClickListener(this);
            this.completeds = (ArrayList<TodayRemindersResponse>) completeds;
            this.ctx = ctx;
            ShopName = (TextView) View.findViewById(R.id.ShopName);
            VisiteType = (TextView) View.findViewById(R.id.VisiteType);
            Reason = (TextView) View.findViewById(R.id.Reason);
            NextVisit = (TextView) View.findViewById(R.id.NextVisit);
            IDRemainder = (TextView) View.findViewById(R.id.IDRemainder);
          //  CreatedDate = (TextView) View.findViewById(R.id.CreatedDate);
            Card = (LinearLayout) View.findViewById(R.id.Card);


            }

        @Override
        public void onClick(View view) {
            int position = getAdapterPosition();
            TodayRemindersResponse posts = this.completeds.get(position);
            Intent intent = new Intent(this.ctx, UpdateRemainderActivity.class);
            intent.putExtra("dateofvisittobe", posts.getDateofvisittobe());
            intent.putExtra("ShopKeeperNamename", posts.getShopKeeperName());
            intent.putExtra("reason", posts.getReason());
            intent.putExtra("VisiteType", posts.getVisiteType());
            intent.putExtra("Id", posts.getId());
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            this.ctx.startActivity(intent);
        }
    }
    private String parseTime(String date){
        SimpleDateFormat format = new SimpleDateFormat("yyyy-dd-MM HH:mm:ss");
        try {
            Date date1 = format.parse(date.replace("T"," "));
            String d= new SimpleDateFormat("dd.MM.yyyy").format(date1);
            return d;
        }catch (Exception e){
            e.printStackTrace();
        }
        return "";
    }
    private String parsDate(String date){
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        try {
           // Log.e("ssss",""+date.replace("T"," "));
            Date date1 = format.parse(date.replace("T"," "));
            String d= new SimpleDateFormat("dd.MM.yyyy").format(date1);
            return d;
        }catch (Exception e){
            e.printStackTrace();
        }
        return "";
    }

}


