package in.ecoprice.tonant.Retrofit.Responce;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Targets {

    // Designation

    @SerializedName("ErrorMessage")
    @Expose
   private String ErrorMessage;

    @SerializedName("RecordStatus")
    @Expose
   private boolean RecordStatus;

    public String getErrorMessage() {
        return ErrorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        ErrorMessage = errorMessage;
    }

    public boolean isRecordStatus() {
        return RecordStatus;
    }

    public void setRecordStatus(boolean recordStatus) {
        RecordStatus = recordStatus;
    }
}

