package in.ecoprice.tonant.Models;

import android.widget.Filter;

import java.util.ArrayList;
import java.util.List;

import in.ecoprice.tonant.Adapters.ShopSelectAdapter;
import in.ecoprice.tonant.Adapters.ShoplistAdapter;

public class SelectShopFilter extends Filter {

    ShopSelectAdapter adapter;
    List<Shoplist> filterList;

    public SelectShopFilter(List<Shoplist> filterList, ShopSelectAdapter adapter)
    {
        this.adapter=adapter;
        this.filterList=filterList;

    }
    @Override
    protected FilterResults performFiltering(CharSequence constraint) {
        FilterResults results=new FilterResults();


        if(constraint != null && constraint.length() > 0)
        {
            constraint=constraint.toString().toUpperCase();
            List<Shoplist> filteredPlayers=new ArrayList<>();


            for (int i=0;i<filterList.size();i++)
            {
                String firLast = filterList.get(i).getFirst_Name()+" "+filterList.get(i).getLast_Name();

                if(firLast.toUpperCase().contains(constraint))
                {
                    filteredPlayers.add(filterList.get(i));
                } else if (filterList.get(i).getShopName().toUpperCase().contains(constraint)) {
                    filteredPlayers.add(filterList.get(i));
                }
            }

            results.count=filteredPlayers.size();
            results.values=filteredPlayers;
        }else
        {
            results.count=filterList.size();
            results.values=filterList;

        }

        return results;
    }

    @Override
    protected void publishResults(CharSequence constraint, FilterResults results) {

        adapter.listitems= (List<Shoplist>) results.values;

        adapter.notifyDataSetChanged();
    }



}

