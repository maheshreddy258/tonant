package in.ecoprice.tonant;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.GsonBuilder;

import java.util.List;

import in.ecoprice.tonant.Models.Cart;
import in.ecoprice.tonant.Retrofit.Responce.AddtoCartRes;
import in.ecoprice.tonant.Retrofit.Responce.Example;
import in.ecoprice.tonant.Retrofit.SharedPref;
import in.ecoprice.tonant.Retrofit.network.ApiService;
import in.ecoprice.tonant.Retrofit.network.RetrofitBuilder;
import in.ecoprice.tonant.dummy.ModelCart;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CheckOutActivity extends AppCompatActivity {

    private static final String TAG = "Checkout";
    ApiService service;
    Call call;
    Button send;
    TextView mNetAmount,mQnty,Shopname;
    TextInputLayout mPIN_Code,paid_amount;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_check_out);
        service = RetrofitBuilder.createService(ApiService.class);

        send = (Button) findViewById(R.id.send);
        mNetAmount = (TextView) findViewById(R.id.NetAmount);
        mQnty = (TextView) findViewById(R.id.mQnty);
        Shopname = (TextView) findViewById(R.id.Shopname);
        mPIN_Code = (TextInputLayout) findViewById(R.id.PIN_Code);
        paid_amount = (TextInputLayout) findViewById(R.id.paid_amount);

       final List<Cart> data = ModelCart.getData();
        final String ShopName = getIntent().getStringExtra("ShopName");
        Shopname.setText("Shop Name :"+ShopName);

        int total = 0;
        Log.d("TAG", "start total = " +total);
        for (int i = 0; i < data.size(); i++) {
            Log.d("TAG", "at "+i+ " quantity = " +data.get(i).getQnty());
            total += data.get(i).getQnty() * data.get(i).getSalePrice();
            Log.d("TAG", "at "+i+ " total = " +total);
            mNetAmount.setText("Total = " +total);
        }

        int total12 = 0;
        Log.d("TAG", "start total = " +total12);
        for (int i = 0; i < data.size(); i++) {
            Log.d("TAG", "at "+i+ " quantity = " +data.get(i).getQnty());
            total12 += data.get(i).getQnty();
            Log.d("TAG", "at "+i+ " total = " +total12);
            mQnty.setText("Qnty = " +total12);
        }


       mPIN_Code.getEditText().setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View view) {
               final String[] text = {"Cash","Account Transfer","Paytm"};
               AlertDialog.Builder builder = new AlertDialog.Builder(view.getRootView().getContext(),AlertDialog.THEME_DEVICE_DEFAULT_LIGHT);
               builder.setItems(text, new DialogInterface.OnClickListener() {
                   @SuppressLint("SetTextI18n")
                   @Override
                   public void onClick(DialogInterface dialog, int which) {

                       ListView lw = ((AlertDialog)dialog).getListView();
                       Object checkedItem = lw.getAdapter().getItem(which);
                       mPIN_Code.getEditText().setText(checkedItem.toString());

                   }
               });
               builder.show();
           }
       });




        send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int total = 0;
                Log.d("TAG", "start total = " +total);
                for (int i = 0; i < data.size(); i++) {
                    Log.d("TAG", "at "+i+ " quantity = " +data.get(i).getQnty());
                    total += data.get(i).getQnty() * data.get(i).getSalePrice();
                    Log.d("TAG", "at "+i+ " total = " +total);
                    mNetAmount.setText("Total = " +total);
                }

                int total12 = 0;
                Log.d("TAG", "start total = " +total12);
                for (int i = 0; i < data.size(); i++) {
                    Log.d("TAG", "at "+i+ " quantity = " +data.get(i).getQnty());
                    total12 += data.get(i).getQnty();
                    Log.d("TAG", "at "+i+ " total = " +total12);
                    mQnty.setText("Qnty = " +total12);
                }





                SharedPref sharedPref = new SharedPref(getApplicationContext());
                final String NetAmount = String.valueOf(total);
                final String paidAmount = paid_amount.getEditText().getText().toString();
                final String Quantity = String.valueOf(total12);
                final String paidthrough = mPIN_Code.getEditText().getText().toString();

                final String  PaymentStatus = "true";
                final String shopuserNmae = getIntent().getStringExtra("ShopUserName");
                final String ShopName = getIntent().getStringExtra("ShopName");
                final String username =sharedPref.getStr("userName");

                Log.w(TAG, String.valueOf(ModelCart.getData()));
                InputMethodManager inputManager = (InputMethodManager) getApplicationContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                inputManager.hideSoftInputFromWindow(CheckOutActivity.this.getCurrentFocus().getWindowToken(), InputMethodManager.RESULT_UNCHANGED_SHOWN);
                Log.w(TAG,shopuserNmae  +" "+ShopName  +" "+username  +" "+NetAmount  +" "+paidthrough  +" "+paidAmount  +" "+Quantity  +" "+PaymentStatus);
                SendAddToCart(shopuserNmae,ShopName,username,NetAmount,paidthrough,paidAmount,Quantity,PaymentStatus);
               // Log.w(TAG,"OderList"+orderList);
                /*Order order = new Order();
                order.setOrderDetailList(data);
                order.setShopUserName(shopuserNmae);
                order.setShopName(ShopName);
                order.setSalesLogin(username);
                order.setNetAmount(NetAmount);
                order.setPaidthrough(paidthrough);
                order.setPaidAmount(paidAmount);
                order.setQuantity(Quantity);
                order.setPaymentStatus(PaymentStatus);
                SendAddToCart(order);*/
            }
        });


        //Log.w(TAG, data.toArray().toString());
        System.out.println(data.size());
        for (int i = 0; i < data.size(); i++) {
            String text = data.get(i).getId() + "       " +data.get(i).getProductName() + "       " + data.get(i).getSalePrice() + "       " + data.get(i).getQnty();
            Log.w(TAG, text);
        }
}

    private void SendAddToCart(String shopuserNmae, String shopName, String username, String netAmount, String paidthrough, String paidAmount, String quantity, String paymentStatus) {

        List<Cart> orderList =  ModelCart.getData();
        final Example example = new Example();
        example.setShopUserName(shopuserNmae);
        example.setShopName(shopName);
        example.setSalesLogin(username);
        example.setNetAmount(Double.parseDouble(netAmount));
        example.setPaidthrough(paidthrough);
        example.setPaidAmount(Double.parseDouble(paidAmount));
        example.setQuantity(Integer.parseInt(quantity));
        example.setPaymentStatus(Boolean.parseBoolean(paymentStatus));
        example.setAgendaWeekDays(orderList);
        Log.w("printed gson => ",new GsonBuilder().setPrettyPrinting().create().toJson(example));

        final ProgressDialog progressDialog= new ProgressDialog(CheckOutActivity.this);
        progressDialog.setMessage("Loading...");
        progressDialog.show();
        progressDialog.setCancelable(false);
        progressDialog.setCanceledOnTouchOutside(false);


       // Log.w(TAG,"Example :"+example.getAgendaWeekDays().toString());

        call = service.AddtoCart(example);
        call.enqueue(new Callback<AddtoCartRes>() {
            @Override
            public void onResponse(@NonNull Call<AddtoCartRes> call, @NonNull Response<AddtoCartRes> response) {
                Log.w(TAG, "onResponse: " + response);
                response.body().toString();
                Log.d("Status :=>", String.valueOf(response.body().isStatus()));
                Log.d("Error MSG :=>", String.valueOf(response.body().getErrorMessage()));
                if (response.isSuccessful()){
                   // AddtoCartRes addtoCartRes = response;
                    if (response.body().isStatus()){
                        Toast.makeText(CheckOutActivity.this,response.body().getErrorMessage(),Toast.LENGTH_LONG).show();
                        int TIME = 2000;
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                progressDialog.dismiss();
                                Intent intent = new Intent(CheckOutActivity.this,MainActivity.class);
                                startActivity(intent);
                                finish();
                            }
                        }, TIME);
                    }else{
                        Toast.makeText(CheckOutActivity.this,"Try Again",Toast.LENGTH_LONG).show();
                        progressDialog.dismiss();
                        Log.d("error message", response.body().getErrorMessage());
                    }
                  }else {
                    Toast.makeText(CheckOutActivity.this,"Error BODY :"+response.errorBody().toString(),Toast.LENGTH_LONG).show();
                    Log.w(TAG,"Error BODY :"+response.errorBody().toString());
                }


              /*  } else {
                    Toast.makeText(CheckOutActivity.this,response.body().getErrorMessage(),Toast.LENGTH_LONG).show();
                    Toast.makeText(CheckOutActivity.this,"Try Again",Toast.LENGTH_LONG).show();
                    progressDialog.dismiss();
                    *//*showForm();
                    tilEmail.setError("Invalid Username");
                    tilPassword.setError("Invalid Password");*//*
                 *//*ErrorMsg.setVisibility(View.VISIBLE);
                        int SPLASH_TIME_OUT = 50000;
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                                ErrorMsg.setVisibility(View.GONE);
                            }
                        }, SPLASH_TIME_OUT);*//*
                }*/
                /*AddtoCartRes addtoCartRes = response.body();
                //Toast.makeText(CheckOutActivity.this,response.body().getErrorMessage(),Toast.LENGTH_LONG).show();
                    /*
                    int TIME = 2000;
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            progressDialog.dismiss();
                            Intent intent = new Intent(CheckOutActivity.this,MainActivity.class);
                            startActivity(intent);
                            finish();
                        }
                    }, TIME);*/

            }

            @Override
            public void onFailure(Call<AddtoCartRes> call, Throwable t) {
                Log.w(TAG, "onFailure: " + t.getMessage());
                progressDialog.dismiss();
            }
        });
    }
}
