package in.ecoprice.tonant.Adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.view.PagerAdapter;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;

import java.util.List;


import in.ecoprice.tonant.R;
import in.ecoprice.tonant.Retrofit.Responce.BannerResponse;

public class SlideAdapter extends PagerAdapter {


    Context context;
    LayoutInflater inflater;


    List<BannerResponse> bannerResponses;

    public SlideAdapter(Context context, List<BannerResponse> bannerResponses) {
        this.context = context;
        this.bannerResponses = bannerResponses;
    }

    @Override
    public int getCount() {
        return bannerResponses.size();
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object o) {


        return (view==o);
    }


    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {

        inflater =(LayoutInflater)context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.slide,container,false);


        CardView cardView = (CardView) view.findViewById(R.id.cardview);
        ImageView imageView = view.findViewById(R.id.imageview);
        String URL = "http://tonantfarmers.com";
        Glide.with(context)
                .load(URL + bannerResponses.get(position).bannerImage)
               // .centerCrop()
                .into(imageView);
        container.addView(view);
        return view;
    }


    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        container.removeView((CardView) object);
    }
}
