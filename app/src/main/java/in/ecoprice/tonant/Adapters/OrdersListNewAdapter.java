package in.ecoprice.tonant.Adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import in.ecoprice.tonant.R;
import in.ecoprice.tonant.Retrofit.Responce.MyOrdersResponse;
import in.ecoprice.tonant.Retrofit.Responce.OrderDetailsResponse;
import in.ecoprice.tonant.Retrofit.Responce.Ordersdetails;

public class OrdersListNewAdapter extends RecyclerView.Adapter<OrdersListNewAdapter.OrderListViewHolder>  {


    private Context mContext;

    List<MyOrdersResponse> myOrdersResponses;
    List<OrderDetailsResponse> myorders;


    public OrdersListNewAdapter(Context mContext, List<OrderDetailsResponse> myorders) {
        this.mContext = mContext;
        this.myorders = myorders;
    }

    @NonNull
    @Override
    public OrderListViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.orderslistitems, viewGroup, false);

        return new OrderListViewHolder(itemView);

    }

    @Override
    public void onBindViewHolder(@NonNull OrderListViewHolder orderListViewHolder, int i) {


        orderListViewHolder.mTvProductname.setText(myorders.get(i).productName);
        Log.e("name....",""+myorders.get(i).productName);

        orderListViewHolder.mTvproductquatity.setText(""+myorders.get(i).qty);
        orderListViewHolder.mTvProductPrice.setText(""+myorders.get(i).price);
        orderListViewHolder.mTvproductunitprice.setText(""+myorders.get(i).unitPrice);


    }

    @Override
    public int getItemCount() {
        return myorders.size();
    }

    public class OrderListViewHolder extends RecyclerView.ViewHolder{


        TextView mTvProductname,mTvproductquatity,mTvProductPrice,mTvproductunitprice;

        public OrderListViewHolder(@NonNull View itemView) {
            super(itemView);

            mTvProductname = itemView.findViewById(R.id.ProductNamelist);
            mTvproductquatity=itemView.findViewById(R.id.SoldQntylist);
            mTvProductPrice = itemView.findViewById(R.id.ProductPrice);
            mTvproductunitprice=itemView.findViewById(R.id.ProductunitPrice);
        }
    }
}
