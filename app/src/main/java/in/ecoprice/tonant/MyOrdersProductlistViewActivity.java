package in.ecoprice.tonant;

import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import in.ecoprice.tonant.Adapters.MyOrderAdapter;
import in.ecoprice.tonant.Adapters.OrdersListNewAdapter;
import in.ecoprice.tonant.Retrofit.Responce.MyOrdersResponse;
import in.ecoprice.tonant.Retrofit.Responce.OrderDetailsResponse;
import in.ecoprice.tonant.Retrofit.Responce.Ordersdetails;
import in.ecoprice.tonant.Retrofit.SharedPref;
import in.ecoprice.tonant.Retrofit.network.ApiClient;
import in.ecoprice.tonant.Retrofit.network.ApiService;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MyOrdersProductlistViewActivity extends AppCompatActivity {


    Toolbar toolbar;
    RecyclerView mRecycler;
    OrdersListNewAdapter ordersListNewAdapter;
    List<OrderDetailsResponse> myOrdersResponseList;
    SharedPref sharedPref;
    String userName,city1,student1;
    ProgressBar progressbar;

    InternetChecker internetChecker;
    SwipeRefreshLayout mSwipeRefreshLayout;
    int id = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_myordersproductlist_view);


        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        internetChecker = new InternetChecker(this);
        id = getIntent().getIntExtra("Id", 0);
        //Log.e("idddd",""+id);

        mRecycler = (RecyclerView) findViewById(R.id.Rec_myproductslist);
        progressbar = (ProgressBar)  findViewById(R.id.Prog_Assigned);
        mRecycler.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        mRecycler.setItemAnimator(new DefaultItemAnimator());

        loadRecyclerViewData();


    }

    private void loadRecyclerViewData() {


        SharedPref sharedPref = new SharedPref(getApplicationContext());
        String username = sharedPref.getStr("userName");


        progressbar.setVisibility(View.VISIBLE);

        ApiService apiInterface = ApiClient.getClient().create(ApiService.class);
        Call<List<OrderDetailsResponse>> call = apiInterface .getOrderDetails(id);
        call.enqueue(new Callback<List<OrderDetailsResponse>>() {
            @Override
            public void onResponse(Call<List<OrderDetailsResponse>> call, Response<List<OrderDetailsResponse>> response) {
                progressbar.setVisibility(View.GONE);

                if (response.isSuccessful() && response!=null){

                    List<OrderDetailsResponse> ordersdetailsList = new ArrayList<>();
                    ordersdetailsList = response.body();
                    if (ordersdetailsList != null && ordersdetailsList.size() > 0) {

                        ordersListNewAdapter = new OrdersListNewAdapter(getApplicationContext(),ordersdetailsList);
                        mRecycler.setAdapter(ordersListNewAdapter);

                    } else {
                        Toast.makeText(getApplicationContext(), "No Orders Found", Toast.LENGTH_SHORT).show();
                    }

                }

            }

            @Override
            public void onFailure(Call<List<OrderDetailsResponse>> call, Throwable t) {

                progressbar.setVisibility(View.GONE);
                Toast.makeText(getApplicationContext(), "" + t.getMessage(), Toast.LENGTH_SHORT).show();

            }
        });



    }

    @Override
    public boolean onSupportNavigateUp() {

        onBackPressed();
        return true;
    }
}
