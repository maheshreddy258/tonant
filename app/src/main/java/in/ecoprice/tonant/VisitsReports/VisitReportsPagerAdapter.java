package in.ecoprice.tonant.VisitsReports;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

/**
 * Created by Nani on 11/06/17.
 */

public class VisitReportsPagerAdapter extends FragmentStatePagerAdapter {

   Context context;
    public VisitReportsPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {

        switch(position) {
            case 0:
                TodayFragment dailyFragment = new TodayFragment();
                return dailyFragment;

            case 1:
                WeekFragment weeklyFragment = new WeekFragment();
                return  weeklyFragment;

            case 2:
                FromDateToDateFragment monthlyFragment = new FromDateToDateFragment();
                return monthlyFragment;

            default:
                return  null;
        }

    }





    @Override
    public int getCount() {
        return 3;
    }

    public CharSequence getPageTitle(int position){

        switch (position) {
            case 0:
                return "Today";

            case 1:
                return "week";

            case 2:
                return "Date to Date";

            default:
                return null;
        }

    }



}
