package in.ecoprice.tonant.Adapters;


import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import in.ecoprice.tonant.Models.CustomFilter;
import in.ecoprice.tonant.Models.Shoplist;
import in.ecoprice.tonant.ProductListActivity;
import in.ecoprice.tonant.R;
import in.ecoprice.tonant.ShopEditActivity;
import in.ecoprice.tonant.TakeOrderActivity;
import in.ecoprice.tonant.VisitActivity;

/**
 * Created by nani on 11/17/2017.
 */

public class ShoplistAdapter extends RecyclerView.Adapter<ShoplistAdapter.ViewHolder> implements Filterable {
     public List<Shoplist> listitems;
    private Context ctx;
    CustomFilter filter;
    public ShoplistAdapter(List<Shoplist> listitems, Context ctx) {
        this.listitems = listitems;
        this.ctx = ctx;

    }
    public ShoplistAdapter(Context ctx) {
        this.ctx = ctx;

    }

   /* public GralleryAdapter(List<Gallery> listitems, Context ctx) {
        this.listitems = listitems;
        this.ctx = ctx;
    }*/

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.shop_item, parent, false);

        return new ViewHolder(v, ctx, listitems);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {
        final Shoplist listItem = listitems.get(position);

        //listItem.setPendingBill("100");
        holder.title.setText(listItem.getShopName());
        holder.Address.setText(listItem.getShopAddress());
       /// holder.PendingBill.setText("Rs. "+listItem.getPendingBill());

        holder.Navigate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Uri navigationIntentUri = Uri.parse("google.navigation:q=" + /*listItem.getLatitude() + "," + listItem.getLongitude()*/
                                                       listItem.getShopAddress());
                Intent mapIntent = new Intent(Intent.ACTION_VIEW, navigationIntentUri);
                mapIntent.setPackage("com.google.android.apps.maps");
                mapIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                ctx.startActivity(mapIntent);
            }
        });
        holder.CallAction.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_DIAL);
                intent.setData(Uri.parse("tel:"+listItem.getMobileNumber()));
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                ctx.startActivity(intent);
            }
        });

        holder.TakeOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ctx, ProductListActivity.class);
                intent.putExtra("ShopUserName", listitems.get(position).getMobileNumber());
                intent.putExtra("ShopName", listitems.get(position).getShopName());
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                ctx.startActivity(intent);
               /* ((Activity)ctx).finish();*/

            }
        });
        holder.Visit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(ctx, VisitActivity.class);
                intent.putExtra("ShopUserName", listitems.get(position).getMobileNumber());
                intent.putExtra("ShopName", listitems.get(position).getShopName());
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                ctx.startActivity(intent);
/*
                ((Activity)ctx).finish();
*/



              /*  Intent intent = new Intent(Intent.ACTION_DIAL);
                intent.setData(Uri.parse("tel:"+listItem.getMobileNumber()));
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                ctx.startActivity(intent);*/
            }
        });
        holder.EditShop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ctx, ShopEditActivity.class);
                intent.putExtra("ShopId", listitems.get(position).getId());
                intent.putExtra("ShopUserName", listitems.get(position).getMobileNumber());
                intent.putExtra("ShopName", listitems.get(position).getShopName());
                intent.putExtra("MobileNumber", listitems.get(position).getMobileNumber());
                intent.putExtra("LeadType", listitems.get(position).getLeadType());
                intent.putExtra("PIN_Code", listitems.get(position).getPIN_Code());
                intent.putExtra("ShopAddress", listitems.get(position).getShopAddress());
                intent.putExtra("Name", listitems.get(position).getFirst_Name());
                intent.putExtra("Email", listitems.get(position).getCROEMailID());
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                ctx.startActivity(intent);
            }
        });

        }

    @Override
    public int getItemCount() {
        return listitems.size();
    }

    @Override
    public Filter getFilter() {
        if(filter==null)
        {
            filter=new CustomFilter(listitems,this);
        }

        return filter;
    }

    public class ViewHolder extends RecyclerView.ViewHolder /*implements View.OnClickListener*/ {

        private TextView title,Address;

       // private Button AddTOCart;
        private LinearLayout Navigate,CallAction,TakeOrder,Visit,EditShop;
        Context ctx;
        ArrayList<Shoplist> completeds = new ArrayList<Shoplist>();

        @SuppressLint("CutPasteId")
        public ViewHolder(View View, final Context ctx, List<Shoplist> completeds) {
            super(View);

           /* View.setOnClickListener(this);*/
            this.completeds = (ArrayList<Shoplist>) completeds;
            this.ctx = ctx;
            title = (TextView) View.findViewById(R.id.TitleShop);
            //PendingBill = (TextView) View.findViewById(R.id.PendingBill);
            Address = (TextView) View.findViewById(R.id.Address);
            Navigate = (LinearLayout) View.findViewById(R.id.Navigate);
            CallAction = (LinearLayout) View.findViewById(R.id.CallAction);
            TakeOrder = (LinearLayout) View.findViewById(R.id.TakeOrder);
            Visit = (LinearLayout) View.findViewById(R.id.Visit);
            EditShop = (LinearLayout) View.findViewById(R.id.EditShop);




        }

        /*@Override
        public void onClick(View v) {
            int position = getAdapterPosition();
            Products posts = this.completeds.get(position);
           // Toast.makeText(ctx, "Selected Item : " + position, Toast.LENGTH_SHORT).show();
            MainPro.setBackgroundResource(R.drawable.selector_row);



            *//* Intent intent = new Intent(this.ctx, VideoViewActivity.class);
            intent.putExtra("link", posts.getLink().replaceAll(" ", "%20"));
            intent.putExtra("name", posts.getName());*//**//*
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            this.ctx.startActivity(intent);*//*


        }*/

    }
}


