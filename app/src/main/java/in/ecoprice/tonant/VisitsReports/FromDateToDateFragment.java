package in.ecoprice.tonant.VisitsReports;


import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import in.ecoprice.tonant.Adapters.VisitsReportsAdapter;
import in.ecoprice.tonant.Models.VisitReports;
import in.ecoprice.tonant.R;
import in.ecoprice.tonant.Reports.OrdersPagerAdapter;
import in.ecoprice.tonant.ReportsTodayActivity;
import in.ecoprice.tonant.Retrofit.SharedPref;
import in.ecoprice.tonant.VisitsReportsViewActivity;

/**
 * A simple {@link Fragment} subclass.
 */
public class FromDateToDateFragment extends Fragment {


    public FromDateToDateFragment() {
        // Required empty public constructor
    }

    private static final String TAG = "reportsFromDateToDate";
    TextInputLayout fromDate,toDate;
    private int mYear, mMonth, mDay, mHour, mMinute;
    private Button BtnGetReports;;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_date_to_date, container, false);
        fromDate = (TextInputLayout) view.findViewById(R.id.fromDate);
        toDate = (TextInputLayout) view.findViewById(R.id.toDate);
        BtnGetReports = (Button) view.findViewById(R.id.BtnGetReports);

        fromDate.getEditText().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Get Current Date
                final Calendar c = Calendar.getInstance();
                mYear = c.get(Calendar.YEAR);
                mMonth = c.get(Calendar.MONTH);
                mDay = c.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog datePickerDialog = new DatePickerDialog(getContext(),
                        new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {

                                @SuppressLint("SimpleDateFormat")
                                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                                c.set(year, monthOfYear , dayOfMonth);
                                String dateString = sdf.format(c.getTime());
                                fromDate.getEditText().setText(dateString);

                            }
                        }, mYear, mMonth, mDay);
                datePickerDialog.show();
            }
        });

        toDate.getEditText().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Get Current Date
                final Calendar c = Calendar.getInstance();
                mYear = c.get(Calendar.YEAR);
                mMonth = c.get(Calendar.MONTH);
                mDay = c.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog datePickerDialog = new DatePickerDialog(getContext(),
                        new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {

                                @SuppressLint("SimpleDateFormat")
                                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                                c.set(year, monthOfYear , dayOfMonth);
                                String dateString = sdf.format(c.getTime());
                                toDate.getEditText().setText(dateString);

                            }
                        }, mYear, mMonth, mDay);
                datePickerDialog.show();
            }
        });


        BtnGetReports.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(validate(fromDate)){
                    Log.w(TAG,"From Date Null");
                }else if(validate(toDate)){
                    Log.w(TAG,"to Date Null");
                }else {
                    String fromDatestr = fromDate.getEditText().getText().toString();
                    String toDatestr = toDate.getEditText().getText().toString();
                    Log.w(TAG,"fromDatestr =>: "+fromDatestr);
                    Log.w(TAG,"toDatestr =>: "+toDatestr);
                    Intent intent = new Intent(getContext(), VisitsReportsViewActivity.class);
                    intent.putExtra("fromDatestr",fromDatestr);
                    intent.putExtra("toDatestr",toDatestr);
                    intent.putExtra("type", getActivity().getIntent().getStringExtra("type"));
                    startActivity(intent);
                }
            }
        });


        return view;
    }
    public boolean validate(TextInputLayout textInputEditText) {
        boolean valid = false;
        textInputEditText.setEnabled(true);
        textInputEditText.setError(null);
        String  textInputEditText123 = textInputEditText.getEditText().getText().toString();
        if (textInputEditText123.length() == 0) {
            textInputEditText.setError("Required");
            textInputEditText.getEditText().requestFocus();
            valid = true;
        } else {
            textInputEditText.setError(null);
        }

        return valid;
    }

}
