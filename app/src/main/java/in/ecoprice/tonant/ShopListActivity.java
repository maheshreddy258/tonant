package in.ecoprice.tonant;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.SearchView;
import android.widget.Toast;

import com.android.volley.Cache;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.ParseError;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import in.ecoprice.tonant.Adapters.ShoplistAdapter;
import in.ecoprice.tonant.Models.CustomFilter;
import in.ecoprice.tonant.Models.Shoplist;
import in.ecoprice.tonant.Retrofit.SharedPref;

public class ShopListActivity extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener {


    private static final String TAG = "ShopList";
    private RecyclerView Rec_Assigned;
    private ShoplistAdapter adapter;
    private List<Shoplist> listItems;
    public static List<Shoplist> listItems1;
    private ProgressBar progressBar;
    InternetChecker internetChecker;
    SwipeRefreshLayout mSwipeRefreshLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shop_list);
        getSupportActionBar().setTitle("Shops List");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        internetChecker = new InternetChecker(this);

        Rec_Assigned = (RecyclerView) findViewById(R.id.Rec_Assigned);
        progressBar = (ProgressBar)  findViewById(R.id.Prog_Assigned);
        Rec_Assigned.setHasFixedSize(true);
        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(getApplicationContext(), 1);
        Rec_Assigned.setLayoutManager(mLayoutManager);
        Rec_Assigned.setNestedScrollingEnabled(false);
        Rec_Assigned.animate();
        listItems = new ArrayList<>();
        listItems1 = new ArrayList<>();

        // SwipeRefreshLayout
        mSwipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_container);
        mSwipeRefreshLayout.setOnRefreshListener(ShopListActivity.this);
        mSwipeRefreshLayout.setColorSchemeResources(R.color.colorPrimary,
                android.R.color.holo_green_dark,
                android.R.color.holo_orange_dark,
                android.R.color.holo_blue_dark);
        if(listItems!=null) {
            listItems.clear();
        }

        if( getIntent().getExtras() != null)
        {
            String user =  getIntent().getStringExtra("username");
            Log.w(TAG,"Username from ADDShop"+" "+user);
            if (user.length() == 4){
                Log.w(TAG,"Username from ADDShop"+" "+user);
            }else {
                    loadRecyclerViewData(user);

                    Log.w(TAG,"Username from ADDShop in else"+" "+user);
                 }
        }else{
            mSwipeRefreshLayout.post(new Runnable() {
                @Override
                public void run() {
                    mSwipeRefreshLayout.setRefreshing(true);
                    loadRecyclerViewData();

                }
            });
        }
    }

    private void loadRecyclerViewData() {
        if(listItems!=null) {
            listItems.clear();
        }
       // mSwipeRefreshLayout.setRefreshing(true);
        progressBar.setVisibility(View.VISIBLE);
        Rec_Assigned.setVisibility(View.GONE);
        SharedPref sharedPref = new SharedPref(getApplicationContext());
       String username = sharedPref.getStr("userName");
        String URL_DATA = "http://apiservices.tonantfarmers.com/api/MyLeads?UserName="+username;
        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(URL_DATA, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                progressBar.setVisibility(View.GONE);
                Rec_Assigned.setVisibility(View.VISIBLE);
                try {
                    if (response.length() > 0) {
                        for (int i = 0; i < response.length(); i++) {
                            JSONObject obj =response.getJSONObject(i);
                            Shoplist item = new Shoplist(
                                    obj.getString("Id"),
                                    obj.getString("First_Name"),
                                    obj.getString("Last_Name"),
                                    obj.getString("CROEMailID"),
                                    obj.getString("MobileNumber"),
                                    obj.getString("AadharNumber"),
                                    obj.getString("PANNumber"),
                                    obj.getString("ShopName"),
                                    obj.getString("ShopAddress"),
                                    obj.getString("CreateBy"),
                                    obj.getString("LeadType"),
                                    obj.getString("PIN_Code"),
                                    obj.getString("Latitude"),
                                    obj.getString("Longitude"),
                                    obj.getString("PendingBill")
                            );
                            listItems = listItems1;
                            listItems.add(item);
                        }
                        adapter = new ShoplistAdapter(listItems , getApplicationContext());
                        Rec_Assigned.setAdapter(adapter);
                    }
                }catch (JSONException e) {
                    e.printStackTrace();
                }
                // Stopping swipe refresh
                mSwipeRefreshLayout.setRefreshing(false);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                mSwipeRefreshLayout.setRefreshing(false);
                progressBar.setVisibility(View.GONE);
                Rec_Assigned.setVisibility(View.GONE);
            }
        }){

            @Override
            protected Response<JSONArray> parseNetworkResponse(NetworkResponse response) {
                try {
                    Cache.Entry cacheEntry = HttpHeaderParser.parseCacheHeaders(response);
                    if (cacheEntry == null) {
                        cacheEntry = new Cache.Entry();
                    }
                    final long cacheHitButRefreshed = 1 * 60 * 1000; // in 3 minutes cache will be hit, but also refreshed on background
                    final long cacheExpired = 24 * 60 * 60 * 1000; // in 24 hours this cache entry expires completely
                    long now = System.currentTimeMillis();
                    final long softExpire = now + cacheHitButRefreshed;
                    final long ttl = now + cacheExpired;
                    cacheEntry.data = response.data;
                    cacheEntry.softTtl = softExpire;
                    cacheEntry.ttl = ttl;
                    String headerValue;
                    headerValue = response.headers.get("Date");
                    if (headerValue != null) {
                        cacheEntry.serverDate = HttpHeaderParser.parseDateAsEpoch(headerValue);
                    }
                    headerValue = response.headers.get("Last-Modified");
                    if (headerValue != null) {
                        cacheEntry.lastModified = HttpHeaderParser.parseDateAsEpoch(headerValue);
                    }
                    cacheEntry.responseHeaders = response.headers;
                    final String jsonString = new String(response.data,
                            HttpHeaderParser.parseCharset(response.headers));
                    return Response.success(new JSONArray(jsonString), cacheEntry);
                } catch (UnsupportedEncodingException e) {
                    return Response.error(new ParseError(e));
                } catch (JSONException e) {
                    return Response.error(new ParseError(e));
                }
            }

            @Override
            protected void deliverResponse(JSONArray response) {
                super.deliverResponse(response);
            }

            @Override
            public void deliverError(VolleyError error) {
                super.deliverError(error);
            }

            @Override
            protected VolleyError parseNetworkError(VolleyError volleyError) {
                return super.parseNetworkError(volleyError);
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(ShopListActivity.this);
        jsonArrayRequest.setRetryPolicy(new DefaultRetryPolicy(
                2000*5,
                /*DefaultRetryPolicy.DEFAULT_MAX_RETRIES*/ 3,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(jsonArrayRequest);

    }
    private void loadRecyclerViewData(final String user) {
       // mSwipeRefreshLayout.setRefreshing(true);
        progressBar.setVisibility(View.VISIBLE);
        Rec_Assigned.setVisibility(View.GONE);
        SharedPref sharedPref = new SharedPref(getApplicationContext());
       String username = sharedPref.getStr("userName");
        String URL_DATA = "http://apiservices.tonantfarmers.com/api/MyLeads?UserName="+username;
        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(URL_DATA, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                progressBar.setVisibility(View.GONE);
                Rec_Assigned.setVisibility(View.VISIBLE);
                try {
                    if (response.length() > 0) {
                        for (int i = 0; i < response.length(); i++) {
                            JSONObject obj =response.getJSONObject(i);
                            Shoplist item = new Shoplist(
                                    obj.getString("Id"),
                                    obj.getString("First_Name"),
                                    obj.getString("Last_Name"),
                                    obj.getString("CROEMailID"),
                                    obj.getString("MobileNumber"),
                                    obj.getString("AadharNumber"),
                                    obj.getString("PANNumber"),
                                    obj.getString("ShopName"),
                                    obj.getString("ShopAddress"),
                                    obj.getString("CreateBy"),
                                    obj.getString("LeadType"),
                                    obj.getString("PIN_Code"),
                                    obj.getString("Latitude"),
                                    obj.getString("Longitude"),
                                    obj.getString("PendingBill")
                            );
                            listItems = listItems1;
                            listItems.add(item);
                        }
                        adapter = new ShoplistAdapter(listItems , getApplicationContext());
                        Rec_Assigned.setAdapter(adapter);

                    }
                }catch (JSONException e) {
                    e.printStackTrace();
                }
                // Stopping swipe refresh
              //  mSwipeRefreshLayout.setRefreshing(false);
                adapter.getFilter().filter(user);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
              //  mSwipeRefreshLayout.setRefreshing(false);
                progressBar.setVisibility(View.GONE);
                Rec_Assigned.setVisibility(View.GONE);
               // getErro();

            }
        });
        RequestQueue requestQueue = Volley.newRequestQueue(ShopListActivity.this);
        jsonArrayRequest.setRetryPolicy(new DefaultRetryPolicy(
                2000*5,
                /*DefaultRetryPolicy.DEFAULT_MAX_RETRIES*/ 3,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(jsonArrayRequest);

    }

    /*private void getErro() {
        Intent intent = new Intent(ShopListActivity.this,MainActivity.class);
        startActivity(intent);
        finish();
      //  Toast.makeText(ShopListActivity.this,"Try Again :(",Toast.LENGTH_SHORT).show();

    }*/
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);

        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.search, menu);
        MenuItem searchItem = menu.findItem(R.id.menu_search);

        SearchView searchView = (SearchView) searchItem.getActionView();
       /* searchView.setIconified(false);
        searchView.setFocusable(true);
       */ if (searchView != null) {
            searchView.setQueryHint(getString(R.string.search_hint));
            searchView.setMaxWidth(Integer.MAX_VALUE);
            searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
                @Override
                public boolean onQueryTextSubmit(String s) {
                    return false;
                }
                @Override
                public boolean onQueryTextChange(String query) {
                    adapter.getFilter().filter(query);
                    return false;
                }
            });
        }

        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        if(null!=searchManager ) {
            searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        }
        searchView.setIconifiedByDefault(false);

        return true;
    }

    @Override
    protected void onResume() {
        internetChecker.isEveryThingEnabled();
        super.onResume();
    }

    @Override
    protected void onPause() {
        internetChecker.isEveryThingEnabled();
        super.onPause();
    }

    @Override
    protected void onRestart() {
        internetChecker.isEveryThingEnabled();
        super.onRestart();
    }

    @Override
    public void onRefresh() {
        loadRecyclerViewData();
    }
    /*@Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.search, menu);
        return true;
        MenuItem searchMenuItem = menu.findItem(R.id.menu_search);

        SearchView  searchView = (SearchView) searchMenuItem.getActionView();
        if (searchView != null) {
            searchView.setQueryHint(getString(R.string.search_hint));
            searchView.setMaxWidth(2129960);
            searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
                @Override
                public boolean onQueryTextSubmit(String s) {
                    return false;
                }

                @Override
                public boolean onQueryTextChange(String query) {
                    adapter.getFilter().filter(query);
                    return false;
                }
            });
        }
    }*/
}
