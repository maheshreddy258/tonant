package in.ecoprice.tonant.Reports;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.RequestFuture;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import in.ecoprice.tonant.Adapters.OrderAdapter;
import in.ecoprice.tonant.Adapters.TodayReportsAdapter;
import in.ecoprice.tonant.Models.Orders;
import in.ecoprice.tonant.Models.TodayReports;
import in.ecoprice.tonant.R;
import in.ecoprice.tonant.Retrofit.SharedPref;

/**
 * A simple {@link Fragment} subclass.
 */
public class TodayOrdersFragment extends Fragment {




    public TodayOrdersFragment() {
        // Required empty public constructor
    }
    private static final String TAG = "ReportsTodayActivity";
    private RecyclerView Gal_Rec;
    private RecyclerView.Adapter adapter;
    private List<TodayReports> listItems;
    public static List<TodayReports> listItems1;
    private LinearLayout linearLayout;
    private ProgressBar progressBar;
    private TextView mTotalSaleAmount,mTotalPending;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_today_orders, container, false);

        Gal_Rec = (RecyclerView) view.findViewById(R.id.Rec_Gallery);
        Gal_Rec.setHasFixedSize(true);
        Gal_Rec.setNestedScrollingEnabled(false);
        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(getContext(), 1);
        Gal_Rec.setLayoutManager(mLayoutManager);
        listItems = new ArrayList<>();
        listItems1 = new ArrayList<>();
        linearLayout = (LinearLayout) view.findViewById(R.id.TextTable);
        progressBar = (ProgressBar) view.findViewById(R.id.Prog_Assigned);
        mTotalSaleAmount = (TextView) view.findViewById(R.id.TotalSaleAmount);
        mTotalPending = (TextView) view.findViewById(R.id.TotalPending);

        Date c = Calendar.getInstance().getTime();
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        String formattedDate = df.format(c);

        loadRecyclerViewData(formattedDate+" 00:00:00",formattedDate+" 23:00:00");
        return  view;
    }

    private void loadRecyclerViewData(String fromDate,String formattedDate) {
        progressBar.setVisibility(View.VISIBLE);
        Gal_Rec.setVisibility(View.GONE);
        linearLayout.setVisibility(View.GONE);
        String username = SharedPref.getStr("userName");
        String URL_DATA = "http://apiservices.tonantfarmers.com/api/SaleReportsfromdate?Sales="+username+"&Fromdate="+fromDate+"&Todate="+formattedDate;
        String url = URL_DATA.replaceAll(" ", "%20");
        Log.w(TAG,"Url Reports replaceAll : "+url);
        StringRequest stringRequest = new StringRequest(Request.Method.GET,
                url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String s) {
                        Gal_Rec.setVisibility(View.VISIBLE);
                        linearLayout.setVisibility(View.VISIBLE);
                        progressBar.setVisibility(View.GONE);
                        try {
                            JSONObject jsonObject = new JSONObject(s);
                            String TotalSaleAmount = jsonObject.getString("TotalSaleAmount");
                            String TotalPending = jsonObject.getString("TotalPending");
                            Log.w(TAG,"TotalSaleAmount : "+TotalSaleAmount);
                            Log.w(TAG,"TotalPending : "+TotalPending);
                            mTotalPending.setText(TotalPending);
                            mTotalSaleAmount.setText(TotalSaleAmount);

                            JSONArray array = jsonObject.getJSONArray("product");
                            for(int i=0; i<array.length(); i++ ){
                                JSONObject obj =array.getJSONObject(i);
                                TodayReports item = new TodayReports(
                                        obj.getString("ProductName"),
                                        obj.getString("SoldQunatity")

                                );
                                listItems = listItems1;
                                listItems.add(item);
                            }
                            adapter = new TodayReportsAdapter(listItems , getActivity());
                            Gal_Rec.setAdapter(adapter);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Gal_Rec.setVisibility(View.GONE);
                linearLayout.setVisibility(View.GONE);
                progressBar.setVisibility(View.GONE);
                VolleyLog.e("Error: ", error.getMessage());
                Toast.makeText(getActivity(),error.getMessage(),Toast.LENGTH_LONG).show();
                }
        });
        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                2000*5,
                /*DefaultRetryPolicy.DEFAULT_MAX_RETRIES*/ 3,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
        }

}
