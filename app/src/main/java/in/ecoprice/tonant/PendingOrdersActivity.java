package in.ecoprice.tonant;

import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import in.ecoprice.tonant.Adapters.MyOrderAdapter;
import in.ecoprice.tonant.Adapters.PendingOrdersAdapter;
import in.ecoprice.tonant.Retrofit.Responce.MyOrdersResponse;
import in.ecoprice.tonant.Retrofit.Responce.PendingOrdersResponse;
import in.ecoprice.tonant.Retrofit.SharedPref;
import in.ecoprice.tonant.Retrofit.network.ApiClient;
import in.ecoprice.tonant.Retrofit.network.ApiService;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PendingOrdersActivity extends AppCompatActivity {



    Toolbar toolbar;
    RecyclerView mRecycler;
    PendingOrdersAdapter pendingOrdersAdapter;
    SharedPref sharedPref;
    String userName,city1,student1;
    ProgressBar progressbar;

    InternetChecker internetChecker;
    SwipeRefreshLayout mSwipeRefreshLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pending_orders);


        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        internetChecker = new InternetChecker(this);


        mRecycler = (RecyclerView) findViewById(R.id.Rec_pendingorders);
        progressbar = (ProgressBar)  findViewById(R.id.Prog_Assigned);
        mRecycler.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        mRecycler.setItemAnimator(new DefaultItemAnimator());

        loadRecyclerViewData();

    }

    private void loadRecyclerViewData() {


        SharedPref sharedPref = new SharedPref(getApplicationContext());
        String username = sharedPref.getStr("userName");

        progressbar.setVisibility(View.VISIBLE);


        ApiService apiInterface = ApiClient.getClient().create(ApiService.class);
        Call<List<PendingOrdersResponse>> call = apiInterface.getPendingordersList(username);
        call.enqueue(new Callback<List<PendingOrdersResponse>>() {
            @Override
            public void onResponse(Call<List<PendingOrdersResponse>> call, Response<List<PendingOrdersResponse>> response) {

                progressbar.setVisibility(View.GONE);
                if (response.isSuccessful() && response != null) {



                    List<PendingOrdersResponse> pendingOrdersResponseList = new ArrayList<>();
                    pendingOrdersResponseList= response.body();

                    if (pendingOrdersResponseList != null && pendingOrdersResponseList.size() > 0) {

                        pendingOrdersAdapter = new PendingOrdersAdapter(getApplicationContext(), pendingOrdersResponseList);
                        mRecycler.setAdapter(pendingOrdersAdapter);
                        Log.e("successs","successs");

                    }
                    else {
                        Toast.makeText(getApplicationContext(), "No Orders Found", Toast.LENGTH_SHORT).show();
                    }
                }

            }

            @Override
            public void onFailure(Call<List<PendingOrdersResponse>> call, Throwable t) {

                progressbar.setVisibility(View.GONE);
                Toast.makeText(getApplicationContext(), "" + t.getMessage(), Toast.LENGTH_SHORT).show();

            }
        });
    }

    @Override
    public boolean onSupportNavigateUp() {

        onBackPressed();
        return true;
    }
}
