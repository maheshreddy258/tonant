package in.ecoprice.tonant.Targets;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

/**
 * Created by Nani on 11/06/17.
 */

public class TargetsPagerAdapter extends FragmentStatePagerAdapter {

   Context context;
    public TargetsPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {

        switch(position) {
            case 0:
                MyTargetsFragment dailyFragment = new MyTargetsFragment();
                return dailyFragment;

            case 1:
                ManagementTargetsFragment weeklyFragment = new ManagementTargetsFragment();
                return  weeklyFragment;

           /* case 2:
                VenderFragment monthlyFragment = new VenderFragment();
                return monthlyFragment;*/

            default:
                return  null;
        }

    }





    @Override
    public int getCount() {
        return 2;
    }

    public CharSequence getPageTitle(int position){

        switch (position) {
            case 0:
                return "My Targets";

            case 1:
                return "Management Targets";

            /*case 2:
                return "Vender List";*/

            default:
                return null;
        }

    }



}
