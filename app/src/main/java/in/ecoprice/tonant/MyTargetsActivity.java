package in.ecoprice.tonant;

import android.content.Intent;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import in.ecoprice.tonant.Targets.CreateTargetActivity;
import in.ecoprice.tonant.Targets.TargetsPagerAdapter;

public class MyTargetsActivity extends AppCompatActivity {


    private Toolbar mToolbar;
    private ViewPager mViewPager;
    private TargetsPagerAdapter mSectionsPagerAdapter;
    private TabLayout mTabLayout;
    private Button button;
    private InternetChecker internetChecker;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_targets);
        mToolbar = (Toolbar) findViewById(R.id.main_page_toolbar);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setTitle("Targets");
        mToolbar.setTitleTextColor(0xFFFFFFFF);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.arrowleft);
        internetChecker = new InternetChecker(this);
        mViewPager = (ViewPager) findViewById(R.id.container);
        button = (Button) findViewById(R.id.WriteATarget);
        mSectionsPagerAdapter = new TargetsPagerAdapter(getSupportFragmentManager());

        mViewPager.setAdapter(mSectionsPagerAdapter);

        mTabLayout = (TabLayout) findViewById(R.id.main_tabs);
        mTabLayout.setupWithViewPager(mViewPager);


        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MyTargetsActivity.this,CreateTargetActivity.class);
                startActivity(intent);
            }
        });




    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }

    }
}
