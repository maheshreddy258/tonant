package in.ecoprice.tonant.Visits;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

/**
 * Created by Nani on 11/06/17.
 */

public class VisitsPagerAdapter extends FragmentStatePagerAdapter {

   Context context;
    public VisitsPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {

        switch(position) {
            case 0:
                TodayVisitsFragment dailyFragment = new TodayVisitsFragment();
                return dailyFragment;

            /*case 1:
                PendingAmoutFragment PendingAmoutFragment = new PendingAmoutFragment();
                return  PendingAmoutFragment;*/

            case 1:
                AllVisitsFragment weeklyFragment = new AllVisitsFragment();
                return  weeklyFragment;

            default:
                return  null;
        }

    }





    @Override
    public int getCount() {
        return 2;
    }

    public CharSequence getPageTitle(int position){

        switch (position) {
            case 0:
                return "Today Visits";

           /* case 1:
                return "Pending Amounts";*/

            case 1:
                return "All Visits";

            default:
                return null;
        }

    }



}
