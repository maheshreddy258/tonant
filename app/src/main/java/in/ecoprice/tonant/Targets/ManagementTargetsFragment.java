package in.ecoprice.tonant.Targets;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import in.ecoprice.tonant.Adapters.MyTargetsAdapter;
import in.ecoprice.tonant.Models.MyTargets;
import in.ecoprice.tonant.R;
import in.ecoprice.tonant.Retrofit.SharedPref;
import in.ecoprice.tonant.TargetTypesActivity;

/**
 * A simple {@link Fragment} subclass.
 */
public class ManagementTargetsFragment extends Fragment {


    public ManagementTargetsFragment() {
        // Required empty public constructor
    }

     private CardView Daily,Monthly;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_management_targets, container, false);

        Daily = (CardView) view.findViewById(R.id.Daily);
        Monthly = (CardView) view.findViewById(R.id.Monthly);


        Daily.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getContext(),TargetTypesActivity.class);
                intent.putExtra("name","TodayTarget");
                startActivity(intent);

            }
        });
        Monthly.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getContext(),TargetTypesActivity.class);
                intent.putExtra("name","MonthTarget");
                startActivity(intent);
            }
        });

        return  view;
    }



}
